import sys
from openai import OpenAI
from huggingface_hub import InferenceClient
from code_postprocessing import remove_after_no_leading_spaces,remove_lines_causing_syntax_errors

class TokenLimitError(Exception):
    pass

class LLM_Wrapper:

    def __init__(self,model = "Llama",endpoint_key = "home_endpoint_81.txt"):

        self.model = model
        with open(endpoint_key,"r") as f:
            client_key = f.read()

        if model == 'gpt-3.5' or model == "gpt-4":
            self.client = OpenAI(api_key = client_key)
        else:
            self.client = InferenceClient(client_key)


    def runLLM(self,prompt,stop_after_no_leading_space = False, comment_out_syntax_errors = False):
        if self.model == 'Llama':
            stop_words = ["<step>","</step>"]
            try:
                answer = self.client.text_generation(
                    prompt,
                    max_new_tokens=512,
                    do_sample=False,
                    temperature=0.5,
                    top_p=0.99,
                    repetition_penalty=1.1,
                    stop_sequences= stop_words[:4]
                )
            except:
                print("Error with running LLM, maybe token limit")
                raise TokenLimitError
            onset = prompt.split("<step> Source: assistant\n\n\n")[-1]
            for stop_word in stop_words:
                if stop_word in answer:
                    answer = answer[:answer.find(stop_word)]
            answer = onset + answer

        elif self.model == 'gpt-3.5':
            answer = self.client.chat.completions.create(
                model="gpt-3.5-turbo-1106",
                messages=prompt
            ).choices[0].message.content
            
        elif self.model == 'gpt-4':
            answer = self.client.chat.completions.create(
                model="gpt-4",
                messages=prompt
            ).choices[0].message.content

        if stop_after_no_leading_space:
            answer = remove_after_no_leading_spaces(answer)
        if comment_out_syntax_errors:
            answer = remove_lines_causing_syntax_errors(answer)
        return answer



def main():
    messages_gpt_format = [{"role": "user", "content": "Tell me a programmer joke"}]
    messages_Llama_forma =f'''Source: system\n\n\nYou are a helpful assistant.\n<step> Source: user\n\n\nTell me a programmer joke. \n<step> Source: assistant\n\n\n'''

    llm_wrapper = LLM_Wrapper("gpt-3.5","openai_api_key.txt")
    answer = llm_wrapper.runLLM(messages_gpt_format)
    print("ChatGPT 3.5: " + answer)

    #llm_wrapper = LLM_Wrapper("gpt-4","openai_api_key.txt")
    #answer = llm_wrapper.runLLM(messages_gpt_format)
    #print("ChatGPT 3.5: " + answer)

    llm_wrapper = LLM_Wrapper("Llama","home_endpoint_81.txt")
    answer = llm_wrapper.runLLM(messages_Llama_forma)
    print("\n\n Lama endpoint 81: " + answer)

    llm_wrapper = LLM_Wrapper("Llama","home_endpoint_82.txt")
    answer = llm_wrapper.runLLM(messages_Llama_forma)
    print("\n\n Lama endpoint 82: " + answer)

if __name__ == "__main__":
    main()