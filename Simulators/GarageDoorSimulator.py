class GarageDoorSimulator:
    def __init__(self):
        self.state = {"power": False, "door": "closed"}

    def get_state(self):
        return self.state

    def toggle(self):
        self.state["power"] = not self.state["power"]

    def close(self):
        if self.state["power"]:
             self.state["door"] = "closed"

    def open_Door(self):
        if self.state["power"]:
            self.state["door"] = "open"