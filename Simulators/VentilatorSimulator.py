class VentilatorSimulator:
    def __init__(self):
        self.status = "off"
        self.level = 3

    def get_status(self):
        return self.status
    
    def get_level(self):
        return self.level

    def start(self):
        if self.status == "off":
            self.status = "on"

    def stop(self):
        if self.status == "on":
            self.status = "off"

    def speed_up(self):
        if self.status == "on" and self.level < 3:
            self.level += 1

    def speed_down(self):
        if self.status == "on" and self.level > 1:
            self.level -= 1