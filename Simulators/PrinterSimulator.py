class PrinterSimulator:
    def __init__(self):
        self.out_of_ink = False
        self.has_paper = 0
        self.on = False

    def get_out_of_ink(self):
        return self.out_of_ink
    
    def get_on(self):
        return self.on
    
    def get_has_paper(self):
        return self.has_paper

    def turn_on(self):
        self.on = True

    def turn_off(self):
        self.on = False

    def refill_ink(self):
        self.out_of_ink = False

    def add_Paper(self):
        self.has_paper = 60

    def printPage(self,document,number_pages):

        if self.on and not self.out_of_ink and self.has_paper >= number_pages:

            # Code to print the document here
            self.has_paper -= number_pages