class MP3PlayerSimulator:
    def __init__(self):
        self.on = False
        self.song = "Hey Ya!"
        self.mode = "MP3"
        self.volume = 15 

    def get_on(self):
        return self.on
    
    def get_song(self):
        return self.song
    
    def get_mode(self):
        return self.mode
    
    def get_volume(self):
        return self.volume

    def toggle(self):
        self.on = not self.on

    def toggle_mode(self):

        if self.on:
            if self.mode == "MP3":
                self.mode = "Radio"
            else:
                self.mode = "MP3"

    def set_volume(self,value):

        if self.on:
            if value > 0 and value <= 30:
                self.volume = value

    def next_Song(self):
        if self.on and self.mode == "MP3":
            songs = ["Toxic","Hey Ya!","Take On Me","Thriller"]
            current_index = songs.index(self.song)
            next_index = (current_index + 1) % len(songs)
            self.song = songs[next_index]