class VacuumRobot:
    def __init__(self):
        self.cleaning = False # States if the robot is currently cleaning or not
        self.storage_full = True # States whether the storage of the robot is full or not
        self.battery = 30 # minimum = 0, maximum = 100 - Describes the current power charge in percent
        self.mapped = True # States whether a current mapping of the room is available, needed in order to start the vacuuming
        self.at_base = True # States whether the robot is currently docked at the charging base station

    def start(self):
        # Starts vacuuming the room, only valid if a mapping for the room and sufficient storage and battery are available

        # requires that the storage is not full
        if self.storage_full:
            self.empty_storage()
        # requires that the robot has enough battery
        if self.battery == 0:
            self.charge()
        # requires that the environment is mapped
        if not self.mapped:
            self.scan()
        self.cleaning = True

    def scan(self):
        # Scans room, creating a mapping

        # requires that the robot has enough battery
        if self.battery == 0:
            self.charge()

        self.mapped = True

    def empty_storage(self):
        # Empties storage

        # requires that the robot is at the base
        if not self.at_base:
            self.return_to_base()
        self.storage_full = False

    def return_to_base(self):
        # Returns to the base, enables further actions

        self.at_base = True
        self.cleaning = False

    def charge(self):
        # Charges robot

        # requires that the robot is at the base
        if not self.at_base:
            self.return_to_base()
        self.battery = 100