class Chiller:
    def __init__(self):
        self.type = "" 
        self.firmware = "v1.0" 
        self.model = "" 
        self.location = "" 
        self.latitude = 0
        self.longitude = 0
        self.location_tag = "" 
        self.floor_tag = "" 
        self.campus_tag = "" 
        self.online = False
        self.temperature = 20
        self.temperature_unit = "C"
        self.humidity = 30
        self.humidity_unit = "percentage"
        self.pressure = 1.5
        self.pressure_unit = "bar"
        self.ora_latitude = 0
        self.ora_longitude = 0
        self.ora_altitude = 0
        self.ora_uncertainty = 0
        self.ora_zone = "" 
        self.ora_txPower = 0
        self.ora_rssi = 0 

    def reboot(self):
        # function to reboot chiller
        pass

    def firmwareUpdate(self, version):
        # function to update firmware
        # :param string version: The new firmware version

        self.firmware = version

    def emergencyValveRelease(self):
        # function to execute emergency valve release

        self.pressure = 1

    def increasePressure(self, increase_by):
        # function to increase pressure
        # :param integer increase_by: The amount to increase the pressure by

        if increase_by >= 0 and increase_by <= 5:
            self.pressure += increase_by
