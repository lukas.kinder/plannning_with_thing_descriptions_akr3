class Counter:
    def __init__(self):
        self.count = 0  # The count value
        self.countAsImage = None  # The count represented as an image
        self.redDotImage = None  # The red dot image representation
        self.lastChange = ''  # The last change to the count (in string format)

    def increment(self):
        # increment value
        self.count += 1
        # Update lastChange
        self.lastChange = 'incremented'

    def decrement(self):
        # decrement value
        self.count -= 1
        # Update lastChange
        self.lastChange = 'decremented'

    def reset(self):
        # Reset the count to zero
        self.count = 0
        # Update lastChange
        self.lastChange = 'reset'