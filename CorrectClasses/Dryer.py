class Dryer:
    def __init__(self):
        self.running = False # True if the device is currently drying laundry
        self.temperature = 50 # minimum = 40, maximum = 80  - Displays the current drying temperature, temperature can not be changed during drying process
        self.distilled_storage = 50 # minimum = 0, maximum = 100 - Displays the current remaining storage for the distilled water output in percentage

    def start(self):
        # Starts drying process
        if self.distilled_storage == 0:
            self.clear()

        self.running = True

    def clear(self):
        # Clear distilled water storage
        self.distilled_storage = 100

    def increase_temperature(self):
        # Increases drying temperature by 10
        if self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        # Decreases drying temperature by 10
        if self.temperature > 40:
            self.temperature -= 10