class Display:
    def __init__(self):
        self.power = {"status": False}  # power status of the Display
        self.content = 'Video'  # the current content showing on the Display

    def toggle(self):
        # Toggle power on/off of Display
        self.power["status"] = not self.power["status"]

    def setVolume(self, value):
        # Set the volume of the Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to set volume would be here

    def setBright(self,value):
        # Set the brightness of the Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to set brightness would be here

    def showText(self,text):
        # Show text on Display
        # :param string text: The text that should be displayed

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to show text would be here
        
    def playVideo(self, video_info):
        # Play video on Display
        # :param string identifier, name, description, url: Video details

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        video_identifier = video_info["identifier"]
        video_name = video_info["name"]
        video_description = video_info["description"]
        video_url = video_info["url"]

        # Code to play video would be here

    def pauseVideo(self):
        # Pause video on Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to pause video would be here
        
    def stopVideo(self):
        # Stop video on Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to stop video would be here
        
    def presentationWebApp(self, app_info):
        # Presentation of web app on Display
        # :param string identifier, name, description, url: Web app details

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        app_identifier = app_info["identifier"]
        app_name = app_info["name"]
        app_description = app_info["description"]
        app_url = app_info["url"]

        # Code to present web app goes here

    def launchNewsApp(self):
        # Launch news app on Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to launch news app would be here