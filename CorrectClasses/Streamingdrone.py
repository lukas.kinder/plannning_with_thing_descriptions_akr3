class Streamingdrone:
    def __init__(self):
        self.recording = False # Indicates if the drone is currently recording or not
        self.streaming = False # Indicates whether the drone is currently live streaming
        self.battery = 50 # minimum = 0, maximum = 100 - Represents the current battery in percentage

    def stream(self):
        # Starts streaming

        # requires battery to have energy left
        if self.battery == 0:
            self.recharge()

        self.streaming = True

    def stop_stream(self):
        # Stops streaming
        self.streaming = False

    def start_recording(self):
        # Starts the recording

        # requires battery to have energy left
        if self.battery == 0:
            self.recharge()

        self.recording = True

    def end(self):
        # Ends recording process deleting not saved recording
        self.recording = False

    def recharge(self):
        # Recharges the battery, can not be recharged during streaming or recording

        # requires that the device is not streaming
        if self.streaming:
            self.stop_stream()
        # requires that the device is not currently recording
        if self.recording:
            self.start_recording()

        self.battery = 100