class AirConditioner:
    def __init__(self):
        self.goal_Temperature = 22 # Minimum 10, Maximum 35 - Indicates temperature the air conditioner aims to reach. When set temperature is reached the air conditioner unit will stop the respective cooling or heating process
        self.status = False # "Staus of the device indicating if the device is turned on(true) or off (false)

    def toggle(self):
        # Toggles status property, turning the device either off or on depending on the current status
        self.status = not self.status

    def temperature_Up(self):
        # Increases the goal temperature by 1 degree celsius.
        if self.status == False:
            self.toggle()
        if self.goal_Temperature < 35:
            self.goal_Temperature += 1

    def temperature_Down(self):
        # Decreases the goal temperature by 1 degree celsius.
        if self.status == False:
            self.toggle()
        if self.goal_Temperature > 10:
            self.goal_Temperature -= 1