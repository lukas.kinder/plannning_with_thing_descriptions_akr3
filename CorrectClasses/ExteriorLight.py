class ExteriorLight:
    def __init__(self):
        self.status = False # True if Light is turned on
        self.brightness = 100 # minimum = 0, maximum = 100 - Brightness level of the light in percentage

    def turn_on(self):
        # Turns the light on
        self.status = True

    def turn_off(self):
        # Turns the light off
        self.status = False

    def increase(self):
        # Increases brightness by 10%

        # requires that the light is on
        if not self.status:
            self.turn_on()

        # Caps brightness at 100
        if self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        # Decreases brightness by 10%

        # requires that the light is on
        if not self.status:
            self.turn_on()

        # Caps brightness at 0
        if self.brightness > 0:
            self.brightness -= 10