class Oven:
    def __init__(self):
        self.on = False # Property stating if the oven is currently turned on or off
        self.heating_method = "convection" # States the current heating method that is selected

    def switch_on(self):
        # Turns the oven on
        self.on = True

    def switch_off(self):
        # Turns the oven off
        self.on = False

    def change_heating_method(self):
        # Changes the currently selected heating method from convection to top and bottom or vice versa

        # requires that the device is on
        if not self.on:
            self.switch_on()

        if self.heating_method == "convection":
            self.heating_method = "top and bottom"
        else:
            self.heating_method = "convection"

    def preheat(self):
        # Starts preheating the oven to 180 Degrees Celcius

        # requires that the device is on
        if not self.on:
            self.switch_on()

        # Code to preheat the oven here