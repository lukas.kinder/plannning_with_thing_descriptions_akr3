class SecurityCamera:
    def __init__(self):
        self.recording = False # Boolean value indicating if the camera is currently recording
        self.storage = 256 # States the current remaining storage in gigabytes

    def record(self):
        # Starts the recording

        # requires that there is storage
        if self.storage == 0:
            self.delete()

        self.recording = True


    def stop(self):
        # Stops the recording
        
        self.recording = False

    def delete(self):
        # Deletes video files freeing storage

        # requires that recording is off
        if self.recording:
            self.stop()

        self.storage = 256 # reset storage to maximum capacity