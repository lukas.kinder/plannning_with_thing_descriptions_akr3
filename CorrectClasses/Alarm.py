class Alarm:
    def __init__(self):
        self.set = False # Indicates whether alarm is currently set or not
        self.vibration = False # States whether the alarm is set to vibration only mode

    def set_alarm(self):
        # Sets alarm, ringing or vibration at the set time
        self.set = True
   
    def unset(self):
        # Unsets alarm
        self.set = False

    def toggle_vibration_mode(self):
        # Switches vibration only mode on or off depending on the current setting.
        self.vibration = not self.vibration
