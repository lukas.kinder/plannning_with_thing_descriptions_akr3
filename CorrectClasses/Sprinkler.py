class Sprinkler:
    def __init__(self):
        self.water = True # States whether the water tank is full or empty. If True water is still remaining in the tank and the device is ready for usage otherwise water needs to be refilled
        self.on = False # Describes whether the device is turned on (if true) or off (if false)

    def power_on(self):
        # Turns power of the watering system on
        self.on = True

    def power_off(self):
        # Turns power of the watering system off
        self.on = False

    def sprinkle(self):
        # Initiates watering process wetting all surrounding plants

        # requires that the device is on
        if not self.on:
            self.power_on()

        # requires that the device has water
        if not self.water:
            self.refill()

        # Code to sprinkle the water here

    def refill(self):
        # Refills water tank
        self.water = True
