class BedsideLamp:
    def __init__(self):
        self.on = False # True if Light is turned on
        self.brightness = 50 # Brightness level of the light in percentage

    def turn_on(self):
        # Turn the lamp on
        self.on = True

    def turn_off(self):
        # Turn the lamp off
        self.on = False

    def increase(self):
        # increase the brightness of the lamp

        # requires that the lamp is on
        if not self.on:
            self.turn_on()

        # Increase the brightness by 10 to a maximum of 100
        self.brightness = min(100,self.brightness +10)

    def decrease(self):
        # decrease the brightness of the lamp

        # requires that the lamp is on
        if not self.on:
            self.turn_on()

        # Decrease the brightness by 10 to a minimum of 0
        if self.brightness > 0:
            self.brightness = max(0,self.brightness -10)