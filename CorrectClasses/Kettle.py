class Kettle:
    def __init__(self):
        self.power = False # Indicates if the kettle is currently turned on and ready for the heating process or in standby
        self.filled = False # Indicates wether the kettle is filled with water or not

    def switch_power(self):
        # Turns the kettle on or off depending on the current state
        self.power = not self.power

    def heat(self):
        # Start the heating process of the kettle

        # requires that the device is on
        if not self.power:
            self.switch_power()

        # requires that the kettle is filled
        if not self.filled:
            self.fill()

    def fill(self):
        # Fills the kettle with water
        self.filled = True

    def empty(self):
        # Empties the kettle
        self.filled = False