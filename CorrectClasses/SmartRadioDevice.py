class SmartRadioDevice:
    def __init__(self):
        self.power = "on" # Power status indicating if the device is turned on or off
        self.channel = "Jamz" # Displays the current channel of the radio

    def power_on(self):
        # Turns the radio on
        self.power = "on"

    def switch_channel(self):
        # Switches channel of the radio to the next available channel

        # requires that the radio is on
        if self.power == "off":
            self.power_on()

        channels = ["MTV", "Jamz", "The Beat", "The Mix"]
        current_index = channels.index(self.channel)
        next_index = (current_index + 1) % len(channels)
        self.channel = channels[next_index]

    def power_off(self):
        # Turns the radio off     
        self.power = "off"