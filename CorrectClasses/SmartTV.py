class SmartTV:
    def __init__(self):
        self.recording = False # True if the TV is currently recording the selected channel
        self.off = True # Indicates if the TV is turned off(True) or turned on (False)
        self.brightness = 70 # minimum = 0, maximum = 100 - Describes brightness of the TV screen in percent
        self.channel = "ZDF" # States the currently selected channel that will be displayed if the device is turned on

    def record(self):
        # Starts recording the current channel

        # requires that the device is on
        if self.off:
            self.turn_on()
        self.recording = True

    def switch(self):
        # Switches to a different channel

        # requires that the device is on
        if self.off:
            self.turn_on()

        channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
        current_index = channels.index(self.channel)
        next_index = (current_index + 1) % len(channels)
        self.channel = channels[next_index]

    def turn_on(self):
        # Turns the TV on
        self.off = False

    def turn_off(self):
        # Turns the TV off
        self.off = True
        self.recording = False

    def set_brightness(self, brightness):
        # Sets the brightness of the tv.
        # :param integer brightness: the brightness level 

        # requires that the device is on
        if self.off:
            self.turn_on()

        if brightness >= 0 and brightness <= 100:
            self.brightness = brightness