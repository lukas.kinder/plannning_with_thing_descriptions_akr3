class Ventilator:
    def __init__(self):
        self.status = 'off' # Displays the current status of the device
        self.level = 3   # 1,2 or 3 - Displays the current fan speed level

    def start(self):
        # Starts the ventilator
        self.status = 'on'

    def stop(self):
        # Stops the ventilator
        self.status = 'off'

    def speed_up(self):
        # Increases fan level

        # requires that the device is on
        if self.status != 'on':
            self.start()

        if self.level < 3:  # Level can't be greater than 3
            self.level += 1

    def speed_down(self):
        # Decreases fan level

        # requires that the device is on
        if self.status != 'on':
            self.start()

        if self.level > 1:  # Level can't be less than 1
            self.level -= 1