class CoffeeMachine:
    def __init__(self):
        self.water = True # Illustrates water level of the machine. If false the machine does not contain enough water to make a coffIee and needs to be refilled
        self.storage = True # Indicates the remaining coffee ground storage. If false no space is remaining and the container needs to be emptied.
        self.on = False # Is the machine on or off

    def toggle(self):
        # Toggles the power status of the coffee machine 
        self.on = not self.on

    def refill(self):
        # Refills the water tank of the machine
        self.water = True

    def empty(self):
        # Empties coffee ground container
        self.storage = True

    def make_coffee(self):
        # Creates a medium cup of coffee

        if not self.on:
            self.toggle()
        # Refill if there's no water or coffee grains
        if not self.water:
            self.refill()
        if not self.storage:
            self.empty()