class AutomatedWarehouse:
    def __init__(self):
        # Current position of product 
        self.currentProductPosition = {
            "row1col1" : "Wood",
            "row1col2" : "Pipe",
            "row2col2" : "PlasticContainer",
            "row3col1" : "WaterBottles",
            "row3col2" : "Trash",
            "row3col3" : "Cables"
        }

        self.holding_object = None

    def goHome(self):
        # Sends the arm home
        pass

    def pickObjectAtPosition(self,row,column):
        # Picks an object at a given position
        # :param integer row: the row where the product is
        # :param integer column: the column where the product is
        position = f"row{row}col{column}"
        self.holding_object = self.currentProductPosition[position]
        del self.currentProductPosition[position]

    def putObjectAtPosition(self,row,column):
        # Put object at a specified warehouse position
        self.arm_is_home  = False
        position = f"row{row}col{column}"
        self.currentProductPosition[position] = self.holding_object
        self.holding_object = None

    def pickObjectFromConveyor(self):
        # Pick object from a conveyor belt

        self.arm_is_home  = False
        self.holding_object = "object"
        self.object_inside = None

    def putObjectOnConveyor(self):
        self.arm_is_home  = False
        self.holding_object = None

    def rollObjectForward(self):
        # Start the conveyor until object reaches inside sensors
        pass

    def rollObjectBackward(self):
        # Start the conveyor until object reaches outside sensors
        pass