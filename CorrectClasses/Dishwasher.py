class Dishwasher:
    def __init__(self):
        self.running = False # States whether the dishwasher is currently running and washing dishes or not
        self.door = "closed" # Represents if the dishwasher is currently closed or open
    
    def close(self):
        # Closes the door
        self.door = "closed"
    
    def open(self):
        # Opens the door
        # Requires that the dishwasher is not running
        if self.running:
            self.stop()

        self.door = "open"

    def stop(self):
        # Stops the washing process
        self.running = False

    def start(self):
        # Starts the washing process
        # Requires that the door is closed
        if self.door == "open":
            self.close()

        self.running = True