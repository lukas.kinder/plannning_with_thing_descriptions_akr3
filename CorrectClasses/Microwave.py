class Microwave:
    def __init__(self):
        self.running = False # Property stating if the microwave is currently running or not

    def start(self):
        # Starts the heating process
        self.running = True

    def stop(self):
        # Stops the heating process
        self.running = False