class SmartLock:
    def __init__(self):
        self.locked = True # Property stating if the locked is currently locked(true) or unlocked(false)

    def lock(self):
        # Locks the lock securing connected objects
        self.locked = True

    def unlock(self):
        # Unlocks the lock making connected objects accessible.
        self.locked = False