class Dehumidifier:
    def __init__(self):
        self.status = False
        self.tank = "empty"  # Describes the current state of the waste water tank
        self.humidity = 45  # Current humidity level the device aims to reach.

    def toggle_status(self):
        # Toggle status of the device turning it either on or off
        self.status = not self.status

    def dehumidify(self):
        # Starts the dehumidification process
        # requires that the device is on
        if not self.status:
            self.toggle_status()
        # requires that the tank is not full
        if self.tank == "full":
            self.drain()
        pass

    def drain(self):
        # Drains waste water tank
        self.tank = "empty"

    def set_humidity(self, value):
        # Sets the goal humidity
        # :param integer value: the humidity level desired
        
        # requires that the device is on
        if not self.status:
            self.toggle_status()
            
        if 0 <= value <= 100:
            self.humidity = value