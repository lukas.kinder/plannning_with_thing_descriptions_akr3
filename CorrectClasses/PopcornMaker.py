class PopcornMaker:

    def __init__(self):
        self.power = False # True if machine is turned on and ready for use

    def toggle(self):
        # Turns the device on or off based on the current state
        self.power = not self.power

    def add_corn(self):
        # Adds corn to the heating section of the maker

        # Code to add corn here
        pass

    def add_sugar(self):
        # Adds sugar to the heating section making the popcorn sweet

        # Code to add sugar here
        pass

    def add_salt(self):
        # Adds salt to the heating section making the popcorn salty

        # Code to add salt here
        pass

    def heat(self):
        # Start heating up the heating section, turning added corn into popcorn
        # requires that the device is on
        if not self.power:
            self.toggle()

        # Code to heat popcorn maker here