class SmartHeater:
    def __init__(self):
        self.level = 4 # from 0 (minimum) to 6 (maximum) - Integer Value indicating the current heating level of the device. Higher values represent higher temperatures.
        self.status = False # Boolean Value that describes if the device is on or off

    def turn_on(self):
        # Turns the Heating Device on
        self.status = True

    def turn_off(self):
        # Turns the Heating device off
        self.status = False

    def increase_level(self):
        # "Increases the Heating level of the device.

        # requires that the device is on
        if not self.status:
            self.turn_on()
        # if the level is less than the max, increase it
        if self.level < 6:
            self.level += 1

    def decrease_level(self):
        # Decreases the Heating level of the device.

        # requires that the device is on
        if not self.status:
            self.turn_on()
        # if the level is more than the min, decrease it
        if self.level > 0:
            self.level -= 1