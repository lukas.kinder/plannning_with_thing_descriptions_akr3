class FoodDispenser:
    def __init__(self):
        self.automatic = True # States if the device is set on automatic or manual food dispension
        self.stored = False # Indicates whether enough food is available for the next dispension process

    def dispense(self):
        # Manually dispenses food for the animal

        # requires that there is food stored
        if not self.stored:
            self.refill()

        # Code to dispense food here

    def refill(self):
        # refills the stored food
        self.stored = True

    def set_state(self,state):
        # Sets the feeding bowl to automatic or manual
        # :param string state: The state the dispenser should be in

        if state == 'manual':
            self.automatic = False
        elif state == 'automatic':
            self.automatic = True