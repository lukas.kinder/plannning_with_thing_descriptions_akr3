class SmartSpeaker:
    def __init__(self):
        self.connection = False # States whether the speaker is connected to the Internet or not. If false music can not be played and the speaker must first be reconnected to the network
        self.playing = False # States whether the speaker is currently playing music or not
        self.status = False # Indicates if the speaker is currently switched on or off
        self.volume = 50 # The speaker volume in db

    def reconnect(self):
        # Reconnects device with the internet, enabling music to be played if connection was lost

        # requires that the SmartSpeaker is on
        if not self.status:
            self.toggle()

        self.connection = True

    def play(self):
        # Starts playing music from the selected player

        # requires that the SmartSpeaker is on
        if not self.status:
            self.toggle()
        # requires that the device is connected
        if not self.connection:
            self.reconnect()

        self.playing = True

    def pause(self):
        # Pauses music if currently playing

        # requires that the SmartSpeaker is on
        if not self.status:
            self.toggle()
        # requires that the device is connected
        if not self.connection:
            self.reconnect()

        self.playing = False

    def toggle(self):
        # Turn the speaker on of off
        self.status = not self.status

    def standby(self):
        # Puts the device into standby, making it unavailable

        # requires that the SmartSpeaker is on
        if not self.status:
            self.toggle()

        self.playing = False
        self.connection = False

    def change_player(self):
        # Changes music streaming service.

        # requires that the SmartSpeaker is on
        if not self.status:
            self.toggle()
        # requires that the device is connected
        if not self.connection:
            self.reconnect()
        
        pass

    def increase_volume(self):
        # Increases the volume by 5 db

        # requires that the SmartSpeaker is on
        if not self.status:
            self.toggle()
        # requires that the device is connected
        if not self.connection:
            self.reconnect()
        # requires that the device is playing
        if not self.playing:
            self.play()

        if self.volume < 120:
            self.volume += 5

    def decrease_volume(self):
        # Decreases volume by 5 db

        # requires that the SmartSpeaker is on
        if not self.status:
            self.toggle()
        # requires that the device is connected
        if not self.connection:
            self.reconnect()
        # requires that the device is playing
        if not self.playing:
            self.play()

        if self.volume > 10:
            self.volume -= 5