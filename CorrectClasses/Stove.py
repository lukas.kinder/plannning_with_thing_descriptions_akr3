class Stove:
    def __init__(self):
        self.plate_1_power = False # Indicates wether plate 1 is currently turned on or off
        self.plate_2_power = False # Indicates wether plate 2 is currently turned on or off
        self.plate_1_level = 0  # level from 0 to 9 - Represents the current heating level of plate 1
        self.plate_2_level = 0  # level from 0 to 9 - Represents the current heating level of plate 2

    def activate_plate_1(self):
        # Activates plate 1 setting its power level to true
        self.plate_1_power = True

    def activate_plate_2(self):
        # Activates plate 2 setting its power level to true
        self.plate_2_power = True

    def deactivate_plate_1(self):
        # Deactivates plate 1 setting its power level to false
        self.plate_1_power = False

    def deactivate_plate_2(self):
        # Deactivates plate 2 setting its power level to false
        self.plate_2_power = False

    def increase_heat_plate_1(self):
        # Increases the heating level for plate 1 by 1
        
        #requires that plate 1 is on
        if not self.plate_1_power:
            self.activate_plate_1()

        if self.plate_1_level < 9:
            self.plate_1_level += 1

    def increase_heat_plate_2(self):
        # Increases the heating level for plate 2 by 1
        
        # requires that plate 2 is on
        if not self.plate_2_power:
            self.activate_plate_2()
        
        if self.plate_2_level < 9:
            self.plate_2_level += 1

    def decrease_heat_plate_1(self):
        # Increases the heating level for plate 1 by 1
        
        # requires that plate 1 is on
        if not self.plate_1_power:
            self.activate_plate_1()

        if self.plate_1_level > 0:
            self.plate_1_level -= 1

    def decrease_heat_plate_2(self):
        # Increases the heating level for plate 2 by 1
        
        # requires that plate 2 is on
        if not self.plate_2_power:
            self.activate_plate_2()

        if self.plate_2_level > 0:
            self.plate_2_level -= 1