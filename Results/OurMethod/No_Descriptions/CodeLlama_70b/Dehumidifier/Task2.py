class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False # Flags if the dehumidifier is on

        self.tank = 'empty' # Indicates the amount of water in the tank (minimum = empty, maximum = full)

        self.humidity = 45 # The current humidity level (minimum = 0, maximum = 100)

    def toggle_status(self):
        # Toggles the dehumidifier on or off
        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def dehumidify(self):
        # Removes water from the tank
        self.tank = 'empty'

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def drain(self):
        # Dumps water out of the tank
        pass

        print(f"drain()")
        self.real_device.drain()

    def set_humidity(self,humidity):
        # Sets the humidity level
        if humidity < 0 or humidity > 100:
            raise ValueError("Invalid humidity level")
        elif humidity == 0:
            self.tank = 'full'
        elif humidity == 100:
            self.tank = 'empty'
        else:
            self.humidity = humidity



        print(f"set_humidity({humidity})")
        self.real_device.set_humidity(humidity)

# Set the humidity to 40
dehumidifier = Dehumidifier()
dehumidifier.set_humidity(40)


simulator = dehumidifier.real_device
print(simulator.humidity == 40)