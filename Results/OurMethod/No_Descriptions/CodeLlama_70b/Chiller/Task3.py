class ChillerSimulator:
    def __init__(self):
        self.type = "" #Type of the chiller
        self.firmware = "v1.0" #Firmware version
        self.model = "" #Chiller model
        self.location = "" #Location of the chiller
        self.latitude = 0 #Latitude of the chiller
        self.longitude = 0 #Longitude of the chiller
        self.location_tag = "" #Location tag
        self.floor_tag = "" #floor level
        self.campus_tag = "" #name of the campus
        self.online = False #online status
        self.temperature = 20 #temperature
        self.temperature_unit = "C" #C for Celsius, F for Fahrenheit
        self.humidity = 30 #humidity of the chiller
        self.humidity_unit = "percentage" #percentage or grams per m3
        self.pressure = 1.5 #pressure
        self.pressure_unit = "bar" #psi or pascal or bar
        self.ora_latitude = 0 #ora_latitude
        self.ora_longitude = 0 #ora_longitude
        self.ora_altitude = 0 #ora_altitude
        self.ora_uncertainty = 0 #ora_accuracy
        self.ora_zone = "" #ora_zone
        self.ora_txPower = 0 #ora_mssi
        self.ora_rssi = 0 #ora_rssi

        self.n_rebooted  = 0

    # Getter functions
    def get_type(self):
        return self.type

    def get_firmware(self):
        return self.firmware

    def get_model(self):
        return self.model

    def get_location(self):
        return self.location

    def get_latitude(self):
        return self.latitude

    def get_longitude(self):
        return self.longitude

    def get_location_tag(self):
        return self.location_tag

    def get_floor_tag(self):
        return self.floor_tag

    def get_campus_tag(self):
        return self.campus_tag

    def get_online(self):
        return self.online

    def get_temperature(self):
        return self.temperature

    def get_temperature_unit(self):
        return self.temperature_unit

    def get_humidity(self):
        return self.humidity

    def get_humidity_unit(self):
        return self.humidity_unit

    def get_pressure(self):
        return self.pressure

    def get_pressure_unit(self):
        return self.pressure_unit

    def get_ora_latitude(self):
        return self.ora_latitude

    def get_ora_longitude(self):
        return self.ora_longitude

    def get_ora_altitude(self):
        return self.ora_altitude

    def get_ora_uncertainty(self):
        return self.ora_uncertainty

    def get_ora_zone(self):
        return self.ora_zone

    def get_ora_txPower(self):
        return self.ora_txPower

    def get_ora_rssi(self):
        return self.ora_rssi
    
    #actions
    def reboot(self):
        self.n_rebooted +=1

    def firmwareUpdate(self, version):
        self.firmware = version

    def emergencyValveRelease(self):
        self.pressure = 1

    def increasePressure(self, value):
        if value >= 0 and value <=5:
            self.pressure += value


class Chiller:
    def __init__(self):

        self.real_device = ChillerSimulator()
        self.type = "" # Type of chiller
        self.firmware = "v1.0" # firmware version
        self.model = "" # Model of chiller
        self.location = "" # Location of chiller
        self.latitude = 0 # Latitude of chiller
        self.longitude = 0 # Longitude of chiller
        self.location_tag = "" # location tag
        self.floor_tag = "" # floor tag
        self.campus_tag = "" # campus tag
        self.online = False # online status
        self.temperature = 20 # temperature
        self.temperature_unit = "C" # temperature unit
        self.humidity = 30 # humidity
        self.humidity_unit = "percentage" # humidity unit
        self.pressure = 1.5 # pressure
        self.pressure_unit = "bar" # pressure unit
        self.ora_latitude = 0 # latitude of chiller
        self.ora_longitude = 0 # longitude of chiller
        self.ora_altitude = 0 # altitude of chiller
        self.ora_uncertainty = 0 # uncertainty of chiller
        self.ora_zone = "" # zone of chiller
        self.ora_txPower = 0 # transmit power of chiller
        self.ora_rssi = 0 # receive signal strength of chiller

    def reboot(self):
        # Reboots the chiller
        pass

        print(f"reboot()")
        self.real_device.reboot()

    def firmwareUpdate(self, firmware):
        # Updates the firmware of the chiller
        pass

        print(f"firmwareUpdate({firmware})")
        self.real_device.firmwareUpdate(firmware)

    def emergencyValveRelease(self):
        # Releases the emergency valve of the chiller
        pass

        print(f"emergencyValveRelease()")
        self.real_device.emergencyValveRelease()

    def increasePressure(self, pressure):
        # Increases the pressure of the chiller
        pass



        print(f"increasePressure({pressure})")
        self.real_device.increasePressure(pressure)

# Increase pressure by 3.
chiller = Chiller()
chiller.increasePressure(3)


simulator = chiller.real_device
print(round(simulator.pressure,3) == 4.5)