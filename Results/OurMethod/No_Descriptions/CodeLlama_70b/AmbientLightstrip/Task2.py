from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}

    def turn_on(self):
        # turns the device on
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # turns the device off
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, value):
        # sets the brightness
        self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def set_saturation(self, value):
        # sets the saturation
        self.state["sat"] = value



        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# I want some light, but not bright.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.turn_on()
ambientLightstrip.set_brightness(75)
ambientLightstrip.set_saturation(50)









simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['bri'] <= 128)