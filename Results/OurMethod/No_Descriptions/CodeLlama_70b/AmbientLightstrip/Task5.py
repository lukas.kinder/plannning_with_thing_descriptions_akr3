from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}

    def turn_on(self):
        # turns the device on
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # turns the device off
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, value):
        # sets the brightness
        self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def set_saturation(self, value):
        # sets the saturation
        self.state["sat"] = value



        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# Brightness to 30.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.set_brightness(30)



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Robot:
    def __init__(self):

        self.arm = False # flags whether the arm is extended
        self.leg = False # flags whether the leg is extended
        self.eye = False # flags whether the eye is open

    def extend_arm(self):
        # Extends the arm
        self.arm = True

    def extend_leg(self):
        # Extends the leg
        self.leg = True

    def open_eye(self):
        # Opens the eye
        self.eye = True

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Extend arm, extend leg, then open eye.".



# Extend arm, extend leg, then open eye.
robot = Robot()
robot.extend_arm()
robot.extend_leg()
robot.open_eye()









simulator = ambientLightstrip.real_device
print(simulator.state['bri'] == 30)