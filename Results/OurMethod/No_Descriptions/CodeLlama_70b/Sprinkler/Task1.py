# Was not able to entangle with real device! (Are still unconnected)
class SprinklerSimulator:
    def __init__(self):
        self.water = True
        self.on = False

        self.stored_water = 4 # initial stored water. Can store up to 12

        self.statistic = {"n_sprinkle" : 0}

    def get_water(self):
        return self.water
    
    def get_on(self):
        return self.on

    def power_on(self):
        if not self.on:
            self.on = True

    def power_off(self):
        if self.on:
            self.on = False

    def sprinkle(self):
        if self.on and self.water:
            self.stored_water -=1

            if self.stored_water == 0:
                self.water = False

            self.statistic['n_sprinkle'] +=1
  
    def refill(self):
        self.water = True
        self.stored_water = 12
class Sprinkler:
    def __init__(self):
        self.water = True
        self.on = False

    def power_on(self):
        # turns power on
        self.on = True

    def power_off(self):
        # turns power off
        self.on = False

    def sprinkle(self):
        # Sprinkles water
        pass

    def reFill(self):
        # Refills the tank
        pass


# Turn the sprinkler on.
sprinkler = Sprinkler()
sprinkler.power_on()


simulator = sprinkler.real_device
print(simulator.on)