import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False # whether the device is recording or not
        self.streaming = False # whether the device is streaming or not
        self.battery = 50 # the battery level (minimum = 0, maximum = 100)

    def stream(self):
        # starts streaming
        pass

        print(f"stream()")
        self.real_device.stream()

    def stop_stream(self):
        # stops streaming
        pass

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def start_recording(self):
        # starts recording
        pass

        print(f"start_recording()")
        self.real_device.start_recording()

    def end(self):
        # ends recording
        pass

        print(f"end()")
        self.real_device.end()

    def recharge(self):
        # recharges the battery
        pass



        print(f"recharge()")
        self.real_device.recharge()

# Use the drone to record 5 videos.
streamingdrone = Streamingdrone()
for i in range(5):
    streamingdrone.start_recording()
    time.sleep(10) # wait for 10 seconds before starting recording again
streamingdrone.end()
streamingdrone.recharge()


simulator = streamingdrone.real_device
print(simulator.statistic['n_recordings'] == 5)