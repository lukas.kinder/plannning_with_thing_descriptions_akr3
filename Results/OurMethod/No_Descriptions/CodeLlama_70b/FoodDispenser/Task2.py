# Was not able to entangle with real device! (Are still unconnected)
import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True
class FoodDispenser:
    def __init__(self):
        self.automatic = True
        self.stored = False

    def dispense(self):
        # Dispenses food
        pass

    def refilled(self):
        # Refills the dispenser
        pass

    def set_state(self,state):
        # Sets the state of the dispenser
        # :param string state: The new state ("manual" or "automatic")
        if state == "manual":
            self.automatic = False
        elif state == "automatic":
            self.automatic = True
        else:
            raise ValueError("Invalid state")


# Turn to automatic and make sure the storage is full.
foodDispenser = FoodDispenser()
foodDispenser.set_state("automatic")
foodDispenser.refilled()

simulator = foodDispenser.real_device
print(simulator.food_storage == 150 and simulator.automatic)