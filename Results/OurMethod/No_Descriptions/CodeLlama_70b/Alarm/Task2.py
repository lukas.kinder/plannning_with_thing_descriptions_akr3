class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration


class Alarm:
    def __init__(self):
        self.real_device = AlarmSimulator()
        self.set = False
        self.vibration = False

    def set_alarm(self):
        # sets the alarm
        self.set = True

        print(f"set_alarm()")
        self.real_device.set_alarm()

    def unset(self):
        # unsets the alarm
        self.set = False

        print(f"unset()")
        self.real_device.unset()

    def toggle_vibration_mode(self):
        # Toggles the vibration mode
        self.vibration = not self.vibration

        print(f"toggle_vibration_mode()")
        self.real_device.toggle_vibration_mode()

# Turn on the alarm. But not on vibration
alarm = Alarm()
alarm.set_alarm()
alarm.unset()
alarm.toggle_vibration_mode()


simulator = alarm.real_device
print(simulator.set and not simulator.vibration)