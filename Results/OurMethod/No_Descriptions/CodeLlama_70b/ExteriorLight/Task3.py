class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10


class ExteriorLight:
    def __init__(self):

        self.real_device = ExteriorLightSimulator()
        self.status = False # whether the light is on or off
        self.brightness = 100 # how bright the light is (minimum = 0, maximum = 100)

    def turn_on(self):
        # turns the light on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # turns the light off
        self.status = False


        print(f"turn_off()")
        self.real_device.turn_off()

# Reduce the brightness to 20.
exteriorLight = ExteriorLight()
exteriorLight.turn_off()
exteriorLight.brightness = 20


simulator = exteriorLight.real_device
print(simulator.brightness == 20)