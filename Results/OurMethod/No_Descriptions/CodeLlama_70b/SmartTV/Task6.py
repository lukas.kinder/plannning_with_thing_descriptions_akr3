class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False # whether or not the TV is recording
        self.off = True # Whether the TV is turned off
        self.brightness = 70 # Brightness level (Minimum = 0, Maximum = 100)
        self.channel = 'ZDF' # Channel name (Possible values: PRO7, RTL, SAT1, ARD, KIKA)

    def record(self):
        # Record the TV
        pass

        print(f"record()")
        self.real_device.record()

    def switch(self):
        # Switch the TV on/off
        pass

        print(f"switch()")
        self.real_device.switch()

    def turn_on(self):
        # Turn the TV on
        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turn the TV off
        self.off = True

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self,value):
        # Set the brightness level (Minimum = 0, Maximum = 100)
        # :param integer value: the new brightness level
        # requires that the TV is on
        if not self.off:
            self.turn_on()

        if value < 0 or value > 100:
            raise ValueError("Invalid brightness level")
        self.brightness = value


        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# Lower the brightness to 20.
smartTV = SmartTV()
smartTV.set_brightness(20)


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Car:
    def __init__(self):
        self.fuel = 0 # The amount of fuel left in the car
        self.speed = 0 # The speed of the car
        self.direction = "forward" # The direction the car is facing

    def move(self, distance):
        # Moves the car forward
        # :param integer distance: The distance to move

        # requires that the car has fuel
        if not self.fuel:
            raise ValueError("Car is out of fuel.")

        self.fuel -= distance

    def turn(self, direction):
        # Turns the car
        # :param string direction: The direction to turn
        # requires that the car is moving
        if not self.fuel:
            raise ValueError("Car is out of fuel.")

        self.direction = direction

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive for 30 miles and then turn right.".



# Drive for 30 miles and then turn right.
car = Car()
car.move(30)
car.turn("right")












simulator = smartTV.real_device
print(simulator.brightness == 20)