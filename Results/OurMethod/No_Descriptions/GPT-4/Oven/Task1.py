class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True


class Oven:
    def __init__(self):
        self.real_device = OvenSimulator()
        self.on = False
        self.heating_method = "convection"

    def switch_on(self):
        # switches oven on

        self.on = True

        print(f"switch_on()")
        self.real_device.switch_on()

    def switch_off(self):
        # switches oven off

        self.on = False

        print(f"switch_off()")
        self.real_device.switch_off()

    def change_heating_method(self):
        # changes heating method

        if self.heating_method == "convection":
            self.heating_method = "top and bottom"
        else:
            self.heating_method = "convection"

        print(f"change_heating_method()")
        self.real_device.change_heating_method()

    def preheat(self):
        # preheats the oven

        # requires that the oven is on
        if not self.on:
            self.switch_on()

        # Preheating usually involves bringing the oven to a specific temperature.
        # As this is not specified and the variable doesn't exist, it won't be implemented here.
        # Code here that preheats the oven (if it is a real oven)

        print(f"preheat()")
        self.real_device.preheat()

# Turn the oven on.
oven = Oven()
oven.switch_on()

simulator = oven.real_device
print(simulator.on)