class AirConditionerSimulator:
    def __init__(self):
        self.goal_Temperature = 22
        self.status = False

    def get_goal_Temperature(self):
        return self.goal_Temperature
    
    def get_status(self):
        return self.status
    
    def toggle(self):
        self.status = not self.status

    def temperature_Up(self):
        if self.status:
            self.goal_Temperature = min(35,self.goal_Temperature +1)

    def temperature_Down(self):
        if self.status:
            self.goal_Temperature = max(10,self.goal_Temperature -1)


class AirConditioner:
    def __init__(self):
        self.real_device = AirConditionerSimulator()
        self.goal_Temperature = 22 # Goal Temperature (between 10 and 35)
        self.status = False # Status of Air Conditioner

    def toggle(self):
        # Toggle the status
        self.status = not self.status

        print(f"toggle()")
        self.real_device.toggle()

    def temperature_Up(self):
        # Increase the goal temperature

        # requires that the device is on
        if not self.status:
            self.toggle()

        if self.goal_Temperature < 35:
            self.goal_Temperature += 1

        print(f"temperature_Up()")
        self.real_device.temperature_Up()

    def temperature_Down(self):
        # Decrease the goal temperature

        # requires that the device is on
        if not self.status:
            self.toggle()

        if self.goal_Temperature > 10:
            self.goal_Temperature -= 1

        print(f"temperature_Down()")
        self.real_device.temperature_Down()

# Cool down the room to 15 degree.
airConditioner = AirConditioner()
while airConditioner.goal_Temperature > 15:
    airConditioner.temperature_Down()

simulator = airConditioner.real_device
print(simulator.status and simulator.goal_Temperature == 15)