class VentilatorSimulator:
    def __init__(self):
        self.status = "off"
        self.level = 3

    def get_status(self):
        return self.status
    
    def get_level(self):
        return self.level

    def start(self):
        if self.status == "off":
            self.status = "on"

    def stop(self):
        if self.status == "on":
            self.status = "off"

    def speed_up(self):
        if self.status == "on" and self.level < 3:
            self.level += 1

    def speed_down(self):
        if self.status == "on" and self.level > 1:
            self.level -= 1


class Ventilator:
    def __init__(self):
        self.real_device = VentilatorSimulator()
        self.status = "off" # The status of the ventilator can be "on" or "off"
        self.level = 3 # The speed level of the ventilator, can be 1, 2, 3

    def start(self):
        # Starts the ventilator
        self.status = "on"

        print(f"start()")
        self.real_device.start()

    def stop(self):
        # Stops the ventilator
        self.status = "off"

        print(f"stop()")
        self.real_device.stop()

    def speed_up(self):
        # Increase the speed by 1 level, maximal level is 3

        # requires that the device is on
        if self.status == "off":
            self.start()

        if self.level < 3:
            self.level += 1

        print(f"speed_up()")
        self.real_device.speed_up()

    def speed_down(self):
        # Decrease the speed by 1 level, minimal level is 1

        # requires that the device is on
        if self.status == "off":
            self.start()

        if self.level > 1:
            self.level -= 1

        print(f"speed_down()")
        self.real_device.speed_down()

# I need some ventilation
ventilator = Ventilator()
ventilator.start()

simulator = ventilator.real_device
print(simulator.status == 'on')