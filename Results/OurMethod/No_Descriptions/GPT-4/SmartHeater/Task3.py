class SmartHeaterSimulator:
    def __init__(self):
        self.level = 4
        self.status = False

    def get_level(self):
        return self.level
    
    def get_status(self):
        return self.status

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase_level(self):
        if self.status and self.level < 6:
            self.level += 1

    def decrease_level(self):
        if self.status and self.level > 0:
            self.level -= 1


class SmartHeater:
    def __init__(self):
        self.real_device = SmartHeaterSimulator()
        self.level = 4 # the level amount, min: 0, max: 6
        self.status = False # indicates the status of the heater, on: True, off: False

    def turn_on(self):
        # turns the heater status to on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # turns the heater status to off
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase_level(self):
        # increases the level of the heater


        # requires that the device is on
        if not self.status:
            self.turn_on()

        if self.level < 6: # check if the level has reached the maximum before increasing
            self.level += 1

        print(f"increase_level()")
        self.real_device.increase_level()

    def decrease_level(self):
        # decreases the level of the heater

        # requires that the device is on
        if not self.status:
            self.turn_on()

        if self.level > 0: # check if the level has reached the minimum before decreasing
            self.level -= 1

        print(f"decrease_level()")
        self.real_device.decrease_level()

# Turn the heater on. But only level 1.
smartHeater = SmartHeater()
smartHeater.turn_on()
while smartHeater.level > 1:
    smartHeater.decrease_level()

simulator = smartHeater.real_device
print(simulator.status and simulator.level == 1)