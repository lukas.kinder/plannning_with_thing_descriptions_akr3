from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}

    def turn_on(self):
        # Turns device on
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns device off
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, brightness):
        # Sets the brightness
        # :param integer brightness: the brightness level between 0 and 254

        # requires that the device is on
        if not self.state["on"]:
            self.turn_on()

        if brightness >= 0 and brightness <= 254:
            self.state["bri"] = brightness

        print(f"set_brightness({brightness})")
        self.real_device.set_brightness(brightness)

    def set_saturation(self, saturation):
        # Sets the saturation
        # :param integer saturation: the saturation level between 0 and 254

        # requires that the device is on
        if not self.state["on"]:
            self.turn_on()

        if saturation >= 0 and saturation <= 254:
            self.state["sat"] = saturation

        print(f"set_saturation({saturation})")
        self.real_device.set_saturation(saturation)

# I want some light, but not bright.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.turn_on()
ambientLightstrip.set_brightness(100)

simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['bri'] <= 128)