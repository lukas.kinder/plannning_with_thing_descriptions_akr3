class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False # status of the device (on or off)
        self.tank = 'empty' # status of the tank (full or empty)
        self.humidity = 45 # the level of humidity (range from 0 to 100)

    def toggle_status(self):
        # switch the status of the dehumidifier
        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def dehumidify(self):
        # activate the dehumidification process

        # requires that the device is on
        if not self.status:
            self.toggle_status()
        # requires that the tank is not full
        if self.tank == 'full':
            self.drain()

        # Code to start the dehumidification here

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def drain(self):
        # empty the tank of the dehumidifier

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        self.tank = 'empty'

        print(f"drain()")
        self.real_device.drain()

    def set_humidity(self, desired_humidity):
        # set the desired level of humidity
        # :param integer desired_humidity: the desired level of humidity

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        if 0 <= desired_humidity <= 100: 
            self.humidity = desired_humidity
            # Code to adjust the humidity here


        print(f"set_humidity({desired_humidity})")
        self.real_device.set_humidity(desired_humidity)

# I want this room to have a humidity of 50
dehumidifier = Dehumidifier()
dehumidifier.set_humidity(50)

simulator = dehumidifier.real_device
print(simulator.status and simulator.humidity == 50)