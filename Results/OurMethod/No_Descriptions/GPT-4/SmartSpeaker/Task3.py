class SmartSpeakerSimulator:
    def __init__(self):
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

        self.current_service = "spotify"

    def get_connection(self):
        return self.connection
    
    def get_playing(self):
        return self.playing
    
    def get_status(self):
        return self.status
    
    def get_volume(self):
        return self.volume

    def reconnect(self):
        if self.status and not self.connection:
            self.connection = True

    def play(self):
        if self.status and self.connection and not self.playing:
            self.playing = True

    def pause(self):
        if self.status and self.connection and self.playing:
            self.playing = False

    def toggle(self):
        self.status = not self.status

    def standby(self):
        if self.status:
            self.playing = False
            self.connection = False

    def change_player(self):
        if self.status and self.connection:
           available_services = ["spotify", "youtube", "cloud", "radio", "new_channel"]
           idex = available_services.index(self.current_service)
           self.current_service = available_services[idex +1 % len(available_services)]

    def increase_volume(self):
        if self.status and self.connection and self.playing and self.volume < 120:
            self.volume += 5

    def decrease_volume(self):
        if self.status and self.connection and self.playing and self.volume > 10:
            self.volume -= 5


class SmartSpeaker:
    def __init__(self):
        self.real_device = SmartSpeakerSimulator()
        self.connection = False # connection status
        self.playing = False # indicates if something is being played
        self.status = False # indicates status of the device
        self.volume = 50 # current volume level (minimum = 10, maximum = 120)

    def reconnect(self):
        # establishes connection if not already connected
        if self.connection == False:
            self.connection = True

        print(f"reconnect()")
        self.real_device.reconnect()

    def play(self):
        # starts playing 

        # requires that the device is connected
        if self.connection == False:
            self.reconnect()
        
        self.playing = True

        print(f"play()")
        self.real_device.play()

    def pause(self):
        # stops playing 

        # requires that the device is playing
        if self.playing == False:
            self.play()
            
        self.playing = False

        print(f"pause()")
        self.real_device.pause()

    def toggle(self):
        # toggles the status
        self.status = not self.status

        print(f"toggle()")
        self.real_device.toggle()

    def standby(self):
        # put the device into standby
        self.status = False
        self.playing = False

        print(f"standby()")
        self.real_device.standby()

    def change_player(self):
        # changes the player
        # This function can be expanded with code as per the functionality and devices available.

# COMMENTED OUT - (SYNTAX ERROR):     def increase_volume(self):
        # increase the volume

        # requires that the volume is less than maximum
        if self.volume < 120:
            self.volume += 1

        print(f"change_player()")
        self.real_device.change_player()

    def decrease_volume(self):
        # decrease the volume 

        # requires that the volume is more than minimum
        if self.volume > 10:
            self.volume -= 1



        print(f"decrease_volume()")
        self.real_device.decrease_volume()

# Play me some music.
smartSpeaker = SmartSpeaker()
smartSpeaker.play()

simulator = smartSpeaker.real_device
print(simulator.playing)