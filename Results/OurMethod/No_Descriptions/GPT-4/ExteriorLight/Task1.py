class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10


class ExteriorLight:
    def __init__(self):
        self.real_device = ExteriorLightSimulator()
        self.status = False
        self.brightness = 100 # Controlled by a range of 0 (minimum) to 100 (maximum)

    def turn_on(self):
        # Turn on the light
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turn off the light
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase(self):
        # Increase the brightness of the light

        # requires that the light is on
        if not self.status:
            self.turn_on()

        if self.brightness < 100:
            self.brightness += 1

        print(f"increase()")
        self.real_device.increase()

    def decrease(self):
        # Decrease the brightness of the light

        # requires that the light is on
        if not self.status:
            self.turn_on()

        if self.brightness > 0:
            self.brightness -= 1

        print(f"decrease()")
        self.real_device.decrease()

# Light!
exteriorLight = ExteriorLight()
exteriorLight.turn_on()

simulator = exteriorLight.real_device
print(simulator.status)