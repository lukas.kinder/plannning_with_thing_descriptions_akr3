import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True
        self.stored = False
  
    def dispense(self):
        # Dispense food 

        # requires that the device is not empty
        if not self.stored:
            self.refill()

        self.stored = False    

        print(f"dispense()")
        self.real_device.dispense()

    def refill(self):
        # Refill the food
        self.stored = True
   
        print(f"refill()")
        self.real_device.refill()

    def set_state(self, state):
        # Set state to either "manual" or "automatic"
        # :param string state: The state to set

        if state.lower() == "manual":
            self.automatic = False
        elif state.lower() == "automatic":
            self.automatic = True
        else:
            print("Invalid state. Please set either 'manual' or 'automatic'")

        print(f"set_state({state})")
        self.real_device.set_state(state)

# Refill the dispenser.
foodDispenser = FoodDispenser()
foodDispenser.refill()

simulator = foodDispenser.real_device
print(simulator.stored)