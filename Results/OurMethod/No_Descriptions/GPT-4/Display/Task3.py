from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {"status": False} # The power status of display
        self.content = "Video" # The content currently on the display

    def toggle(self):
        # Turn power off/on

        self.power["status"] = not self.power["status"]

        print(f"toggle()")
        self.real_device.toggle()

    def setVolume(self):
        # Set the volume of the display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        print(f"setVolume()")
        self.real_device.setVolume()

    def setBright(self):
        # Set brightness of the display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        print(f"setBright()")
        self.real_device.setBright()

    def showText(self):
        # Show text on the display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        print(f"showText()")
        self.real_device.showText()

    def playVideo(self, identifier, name, description, url):
        # Play video on the display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.content = f"Video: {name}"

        print(f"playVideo({identifier},{name},{description},{url})")
        self.real_device.playVideo(identifier, name, description, url)

    def pauseVideo(self):
        # Pause the current video

        # requires that the device is on and video is playing
        if not self.power["status"] or "Video:" not in self.content:
            return

        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def stopVideo(self):
        # Stop the current video

        # requires that the device is on and video is playing
        if not self.power["status"] or "Video:" not in self.content:
            return

        self.content = ''

        print(f"stopVideo()")
        self.real_device.stopVideo()

    def presentationWebApp(self, identifier, name, description, url):
        # Present a web app

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.content = f"App: {name}"

        print(f"presentationWebApp({identifier},{name},{description},{url})")
        self.real_device.presentationWebApp(identifier, name, description, url)

    def launchNewsApp(self):
        # Launch a news app

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.content = 'News App'

        print(f"launchNewsApp()")
        self.real_device.launchNewsApp()

# Display the text 'Hello World' with a brightness of 90.
display = Display()
display.showText()
display.setBright()

simulator = display.real_device
print(simulator.power['status'] and simulator.content == 'Text' and simulator.text_buffer == 'Hello World' and simulator.brightness == 90)