class SmartSpeakerSimulator:
    def __init__(self):
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

        self.current_service = "spotify"

    def get_connection(self):
        return self.connection
    
    def get_playing(self):
        return self.playing
    
    def get_status(self):
        return self.status
    
    def get_volume(self):
        return self.volume

    def reconnect(self):
        if self.status and not self.connection:
            self.connection = True

    def play(self):
        if self.status and self.connection and not self.playing:
            self.playing = True

    def pause(self):
        if self.status and self.connection and self.playing:
            self.playing = False

    def toggle(self):
        self.status = not self.status

    def standby(self):
        if self.status:
            self.playing = False
            self.connection = False

    def change_player(self):
        if self.status and self.connection:
           available_services = ["spotify", "youtube", "cloud", "radio", "new_channel"]
           idex = available_services.index(self.current_service)
           self.current_service = available_services[idex +1 % len(available_services)]

    def increase_volume(self):
        if self.status and self.connection and self.playing and self.volume < 120:
            self.volume += 5

    def decrease_volume(self):
        if self.status and self.connection and self.playing and self.volume > 10:
            self.volume -= 5


class SmartSpeaker:
    def __init__(self):
        self.real_device = SmartSpeakerSimulator()
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

    def reconnect(self):
        # reconnects to the speaker
        self.connection = True

        print(f"reconnect()")
        self.real_device.reconnect()

    def play(self):
        # plays music
        self.playing = True

        print(f"play()")
        self.real_device.play()

    def pause(self):
        # pauses music
        self.playing = False

        print(f"pause()")
        self.real_device.pause()

    def toggle(self):
        # toggles between playing and paused
        self.playing = not self.playing

        print(f"toggle()")
        self.real_device.toggle()

    def standby(self):
        # puts the speaker in standby mode
        self.status = False

        print(f"standby()")
        self.real_device.standby()

    def change_player(self):
        # changes the player
        self.status = not self.status

        print(f"change_player()")
        self.real_device.change_player()

    def increase_volume(self):
        # increases the volume
        if self.volume < 120:
            self.volume += 10

        print(f"increase_volume()")
        self.real_device.increase_volume()

    def decrease_volume(self):
        # decreases the volume
        if self.volume > 10:
            self.volume -= 10

        print(f"decrease_volume()")
        self.real_device.decrease_volume()

# Connect the Speaker to the internet.
smartSpeaker = SmartSpeaker()
smartSpeaker.reconnect()


simulator = smartSpeaker.real_device
print(simulator.status and simulator.connection)