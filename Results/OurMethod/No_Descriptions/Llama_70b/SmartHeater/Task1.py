class SmartHeaterSimulator:
    def __init__(self):
        self.level = 4
        self.status = False

    def get_level(self):
        return self.level
    
    def get_status(self):
        return self.status

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase_level(self):
        if self.status and self.level < 6:
            self.level += 1

    def decrease_level(self):
        if self.status and self.level > 0:
            self.level -= 1


class SmartHeater:
    def __init__(self):
        self.real_device = SmartHeaterSimulator()
        self.level = 4
        self.status = False

    def turn_on(self):
        # Turns the heater on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the heater off
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase_level(self):
        # Increases the level of the heater

        # requires that the heater is on
        if not self.status:
            self.turn_on()

        # requires that the level is below the maximum
        if self.level >= 6:
            return

        self.level += 1

        print(f"increase_level()")
        self.real_device.increase_level()

    def decrease_level(self):
        # Decreases the level of the heater

        # requires that the heater is on
        if not self.status:
            self.turn_on()

        # requires that the level is above the minimum
        if self.level <= 0:
            return

        self.level -= 1


        print(f"decrease_level()")
        self.real_device.decrease_level()

# Turn the heater on.
smartHeater = SmartHeater()
smartHeater.turn_on()


simulator = smartHeater.real_device
print(simulator.status)