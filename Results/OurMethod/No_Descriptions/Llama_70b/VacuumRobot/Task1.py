import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100


class VacuumRobot:
    def __init__(self):
        self.real_device = VacuumRobotSimulator()
        self.cleaning = False # indicates if the robot is cleaning
        self.storage_full = True # indicates if the storage is full
        self.battery = 30 # the battery level (minimum = 0, maximum = 100)
        self.mapped = True # indicates if the robot has mapped its environment
        self.at_base = True # indicates if the robot is at its base

    def start(self):
        # start the vacuum cleaner
        self.cleaning = True

        print(f"start()")
        self.real_device.start()

    def scan(self):
        # scans for objects

        # requires that the device is cleaning
        if not self.cleaning:
            self.start()

        print(f"scan()")
        self.real_device.scan()

    def empty_storage(self):
        # empties the storage

        # requires that the storage is full
        if not self.storage_full:
            self.storage_full = True

        print(f"empty_storage()")
        self.real_device.empty_storage()

    def return_to_base(self):
        # returns the robot to its base

        # requires that the robot is not at its base
        if self.at_base:
            self.at_base = False

        print(f"return_to_base()")
        self.real_device.return_to_base()

    def charge(self):
        # charges the robot
        self.battery = 100


        print(f"charge()")
        self.real_device.charge()

# Recharge the battery and empty the storage.
vacuumRobot = VacuumRobot()
vacuumRobot.charge()
vacuumRobot.empty_storage()


simulator = vacuumRobot.real_device
print(simulator.battery == 100 and not simulator.storage_full)