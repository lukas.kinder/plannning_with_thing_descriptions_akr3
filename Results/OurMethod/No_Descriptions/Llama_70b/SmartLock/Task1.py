class SmartLockSimulator:
    def __init__(self):
        self.locked = True

        self.statistic = {"counter_locked" : 0}

    def get_locked(self):
        return self.locked

    def lock(self):
        if not self.locked:
            self.locked = True
            self.statistic["counter_locked"] +=1

    def unlock(self):
        if self.locked:
            self.locked = False


class SmartLock:
    def __init__(self):
        self.real_device = SmartLockSimulator()
        self.locked = True

    def lock(self):
        # locks the door

        self.locked = True

        print(f"lock()")
        self.real_device.lock()

    def unlock(self):
        # unlocks the door

        self.locked = False


        print(f"unlock()")
        self.real_device.unlock()

# Open the lock
smartLock = SmartLock()
smartLock.unlock()


simulator = smartLock.real_device
print(not simulator.locked)