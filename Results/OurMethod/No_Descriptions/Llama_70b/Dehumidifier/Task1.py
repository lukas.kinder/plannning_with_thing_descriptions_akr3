class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False
        self.tank = "empty" # indicates the tank content
        self.humidity = 45 # The humidity level

    def toggle_status(self):
        # Turns the dehumidifier on or off
        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def dehumidify(self):
        # dehumidifies the air

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        # requires that the tank is full
        if self.tank != "full":
            self.drain()

        # requires that the humidity level is below 80%
        if self.humidity >= 80:
            self.set_humidity(79)

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def drain(self):
        # drains the tank

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        # requires that the tank is not empty
        if self.tank == "empty":
            self.set_humidity(0)
        else:
            self.set_humidity(100)

        print(f"drain()")
        self.real_device.drain()

    def set_humidity(self,value):
        # sets the humidity level
        # :param integer value: the desired humidity level

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        # requires that the humidity level is within range
        if value < 0 or value > 100:
            self.set_humidity(50)
        else:
            self.humidity = value


        print(f"set_humidity({value})")
        self.real_device.set_humidity(value)

# Turn the dehumidifier on.
dehumidifier = Dehumidifier()
dehumidifier.toggle_status()


simulator = dehumidifier.real_device
print(simulator.status)