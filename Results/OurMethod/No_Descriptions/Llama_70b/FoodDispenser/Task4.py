import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True
        self.stored = False

    def dispense(self):
        # Dispenses food

        # requires that there is stored food
        if not self.stored:
            raise ValueError("There is no stored food")

        # requires that the device is automatic
        if not self.automatic:
            raise ValueError("Device is not in automatic mode")

        # Code to dispense food here

        self.stored = False

        print(f"dispense()")
        self.real_device.dispense()

    def refill(self):
        # Refills food

        # requires that the device is automatic
        if not self.automatic:
            raise ValueError("Device is not in automatic mode")

        # Code to refill food here

        self.stored = True

        print(f"refill()")
        self.real_device.refill()

    def set_state(self,state):
        # Set state to either "manual" or "automatic"
        # :param string state: the new state of the device

        if state == "manual":
            self.automatic = False
        elif state == "automatic":
            self.automatic = True
        else:
            raise ValueError("Invalid state")


        print(f"set_state({state})")
        self.real_device.set_state(state)

# Dispense 10 potions of food times.
foodDispenser = FoodDispenser()
for i in range(10):
    foodDispenser.dispense()


simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 10)