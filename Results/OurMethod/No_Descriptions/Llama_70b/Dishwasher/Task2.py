class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False
        self.door = "closed"

    def close(self):
        # Closes the dish washer

        self.door = "closed"

        print(f"close()")
        self.real_device.close()

    def open(self):
        # Opens the dish washer

        self.door = "open"

        print(f"open()")
        self.real_device.open()

    def stop(self):
        # Stops the dish washer

        self.running = False

        print(f"stop()")
        self.real_device.stop()

    def start(self):
        # Starts the dish washer

        self.running = True


        print(f"start()")
        self.real_device.start()

# Wash the dishes. Open the door afterwards.
dishwasher = Dishwasher()
dishwasher.start()
dishwasher.stop()
dishwasher.close()
dishwasher.open()


simulator = dishwasher.real_device
print(simulator.statistics['used'] == 1)