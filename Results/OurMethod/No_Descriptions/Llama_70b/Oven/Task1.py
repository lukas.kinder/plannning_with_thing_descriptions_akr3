class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True


class Oven:
    def __init__(self):
        self.real_device = OvenSimulator()
        self.on = False
        self.heating_method = "convection"

    def switch_on(self):
        self.on = True

        print(f"switch_on()")
        self.real_device.switch_on()

    def switch_off(self):
        self.on = False

        print(f"switch_off()")
        self.real_device.switch_off()

    def change_heating_method(self):
        self.heating_method = "top and bottom"

        print(f"change_heating_method()")
        self.real_device.change_heating_method()

    def preheat(self):
        pass


        print(f"preheat()")
        self.real_device.preheat()

# Turn the oven on.
oven = Oven()
oven.switch_on()


simulator = oven.real_device
print(simulator.on)