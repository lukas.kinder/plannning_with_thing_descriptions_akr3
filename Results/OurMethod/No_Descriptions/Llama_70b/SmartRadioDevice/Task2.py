class SmartRadioDeviceSimulator:
    def __init__(self):
        self.power = "on"
        self.channel = "Jamz"

    def get_power(self):
        return self.power
    
    def get_channel(self):
        return self.channel

    def power_on(self):
        if self.power == "off":
            self.power = "on"

    def switch_channel(self):
        if self.power == "on":
            channels = ["MTV", "Jamz", "The Beat", "The Mix"]
            current_index = channels.index(self.channel)
            next_index = (current_index + 1) % len(channels)
            self.channel = channels[next_index]

    def power_off(self):
        if self.power == "on":
            self.power = "off"


class SmartRadioDevice:
    def __init__(self):
        self.real_device = SmartRadioDeviceSimulator()
        self.power = "on" # characterics: ['off', 'on']
        self.channel = "Jamz" # characterics: ['MTV', 'Jamz', 'The Beat', 'The Mix']

    def power_on(self):
        # Turns the device on

        # requires that the device is off
        if self.power == "off":
            self.power = "on"

        print(f"power_on()")
        self.real_device.power_on()

    def switch_channel(self):
        # Changes the channel

        # requires that the device is on
        if self.power == "on":
            # requires that the device is on
            if self.channel == "MTV":
                self.channel = "Jamz"
            elif self.channel == "Jamz":
                self.channel = "The Beat"
            else:
                self.channel = "The Mix"

        print(f"switch_channel()")
        self.real_device.switch_channel()

    def power_off(self):
        # Turns the device off

        # requires that the device is on
        if self.power == "on":
            self.power = "off"


        print(f"power_off()")
        self.real_device.power_off()

# I want to listen to 'The Beat'.
smartRadioDevice = SmartRadioDevice()
smartRadioDevice.power_on()
while smartRadioDevice.channel != "The Beat":
    smartRadioDevice.switch_channel()
smartRadioDevice.power_off()


simulator = smartRadioDevice.real_device
print(simulator.power and simulator.channel == 'The Beat')