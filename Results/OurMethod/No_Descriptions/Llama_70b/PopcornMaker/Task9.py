class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0


class PopcornMaker:
    def __init__(self):
        self.real_device = PopcornMakerSimulator()
        self.power = False

    def toggle(self):
        # Toggles the power of the machine

        self.power = not self.power

        print(f"toggle()")
        self.real_device.toggle()

    def add_corn(self):
        # Adds corn to the machine

        # requires that the machine is off
        if self.power:
            self.toggle()

        print(f"add_corn()")
        self.real_device.add_corn()

    def add_sugar(self):
        # Adds sugar to the machine

        # requires that the machine is off
        if self.power:
            self.toggle()

        print(f"add_sugar()")
        self.real_device.add_sugar()

    def add_salt(self):
        # Adds salt to the machine

        # requires that the machine is off
        if self.power:
            self.toggle()

        print(f"add_salt()")
        self.real_device.add_salt()

    def heat(self):
        # Heats the machine

        # requires that the machine is off
        if self.power:
            self.toggle()


        print(f"heat()")
        self.real_device.heat()

# Please make us 3 times sweet popcorn, 2 salty popcorn, 1 extra salty popcorn and 3 extra sweet popcorn. For the extras just add the sugar/salt twice.
popcornMaker = PopcornMaker()
popcornMaker.toggle()
popcornMaker.add_salt()
popcornMaker.heat()
popcornMaker.add_salt()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_salty()
popcornMaker.heat()
popcornMaker.add_salty()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_sweet()
popcornMaker.add_sweet()
popcornMaker.heat()
popcornMaker.add_

simulator = popcornMaker.real_device
print(simulator.statistic['made_sweet_popcorn'] == 3 and simulator.statistic['made_salty_popcorn'] == 2 and simulator.statistic['made_extra_salty_popcorn'] == 1 and simulator.statistic['made_extra_sweet_popcorn'] == 3)