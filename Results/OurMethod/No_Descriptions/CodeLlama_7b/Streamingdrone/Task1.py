import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False
        self.streaming = False
        self.battery = 50

    def stream(self):
        # Start streaming

        self.streaming = True

        print(f"stream()")
        self.real_device.stream()

    def stop_stream(self):
        # Stop streaming

        self.streaming = False

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def start_recording(self):
        # Start recording

        self.recording = True

        print(f"start_recording()")
        self.real_device.start_recording()

    def end(self):
        # Ends the recording

        self.recording = False

        print(f"end()")
        self.real_device.end()

    def recharge(self):
        # Recharges the drone

        self.battery = 100


        print(f"recharge()")
        self.real_device.recharge()

# Recharge the drone.
streamingdrone = Streamingdrone()
streamingdrone.recharge()


simulator = streamingdrone.real_device
print(simulator.battery == 100)