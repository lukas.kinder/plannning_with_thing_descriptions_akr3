class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False
        self.tank = "empty" # characteristics are full or empty
        self.humidity = 45

    def toggle_status(self):
        # Turns power off/on

        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def dehumidify(self):
        # dehumidifies

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        # requires that tank is full
        if self.tank == "empty":
            self.drain()

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def drain(self):
        # drains tank

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        self.tank = "empty"

        print(f"drain()")
        self.real_device.drain()

    def set_humidity(self,value):
        # sets the humidity
        # :param integer value: the desired humidity

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        if value >= 0 and value <= 100:
            self.humidity = value


        print(f"set_humidity({value})")
        self.real_device.set_humidity(value)

# Set the humidity to 40
dehumidifier = Dehumidifier()
dehumidifier.set_humidity(40)


simulator = dehumidifier.real_device
print(simulator.humidity == 40)