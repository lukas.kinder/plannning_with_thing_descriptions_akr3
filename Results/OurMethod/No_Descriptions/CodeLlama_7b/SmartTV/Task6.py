class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def record(self):
        # record

        self.recording = True

        print(f"record()")
        self.real_device.record()

    def switch(self):
        # switch

        self.off = not self.off

        print(f"switch()")
        self.real_device.switch()

    def turn_on(self):
        # turn on

        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # turn off

        self.off = True

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self,value):
        # set brightness
        # :param integer value: the value the brightness should be

        if value >= 0 and value <= 100:
            self.brightness = value


        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# Lower the brightness to 20.
smartTV = SmartTV()
smartTV.set_brightness(20)


simulator = smartTV.real_device
print(simulator.brightness == 20)