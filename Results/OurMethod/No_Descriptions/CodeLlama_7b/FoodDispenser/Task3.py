import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True
        self.stored = False

    def dispense(self):
        # dispenses food
        # :warning: This function does not work yet

        pass

        print(f"dispense()")
        self.real_device.dispense()

    def refill(self):
        # refills food
        # :warning: This function does not work yet

        pass

        print(f"refill()")
        self.real_device.refill()

    def set_state(self,state):
        # set state to either manual or automatic
        # :param string state: the new state

        if state == "manual":
            self.automatic = False
        elif state == "automatic":
            self.automatic = True


        print(f"set_state({state})")
        self.real_device.set_state(state)

# Dispense food three times.
foodDispenser = FoodDispenser()
for i in range(3):
    foodDispenser.dispense()


simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 3)