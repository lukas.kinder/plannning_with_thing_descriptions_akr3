class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False
        self.filled = False

    def switch_power(self):
        # switch power

        self.power = not self.power

        print(f"switch_power()")
        self.real_device.switch_power()

    def heat(self):
        # heats the kettle

        # requires that the device is off
        if self.power:
            self.switch_power()

        # requires that the kettle is empty
        if not self.filled:
            self.empty()

        print(f"heat()")
        self.real_device.heat()

    def fill(self):
        # fills the kettle

        # requires that the device is off
        if self.power:
            self.switch_power()

        self.filled = True

        print(f"fill()")
        self.real_device.fill()

    def empty(self):
        # empties the kettle

        # requires that the device is off
        if self.power:
            self.switch_power()

        self.filled = False


        print(f"empty()")
        self.real_device.empty()

# Start heating up some water.
kettle = Kettle()
kettle.heat()


simulator = kettle.real_device
print(simulator.contain_hot_water)