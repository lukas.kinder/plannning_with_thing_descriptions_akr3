class SprinklerSimulator:
    def __init__(self):
        self.water = True
        self.on = False

        self.stored_water = 4 # initial stored water. Can store up to 12

        self.statistic = {"n_sprinkle" : 0}

    def get_water(self):
        return self.water
    
    def get_on(self):
        return self.on

    def power_on(self):
        if not self.on:
            self.on = True

    def power_off(self):
        if self.on:
            self.on = False

    def sprinkle(self):
        if self.on and self.water:
            self.stored_water -=1

            if self.stored_water == 0:
                self.water = False

            self.statistic['n_sprinkle'] +=1
  
    def refill(self):
        self.water = True
        self.stored_water = 12


class Sprinkler:
    def __init__(self):
        self.real_device = SprinklerSimulator()
        self.water = True
        self.on = False

    def power_on(self):
        # Turns the sprinkler on
        self.on = True

        print(f"power_on()")
        self.real_device.power_on()

    def power_off(self):
        # Turns the sprinkler off
        self.on = False

        print(f"power_off()")
        self.real_device.power_off()

    def sprinkle(self):
        # Sprinkles water

        # requires that the device is on
        if not self.on:
            self.power_on()

        # requires that there is water
        if not self.water:
            self.refill()

        # code to sprinkle water here

        self.water = False

        print(f"sprinkle()")
        self.real_device.sprinkle()

    def refill(self):
        # Refills the water

        # requires that the device is on
        if not self.on:
            self.power_on()

        self.water = True


        print(f"refill()")
        self.real_device.refill()

# Start watering the garden.
sprinkler = Sprinkler()
sprinkler.power_on()
sprinkler.sprinkle()


simulator = sprinkler.real_device
print(simulator.statistic['n_sprinkle'] == 1)