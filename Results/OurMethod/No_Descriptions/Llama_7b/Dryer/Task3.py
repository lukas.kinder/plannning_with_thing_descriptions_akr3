import random

class DryerSimulator:
    def __init__(self):
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def get_running(self):
        return self.running
    
    def get_temperature(self):
        return self.temperature
    
    def get_distilled_storage(self):
        return self.distilled_storage

    def start(self):
        if not self.running and self.distilled_storage > 0:
            self.running = True
            self.distilled_storage -= random.randint(10,30)

    def clear(self):
        self.distilled_storage = 100

    def increase_temperature(self):
        if not self.running and self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        if not self.running and self.temperature > 40:
            self.temperature -= 10


class Dryer:
    def __init__(self):
        self.real_device = DryerSimulator()
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def start(self):
        # starts the dryer
        self.running = True

        print(f"start()")
        self.real_device.start()

    def clear(self):
        # clears the dryer
        self.running = False

        print(f"clear()")
        self.real_device.clear()

    def increase_temperature(self):
        # increases the temperature
        self.temperature += 1

        print(f"increase_temperature()")
        self.real_device.increase_temperature()

    def decrease_temperature(self):
        # decreases the temperature
        self.temperature -= 1


        print(f"decrease_temperature()")
        self.real_device.decrease_temperature()

# Run the dryer with a temperature of 60.
dryer = Dryer()
dryer.start()
dryer.increase_temperature()



simulator = dryer.real_device
print(simulator.running and simulator.temperature == 60)