class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False
        self.door = "closed"

    def close(self):
        # closes dishwasher

        # requires that the device is running
        if not self.running:
            self.stop()

        self.door = "closed"

        print(f"close()")
        self.real_device.close()

    def open(self):
        # opens dishwasher

        # requires that the device is not running
        if self.running:
            self.stop()

        self.door = "open"

        print(f"open()")
        self.real_device.open()

    def stop(self):
        # stops dishwasher

        # requires that the device is running
        if self.running:
            self.running = False

        print(f"stop()")
        self.real_device.stop()

    def start(self):
        # starts dishwasher

        # requires that the device is not running
        if not self.running:
            self.running = True



        print(f"start()")
        self.real_device.start()

# Start the dishwasher
dishwasher = Dishwasher()
dishwasher.start()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Lamp:
    def __init__(self):
        self.brightness = 50 # The brightness of the lamp (0 - 100)
        self.color = "white" # The color of the lamp (white, red, green, blue)

    def increaseBrightness(self):
        # Increase the brightness
        # requires that the lamp is on
        if not self.on:
            self.turn_on()

        self.brightness += 5

    def decreaseBrightness(self):
        # Decrease the brightness
        # requires that the lamp is on
        if not self.on:
            self.turn_off()

        self.brightness -= 5

    def changeColor(self,color):
        # Change the color of the lamp
        # requires that the lamp is on
        if not self.on:
            self.turn_off()

        if color == "red":
            self.color = "red"
        elif color == "green":
            self.color = "green"
        elif color == "blue":
            self.color = "blue"
        else:
            raise ValueError("Invalid color")

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Make the lamp brighter and turn it red.".


























































































































































































































































































































































































































































































































































































































































































































simulator = dishwasher.real_device
print(simulator.running)