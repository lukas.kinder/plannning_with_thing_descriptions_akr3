class VentilatorSimulator:
    def __init__(self):
        self.status = "off"
        self.level = 3

    def get_status(self):
        return self.status
    
    def get_level(self):
        return self.level

    def start(self):
        if self.status == "off":
            self.status = "on"

    def stop(self):
        if self.status == "on":
            self.status = "off"

    def speed_up(self):
        if self.status == "on" and self.level < 3:
            self.level += 1

    def speed_down(self):
        if self.status == "on" and self.level > 1:
            self.level -= 1


class Ventilator:
    def __init__(self):
        self.real_device = VentilatorSimulator()
        self.status = "off"
        self.level = 3

    def start(self):
        # starts the ventilator
        self.status = "on"

        print(f"start()")
        self.real_device.start()

    def stop(self):
        # stops the ventilator
        self.status = "off"

        print(f"stop()")
        self.real_device.stop()

    def speed_up(self):
        # increases the speed of the ventilator
        self.level += 1

        print(f"speed_up()")
        self.real_device.speed_up()

    def speed_down(self):
        # decreases the speed of the ventilator
        self.level -= 1


        print(f"speed_down()")
        self.real_device.speed_down()

# I need some ventilation.
ventilator = Ventilator()
ventilator.start()



simulator = ventilator.real_device
print(simulator.status == 'on')