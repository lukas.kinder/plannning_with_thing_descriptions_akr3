class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False # Flag if the TV is recording
        self.off = True # Flag if the TV is turned off
        self.brightness = 70 # Brightness of the TV (Minimum = 0, Maximum = 100)
        self.channel = 'ZDF' # Channel of the TV (Characteristics: PRO7, RTL, SAT1, ARD, KIKA)

    def record(self):
        # Records the TV
        self.recording = True

        print(f"record()")
        self.real_device.record()

    def switch(self):
        # Toggles the channel
        self.channel = random.choice(list(self.channel))

        print(f"switch()")
        self.real_device.switch()

    def turn_on(self):
        # Turns the TV on
        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the TV off
        self.off = True

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, value):
        # Sets the brightness of the TV
        # :param integer value: the new brightness
        # requires that the TV is on
        if not self.off:
            self.turn_on()


        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# I want to watch some 'KIKA'.
smartTV = SmartTV()
smartTV.switch()
smartTV.record()
smartTV.set_brightness(50)


simulator = smartTV.real_device
print(simulator.channel == 'KIKA')