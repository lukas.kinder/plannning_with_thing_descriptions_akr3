class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False # Whether the dehumidifier is on or off
        self.tank = "empty" # The current state of the tank
        self.humidity = 45 # The current humidity level

    def toggle_status(self):
        # Toggles the device on or off
        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def dehumidify(self):
        # Dehumidifies the air
        pass

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def drain(self):
        # Drains the tank
        pass

        print(f"drain()")
        self.real_device.drain()

    def set_humidity(self,value):
        # Sets the humidity level
        # :param integer value: the desired humidity level

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        # requires that the tank is not empty
        if self.tank == "empty":
            self.dehumidify()

        # requires that the humidity level is between 0 and 100
        if value >= 0 and value <= 100:
            self.humidity = value

        else:
            raise ValueError("Invalid humidity level")


        print(f"set_humidity({value})")
        self.real_device.set_humidity(value)

# I want this room to have a humidity of 50
dehumidifier = Dehumidifier()
dehumidifier.set_humidity(50)







simulator = dehumidifier.real_device
print(simulator.status and simulator.humidity == 50)