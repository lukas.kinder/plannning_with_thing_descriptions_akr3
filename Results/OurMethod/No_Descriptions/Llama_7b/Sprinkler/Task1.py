# Was not able to entangle with real device! (Are still unconnected)
class SprinklerSimulator:
    def __init__(self):
        self.water = True
        self.on = False

        self.stored_water = 4 # initial stored water. Can store up to 12

        self.statistic = {"n_sprinkle" : 0}

    def get_water(self):
        return self.water
    
    def get_on(self):
        return self.on

    def power_on(self):
        if not self.on:
            self.on = True

    def power_off(self):
        if self.on:
            self.on = False

    def sprinkle(self):
        if self.on and self.water:
            self.stored_water -=1

            if self.stored_water == 0:
                self.water = False

            self.statistic['n_sprinkle'] +=1
  
    def refill(self):
        self.water = True
        self.stored_water = 12
class Sprinkler:
    def __init__(self):
        self.water = True
        self.on = False

    def power_on(self):
        # Turns power on
        self.on = True

    def power_off(self):
        # Turns power off
        self.on = False

    def sprinkle(self):
        # Sprinkles water

        # requires that the device is on
        if not self.on:
            self.power_on()

    def refilled(self):
        # Refills the water

        # requires that the device is on
        if not self.on:
            self.power_on()


# Turn the sprinkler on.
sprinkler = Sprinkler()
sprinkler.power_on()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Light:
    def __init__(self):
        self.brightness = 50 # Brightness level (0-100)
        self.color = "white" # Color of light (white, red, green, blue)
        self.on = False # Whether the light is on or off

    def brighten(self):
        # Increases the brightness
        # :param integer amount: The amount to increase the brightness by (optional)

        # requires that the device is on
        if not self.on:
            self.power_on()

        if amount:
            self.brightness += amount
            if self.brightness > 100:
                self.brightness = 100
            if self.brightness < 0:
                self.brightness = 0

    def color_change(self,color):
        # Changes the color of the light
        # :param str color: The new color of the light (white, red, green, blue)

        # requires that the device is on
        if not self.on:
            self.power_on()

        if color == "red":
            self.color = "red"
        elif color == "green":
            self.color = "green"
        elif color == "blue":
            self.color = "blue"
        else:
            self.color = "white"

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Brighten the light to 75 and change its color to red.".




# Brighten the light to 75 and change its color to red.
light = Light()
light.brighten(75)
light.color_change("red")









simulator = sprinkler.real_device
print(simulator.on)