import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100


class VacuumRobot:
    def __init__(self):
        self.real_device = VacuumRobotSimulator()
        self.cleaning = False # Flag indicating whether the robot is cleaning or not
        self.storage_full = True # Flag indicating whether the storage is full or not
        self.battery = 30 # The battery level (minimum = 0, maximum = 100)
        self.mapped = True # Flag indicating whether the robot is mapped or not
        self.at_base = True # Flag indicating whether the robot is at the base or not

    def start(self):
        # Start the robot
        pass

        print(f"start()")
        self.real_device.start()

    def scan(self):
        # Scan the area for obstacles
        pass

        print(f"scan()")
        self.real_device.scan()

    def empty_storage(self):
        # Empty the storage
        pass

        print(f"empty_storage()")
        self.real_device.empty_storage()

    def return_to_base(self):
        # Return the robot to the base
        pass

        print(f"return_to_base()")
        self.real_device.return_to_base()

    def charge(self):
        # Charge the battery
        pass




        print(f"charge()")
        self.real_device.charge()

# Start cleaning.
vacuumRobot = VacuumRobot()
vacuumRobot.start()


simulator = vacuumRobot.real_device
print(simulator.cleaning)