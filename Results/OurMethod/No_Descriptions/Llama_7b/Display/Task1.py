# Was not able to entangle with real device! (Are still unconnected)
from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"
class Display:
    def __init__(self):

        self.power = {"status": False}
        self.content = "Video"


    def toggle(self):
        # Turns power off/on
        self.power["status"] = not self.power["status"]

    def setVolume(self, value):
        # sets the volume
        # :param integer value: the value the volume should be

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.power["status"] = value

    def setBright(self, value):
        # sets the brightness
        # :param integer value: the value the brightness should be

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.power["bright"] = value

    def showText(self, text):
        # shows the text
        # :param string text: the text to be shown

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.content = text

    def playVideo(self, identifier, name, description, url):
        # plays the video
        # :param string identifier: the identifier of the video
        # :param string name: the name of the video
        # :param string description: the description of the video
        # :param string url: the url of the video

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.content = None

    def pauseVideo(self):
        # pauses the video
        # requires that the device is on

        if not self.power["status"]:
            self.toggle()

    def stopVideo(self):
        # stops the video
        # requires that the device is on

        if not self.power["status"]:
            self.toggle()

    def presentationWebApp(self, identifier, name, description, url):
        # presents the web app
        # :param string identifier: the identifier of the web app
        # :param string name: the name of the web app
        # :param string description: the description of the web app
        # :param string url: the url of the web app

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.content = None

    def launchNewsApp(self, identifier, name, description, url):
        # launches the news app
        # :param string identifier: the identifier of the news app
        # :param string name: the name of the news app
        # :param string description: the description of the news app
        # :param string url: the url of the news app

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        self.content = None


    def getContent(self):
        # returns the content
        return self.content


# Turn the display on.
display = Display()
display.toggle()


simulator = display.real_device
print(simulator.power['status'])