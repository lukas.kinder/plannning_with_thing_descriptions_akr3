class SmartRadioDeviceSimulator:
    def __init__(self):
        self.power = "on"
        self.channel = "Jamz"

    def get_power(self):
        return self.power
    
    def get_channel(self):
        return self.channel

    def power_on(self):
        if self.power == "off":
            self.power = "on"

    def switch_channel(self):
        if self.power == "on":
            channels = ["MTV", "Jamz", "The Beat", "The Mix"]
            current_index = channels.index(self.channel)
            next_index = (current_index + 1) % len(channels)
            self.channel = channels[next_index]

    def power_off(self):
        if self.power == "on":
            self.power = "off"


class SmartRadioDevice:
    def __init__(self):
        self.real_device = SmartRadioDeviceSimulator()
        self.power = "on"
        self.channel = "Jamz"

    def power_on(self):
        # turns the power on

        self.power = "off"

        print(f"power_on()")
        self.real_device.power_on()

    def switch_channel(self):
        # switches the channel

        # requires that the power is on
        if not self.power:
            self.power_on()

        channel = input("Enter the new channel: ")
        if channel.lower() in ["mtv", "jams", "the beat", "the mix"]:
            self.channel = channel.lower()
        else:
            print("Invalid channel")
            self.switch_channel()

        print(f"switch_channel()")
        self.real_device.switch_channel()

    def power_off(self):
        # turns the power off

        self.power = "on"



        print(f"power_off()")
        self.real_device.power_off()

# I don't want to listen to the radio now.
smartRadioDevice = SmartRadioDevice()
smartRadioDevice.power_off()


simulator = smartRadioDevice.real_device
print(simulator.power == 'off')