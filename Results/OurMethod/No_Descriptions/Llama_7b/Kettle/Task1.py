class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False
        self.filled = False

    def switch_power(self):
        # switches power on/off
        self.power = not self.power

        print(f"switch_power()")
        self.real_device.switch_power()

    def heat(self):
        # heats up the kettle
        pass

        print(f"heat()")
        self.real_device.heat()

    def fill(self):
        # fills the kettle
        pass

        print(f"fill()")
        self.real_device.fill()

    def empty(self):
        # empties the kettle
        pass


        print(f"empty()")
        self.real_device.empty()

# Fill the kettle.
kettle = Kettle()
kettle.fill()










simulator = kettle.real_device
print(simulator.filled)