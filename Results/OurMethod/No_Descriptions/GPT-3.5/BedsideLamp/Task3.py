class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False
        self.brightness = 50

    def turn_on(self):
        # turns the lamp on
        self.on = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # turns the lamp off
        self.on = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase(self):
        # increases the brightness

        # requires that the device is on
        if not self.on:
            self.turn_on()

        if self.brightness < 100:
            self.brightness += 1

        print(f"increase()")
        self.real_device.increase()

    def decrease(self):
        # decreases the brightness

        # requires that the device is on
        if not self.on:
            self.turn_on()

        if self.brightness > 0:
            self.brightness -= 1

        print(f"decrease()")
        self.real_device.decrease()

# Make it is bright as possible.
bedsideLamp = BedsideLamp()
while bedsideLamp.brightness < 100:
    bedsideLamp.increase()

simulator = bedsideLamp.real_device
print(simulator.on and simulator.brightness == 100)