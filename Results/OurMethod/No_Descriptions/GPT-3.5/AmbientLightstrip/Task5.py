from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {
            "on": False,  # Boolean Value that describes if the device is on or off
            "bri": 254,  # Value between 0 and 254 indicating the Brightness of the Light
            "sat": 128  # Sets the intensity of the colors
        }

    def turn_on(self):
        # turn on the light
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # turn off the light
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, brightness):
        # set the brightness of the light
        # :param integer brightness: Value between 0 and 254 indicating the brightness of the light
        
        # requires that the device is on
        if not self.state["on"]:
            self.turn_on()

        if 0 <= brightness <= 254:
            self.state["bri"] = brightness

        print(f"set_brightness({brightness})")
        self.real_device.set_brightness(brightness)

    def set_saturation(self, saturation):
        # set the saturation of the light
        # :param integer saturation: Value between 0 and 254 indicating the saturation of the light
        
        # requires that the device is on
        if not self.state["on"]:
            self.turn_on()

        if 0 <= saturation <= 254:
            self.state["sat"] = saturation

        print(f"set_saturation({saturation})")
        self.real_device.set_saturation(saturation)

# Brightness to 30.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.set_brightness(30)

simulator = ambientLightstrip.real_device
print(simulator.state['bri'] == 30)