class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False
        self.tank = "empty"
        self.humidity = 45

    def toggle_status(self):
        # toggle status
        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def dehumidify(self):
        # dehumidify the air

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def drain(self):
        # drain the water from the tank

        # requires that the tank is not empty
        if self.tank == "full":
            self.tank = "empty"

        print(f"drain()")
        self.real_device.drain()

    def set_humidity(self,level):
        # Set the desired humidity level
        # :param integer value: the desired humidity level

        # requires that the device is on
        if not self.status:
            self.toggle_status()
        self.humidity = level

        print(f"set_humidity({level})")
        self.real_device.set_humidity(level)

# Turn the dehumidifier on.
dehumidifier = Dehumidifier()
dehumidifier.toggle_status()

simulator = dehumidifier.real_device
print(simulator.status)