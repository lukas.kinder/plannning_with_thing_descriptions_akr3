class SmartRadioDeviceSimulator:
    def __init__(self):
        self.power = "on"
        self.channel = "Jamz"

    def get_power(self):
        return self.power
    
    def get_channel(self):
        return self.channel

    def power_on(self):
        if self.power == "off":
            self.power = "on"

    def switch_channel(self):
        if self.power == "on":
            channels = ["MTV", "Jamz", "The Beat", "The Mix"]
            current_index = channels.index(self.channel)
            next_index = (current_index + 1) % len(channels)
            self.channel = channels[next_index]

    def power_off(self):
        if self.power == "on":
            self.power = "off"


class SmartRadioDevice:
    def __init__(self):
        self.real_device = SmartRadioDeviceSimulator()
        self.power = 'on'
        self.channel = 'Jamz'

    def power_on(self):
        # power on the device
        self.power = 'on'

        print(f"power_on()")
        self.real_device.power_on()

    def switch_channel(self, new_channel):
        # switch to the specified channel
        # :param String new_channel: the channel to switch to

        # requires that the device is on
        if self.power == 'off':
            self.power_on()

        if new_channel in ["MTV", "Jamz", "The Beat", "The Mix"]:
            self.channel = new_channel

        print(f"switch_channel({new_channel})")
        self.real_device.switch_channel(new_channel)

    def power_off(self):
        # power off the device
        self.power = 'off'

        print(f"power_off()")
        self.real_device.power_off()

# I want to listen to 'The Beat'.
smartRadioDevice = SmartRadioDevice()
smartRadioDevice.switch_channel("The Beat")

simulator = smartRadioDevice.real_device
print(simulator.power and simulator.channel == 'The Beat')