from copy import deepcopy
class ObjectOverflow(Exception):
    pass

class AutomatedWarehouseSimulator:
    def __init__(self):
        self.currentProductPosition = {
            "row1col1" : "Wood",
            "row1col2" : "Pipe",
            "row2col2" : "PlasticContainer",
            "row3col1" : "WaterBottles",
            "row3col2" : "Trash",
            "row3col3" : "Cables"
        }

        self.object_inside = None  # wether there is an object inside on the conveyor belt
        self.object_in_arm = None # wether the robotic arm is holding something
        self.arm_is_home = True # if the arm is at the 'home position'
        self.object_waiting_to_get_inside = ["Glass", "FuelContainer"]
        self.objects_moved_out = []

    def get_currentProductPosition(self):
        return deepcopy( self.currentProductPosition )

    def goHome(self):
        self.arm_is_home  = True

    def pickObjectAtPosition(self, row, column):
        # Pick object at a specified warehouse position
        self.arm_is_home  = False

        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            if self.object_in_arm != None:
                raise ObjectOverflow
            self.object_in_arm = self.currentProductPosition[position]
            del self.currentProductPosition[position]

    def putObjectAtPosition(self, row, column):
        # Put object at a specified warehouse position
        self.arm_is_home  = False
        if self.object_in_arm == None:
            #nothing to put
            return
        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            raise ObjectOverflow
        self.currentProductPosition[position] = self.object_in_arm
        self.object_in_arm = None
        

    def pickObjectFromConveyor(self):
        self.arm_is_home  = False
        # Pick object from a conveyor belt

        if self.object_in_arm != None:
            raise ObjectOverflow

        if self.object_inside != None:
            self.object_in_arm = self.object_inside
            self.object_inside = None

    def putObjectOnConveyor(self):
        self.arm_is_home  = False
        if self.object_in_arm != None:
            # An object is put on the conveyor belt
            if self.object_inside:
                #  There is already an object inside. 
                raise ObjectOverflow
            self.object_inside = self.object_in_arm
            self.object_in_arm = None

    def rollObjectForward(self):
        # An object is moved inside using the conveyor belt.
        if self.object_inside:
            #  There is already an object inside. 
            raise ObjectOverflow
        
        if len(self.object_waiting_to_get_inside) != 0:
            self.object_inside = self.object_waiting_to_get_inside.pop()

    def rollObjectBackward(self):
        if self.object_inside:
            self.objects_moved_out.append(self.object_inside)
            self.object_inside = None


class AutomatedWarehouse:
    def __init__(self):
        self.real_device = AutomatedWarehouseSimulator()
        self.currentProductPosition = {
            'row1col1': 'Wood',
            'row1col2': 'Pipe',
            'row2col2': 'PlasticContainer',
            'row3col1': 'WaterBottles',
            'row3col2': 'Trash',
            'row3col3': 'Cables'
        }

    def goHome(self):
        # Go to the home position
        pass  # implementation specific to the automated warehouse system

        print(f"goHome()")
        self.real_device.goHome()

    def pickObjectAtPosition(self, row, column):
        # Picks an object at the specified position
        # :param integer row: The row number (1-3)
        # :param integer column: The column number (1-3)
        pass  # implementation specific to the automated warehouse system

        print(f"pickObjectAtPosition({row},{column})")
        self.real_device.pickObjectAtPosition(row, column)

    def putObjectAtPosition(self, row, column):
        # Puts an object at the specified position
        # :param integer row: The row number (1-3)
        # :param integer column: The column number (1-3)
        pass  # implementation specific to the automated warehouse system

        print(f"putObjectAtPosition({row},{column})")
        self.real_device.putObjectAtPosition(row, column)

    def pickObjectFromConveyor(self):
        # Picks an object from the conveyor belt
        pass  # implementation specific to the automated warehouse system

        print(f"pickObjectFromConveyor()")
        self.real_device.pickObjectFromConveyor()

    def putObjectOnConveyor(self):
        # Puts an object on the conveyor belt
        pass  # implementation specific to the automated warehouse system

        print(f"putObjectOnConveyor()")
        self.real_device.putObjectOnConveyor()

    def rollObjectForward(self):
        # Rolls the object forward
        pass  # implementation specific to the automated warehouse system

        print(f"rollObjectForward()")
        self.real_device.rollObjectForward()

    def rollObjectBackward(self):
        # Rolls the object backward
        pass  # implementation specific to the automated warehouse system

        print(f"rollObjectBackward()")
        self.real_device.rollObjectBackward()

# Pick up the Pipe.
automatedWarehouse = AutomatedWarehouse()
automatedWarehouse.pickObjectAtPosition(1, 2)

simulator = automatedWarehouse.real_device
print(simulator.object_in_arm == 'Pipe')