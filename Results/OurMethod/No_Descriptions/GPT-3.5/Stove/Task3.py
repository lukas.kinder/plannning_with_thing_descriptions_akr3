class StoveSimulator:
    def __init__(self):
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def get_plate_1_power(self):
        return self.plate_1_power
    
    def get_plate_2_power(self):
        return self.plate_2_power
    
    def get_plate_1_level(self):
        return self.plate_1_level
    
    def get_plate_2_level(self):
        return self.plate_2_level

    def activate_plate_1(self):
        if not self.plate_1_power:
            self.plate_1_power = True

    def activate_plate_2(self):
        if not self.plate_2_power:
            self.plate_2_power = True

    def deactivate_plate_1(self):
        if self.plate_1_power:
            self.plate_1_power = False

    def deactivate_plate_2(self):
        if self.plate_2_power:
            self.plate_2_power = False

    def increase_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level < 9:
            self.plate_1_level += 1

    def increase_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level < 9:
            self.plate_2_level += 1

    def decrease_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level > 0:
            self.plate_1_level -= 1

    def decrease_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level > 0:
            self.plate_2_level -= 1


class Stove:
    def __init__(self):
        self.real_device = StoveSimulator()
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def activate_plate_1(self):
        self.plate_1_power = True

        print(f"activate_plate_1()")
        self.real_device.activate_plate_1()

    def activate_plate_2(self):
        self.plate_2_power = True

        print(f"activate_plate_2()")
        self.real_device.activate_plate_2()

    def deactivate_plate_1(self):
        self.plate_1_power = False

        print(f"deactivate_plate_1()")
        self.real_device.deactivate_plate_1()

    def deactivate_plate_2(self):
        self.plate_2_power = False

        print(f"deactivate_plate_2()")
        self.real_device.deactivate_plate_2()

    def increase_heat_plate_1(self):
        if self.plate_1_power:
            if self.plate_1_level < 9:
                self.plate_1_level += 1

        print(f"increase_heat_plate_1()")
        self.real_device.increase_heat_plate_1()

    def increase_heat_plate_2(self):
        if self.plate_2_power:
            if self.plate_2_level < 9:
                self.plate_2_level += 1

        print(f"increase_heat_plate_2()")
        self.real_device.increase_heat_plate_2()

    def decrease_heat_plate_1(self):
        if self.plate_1_power:
            if self.plate_1_level > 0:
                self.plate_1_level -= 1

        print(f"decrease_heat_plate_1()")
        self.real_device.decrease_heat_plate_1()

    def decrease_heat_plate_2(self):
        if self.plate_2_power:
            if self.plate_2_level > 0:
                self.plate_2_level -= 1

        print(f"decrease_heat_plate_2()")
        self.real_device.decrease_heat_plate_2()

# There is a pot of water on plate 1. Boil it as quick as possible.
stove = Stove()
stove.activate_plate_1()
while stove.plate_1_level < 9:
    stove.increase_heat_plate_1()

simulator = stove.real_device
print(simulator.plate_1_power and simulator.plate_1_level == 9)