class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def record(self):
        # Start recording
        # requires that the TV is on
        if self.off:
            self.turn_on()
        
        self.recording = True

        print(f"record()")
        self.real_device.record()

    def switch(self):
        # Switch to the next channel
        # requires that the TV is on
        if self.off:
            self.turn_on()

        channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
        current_index = channels.index(self.channel)
        next_index = (current_index + 1) % len(channels)
        self.channel = channels[next_index]
        
        print(f"switch()")
        self.real_device.switch()

    def turn_on(self):
        # Turn on the TV
        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turn off the TV
        self.off= True

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, new_brightness):
        # Set the brightness to the given value
        # :param integer new_brightness: The new brightness value
        if new_brightness >= 0 and new_brightness <= 100:
          self.brightness = new_brightness

        print(f"set_brightness({new_brightness})")
        self.real_device.set_brightness(new_brightness)

# Show me what runs on RTL. But increase the brightness please.
smartTV = SmartTV()
smartTV.switch()
smartTV.set_brightness(80)

simulator = smartTV.real_device
print(simulator.channel == 'RTL' and simulator.brightness > 70)