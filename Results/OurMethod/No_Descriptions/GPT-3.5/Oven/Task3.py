class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True


class Oven:
    def __init__(self):
        self.real_device = OvenSimulator()
        self.on = False
        self.heating_method = "convection"

    def switch_on(self):
        # switch on

        self.on = True

        print(f"switch_on()")
        self.real_device.switch_on()

    def switch_off(self):
        # switch off

        self.on = False

        print(f"switch_off()")
        self.real_device.switch_off()

    def change_heating_method(self,new_method):
        # change heating method
        # :param String new_method: the new heating method

        # requires that the device is off
        if self.on:
            self.switch_off()

        if new_method in ["convection","top and bottom"]:
            self.heating_method = new_method

        print(f"change_heating_method({new_method})")
        self.real_device.change_heating_method(new_method)

    def preheat(self):
        # preheat

        # requires that the device is off
        if self.on:
            self.switch_off()

        # Code to start preheating the oven here
            
        self.on = True

        print(f"preheat()")
        self.real_device.preheat()

# Set mode to 'top and bottom'.
oven = Oven()
oven.change_heating_method("top and bottom")

simulator = oven.real_device
print(simulator.heating_method == 'top and bottom')