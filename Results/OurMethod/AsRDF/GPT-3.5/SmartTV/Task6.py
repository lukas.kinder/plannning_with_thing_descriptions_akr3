class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False
        self.off = True # True if the TV is turned off
        self.brightness = 70 # Describes brightness of the TV screen in percent (minimum = 0, maximum = 100)
        self.channel = "ZDF" # States the currently selected channel
       
    def record(self):
        # Starts recording the current channel

        # requires that the TV is on
        if self.off:
            self.turn_on()

        self.recording = True

        print(f"record()")
        self.real_device.record()

    def switch(self,new_channel):
        # Switches to a different channel
        # :param string new_channel: The new channel to switch to

        # requires that the TV is on
        if self.off:
            self.turn_on()

        self.channel = new_channel

        print(f"switch({new_channel})")
        self.real_device.switch(new_channel)

    def turn_on(self):
        # Turns the TV on
        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the TV off
        self.off = True

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self,value):
        # Sets the brightness of the TV screen
        # :param integer value: The value of the brightness (0-100)

        # requires that the TV is on
        if self.off:
            self.turn_on()

        if value >= 0 and value <= 100:
            self.brightness = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# Lower the brightness to 20.
smartTV = SmartTV()
smartTV.set_brightness(20)

simulator = smartTV.real_device
print(simulator.brightness == 20)