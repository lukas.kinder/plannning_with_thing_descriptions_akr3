from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {'on': False, 'bri': 254, 'sat': 128}

    def turn_on(self):
        # Turns the Light on
        self.state['on'] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the Light off
        self.state['on'] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, brightness):
        # Sets the brightness
        # :param integer brightness: The brightness value to be set

        # requires that the device is on
        if not self.state['on']:
            self.turn_on()

        if 0 <= brightness <= 254:
            self.state['bri'] = brightness

        print(f"set_brightness({brightness})")
        self.real_device.set_brightness(brightness)

    def set_saturation(self, saturation):
        # Sets the saturation
        # :param integer saturation: The saturation value to be set

        # requires that the device is on
        if not self.state['on']:
            self.turn_on()

        if 0 <= saturation <= 254:
            self.state['sat'] = saturation

        print(f"set_saturation({saturation})")
        self.real_device.set_saturation(saturation)

# Give me some light with maximum saturation.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.set_saturation(254)

simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['sat'] == 254)