import random

class SecurityCameraSimulator:
    def __init__(self):
        self.recording = False
        self.storage = 256

        self.statistic = {"recorded_videos" : 0}

    def get_recording(self):
        return self.recording
    
    def get_storage(self):
        return self.storage

    def record(self):
        if not self.recording and self.storage > 0:
            self.recording = True

    def stop(self):
        if self.recording:
            self.recording = False

            recording_size = random.randint(0,3)
            self.storage -= recording_size

            self.statistic['recorded_videos'] += 1

    def delete(self):
        if not self.recording:
            self.storage = 256


class SecurityCamera:
    def __init__(self):
        self.real_device = SecurityCameraSimulator()
        self.recording = False  # Boolean value indicating if the camera is currently recording
        self.storage = 256  # States the current remaining storage in gigabytes (minimum = 0, maximum = 256)

    def record(self):
        # Starts the recording
        self.recording = True

        print(f"record()")
        self.real_device.record()

    def stop(self):
        # Stops the recording
        self.recording = False

        print(f"stop()")
        self.real_device.stop()

    def delete(self):
        # Deletes video files freeing storage
        # Deleting video files does not require specific conditions
        pass  # Placeholder for the function logic, if any requirements were needed they would be handled here

        print(f"delete()")
        self.real_device.delete()

# Start recording
securityCamera = SecurityCamera()
securityCamera.record()

simulator = securityCamera.real_device
print(simulator.recording)