import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True  # States if the device is set on automatic or manual food dispension
        self.stored = False  # Indicates whether enough food is available for the next dispension process

    def dispense(self):
        # Manually dispenses food for the animal

        # requires that the device is set on automatic
        if not self.automatic:
            self.set_state("automatic")

        # requires that enough food is available
        if not self.stored:
            self.refill()

        # Code to dispense food here

        print(f"dispense()")
        self.real_device.dispense()

    def refill(self):
        # refills the stored food
        self.stored = True

        print(f"refill()")
        self.real_device.refill()

    def set_state(self, state):
        # Sets the feeding bowl to automatic or manual

        # :param string state: Set state to either 'manual' or 'automatic'

        if state == "automatic" or state == "manual":
            self.automatic = (state == "automatic")

        print(f"set_state({state})")
        self.real_device.set_state(state)

# Let the device output food 130 times. Then refill and set to automatic.
foodDispenser = FoodDispenser()
for i in range(130):
    foodDispenser.dispense()
foodDispenser.refill()
foodDispenser.set_state("automatic")

simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 130 and simulator.food_storage == 150 and simulator.automatic)