class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):

        self.real_device = KettleSimulator()
        self.power = False # Indicates wether the kettle is turned on or not
        self.filled = False # Indicates whether the kettle is filled with water or not

    def switch_power(self):
        # turns the kettle on or off
        self.power = not self.power

        print(f"switch_power()")
        self.real_device.switch_power()

    def heat(self):
        # starts the heating process of the kettle
        pass

        print(f"heat()")
        self.real_device.heat()

    def fill(self):
        # fills the kettle with water
        pass

        print(f"fill()")
        self.real_device.fill()

    def empty(self):
        # empties the kettle with water
        pass










        print(f"empty()")
        self.real_device.empty()

# Start heating up some water.
kettle = Kettle()
kettle.heat()













simulator = kettle.real_device
print(simulator.contain_hot_water)