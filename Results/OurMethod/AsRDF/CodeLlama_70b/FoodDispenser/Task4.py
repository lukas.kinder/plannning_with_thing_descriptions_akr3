# Was not able to entangle with real device! (Are still unconnected)
import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True
class FoodDispenser:
    def __init__(self):

        self.automatic = True
        self.stored = False

    def dispense(self):
        # Manually dispenses food for the animal
        pass

    def replenish(self):
        # Refills the stored food
        pass

    def set_state(self,state):
        # Sets the feeding bowl to either 'automatic' or 'manual'
        # :param string state: the new state of the device
        if state == "automatic":
            self.automatic = True
        elif state == "manual":
            self.automatic = False
        else:
            raise ValueError("Invalid state")

        self.stored = False
# Dispense 10 potions of food times.
foodDispenser = FoodDispenser()
for i in range(10):
    foodDispenser.dispense()


simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 10)