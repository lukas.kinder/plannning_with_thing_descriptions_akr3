class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration


class Alarm:
    def __init__(self):
        self.real_device = AlarmSimulator()
        self.set = False # indicates whether the alarm is set or not
        self.vibration = False # States whether the alarm is set to vibration only mode

    def set_alarm(self,time):
        # Sets the alarm
        # :param datetime time: the time the alarm should go off at
        # requires that the device is on
        if not self.set:
            self.set = True
        self.time = time

        print(f"set_alarm({time})")
        self.real_device.set_alarm(time)

    def unset(self):
        # Unsets the alarm
        # requires that the device is on
        if self.set:
            self.set = False

        print(f"unset()")
        self.real_device.unset()

    def toggle_vibration_mode(self):
        # Toggles vibration only mode
        # requires that the device is on
        if not self.vibration:
            self.vibration = True
        else:
            self.vibration = False

        print(f"toggle_vibration_mode()")
        self.real_device.toggle_vibration_mode()

# I want you to set an alarm with vibration.
alarm = Alarm()
alarm.set_alarm("7:00 AM")
alarm.toggle_vibration_mode()


simulator = alarm.real_device
print(simulator.set and simulator.vibration)