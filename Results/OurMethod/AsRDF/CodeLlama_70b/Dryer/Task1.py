import random

class DryerSimulator:
    def __init__(self):
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def get_running(self):
        return self.running
    
    def get_temperature(self):
        return self.temperature
    
    def get_distilled_storage(self):
        return self.distilled_storage

    def start(self):
        if not self.running and self.distilled_storage > 0:
            self.running = True
            self.distilled_storage -= random.randint(10,30)

    def clear(self):
        self.distilled_storage = 100

    def increase_temperature(self):
        if not self.running and self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        if not self.running and self.temperature > 40:
            self.temperature -= 10


class Dryer:
    def __init__(self):

        self.real_device = DryerSimulator()
        self.running = False

        self.temperature = 50 # The current drying temperature

        self.distilled_storage = 50 # The current distilled water storage

    def start(self):
        # Starts the drying process
        self.running = True

        print(f"start()")
        self.real_device.start()

    def clear(self):
        # Clears the distilled water storage
        self.distilled_storage = 0

        print(f"clear()")
        self.real_device.clear()

    def increase_temperature(self):
        # Increases the drying temperature by 10
        self.temperature += 10

        print(f"increase_temperature()")
        self.real_device.increase_temperature()

    def decrease_temperature(self):
        # Decreases the drying temperature by 10
        self.temperature -= 10




        print(f"decrease_temperature()")
        self.real_device.decrease_temperature()

# Start the dryer.
dryer = Dryer()
dryer.start()


simulator = dryer.real_device
print(simulator.running)