import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100


class VacuumRobot:
    def __init__(self):
        self.real_device = VacuumRobotSimulator()
        self.cleaning = False # States if the robot is currently cleaning or not

        self.storage_full = True # Describes the current power charge in percent

        self.battery = 30 # The current battery level

        self.mapped = True # States whether a current mapping of the room is available, needed in order to start the vacuuming

        self.at_base = True # States whether the robot is currently docked at the charging base station


    def start(self):
        # Starts vacuuming the room, only valid if a mapping for the room and sufficient storage and battery are available
        self.cleaning = True

        print(f"start()")
        self.real_device.start()

    def scan(self):
        # Scans the room, creating a mapping
        self.mapped = False

        print(f"scan()")
        self.real_device.scan()

    def empty_storage(self):
        # Empties storage
        self.storage_full = False

        print(f"empty_storage()")
        self.real_device.empty_storage()

    def return_to_base(self):
        # Returns to the base, enables further actions
        self.at_base = False

        print(f"return_to_base()")
        self.real_device.return_to_base()

    def charge(self):
        # Charges the robot
        self.battery = 0



        print(f"charge()")
        self.real_device.charge()

# Start cleaning.
vacuumRobot = VacuumRobot()
vacuumRobot.start()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left in tank
        self.speed = 0 # Current speed of vehicle (in km/h)
        self.direction = "forward" # Direction of travel (either "forward", "backward", or "stop")

    def moveForward(self):
        # Moves the car forward
        self.speed += 1

    def moveBackward(self):
        # Moves the car backward
        self.speed -= 1

    def stop(self):
        # Stops the car
        self.speed = 0

# COMMENTED OUT - (SYNTAX ERROR):  Write code that fulfills the task: "Drive for 30 minutes at a speed of 50 km/h.".






simulator = vacuumRobot.real_device
print(simulator.cleaning)