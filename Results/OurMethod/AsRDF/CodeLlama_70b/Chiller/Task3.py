class ChillerSimulator:
    def __init__(self):
        self.type = "" #Type of the chiller
        self.firmware = "v1.0" #Firmware version
        self.model = "" #Chiller model
        self.location = "" #Location of the chiller
        self.latitude = 0 #Latitude of the chiller
        self.longitude = 0 #Longitude of the chiller
        self.location_tag = "" #Location tag
        self.floor_tag = "" #floor level
        self.campus_tag = "" #name of the campus
        self.online = False #online status
        self.temperature = 20 #temperature
        self.temperature_unit = "C" #C for Celsius, F for Fahrenheit
        self.humidity = 30 #humidity of the chiller
        self.humidity_unit = "percentage" #percentage or grams per m3
        self.pressure = 1.5 #pressure
        self.pressure_unit = "bar" #psi or pascal or bar
        self.ora_latitude = 0 #ora_latitude
        self.ora_longitude = 0 #ora_longitude
        self.ora_altitude = 0 #ora_altitude
        self.ora_uncertainty = 0 #ora_accuracy
        self.ora_zone = "" #ora_zone
        self.ora_txPower = 0 #ora_mssi
        self.ora_rssi = 0 #ora_rssi

        self.n_rebooted  = 0

    # Getter functions
    def get_type(self):
        return self.type

    def get_firmware(self):
        return self.firmware

    def get_model(self):
        return self.model

    def get_location(self):
        return self.location

    def get_latitude(self):
        return self.latitude

    def get_longitude(self):
        return self.longitude

    def get_location_tag(self):
        return self.location_tag

    def get_floor_tag(self):
        return self.floor_tag

    def get_campus_tag(self):
        return self.campus_tag

    def get_online(self):
        return self.online

    def get_temperature(self):
        return self.temperature

    def get_temperature_unit(self):
        return self.temperature_unit

    def get_humidity(self):
        return self.humidity

    def get_humidity_unit(self):
        return self.humidity_unit

    def get_pressure(self):
        return self.pressure

    def get_pressure_unit(self):
        return self.pressure_unit

    def get_ora_latitude(self):
        return self.ora_latitude

    def get_ora_longitude(self):
        return self.ora_longitude

    def get_ora_altitude(self):
        return self.ora_altitude

    def get_ora_uncertainty(self):
        return self.ora_uncertainty

    def get_ora_zone(self):
        return self.ora_zone

    def get_ora_txPower(self):
        return self.ora_txPower

    def get_ora_rssi(self):
        return self.ora_rssi
    
    #actions
    def reboot(self):
        self.n_rebooted +=1

    def firmwareUpdate(self, version):
        self.firmware = version

    def emergencyValveRelease(self):
        self.pressure = 1

    def increasePressure(self, value):
        if value >= 0 and value <=5:
            self.pressure += value


class Chiller:
    def __init__(self):
        self.real_device = ChillerSimulator()
        self.type = "" # Type of the device
        self.firmware = "v1.0" # Firmware version
        self.model = "" # Model of the device
        self.location = "" # Location of the device
        self.latitude = 0 # Latitude of the device
        self.longitude = 0 # Longitude of the device
        self.location_tag = "" # Location tag of the device
        self.floor_tag = "" # Floor tag of the device
        self.campus_tag = "" # Campus tag of the device
        self.online = False # Online status of the device
        self.temperature = 20 # Temperature of the device
        self.temperature_unit = "C" # Temperature unit of the device
        self.humidity = 30 # Humidity of the device
        self.humidity_unit = "percentage" # Humidity unit of the device
        self.pressure = 1.5 # Pressure of the device
        self.pressure_unit = "bar" # Pressure unit of the device
        self.ora_latitude = 0 # Latitude of the device
        self.ora_longitude = 0 # Longitude of the device
        self.ora_altitude = 0 # Altitude of the device
        self.ora_uncertainty = 0 # Uncertainty of the device
        self.ora_zone = "" # Zone of the device
        self.ora_txPower = 0 # Transmit power of the device
        self.ora_rssi = 0 # Receive power of the device

    def reboot(self):
        # Reboots the device
        pass

        print(f"reboot()")
        self.real_device.reboot()

    def firmwareUpdate(self, new_firmware):

        # Updates the firmware of the device
        pass

        print(f"firmwareUpdate({new_firmware})")
        self.real_device.firmwareUpdate(new_firmware)

    def emergencyValveRelease(self):

        # Releases the emergency valve of the device
        pass

        print(f"emergencyValveRelease()")
        self.real_device.emergencyValveRelease()

    def increasePressure(self):

        # Increases the pressure of the device
        pass




        print(f"increasePressure()")
        self.real_device.increasePressure()

# Increase pressure by 3.
chiller = Chiller()
chiller.increasePressure()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class SmartPhone:
    def __init__(self):
        self.screen_on = False # Flag indicating whether the screen is on or off
        self.battery = 0 # Battery level (0-100)
        self.storage = 0 # Storage level (0-100)
        self.signal = 0 # Signal strength (0-100)

    def turn_screen_on(self):
        # Turns the screen on
        self.screen_on = True

    def turn_screen_off(self):
        # Turns the screen off
        self.screen_on = False

    def increase_battery(self):
        # Increases the battery level
        self.battery += 10

        if self.battery >= 100:
            self.battery = 100

    def decrease_battery(self):
        # Decreases the battery level
        self.battery -= 10

        if self.battery <= 0:
            self.battery = 0

    def store_data(self, data):
        # Stores some data
        self.storage += data

    def retrieve_data(self):
        # Retrieves some data
        return self.storage

    def update_signal(self, signal):
        # Updates the signal strength
        self.signal += signal

        if self.signal >= 100:
            self.signal = 100

    def restart(self):
        # Restarts the phone
        pass

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "I want to increase my battery level by 10, then store some data, and then turn off the screen.".










































































































































































































































































































































































































































































































































































































simulator = chiller.real_device
print(round(simulator.pressure,3) == 4.5)