import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):

        self.real_device = StreamingdroneSimulator()
        self.recording = False # indicates whether the drone is currently recording or not

        self.streaming = False # Indicates whether the drone is currently live streaming or not

        self.battery = 50 # represents the current battery level in percentage


    def stream(self):
        # Starts the live streaming process
        self.streaming = True


        print(f"stream()")
        self.real_device.stream()

    def stop_stream(self):
        # Stops the live streaming process
        self.streaming = False

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def start_recording(self):
        # Starts the recording process
        self.recording = True

        print(f"start_recording()")
        self.real_device.start_recording()

    def end(self):
        # Ends the recording process, deleting any unsaved recordings
        self.recording = False

        print(f"end()")
        self.real_device.end()

    def recharge(self):
        # Recharges the battery, can not be recharged during streaming or recording
        self.battery = 0








        print(f"recharge()")
        self.real_device.recharge()

# Recharge the drone.
streamingdrone = Streamingdrone()
streamingdrone.recharge()









# COMMENTED OUT - (SYNTAX ERROR): consider the following python class:


class WeatherBot:
    def __init__(self):

        self.forecast = None # The current weather forecast

        self.temperature = None # The current temperature


    def get_Forecast(self):
        # Gets the current weather forecast
        return self.forecast

    def get_Temperature(self):
        # Gets the current temperature
        return self.temperature


# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Get the current weather forecast.".





simulator = streamingdrone.real_device
print(simulator.battery == 100)