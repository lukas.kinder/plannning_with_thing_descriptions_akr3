class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration


class Alarm:
    def __init__(self):
        self.real_device = AlarmSimulator()
        self.set = False # indicates whether alarm is currently set or not
        self.vibration = False # States whether the alarm is set to vibration only mode

    def set_alarm(self):
        # Sets alarm, ringing or vibration at the set time
        self.set = True

        print(f"set_alarm()")
        self.real_device.set_alarm()

    def unset(self):
        # Unsets alarm
        self.set = False

        print(f"unset()")
        self.real_device.unset()

    def toggle_vibration_mode(self):
        # Switches vibration only mode on or off depending on the current setting.
        self.vibration = not self.vibration


        print(f"toggle_vibration_mode()")
        self.real_device.toggle_vibration_mode()

# Turn on the alarm.
alarm = Alarm()
alarm.set_alarm()


simulator = alarm.real_device
print(simulator.set)