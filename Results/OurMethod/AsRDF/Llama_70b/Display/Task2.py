from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {"status": False} # Flag indicating whether the display is turned on or off
        self.content = "Video" # Content of the display (choices are Video, Image or Text)

    def toggle(self):
        # Toggle power status
        self.power["status"] = not self.power["status"]

        print(f"toggle()")
        self.real_device.toggle()

    def setVolume(self, volume):
        # Set the volume level
        pass # This function does nothing. It is just a placeholder.

        print(f"setVolume({volume})")
        self.real_device.setVolume(volume)

    def setBright(self, brightness):
        # Set the brightness level
        pass # This function does nothing. It is just a placeholder.

        print(f"setBright({brightness})")
        self.real_device.setBright(brightness)

    def showText(self, text):
        # Show text on the display
        pass # This function does nothing. It is just a placeholder.

        print(f"showText({text})")
        self.real_device.showText(text)

    def playVideo(self, video_url, name=None, identifier=None, description=None):
        # Play a video on the display
        pass # This function does nothing. It is just a placeholder.

        print(f"playVideo({video_url},{name=None},{identifier=None},{description=None})")
        self.real_device.playVideo(video_url, name=None, identifier=None, description=None)

    def pauseVideo(self):
        # Pauses the currently playing video
        pass # This function does nothing. It is just a placeholder.

        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def stopVideo(self):
        # Stops the currently playing video
        pass # This function does nothing. It is just a placeholder.

        print(f"stopVideo()")
        self.real_device.stopVideo()

    def presentationWebApp(self, url, name=None, identifier=None, description=None):
        # Presents a web application on the display
        pass # This function does nothing. It is just a placeholder.

        print(f"presentationWebApp({url},{name=None},{identifier=None},{description=None})")
        self.real_device.presentationWebApp(url, name=None, identifier=None, description=None)

    def launchNewsApp(self):
        # Launches the news app on the display
        pass # This function does nothing. It is just a placeholder.



        print(f"launchNewsApp()")
        self.real_device.launchNewsApp()

# Set volume to 33.
display = Display()
while display.content != "Video":
    display.presentationWebApp("http://www.youtube.com")
display.setVolume(33)


simulator = display.real_device
print(simulator.volume ==33)