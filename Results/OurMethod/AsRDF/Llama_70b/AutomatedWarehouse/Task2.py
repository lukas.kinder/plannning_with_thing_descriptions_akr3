# Was not able to entangle with real device! (Are still unconnected)
from copy import deepcopy
class ObjectOverflow(Exception):
    pass

class AutomatedWarehouseSimulator:
    def __init__(self):
        self.currentProductPosition = {
            "row1col1" : "Wood",
            "row1col2" : "Pipe",
            "row2col2" : "PlasticContainer",
            "row3col1" : "WaterBottles",
            "row3col2" : "Trash",
            "row3col3" : "Cables"
        }

        self.object_inside = None  # wether there is an object inside on the conveyor belt
        self.object_in_arm = None # wether the robotic arm is holding something
        self.arm_is_home = True # if the arm is at the 'home position'
        self.object_waiting_to_get_inside = ["Glass", "FuelContainer"]
        self.objects_moved_out = []

    def get_currentProductPosition(self):
        return deepcopy( self.currentProductPosition )

    def goHome(self):
        self.arm_is_home  = True

    def pickObjectAtPosition(self, row, column):
        # Pick object at a specified warehouse position
        self.arm_is_home  = False

        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            if self.object_in_arm != None:
                raise ObjectOverflow
            self.object_in_arm = self.currentProductPosition[position]
            del self.currentProductPosition[position]

    def putObjectAtPosition(self, row, column):
        # Put object at a specified warehouse position
        self.arm_is_home  = False
        if self.object_in_arm == None:
            #nothing to put
            return
        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            raise ObjectOverflow
        self.currentProductPosition[position] = self.object_in_arm
        self.object_in_arm = None
        

    def pickObjectFromConveyor(self):
        self.arm_is_home  = False
        # Pick object from a conveyor belt

        if self.object_in_arm != None:
            raise ObjectOverflow

        if self.object_inside != None:
            self.object_in_arm = self.object_inside
            self.object_inside = None

    def putObjectOnConveyor(self):
        self.arm_is_home  = False
        if self.object_in_arm != None:
            # An object is put on the conveyor belt
            if self.object_inside:
                #  There is already an object inside. 
                raise ObjectOverflow
            self.object_inside = self.object_in_arm
            self.object_in_arm = None

    def rollObjectForward(self):
        # An object is moved inside using the conveyor belt.
        if self.object_inside:
            #  There is already an object inside. 
            raise ObjectOverflow
        
        if len(self.object_waiting_to_get_inside) != 0:
            self.object_inside = self.object_waiting_to_get_inside.pop()

    def rollObjectBackward(self):
        if self.object_inside:
            self.objects_moved_out.append(self.object_inside)
            self.object_inside = None
class AutomatedWarehouse:
    def __init__(self):
        self.current_product_position = {'row1col1': 'Wood', 'row1col2': 'Pipe', 'row2col2': 'PlasticContainer', 'row3col1': 'WaterBottles', 'row3col2': 'Trash', 'row3col3': 'Cables'}

    def go_home(self):
        # Sends the arm home
        pass

    def pick_object_at_position(self, row, column):
        # Pick object at a specified warehouse position
        # :param string row: Row number
        # :param string column: Column number
        pass

    def put_object_at_position(self, row, column):
        # Put object at a specified warehouse position
        # :param string row: Row number
        # :param string column: Column number
        pass

    def pick_object_from_conveyor(self):
        # Pick object from a conveyor belt
        pass

    def put_object_on_conveyor(self):
        # Put object on a conveyor belt
        pass

    def roll_object_forward(self):
        # Start the conveyor until object reaches inside sensors
        pass

    def roll_object_backward(self):
        # Start the conveyor until object reaches outside sensors
        pass

# Put the Bottles on the conveyor belt.
automatedWarehouse = AutomatedWarehouse()
automatedWarehouse.put_object_on_conveyor()


simulator = automatedWarehouse.real_device
print(simulator.object_inside == 'WaterBottles')