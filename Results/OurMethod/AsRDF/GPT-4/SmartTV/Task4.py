class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False # True if the TV is currently recording the selected channel
        self.off = True # Indicates if the TV is turned off(True) or turned on (False)
        self.brightness = 70 # Describes brightness of the TV screen in percent
        self.channel = "ZDF" # States the currently selected channel that will be displayed if the device is turned on.

    def record(self):
        # Starts recording the current channel

        # requires that the device is on
        if self.off:
            self.turn_on()

        self.recording = True

        print(f"record()")
        self.real_device.record()

    def switch(self,channel):
        # Switches to a different channel

        # requires that the device is on
        if self.off:
            self.turn_on()

        channels = ["ARD","KIKA","PRO7","RTL","SAT1","ZDF"]
        if channel in channels:
            self.channel = channel

        print(f"switch({channel})")
        self.real_device.switch(channel)

    def turn_on(self):
        # Turns the TV on
        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the TV off
        self.off = True
        self.recording = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self,value):
        # Sets the brightness of the tv.
        # :param integer value: The brightness in percent
        
        # requires that the device is on
        if self.off:
            self.turn_on()
        
        if value >= 0 and value <= 100:
            self.brightness = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# There is something interesting on ARD right now. Please record it for me.
smartTV = SmartTV()
smartTV.switch("ARD")
smartTV.record()

simulator = smartTV.real_device
print(simulator.channel == 'ARD' and simulator.recording)