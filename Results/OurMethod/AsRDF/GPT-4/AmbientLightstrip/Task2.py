from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128} # state includes if the device is on or off, the Brightness and the Saturation of the light

    def turn_on(self):
        # Turns the Light on
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the Light off
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, value):
        # Sets the brightness
        # :param integer value: the value the brightness should be

        # requires that the device is on
        if not self.state["on"]:
            self.turn_on()

        # brightness value should be within the permissible range
        if value >= 0 and value <= 254:
            self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def set_saturation(self, value):
        # Sets the saturation
        # :param integer value: the value the saturation should be

        # requires that the device is on
        if not self.state["on"]:
            self.turn_on()

        # saturation value should be within the permissible range
        if value >= 0 and value <= 254:
            self.state["sat"] = value

        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# I want some light, but not bright.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.turn_on()
ambientLightstrip.set_brightness(100)

simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['bri'] <= 128)