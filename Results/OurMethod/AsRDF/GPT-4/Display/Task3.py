from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {"status": False}
        self.content = "Video" # availableContent

    def toggle(self):
        # togglePowerStatus

        self.power["status"] = not self.power["status"]

        print(f"toggle()")
        self.real_device.toggle()

    def setVolume(self, volume):
        # setVolume
        # :param integer volume: the volume level to be set

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to set volume would go here

        print(f"setVolume({volume})")
        self.real_device.setVolume(volume)

    def setBright(self, brightness):
        # setBright
        # :param integer brightness: the brightness level to be set

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to set brightness would go here

        print(f"setBright({brightness})")
        self.real_device.setBright(brightness)

    def showText(self, text):
        # showText
        # :param string text: the text to be displayed

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to display text would go here

        print(f"showText({text})")
        self.real_device.showText(text)

    def playVideo(self, url, identifier, name, description):
        # playVideo
        # :param string url: URL of the video
        # :param string identifier: Identifier of the video
        # :param string name: Name of the video
        # :param string description: Description of the video

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to play video would go here

        print(f"playVideo({url},{identifier},{name},{description})")
        self.real_device.playVideo(url, identifier, name, description)

    def pauseVideo(self):
        # pauseVideo

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to pause video would go here

        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def stopVideo(self):
        # stopVideo

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to stop video would go here

        print(f"stopVideo()")
        self.real_device.stopVideo()

    def presentationWebApp(self, url, identifier, name, description):
        # presentationWebApp
        # :param string url: URL of the WebApp
        # :param string identifier: Identifier of the WebApp
        # :param string name: Name of the WebApp
        # :param string description: Description of the WebApp

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to present web app would go here

        print(f"presentationWebApp({url},{identifier},{name},{description})")
        self.real_device.presentationWebApp(url, identifier, name, description)

    def launchNewsApp(self):
        # launchNewsApp

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()
        
        # Code to Launch news app would go here.

        print(f"launchNewsApp()")
        self.real_device.launchNewsApp()

# Display the text 'Hello World' with a brightness of 90.
display = Display()
display.setBright(90)
display.showText("Hello World")

simulator = display.real_device
print(simulator.power['status'] and simulator.content == 'Text' and simulator.text_buffer == 'Hello World' and simulator.brightness == 90)