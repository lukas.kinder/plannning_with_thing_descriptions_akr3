from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {'status': False} # Power status of the display
        self.content = 'Video' # Available content on the display

    def toggle(self):
        # Toggles power status of the device
        self.power['status'] = not self.power['status']

        print(f"toggle()")
        self.real_device.toggle()

    def setVolume(self, volume):
        # Sets the volume of the device
        pass

        print(f"setVolume({volume})")
        self.real_device.setVolume(volume)

    def setBright(self, brightness):
        # Sets the brightness of the display
        pass

        print(f"setBright({brightness})")
        self.real_device.setBright(brightness)

    def showText(self, text):
        # Shows text on the screen
        pass

        print(f"showText({text})")
        self.real_device.showText(text)

    def playVideo(self, video):
        # Plays a video
        pass

        print(f"playVideo({video})")
        self.real_device.playVideo(video)

    def pauseVideo(self):
        # Pauses the current video
        pass

        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def stopVideo(self):
        # Stops the current video
        pass

        print(f"stopVideo()")
        self.real_device.stopVideo()

    def presentationWebApp(self, url, identifier, name, description):
        # Launches a presentation web app
        pass

        print(f"presentationWebApp({url},{identifier},{name},{description})")
        self.real_device.presentationWebApp(url, identifier, name, description)

    def launchNewsApp(self, url, identifier, name, description):
        # Launches a news app
        pass


        print(f"launchNewsApp({url},{identifier},{name},{description})")
        self.real_device.launchNewsApp(url, identifier, name, description)

# Show me a video with url 'https://www.youtube.com/watch?v=UIySlKLIVBo'. Use  name: 'chess', description: 'some game' and identifier: 'NA'.
display = Display()
display.playVideo('https://www.youtube.com/watch?v=UIySlKLIVBo')
display.presentationWebApp('https://www.youtube.com/watch?v=UIySlKLIVBo','NA','chess','some game')


simulator = display.real_device
print(simulator.content == 'Video' and simulator.video_details['identifier'] == 'NA' and simulator.video_details['name'] == 'chess' and simulator.video_details['description'] == 'some game'and simulator.video_details['url'] == 'https://www.youtube.com/watch?v=UIySlKLIVBo')