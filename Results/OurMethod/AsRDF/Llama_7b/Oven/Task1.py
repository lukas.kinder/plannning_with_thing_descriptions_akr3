class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True


class Oven:
    def __init__(self):

        self.real_device = OvenSimulator()
        self.on = False
        self.heating_method = "convection" # the current heating method


    def switch_on(self):
        # turns the oven on
        self.on = True

        print(f"switch_on()")
        self.real_device.switch_on()

    def switch_off(self):
        # turns the oven off
        self.on = False

        print(f"switch_off()")
        self.real_device.switch_off()

    def change_heating_method(self,new_heating_method):
        # changes the currently selected heating method
        # :param string new_heating_method: the new heating method to select
        # requires that the oven is on
        if self.on:
            if new_heating_method == "top and bottom":
                self.heating_method = "top and bottom"
            elif new_heating_method == "bottom":
                self.heating_method = "bottom"
            else:
                self.heating_method = "convection"
            self.switch_off()

        print(f"change_heating_method({new_heating_method})")
        self.real_device.change_heating_method(new_heating_method)

    def preheat(self):
        # starts preheating the oven to 180 degrees celsius
        # requires that the oven is on
        if self.on:
            self.heating_method = "preheat"
            self.switch_off()






        print(f"preheat()")
        self.real_device.preheat()

# Turn the oven on.
oven = Oven()
oven.switch_on()


simulator = oven.real_device
print(simulator.on)