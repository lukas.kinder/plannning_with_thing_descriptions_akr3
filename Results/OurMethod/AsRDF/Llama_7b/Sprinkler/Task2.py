# Was not able to entangle with real device! (Are still unconnected)
class SprinklerSimulator:
    def __init__(self):
        self.water = True
        self.on = False

        self.stored_water = 4 # initial stored water. Can store up to 12

        self.statistic = {"n_sprinkle" : 0}

    def get_water(self):
        return self.water
    
    def get_on(self):
        return self.on

    def power_on(self):
        if not self.on:
            self.on = True

    def power_off(self):
        if self.on:
            self.on = False

    def sprinkle(self):
        if self.on and self.water:
            self.stored_water -=1

            if self.stored_water == 0:
                self.water = False

            self.statistic['n_sprinkle'] +=1
  
    def refill(self):
        self.water = True
        self.stored_water = 12
class Sprinkler:
    def __init__(self):

        self.water = True # States whether the water tank is full or empty

        self.on = False # Describes whether the device is turned on (if true) or off (if false)


    def power_on(self):
        # Turns the power of the sprinkler on
        self.on = True

    def power_off(self):
        # Turns the power of the sprinkler off
        self.on = False

    def sprinkle(self):
        # Initiates the watering process wetting all surrounding plants
        pass

    def refilled(self):
        # Refills the water tank
        pass



# Start watering the garden.
sprinkler = Sprinkler()
sprinkler.power_on()
sprinkler.sprinkle()


simulator = sprinkler.real_device
print(simulator.statistic['n_sprinkle'] == 1)