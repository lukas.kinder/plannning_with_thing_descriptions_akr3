import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False # Indicates whether the drone is currently recording or not
        self.streaming = False # Indicates whether the drone is currently live streaming or not
        self.battery = 50 # Represents the current battery in percentage

    def stream(self):
        # Starts streaming
        self.streaming = True

        print(f"stream()")
        self.real_device.stream()

    def stop_stream(self):
        # Stops streaming
        self.streaming = False

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def start_recording(self):
        # Starts the recording
        self.recording = True

        print(f"start_recording()")
        self.real_device.start_recording()

    def end(self):
        # Ends the recording
        self.recording = False

        print(f"end()")
        self.real_device.end()

    def recharge(self):
        # Recharges the battery, can not be recharged during streaming or recording
        pass








        print(f"recharge()")
        self.real_device.recharge()

# Recharge the drone.
streamingdrone = Streamingdrone()
streamingdrone.recharge()













# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel in the tank
        self.speed = 0 # Current speed of the car

    def drive(self, distance):
        # Drives the car for the given distance
        # requires that the car has fuel
        if self.fuel < distance:
            self.fuel += distance

        self.speed -= distance / 10

    def fill_up(self, fuel):
        # Fills up the car's fuel tank
        # :param float fuel: The amount of fuel to fill up the tank with

        # requires that the car has enough space for the fuel
        if self.fuel + fuel > self.capacity:
            self.fuel += fuel


# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive for 20 miles and then fill up the gas tank with 10 gallons of fuel.".

















# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Robot:
    def __init__(self):
        self.arm = False # Flag indicating whether the arm is extended or retracted
        self.leg = False # Flag indicating whether the leg is extended or retracted
        self.power = False # Flag indicating whether the robot is powered on or off
        self.speed = 0 # Speed of the robot

    def extend_arm(self):
        # Extends the arm
        self.arm = True

    def extend_leg(self):
        # Extends the leg
        self.leg = True

    def turn_on(self):
        # Turns on the robot
        self.power = True

    def turn_off(self):
        # Turns off the robot
        self.power = False


    def move(self, distance):
        # Moves the robot the given distance
        self.speed += distance

        if self.speed > 10:
            self.speed = 10


# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Move forward 5 meters and extend the arm.".



















# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Computer:
    def __init__(self):
        self.screen = False # Indicates whether the screen is on or off
        self.cpu = False # Indicates whether the CPU is on or off
        self.ram = False # Indicates whether the RAM is on or off
        self.harddrive = False # Indicates whether the hard drive is on or off

    def turn_on(self):
        # Turns on the computer
        self.screen = True
        self.cpu = True
        self.ram = True
        self.harddrive = True

    def turn_off(self):
        # Turns off the computer
        self.screen = False
        self.cpu = False
        self.ram = False
        self.harddrive = False

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Turn on the computer and open the web browser.".


















# COMMENTED OUT - (SYNTAX ERROR): consider the following python class:


class Light:
    def __init__(self):
        self.brightness = 0 # Brightness of the light

    def increase_brightness(self, amount):
        # Increases the brightness of the light
        self.brightness += amount

    def decrease_brightness(self, amount):
        # Decreases the brightness of the light
        self.brightness -= amount

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Increase the brightness of the light by 50%.".


















# COMMENTED OUT - (SYNTAX ERROR): consider the following python class:


class Door:
    def __init__(self):
        self.locked = False # Flag indicating whether the door is locked or unlocked

    def lock(self):
        # Locks the door
        self.locked = True

    def unlock(self):
        # Unlocks the door
        self.locked = False










simulator = streamingdrone.real_device
print(simulator.battery == 100)