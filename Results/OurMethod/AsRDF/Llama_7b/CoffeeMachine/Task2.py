# Was not able to entangle with real device! (Are still unconnected)
import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False

class CoffeeMachine:
    def __init__(self):

        self.water = True # Illustrates water level of the machine. If false the machine does not contain enough water to make a coffIee and needs to be refilled
        self.storage = True # Indicates the remaining coffee ground storage. If false no space is remaining and the container needs to be emptied
        self.on = False # Displays the current power state of the machine

    def toggle(self):
        # Toggles the on property of the device
        self.on = not self.on

    def reFill(self):
        # Refills the water tank of the machine
        self.water = True

    def empty(self):
        # Empties the coffee ground container
        self.storage = False

    def make_coffee(self):
        # Creates a medium cup of coffee
        pass







# Make me a coffee. Make sure to turn the machine off afterwards.
coffeeMachine = CoffeeMachine()
coffeeMachine.make_coffee()
coffeeMachine.toggle()


simulator = coffeeMachine.real_device
print(simulator.statistic['coffee_made'] == 1 and not simulator.on)