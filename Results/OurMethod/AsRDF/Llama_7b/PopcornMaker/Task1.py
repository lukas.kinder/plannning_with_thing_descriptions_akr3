class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0


class PopcornMaker:
    def __init__(self):
        self.real_device = PopcornMakerSimulator()
        self.power = False

    def toggle(self):
        # Toggles the power of the device
        self.power = not self.power

        print(f"toggle()")
        self.real_device.toggle()

    def add_corn(self):
        # Adds corn to the heating section of the maker
        pass

        print(f"add_corn()")
        self.real_device.add_corn()

    def add_sugar(self):
        # Adds sugar to the heating section making the popcorn sweet
        pass

        print(f"add_sugar()")
        self.real_device.add_sugar()

    def add_salt(self):
        # Adds salt to the heating section making the popcorn salty
        pass

        print(f"add_salt()")
        self.real_device.add_salt()

    def heat(self):
        # Heats up the heating section, turning added corn into popcorn
        pass




        print(f"heat()")
        self.real_device.heat()

# Turn the machine on.
popcornMaker = PopcornMaker()
popcornMaker.toggle()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left in the tank
        self.speed = 0 # Current speed of the car

    def accelerate(self,distance):
        # Accelerates the car
        # :param distance: The distance to be traveled
        # requires that the car has fuel
        if not self.fuel:
            raise Exception("Out of fuel!")
        self.fuel -= distance * 0.01

    def brake(self):
        # Brakes the car
        # requires that the car is moving
        if not self.speed:
            raise Exception("Not moving!")
        self.speed -= 1

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive from New York to Los Angeles.".





simulator = popcornMaker.real_device
print(simulator.power)