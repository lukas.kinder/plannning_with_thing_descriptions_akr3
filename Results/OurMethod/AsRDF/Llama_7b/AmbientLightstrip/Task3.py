from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {'on': False, 'bri': 254, 'sat': 128}

    def turn_on(self):
        # Turns the device on
        self.state['on'] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the device off
        self.state['on'] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, value):
        # Sets the brightness
        # :param integer value: the new brightness level (range: 0 - 254)

        # requires that the device is on
        if not self.state['on']:
            self.turn_on()

        if value >= 0 and value <= 254:
            self.state['bri'] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def set_saturation(self, value):
        # Sets the saturation
        # :param integer value: the new saturation level (range: 0 - 254)

        # requires that the device is on
        if not self.state['on']:
            self.turn_on()

        if value >= 0 and value <= 254:
            self.state['sat'] = value









        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# Give me some light with maximum saturation.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.set_brightness(254)
ambientLightstrip.set_saturation(254)













# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class TV:
    def __init__(self):
        self.on = False
        self.channel = 0 # Channel number (0-79)
        self.volume = 50 # Volume level (0-100)

    def turn_on(self):
        # Turns the device on
        self.on = True

    def turn_off(self):
        # Turns the device off
        self.on = False

    def change_channel(self, channel):
        # Changes the channel
        if channel < 0 or channel >= 80:
            raise ValueError("Invalid channel number")
        self.channel = channel

    def increase_volume(self):
        # Increases the volume
        self.volume += 10

    def decrease_volume(self):
        # Decreases the volume
        if self.volume <= 0:
            self.volume = 0
        self.volume -= 10
















# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Watch channel 42 for 30 minutes.".






simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['sat'] == 254)