# Was not able to entangle with real device! (Are still unconnected)
import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True
class FoodDispenser:
    def __init__(self):
        self.automatic = True
        self.stored = False

    def dispense(self):
        # Manually dispenses food for the animal
        pass

    def refilled(self):
        # Refills the stored food
        pass

    def set_state(self,state):
        # Sets the feeding bowl to either 'automatic' or 'manual'
        self.automatic = state == "automatic"
        self.stored = state == "manual"






# Let the device output food 130 times. Then refill and set to automatic.
foodDispenser = FoodDispenser()
for i in range(130):
    foodDispenser.dispense()


foodDispenser.refilled()
foodDispenser.set_state("automatic")







# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Robot:
    def __init__(self):
        self.arm = False # Flags whether the arm is extended or retracted
        self.legs = False # Flags whether the legs are extended or retracted
        self.on = False # Flags whether the robot is on or off

    def extend_arm(self):
        # Extends the arm
        self.arm = True

    def extend_legs(self):
        # Extends the legs
        self.legs = True

    def turn_on(self):
        # Turns the robot on
        self.on = True

    def turn_off(self):
        # Turns the robot off
        self.on = False

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Extend the arm and legs, then turn on and move forward.".





simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 130 and simulator.food_storage == 150 and simulator.automatic)