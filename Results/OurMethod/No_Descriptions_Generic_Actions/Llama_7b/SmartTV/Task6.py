class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False # Flag if the TV is recording
        self.off = True # Flag if the TV is turned off
        self.brightness = 70 # Brightness of the TV (minimum = 0, maximum = 100)
        self.channel = 'ZDF' # Channel of the TV (characteristics: PRO7, RTL, SAT1, ARD, KIKA)

    def action1(self):
        # Unknown action 1

# SE:     def action2(self):
        # Unknown action 2

# SE:     def action3(self):
        # Unknown action 3

# SE:     def action4(self):
        # Unknown action 4

# SE:     def action5(self, number):
        # unknown action 5
        # requires that the TV is on
        if not self.off:
            self.toggle()

        # requires that the TV is recording
        if self.recording:
            self.stopRecording()

        # requires that the TV is set to a channel
        if not self.channel:
            self.selectChannel('ZDF')







        print(f"record()")
        self.real_device.record()

# Lower the brightness to 20.
smartTV = SmartTV()
smartTV.brightness = 20












# SE: Consider the following python class:


class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left (minimum = 0, maximum = 100)
        self.speed = 0 # Speed of the car (in km/h)

    def drive(self, distance):
        # Drives the car for the given distance

        # requires that the car has fuel
        if not self.fuel:
            self.refillFuel()

        self.fuel -= distance * 3.6 # Calculate the amount of fuel used

# SE: Write code that fulfills the task: "Drive 200 km at a speed of 80 km/h.".







simulator = smartTV.real_device
print(simulator.brightness == 20)