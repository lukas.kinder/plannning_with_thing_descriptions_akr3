class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False
        self.brightness = 50 # The brightness of the lamp


    def action1(self):
        # Does something when the on property is true
        pass

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Does something when the brightness is low
        if self.brightness < 50:
            self.brightness += 10

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        # Does something when the brightness is high
        if self.brightness > 80:
            self.brightness -= 10

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        # Does something when the on property is false
        pass



        print(f"decrease()")
        self.real_device.decrease()

# Turn it on.
bedsideLamp = BedsideLamp()
bedsideLamp.action1()

simulator = bedsideLamp.real_device
print(simulator.on)