class MicrowaveSimulator:
    def __init__(self):
        self.running = False

        self.statistic = {"n_runs" : 0}

    def get_running(self):
        return self.running

    def start(self):
        if not self.running:
            self.running = True

    def stop(self):
        if self.running:
            self.running = False
            self.statistic['n_runs'] +=1


class Microwave:
    def __init__(self):
        self.real_device = MicrowaveSimulator()
        self.running = False

    def action1(self):
        # Does something
        pass

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Does something else
        pass




        print(f"stop()")
        self.real_device.stop()

# Start the microwave and stop it afterwards.
microwave = Microwave()
microwave.action1()
microwave.action2()
microwave.running = False


simulator = microwave.real_device
print(simulator.statistic['n_runs'] == 1)