class SmartLockSimulator:
    def __init__(self):
        self.locked = True

        self.statistic = {"counter_locked" : 0}

    def get_locked(self):
        return self.locked

    def lock(self):
        if not self.locked:
            self.locked = True
            self.statistic["counter_locked"] +=1

    def unlock(self):
        if self.locked:
            self.locked = False


class SmartLock:
    def __init__(self):

        self.real_device = SmartLockSimulator()
        self.locked = True

    def action1(self):

        # performs action 1


# SE:     def action2(self):

        # performs action 2



# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 


        print(f"lock()")
        self.real_device.lock()

# Open the lock and close afterwards.
smartLock = SmartLock()
smartLock.action1()
smartLock.close()


simulator = smartLock.real_device
print(simulator.locked and simulator.statistic['counter_locked'] == 1)