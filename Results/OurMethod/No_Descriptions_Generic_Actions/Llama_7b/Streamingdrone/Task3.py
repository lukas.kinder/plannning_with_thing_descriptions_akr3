import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False # whether the device is recording or not
        self.streaming = False # whether the device is streaming or not
        self.battery = 50 # the battery level (minimum = 0, maximum = 100)

    def action1(self):
        # Does something when the device is recording
        pass

        print(f"stream()")
        self.real_device.stream()

    def action2(self):
        # Does something when the device is streaming
        pass

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def action3(self):
        # Does something when the battery level is low
        pass

        print(f"start_recording()")
        self.real_device.start_recording()

    def action4(self):
        # Does something when the battery level is high
        pass

        print(f"end()")
        self.real_device.end()

    def action5(self):
        # Does something when the device is on
        pass





        print(f"recharge()")
        self.real_device.recharge()

# Record a video. Recharge the drone after you are done.
streamingdrone = Streamingdrone()
streamingdrone.action1()
streamingdrone.recharge()


simulator = streamingdrone.real_device
print(simulator.battery == 100 and simulator.statistic['n_recordings'] == 1)