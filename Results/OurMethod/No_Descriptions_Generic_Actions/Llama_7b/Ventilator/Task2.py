class VentilatorSimulator:
    def __init__(self):
        self.status = "off"
        self.level = 3

    def get_status(self):
        return self.status
    
    def get_level(self):
        return self.level

    def start(self):
        if self.status == "off":
            self.status = "on"

    def stop(self):
        if self.status == "on":
            self.status = "off"

    def speed_up(self):
        if self.status == "on" and self.level < 3:
            self.level += 1

    def speed_down(self):
        if self.status == "on" and self.level > 1:
            self.level -= 1


class Ventilator:
    def __init__(self):
        self.real_device = VentilatorSimulator()
        self.status = "off"
        self.level = 3

    def action1(self):
        # Unknown action

# SE:     def action2(self):
        # Unknown action

# SE:     def action3(self):
        # Increase level by one

        self.level += 1

        print(f"start()")
        self.real_device.start()

    def action4(self):
        # Decrease level by one

        self.level -= 1





        print(f"speed_down()")
        self.real_device.speed_down()

# Turn on the ventilator with level 1.
ventilator = Ventilator()
ventilator.action1()



simulator = ventilator.real_device
print(simulator.status == 'on' and simulator.level == 1)