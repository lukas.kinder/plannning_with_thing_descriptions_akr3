import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100


class VacuumRobot:
    def __init__(self):
        self.real_device = VacuumRobotSimulator()
        self.cleaning = False
        self.storage_full = True
        self.battery = 30
        self.mapped = True
        self.at_base = True

    def action1(self):
        # Code to perform action 1 goes here
        pass

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Code to perform action 2 goes here
        pass

        print(f"scan()")
        self.real_device.scan()

    def action3(self):
        # Code to perform action 3 goes here
        pass

        print(f"empty_storage()")
        self.real_device.empty_storage()

    def action4(self):
        # Code to perform action 4 goes here
        pass

        print(f"return_to_base()")
        self.real_device.return_to_base()

    def action5(self):
        # Code to perform action 5 goes here
        pass




        print(f"charge()")
        self.real_device.charge()

# Clean the room. Afterwards, return to the base, empty the storage and recharge.
vacuumRobot = VacuumRobot()
vacuumRobot.action1()
vacuumRobot.action2()
vacuumRobot.action3()
vacuumRobot.action4()
vacuumRobot.action5()


simulator = vacuumRobot.real_device
print(simulator.statistic['n_cleaned'] == 1 and simulator.battery == 100 and not simulator.storage_full)