import random

class DryerSimulator:
    def __init__(self):
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def get_running(self):
        return self.running
    
    def get_temperature(self):
        return self.temperature
    
    def get_distilled_storage(self):
        return self.distilled_storage

    def start(self):
        if not self.running and self.distilled_storage > 0:
            self.running = True
            self.distilled_storage -= random.randint(10,30)

    def clear(self):
        self.distilled_storage = 100

    def increase_temperature(self):
        if not self.running and self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        if not self.running and self.temperature > 40:
            self.temperature -= 10


class Dryer:
    def __init__(self):
        self.real_device = DryerSimulator()
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def action1(self):
        # Code to be executed when action1 is triggered
        pass

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Code to be executed when action2 is triggered
        pass

        print(f"clear()")
        self.real_device.clear()

    def action3(self):
        # Code to be executed when action3 is triggered
        pass

        print(f"increase_temperature()")
        self.real_device.increase_temperature()

    def action4(self):
        # Code to be executed when action4 is triggered
        pass




        print(f"decrease_temperature()")
        self.real_device.decrease_temperature()

# Run the dryer with a temperature of 60.
dryer = Dryer()
dryer.action1()
dryer.temperature = 60
dryer.action2()


simulator = dryer.real_device
print(simulator.running and simulator.temperature == 60)