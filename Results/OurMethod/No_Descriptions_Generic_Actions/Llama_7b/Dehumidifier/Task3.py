class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False # flags if the dehumidifier is on
        self.tank = 'empty' # indicates the amount of water in the tank (minimum = 'empty', maximum = 'full')
        self.humidity = 45 # the current humidity level (minimum = 0, maximum = 100)

    def action1(self):
        # Code to perform action 1 here
        pass

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def action2(self):
        # Code to perform action 2 here
        pass

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def action3(self):
        # Code to perform action 3 here
        pass

        print(f"drain()")
        self.real_device.drain()

    def action4(self, Humidity):
        # Code to perform action 4 here
        pass

        # requires that the dehumidifier is on
        # requires that the humidity is between 0 and 100
        if not self.status or Humidity < 0 or Humidity > 100:
            self.action1()
        else:
            self.humidity = Humidity


        print(f"set_humidity({Humidity})")
        self.real_device.set_humidity(Humidity)

# I want this room to have a humidity of 50
dehumidifier = Dehumidifier()
dehumidifier.action4(50)


simulator = dehumidifier.real_device
print(simulator.status and simulator.humidity == 50)