class AirConditionerSimulator:
    def __init__(self):
        self.goal_Temperature = 22
        self.status = False

    def get_goal_Temperature(self):
        return self.goal_Temperature
    
    def get_status(self):
        return self.status
    
    def toggle(self):
        self.status = not self.status

    def temperature_Up(self):
        if self.status:
            self.goal_Temperature = min(35,self.goal_Temperature +1)

    def temperature_Down(self):
        if self.status:
            self.goal_Temperature = max(10,self.goal_Temperature -1)


class AirConditioner:
    def __init__(self):
        self.real_device = AirConditionerSimulator()
        self.goal_Temperature = 22 # Goal temperature (in degrees Celsius)
        self.status = False # Whether the air conditioner is on or off

    def action1(self):
        # Does something

# SE:     def action2(self):
        # Does something else

# SE:     def action3(self):
        # Does something else



# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 


        print(f"toggle()")
        self.real_device.toggle()

# Make it as cold as possible!
airConditioner = AirConditioner()
airConditioner.goal_Temperature = 22
airConditioner.action1()
airConditioner.action2()
airConditioner.action3()
airConditioner.action4()
airConditioner.action5()
airConditioner.action6()
airConditioner.action7()
airConditioner.action8()
airConditioner.action9()
airConditioner.action10()
airConditioner.action11()
airConditioner.action12()
airConditioner.action13()
airConditioner.action14()
airConditioner.action15()
airConditioner.action16()
airConditioner.action17()
airConditioner.action18()
airConditioner.action19()
airConditioner.action20()
airConditioner.action21()
airConditioner.action22()
airConditioner.action23()
airConditioner.action24()
airConditioner.action25()
airConditioner.action26()
airConditioner.action27()
airConditioner.action28()
airConditioner.action29()
airConditioner.action30()







simulator = airConditioner.real_device
print(simulator.status and simulator.goal_Temperature == 10)