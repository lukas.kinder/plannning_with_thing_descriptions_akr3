class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10


class ExteriorLight:
    def __init__(self):
        self.real_device = ExteriorLightSimulator()
        self.status = False # whether the light is on or off
        self.brightness = 100 # how bright the light is (minimum = 0, maximum = 100)

    def action1(self):
        # Code to turn the light on goes here

# SE:     def action2(self):
        # Code to dim the light goes here

        # requires that the light is on
        if not self.status:
            self.action1()

        print(f"turn_on()")
        self.real_device.turn_on()

    def action3(self):
        # Code to turn the light off goes here

        # requires that the light is on
        if self.status:
            self.action2()

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        # Code to adjust the brightness goes here

        # requires that the light is on
        if not self.status:
            self.action3()




        print(f"decrease()")
        self.real_device.decrease()

# Reduce the brightness to 20.
exteriorLight = ExteriorLight()
exteriorLight.action4()

simulator = exteriorLight.real_device
print(simulator.brightness == 20)