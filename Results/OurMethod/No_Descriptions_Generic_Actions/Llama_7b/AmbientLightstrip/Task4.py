from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}

    def action1(self):
        # Action 1
        pass

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Action 2
        pass

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self, value):
        # Action 3
        self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def action4(self, value):
        # Action 4
        self.state["sat"] = value



        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# Turn the lights on with 200 brightness and a saturation of 50.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.action1()
ambientLightstrip.action2()
ambientLightstrip.action3(200)
ambientLightstrip.action4(50)


simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['bri'] == 200 and simulator.state['sat'] == 50)