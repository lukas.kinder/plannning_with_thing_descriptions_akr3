class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False
        self.tank = 'empty'
        self.humidity = 45

    def action1(self):
        pass

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def action2(self):
        pass

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def action3(self):
        pass

        print(f"drain()")
        self.real_device.drain()

    def action4(self, desired_humidity):
        # :param integer humidity: The target humidity level

        # requires that the device is on
        if not self.status:
            self.status = True
        # requires that the tank is not empty
        if self.tank == 'empty':
            self.tank = 'full'

        if 0 <= desired_humidity <= 100:
            self.humidity = desired_humidity

        # Code to adjust the humidity here

        if desired_humidity == self.humidity:
            print('Humidity has been set to:', desired_humidity)
        else:
            print('Failed to set the humidity')

        print(f"set_humidity({desired_humidity})")
        self.real_device.set_humidity(desired_humidity)

# Turn the dehumidifier on.
dehumidifier = Dehumidifier()
dehumidifier.status = True

simulator = dehumidifier.real_device
print(simulator.status)