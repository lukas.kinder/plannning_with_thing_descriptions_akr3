class VentilatorSimulator:
    def __init__(self):
        self.status = "off"
        self.level = 3

    def get_status(self):
        return self.status
    
    def get_level(self):
        return self.level

    def start(self):
        if self.status == "off":
            self.status = "on"

    def stop(self):
        if self.status == "on":
            self.status = "off"

    def speed_up(self):
        if self.status == "on" and self.level < 3:
            self.level += 1

    def speed_down(self):
        if self.status == "on" and self.level > 1:
            self.level -= 1


class Ventilator:
    def __init__(self):
        self.real_device = VentilatorSimulator()
        self.status = "off" # Ventilator status (on/off)
        self.level = 3 # Ventilator speed level (1, 2, 3)

    def action1(self):
        # Action 1, specifics not provided in device description
        pass

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Action 2, specifics not provided in device description
        
        # if action requires device to be on
        if self.status == "off":
            self.status = "on"

        print(f"stop()")
        self.real_device.stop()

    def action3(self):
        # Action 3, specifics not provided in device description
        
        # if action requires device to be off        
        if self.status == "on":
            self.status = "off"

        print(f"speed_up()")
        self.real_device.speed_up()

    def action4(self):
        # Action 4, specifics not provided in device description
        pass

        print(f"speed_down()")
        self.real_device.speed_down()

# I need some ventilation.
ventilator = Ventilator()
if ventilator.status == "off":
    ventilator.action2()

simulator = ventilator.real_device
print(simulator.status == 'on')