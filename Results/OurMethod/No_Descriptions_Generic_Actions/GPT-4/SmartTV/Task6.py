class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False
        self.off = True
        self.brightness = 70 # brightness can be between 0 and 100
        self.channel = "ZDF" # the current channel

    def action1(self):
        # description of action1 is not provided in the device description

        # check any preconditions, update relevant properties, perform the actions
        pass # replace with actual code as needed

        print(f"record()")
        self.real_device.record()

    def action2(self):
        # description of action2 is not provided in the device description

        # check any preconditions, update relevant properties, perform the actions
        pass # replace with actual code as needed

        print(f"switch()")
        self.real_device.switch()

    def action3(self):
        # description of action3 is not provided in the device description

        # check any preconditions, update relevant properties, perform the actions
        pass # replace with actual code as needed

        print(f"turn_on()")
        self.real_device.turn_on()

    def action4(self):
        # description of action4 is not provided in the device description

        # check any preconditions, update relevant properties, perform the actions
        pass # replace with actual code as needed

        print(f"turn_off()")
        self.real_device.turn_off()

    def action5(self,value):
        # description of action5 is not provided in the device description

        # requires that the device is on
        if self.off:
            self.off = False

        # value must be between 0 and 100
        if value >= 0 and value <= 100:
            # update the relevant property, execute the action
            pass # replace with actual code as needed

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# Lower the brightness to 20.
smartTV = SmartTV()
smartTV.action5(20)

simulator = smartTV.real_device
print(simulator.brightness == 20)