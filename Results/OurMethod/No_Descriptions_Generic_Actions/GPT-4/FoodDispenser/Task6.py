import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True
        self.stored = False

    def action1(self):
        # Code to perform action1 here
        pass

        print(f"dispense()")
        self.real_device.dispense()

    def action2(self):
        # Code to perform action2 here
        pass

        print(f"refill()")
        self.real_device.refill()

    def action3(self, state):
        # Set state to either "manual" or "automatic"
        # :param string state: State to be set

        if state.lower() == "manual":
            self.automatic = False
        elif state.lower() == "automatic":
            self.automatic = True

        print(f"set_state({state})")
        self.real_device.set_state(state)

# Let the device output food 130 times. Then refill and set to automatic.
foodDispenser = FoodDispenser()
for _ in range(130):
    foodDispenser.action1()
foodDispenser.action2()
foodDispenser.action3("automatic")

simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 130 and simulator.food_storage == 150 and simulator.automatic)