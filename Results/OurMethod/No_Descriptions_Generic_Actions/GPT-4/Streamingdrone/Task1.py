import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False # the drone's recording state
        self.streaming = False # the drone's streaming state
        self.battery = 50 # the battery life of the drone

    def action1(self):
        #assumed action tied to the drone

        # check the battery level before executing the action
        if self.battery <= 0:
            print("Battery is empty, action cannot be performed")
            return 
        # code for action to be performed 
        # decrease the battery level after action
        self.battery -= 10

        print(f"stream()")
        self.real_device.stream()

    def action2(self):
        #assumed action tied to the drone

        # check the battery level before executing the action
        if self.battery <= 0:
            print("Battery is empty, action cannot be performed")
            return 
        # code for action to be performed
        # decrease the battery level after action
        self.battery -= 10

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def action3(self):
        #assumed action tied to the drone

        # check the battery level before executing the action
        if self.battery <= 0:
            print("Battery is empty, action cannot be performed")
            return 
        # code for action to be performed
        # decrease the battery level after action
        self.battery -= 10

        print(f"start_recording()")
        self.real_device.start_recording()

    def action4(self):
        #assumed action tied to the drone

        # check the battery level before executing the action
        if self.battery <= 0:
            print("Battery is empty, action cannot be performed")
            return 
        # code for action to be performed
        # decrease the battery level after action
        self.battery -= 10

        print(f"end()")
        self.real_device.end()

    def action5(self):
        #assumed action tied to the drone

        # check the battery level before executing the action
        if self.battery <= 0:
            print("Battery is empty, action cannot be performed")
            return 
        # code for action to be performed
        # decrease the battery level after action
        self.battery -= 10

        print(f"recharge()")
        self.real_device.recharge()

# Recharge the drone.
streamingdrone = Streamingdrone()
streamingdrone.battery = 100

simulator = streamingdrone.real_device
print(simulator.battery == 100)