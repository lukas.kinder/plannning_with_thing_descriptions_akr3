class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True


class Oven:
    def __init__(self):
        self.real_device = OvenSimulator()
        self.on = False
        self.heating_method = "convection" # the heating method could be either "convection" or "top and bottom"

    def action1(self):
        # No detailed description is provided for this action. Action1 will be performed assuming the condition that the device is turned on.
        if not self.on:
            self.on = True
        # Action1 code here

        print(f"switch_on()")
        self.real_device.switch_on()

    def action2(self):
        # No detailed description is provided for this action. Action2 will be performed assuming the condition that the device is turned on.
        if not self.on:
            self.on = True
        # Action2 code here
    
        print(f"switch_off()")
        self.real_device.switch_off()

    def action3(self):
        # No detailed description is provided for this action. Action3 will be performed assuming the condition that the device is turned on.
        if not self.on:
            self.on = True
        # Action3 code here

        print(f"change_heating_method()")
        self.real_device.change_heating_method()

    def action4(self):
        # No detailed description is provided for this action. Action4 will be performed assuming the condition that the device is turned on.
        if not self.on:
            self.on = True
        # Action4 code here

        print(f"preheat()")
        self.real_device.preheat()

# Set mode to 'top and bottom'
oven = Oven()
oven.heating_method = "top and bottom"

simulator = oven.real_device
print(simulator.heating_method == 'top and bottom')