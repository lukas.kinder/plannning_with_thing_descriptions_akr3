class MicrowaveSimulator:
    def __init__(self):
        self.running = False

        self.statistic = {"n_runs" : 0}

    def get_running(self):
        return self.running

    def start(self):
        if not self.running:
            self.running = True

    def stop(self):
        if self.running:
            self.running = False
            self.statistic['n_runs'] +=1


class Microwave:
    def __init__(self):
        self.real_device = MicrowaveSimulator()
        self.running = False

    def action1(self):
        # Specifics of action1 are not provided in the device description

        # Assuming that the microwave should be running for action1
        if not self.running:
            self.running = True

        # Perform action1 (details need to be provided by the application)

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Specifics of action2 are not provided in the device description

        # Assuming that the microwave should be running for action2
        if not self.running:
            self.running = True

        # Perform action2 (details need to be provided by the application)

        print(f"stop()")
        self.real_device.stop()

# Start the microwave.
microwave = Microwave()
microwave.action1()

simulator = microwave.real_device
print(simulator.running)