class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False
        self.door = "closed"

    def action1(self):
        # As the action's requirements and what it exactly does are not specified 
        # in the provided description, this is a placeholder function.
        pass 

        print(f"close()")
        self.real_device.close()

    def action2(self):
        # As the action's requirements and what it exactly does are not specified 
        # in the provided description, this is a placeholder function.
        pass

        print(f"open()")
        self.real_device.open()

    def action3(self):
        # As the action's requirements and what it exactly does are not specified 
        # in the provided description, this is a placeholder function.
        pass

        print(f"stop()")
        self.real_device.stop()

    def action4(self):
        # As the action's requirements and what it exactly does are not specified 
        # in the provided description, this is a placeholder function.
        pass

        print(f"start()")
        self.real_device.start()

# Wash the dishes. Open the door afterwards.
dishwasher = Dishwasher()

# Assuming that action1 is the method that starts the dishwasher 
dishwasher.action1()

# Assuming that action4 is the method that opens the dishwasher door
dishwasher.action4()

simulator = dishwasher.real_device
print(simulator.statistics['used'] == 1)