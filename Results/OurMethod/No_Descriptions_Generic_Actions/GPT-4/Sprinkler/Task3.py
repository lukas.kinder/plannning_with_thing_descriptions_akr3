class SprinklerSimulator:
    def __init__(self):
        self.water = True
        self.on = False

        self.stored_water = 4 # initial stored water. Can store up to 12

        self.statistic = {"n_sprinkle" : 0}

    def get_water(self):
        return self.water
    
    def get_on(self):
        return self.on

    def power_on(self):
        if not self.on:
            self.on = True

    def power_off(self):
        if self.on:
            self.on = False

    def sprinkle(self):
        if self.on and self.water:
            self.stored_water -=1

            if self.stored_water == 0:
                self.water = False

            self.statistic['n_sprinkle'] +=1
  
    def refill(self):
        self.water = True
        self.stored_water = 12


class Sprinkler:
    def __init__(self):
        self.real_device = SprinklerSimulator()
        self.water = True
        self.on = False

    def action1(self):
        # This function does some action.

        # requires that the device is on
        if not self.on:
            self.on = True
        # Placeholder for the actual function code

        print(f"power_on()")
        self.real_device.power_on()

    def action2(self):
        # This function does some action.

        # requires that the device is on
        if not self.on:
            self.on = True
        # Placeholder for the actual function code

        print(f"power_off()")
        self.real_device.power_off()

    def action3(self):
        # This function does some action.

        # requires that the device is on
        if not self.on:
            self.on = True
        # Placeholder for the actual function code

        print(f"sprinkle()")
        self.real_device.sprinkle()

    def action4(self):
        # This function does some action.

        # requires that the device is on
        if not self.on:
            self.on = True
        # Placeholder for the actual function code

        print(f"refill()")
        self.real_device.refill()

# Refill the sprinkler. But turn it off afterwards.
sprinkler = Sprinkler()
sprinkler.water = True
sprinkler.on = False

simulator = sprinkler.real_device
print(simulator.stored_water == 12 and not simulator.on)