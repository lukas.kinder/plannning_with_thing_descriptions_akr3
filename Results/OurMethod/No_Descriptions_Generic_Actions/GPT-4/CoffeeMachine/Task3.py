import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False



class CoffeeMachine:
    def __init__(self):
        self.real_device = CoffeeMachineSimulator()
        self.water = True
        self.storage = True
        self.on = False

    def action1(self):
        # Action 1 - specific procedure to be specified
        pass # To be filled with specific steps

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # Action 2 - specific procedure to be specified
        pass # To be filled with specific steps

        print(f"refill()")
        self.real_device.refill()

    def action3(self):
        # Action 3 - specific procedure to be specified
        pass # To be filled with specific steps

        print(f"empty()")
        self.real_device.empty()

    def action4(self):
        # Action 4 - specific procedure to be specified
        pass # To be filled with specific steps

        print(f"make_coffee()")
        self.real_device.make_coffee()

# Make me a coffee. Refill and empty the machine once you are done.
coffeeMachine = CoffeeMachine()
coffeeMachine.action1() # suppose this is to make coffee
coffeeMachine.action2() # suppose this is to refill machine
coffeeMachine.action3() # suppose this is to empty machine
coffeeMachine.action4() # suppose this is to turn machine off

simulator = coffeeMachine.real_device
print(simulator.statistic['coffee_made'] == 1 and simulator.empty_storage == 20 and simulator.water_left == 10)