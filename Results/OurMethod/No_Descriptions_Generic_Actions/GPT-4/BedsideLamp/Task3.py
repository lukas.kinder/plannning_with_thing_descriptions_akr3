class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False
        self.brightness = 50 # the value can range between 0 and 100

    def action1(self):
        # implementation of action1
        # requires that the device is on
        if not self.on:
            self.on = True
        # implement the specific action here

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # implementation of action2
        # requires that the device is on
        if not self.on:
            self.on = True
        # implement the specific action here

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        # implementation of action3
        # requires that the device is on
        if not self.on:
            self.on = True
        # implement the specific action here

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        # implementation of action4
        # requires that the device is on
        if not self.on:
            self.on = True
        # implement the specific action here

        print(f"decrease()")
        self.real_device.decrease()

# Make it is bright as possible.
bedsideLamp = BedsideLamp()
bedsideLamp.brightness = 100

simulator = bedsideLamp.real_device
print(simulator.on and simulator.brightness == 100)