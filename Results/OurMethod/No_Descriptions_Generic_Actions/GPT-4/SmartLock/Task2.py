class SmartLockSimulator:
    def __init__(self):
        self.locked = True

        self.statistic = {"counter_locked" : 0}

    def get_locked(self):
        return self.locked

    def lock(self):
        if not self.locked:
            self.locked = True
            self.statistic["counter_locked"] +=1

    def unlock(self):
        if self.locked:
            self.locked = False


class SmartLock:
    def __init__(self):
        self.real_device = SmartLockSimulator()
        self.locked = True

    def action1(self):
        # This function would contain the logic for action1
        pass

        print(f"lock()")
        self.real_device.lock()

    def action2(self):
        # This function would contain the logic for action2
        pass

        print(f"unlock()")
        self.real_device.unlock()

# Open the lock and close afterwards.
# The SmartLock class doesn't provide methods for lock/unlock actions, it's assumed that these are represented by action1/action2 methods

smartLock = SmartLock()
smartLock.action1()  # Assuming action1 opens the lock
smartLock.action2()  # Assuming action2 closes the lock

simulator = smartLock.real_device
print(simulator.locked and simulator.statistic['counter_locked'] == 1)