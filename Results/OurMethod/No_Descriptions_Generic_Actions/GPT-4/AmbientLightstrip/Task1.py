from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128} # Defines if the device is on and sets brightness and saturation levels

    def action1(self):
        # details for action1 are not specified in the description
        # An implementation could be like turning on the AmbientLightstrip

        # requires that the device is off
        if self.state["on"]:
            return
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # details for action2 are not specified in the description
        # An implementation could be like turning off the AmbientLightstrip

        # requires that the device is on
        if not self.state["on"]:
            return
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self, brightness):
        # details for action3 are not specified in the description
        # An implementation could be like setting the brightness of the AmbientLightstrip

        # requires that the device is on
        if not self.state["on"]:
            self.action1()
        
        # validating brightness value
        if 0 <= brightness <= 254:
            self.state["bri"] = brightness

        print(f"set_brightness({brightness})")
        self.real_device.set_brightness(brightness)

    def action4(self, saturation):
        # details for action4 are not specified in the description
        # An implementation could be like setting the saturation of the AmbientLightstrip

        # requires that the device is on
        if not self.state["on"]:
            self.action1()

        # validating saturation value
        if 0 <= saturation <= 254:
            self.state["sat"] = saturation

        print(f"set_saturation({saturation})")
        self.real_device.set_saturation(saturation)

# Lights please!
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.action1()

simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['bri'] > 0)