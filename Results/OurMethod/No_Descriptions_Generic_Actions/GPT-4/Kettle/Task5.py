class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False
        self.filled = False

    def action1(self):
        # action1 to be implemented
        pass
    
        print(f"switch_power()")
        self.real_device.switch_power()

    def action2(self):
        # action2 to be implemented
        pass

        print(f"heat()")
        self.real_device.heat()

    def action3(self):
        # action3 to be implemented
        pass

        print(f"fill()")
        self.real_device.fill()

    def action4(self):
        # action4 to be implemented
        pass

        print(f"empty()")
        self.real_device.empty()

class Kettle:
    def __init__(self):
        self.power = False
        self.filled = False

    def fill(self):
        # fill the kettle
        self.filled = True

    def heat(self):
        # heat up the water
        if self.filled:
            self.power = True

    def empty(self):
        # empty the kettle
        if self.filled and self.power:
            self.power = False
            self.filled = False
            
# Heat up hot water and empty it. Do this 5 times.
kettle = Kettle()
for _ in range(5):
    kettle.fill()
    kettle.heat()
    kettle.empty()

simulator = kettle.real_device
print(simulator.statistic['got_hot_water'] ==5)