class SmartHeaterSimulator:
    def __init__(self):
        self.level = 4
        self.status = False

    def get_level(self):
        return self.level
    
    def get_status(self):
        return self.status

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase_level(self):
        if self.status and self.level < 6:
            self.level += 1

    def decrease_level(self):
        if self.status and self.level > 0:
            self.level -= 1


class SmartHeater:
    def __init__(self):
        self.real_device = SmartHeaterSimulator()
        self.level = 4 # min = 0, max = 6
        self.status = False

    def action1(self):
        pass

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        pass

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        pass

        print(f"increase_level()")
        self.real_device.increase_level()

    def action4(self):
        pass


        print(f"decrease_level()")
        self.real_device.decrease_level()

# Start heating with level 5.
smartHeater = SmartHeater()
smartHeater.action1()
smartHeater.action2()
smartHeater.action3()
smartHeater.action4()


simulator = smartHeater.real_device
print(simulator.status and simulator.level == 5)