class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False
        self.filled = False

    def action1(self):
        # ...
        pass

        print(f"switch_power()")
        self.real_device.switch_power()

    def action2(self):
        # ...
        pass

        print(f"heat()")
        self.real_device.heat()

    def action3(self):
        # ...
        pass

        print(f"fill()")
        self.real_device.fill()

    def action4(self):
        # ...
        pass


        print(f"empty()")
        self.real_device.empty()

# Make some hot water using the kettle and empty it afterwards.
kettle = Kettle()
if not kettle.power:
    kettle.action1()
kettle.action2()
while not kettle.filled:
    kettle.action3()
kettle.action4()


simulator = kettle.real_device
print(simulator.statistic['got_hot_water'] ==1)