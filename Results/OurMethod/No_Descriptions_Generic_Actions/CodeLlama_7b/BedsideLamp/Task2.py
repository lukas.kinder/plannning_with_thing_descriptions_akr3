class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False
        self.brightness = 50

    def action1(self):
        pass

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        pass

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        pass

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        pass


        print(f"decrease()")
        self.real_device.decrease()

# Give me a brightness of 30
bedsideLamp = BedsideLamp()
bedsideLamp.action1()
bedsideLamp.action3()
bedsideLamp.action4()


simulator = bedsideLamp.real_device
print(simulator.on and simulator.brightness == 30)