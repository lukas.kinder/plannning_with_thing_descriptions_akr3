class ChillerSimulator:
    def __init__(self):
        self.type = "" #Type of the chiller
        self.firmware = "v1.0" #Firmware version
        self.model = "" #Chiller model
        self.location = "" #Location of the chiller
        self.latitude = 0 #Latitude of the chiller
        self.longitude = 0 #Longitude of the chiller
        self.location_tag = "" #Location tag
        self.floor_tag = "" #floor level
        self.campus_tag = "" #name of the campus
        self.online = False #online status
        self.temperature = 20 #temperature
        self.temperature_unit = "C" #C for Celsius, F for Fahrenheit
        self.humidity = 30 #humidity of the chiller
        self.humidity_unit = "percentage" #percentage or grams per m3
        self.pressure = 1.5 #pressure
        self.pressure_unit = "bar" #psi or pascal or bar
        self.ora_latitude = 0 #ora_latitude
        self.ora_longitude = 0 #ora_longitude
        self.ora_altitude = 0 #ora_altitude
        self.ora_uncertainty = 0 #ora_accuracy
        self.ora_zone = "" #ora_zone
        self.ora_txPower = 0 #ora_mssi
        self.ora_rssi = 0 #ora_rssi

        self.n_rebooted  = 0

    # Getter functions
    def get_type(self):
        return self.type

    def get_firmware(self):
        return self.firmware

    def get_model(self):
        return self.model

    def get_location(self):
        return self.location

    def get_latitude(self):
        return self.latitude

    def get_longitude(self):
        return self.longitude

    def get_location_tag(self):
        return self.location_tag

    def get_floor_tag(self):
        return self.floor_tag

    def get_campus_tag(self):
        return self.campus_tag

    def get_online(self):
        return self.online

    def get_temperature(self):
        return self.temperature

    def get_temperature_unit(self):
        return self.temperature_unit

    def get_humidity(self):
        return self.humidity

    def get_humidity_unit(self):
        return self.humidity_unit

    def get_pressure(self):
        return self.pressure

    def get_pressure_unit(self):
        return self.pressure_unit

    def get_ora_latitude(self):
        return self.ora_latitude

    def get_ora_longitude(self):
        return self.ora_longitude

    def get_ora_altitude(self):
        return self.ora_altitude

    def get_ora_uncertainty(self):
        return self.ora_uncertainty

    def get_ora_zone(self):
        return self.ora_zone

    def get_ora_txPower(self):
        return self.ora_txPower

    def get_ora_rssi(self):
        return self.ora_rssi
    
    #actions
    def reboot(self):
        self.n_rebooted +=1

    def firmwareUpdate(self, version):
        self.firmware = version

    def emergencyValveRelease(self):
        self.pressure = 1

    def increasePressure(self, value):
        if value >= 0 and value <=5:
            self.pressure += value


class Chiller:
    def __init__(self):
        self.real_device = ChillerSimulator()
        self.type = ""
        self.firmware = "v1.0"
        self.model = ""
        self.location = ""
        self.latitude = 0
        self.longitude = 0
        self.location_tag = ""
        self.floor_tag = ""
        self.campus_tag = ""
        self.online = False
        self.temperature = 20
        self.temperature_unit = "C"
        self.humidity = 30
        self.humidity_unit = "percentage"
        self.pressure = 1.5
        self.pressure_unit = "bar"
        self.ora_latitude = 0
        self.ora_longitude = 0
        self.ora_altitude = 0
        self.ora_uncertainty = 0
        self.ora_zone = ""
        self.ora_txPower = 0
        self.ora_rssi = 0

    def action1(self):
        pass

        print(f"reboot()")
        self.real_device.reboot()

    def action2(self, string):
        pass

        print(f"firmwareUpdate({string})")
        self.real_device.firmwareUpdate(string)

    def action3(self):
        pass

        print(f"emergencyValveRelease()")
        self.real_device.emergencyValveRelease()

    def action4(self, int):
        pass


        print(f"increasePressure({int})")
        self.real_device.increasePressure(int)

# Increase pressure by 3.
chiller = Chiller()
chiller.action4(3)


simulator = chiller.real_device
print(round(simulator.pressure,3) == 4.5)