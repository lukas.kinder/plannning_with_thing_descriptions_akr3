class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False
        self.door = "closed"

    def action1(self):
        pass

        print(f"close()")
        self.real_device.close()

    def action2(self):
        pass

        print(f"open()")
        self.real_device.open()

    def action3(self):
        pass

        print(f"stop()")
        self.real_device.stop()

    def action4(self):
        pass


        print(f"start()")
        self.real_device.start()

# Open the door.
dishwasher = Dishwasher()
dishwasher.action2()


simulator = dishwasher.real_device
print(simulator.door == 'open')