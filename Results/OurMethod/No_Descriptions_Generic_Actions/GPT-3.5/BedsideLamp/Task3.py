class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False
        self.brightness = 50

    def action1(self):
        pass
        # Write code to perform action1 here

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        pass
        # Write code to perform action2 here

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        pass
        # Write code to perform action3 here

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        pass
        # Write code to perform action4 here

        print(f"decrease()")
        self.real_device.decrease()

# Make it is bright as possible.
bedsideLamp = BedsideLamp()
bedsideLamp.brightness = 100

simulator = bedsideLamp.real_device
print(simulator.on and simulator.brightness == 100)