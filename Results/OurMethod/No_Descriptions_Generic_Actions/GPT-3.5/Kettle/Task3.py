class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False  # characteristics: ["on", "off"]
        self.filled = False

    def action1(self):
        pass
        # Add code for action1

        print(f"switch_power()")
        self.real_device.switch_power()

    def action2(self):
        pass
        # Add code for action2

        print(f"heat()")
        self.real_device.heat()

    def action3(self):
        pass
        # Add code for action3

        print(f"fill()")
        self.real_device.fill()

    def action4(self):
        pass
        # Add code for action4

        print(f"empty()")
        self.real_device.empty()

kettle = Kettle()
kettle.filled = True
kettle.power = True

simulator = kettle.real_device
print(simulator.contain_hot_water)