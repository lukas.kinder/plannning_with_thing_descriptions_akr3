class StoveSimulator:
    def __init__(self):
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def get_plate_1_power(self):
        return self.plate_1_power
    
    def get_plate_2_power(self):
        return self.plate_2_power
    
    def get_plate_1_level(self):
        return self.plate_1_level
    
    def get_plate_2_level(self):
        return self.plate_2_level

    def activate_plate_1(self):
        if not self.plate_1_power:
            self.plate_1_power = True

    def activate_plate_2(self):
        if not self.plate_2_power:
            self.plate_2_power = True

    def deactivate_plate_1(self):
        if self.plate_1_power:
            self.plate_1_power = False

    def deactivate_plate_2(self):
        if self.plate_2_power:
            self.plate_2_power = False

    def increase_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level < 9:
            self.plate_1_level += 1

    def increase_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level < 9:
            self.plate_2_level += 1

    def decrease_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level > 0:
            self.plate_1_level -= 1

    def decrease_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level > 0:
            self.plate_2_level -= 1


class Stove:
    def __init__(self):
        self.real_device = StoveSimulator()
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def action1(self):
        pass
        print(f"activate_plate_1()")
        self.real_device.activate_plate_1()

    def action2(self):
        pass
        print(f"activate_plate_2()")
        self.real_device.activate_plate_2()

    def action3(self):
        pass
        print(f"deactivate_plate_1()")
        self.real_device.deactivate_plate_1()

    def action4(self):
        pass
        print(f"deactivate_plate_2()")
        self.real_device.deactivate_plate_2()

    def action5(self):
        pass
        print(f"increase_heat_plate_1()")
        self.real_device.increase_heat_plate_1()

    def action6(self):
        pass
        print(f"increase_heat_plate_2()")
        self.real_device.increase_heat_plate_2()

    def action7(self):
        pass
        print(f"decrease_heat_plate_1()")
        self.real_device.decrease_heat_plate_1()

    def action8(self):
        pass

        print(f"decrease_heat_plate_2()")
        self.real_device.decrease_heat_plate_2()

stove = Stove()
stove.plate_2_level = 2

simulator = stove.real_device
print(simulator.plate_2_level == 2)