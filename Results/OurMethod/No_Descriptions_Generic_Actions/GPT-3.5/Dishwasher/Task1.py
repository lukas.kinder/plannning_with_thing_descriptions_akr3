class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False
        self.door = "closed"

    def action1(self):
        # Add action 1 behavior here
        pass

        print(f"close()")
        self.real_device.close()

    def action2(self):
        # Add action 2 behavior here
        pass

        print(f"open()")
        self.real_device.open()

    def action3(self):
        # Add action 3 behavior here
        pass

        print(f"stop()")
        self.real_device.stop()

    def action4(self):
        # Add action 4 behavior here
        pass

        print(f"start()")
        self.real_device.start()

dishwasher = Dishwasher()
dishwasher.action1()

simulator = dishwasher.real_device
print(simulator.running)