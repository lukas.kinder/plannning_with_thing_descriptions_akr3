from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}  # initial state

    def action1(self):
        # Function for action1
        pass  # Add action1 logic

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Function for action2
        pass  # Add action2 logic

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self, value):
        # Function for action3
        # :param integer value: input value for action3
        pass  # Add action3 logic

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def action4(self, value):
        # Function for action4
        # :param integer value: input value for action4
        pass  # Add action4 logic

        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

ambientLightstrip = AmbientLightstrip()
ambientLightstrip.state["on"] = True
ambientLightstrip.state["bri"] = 100

simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['bri'] <= 128)