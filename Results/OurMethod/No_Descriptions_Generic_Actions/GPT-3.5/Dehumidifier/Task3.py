class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False
        self.tank = "empty"
        self.humidity = 45

    def action1(self):
        pass
        # No specific description for action1 given

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def action2(self):
        pass
        # No specific description for action2 given

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def action3(self):
        pass
        # No specific description for action3 given

        print(f"drain()")
        self.real_device.drain()

    def action4(self, desired_humidity):
        # :param integer desired_humidity: the desired humidity

        # requires that the device is on
        if not self.status:
            # Assuming action1 is turning the device on
            self.action1()

        self.humidity = desired_humidity

        print(f"set_humidity({desired_humidity})")
        self.real_device.set_humidity(desired_humidity)

# I want this room to have a humidity of 50.
dehumidifier = Dehumidifier()
dehumidifier.action4(50)

simulator = dehumidifier.real_device
print(simulator.status and simulator.humidity == 50)