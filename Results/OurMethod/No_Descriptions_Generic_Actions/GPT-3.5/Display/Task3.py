from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {"status": False}
        self.content = "Video"

    def action1(self):
        # action1 implementation
        pass

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # action2 implementation
        pass

        print(f"setVolume()")
        self.real_device.setVolume()

    def action3(self):
        # action3 implementation
        pass

        print(f"setBright()")
        self.real_device.setBright()

    def action4(self):
        # action4 implementation
        pass

        print(f"showText()")
        self.real_device.showText()

    def action5(self, identifier, name, description, url):
        # action5 implementation
        pass

        print(f"playVideo({identifier},{name},{description},{url})")
        self.real_device.playVideo(identifier, name, description, url)

    def action6(self):
        # action6 implementation
        pass

        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def action7(self):
        # action7 implementation
        pass

        print(f"stopVideo()")
        self.real_device.stopVideo()

    def action8(self, identifier, name, description, url):
        # action8 implementation
        pass

        print(f"presentationWebApp({identifier},{name},{description},{url})")
        self.real_device.presentationWebApp(identifier, name, description, url)

    def action9(self):
        # action9 implementation
        pass

        print(f"launchNewsApp()")
        self.real_device.launchNewsApp()

display = Display()
# Assuming that displaying text and setting brightness would be achieved through action5 and action8 respectively
display.action5("text_identifier", "Hello World", "Displaying hello world text", "text_url")
display.action8("brightness_identifier", "Brightness setting", "Setting the brightness to 90", "brightness_url")

simulator = display.real_device
print(simulator.power['status'] and simulator.content == 'Text' and simulator.text_buffer == 'Hello World' and simulator.brightness == 90)