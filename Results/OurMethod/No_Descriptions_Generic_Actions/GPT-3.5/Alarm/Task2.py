class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration


class Alarm:
    def __init__(self):
        self.real_device = AlarmSimulator()
        self.set = False
        self.vibration = False

    def action1(self):
        pass  # Add logic for action1 here

        print(f"set_alarm()")
        self.real_device.set_alarm()

    def action2(self):
        pass  # Add logic for action2 here

        print(f"unset()")
        self.real_device.unset()

    def action3(self):
        pass  # Add logic for action3 here

        print(f"toggle_vibration_mode()")
        self.real_device.toggle_vibration_mode()

# Turn on the alarm. But not on vibration
alarm = Alarm()
alarm.set = True

simulator = alarm.real_device
print(simulator.set and not simulator.vibration)