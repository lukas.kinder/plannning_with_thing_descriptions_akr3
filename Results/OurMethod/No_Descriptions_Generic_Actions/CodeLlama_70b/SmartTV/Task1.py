class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False # whether the TV is recording or not
        self.off = True # Whether the TV is on or off
        self.brightness = 70 # Brightness of the TV (minimum = 0, maximum = 100)
        self.channel = 'ZDF' # Channel of the TV (PRO7, RTL, SAT1, ARD, KIKA are possible values)

    def action1(self):
        # To be implemented

# SE:     def action2(self):
        # To be implemented

# SE:     def action3(self):
        # To be implemented

# SE:     def action4(self):
        # To be implemented

# SE:     def action5(self, number):
        # To be implemented

        # requires that the device is on
        if not self.off:
            self.toggle()

        # requires that the brightness is within the minimum and maximum range
        if number < self.brightness.minimum or number > self.brightness.maximum:
            self.brightness = (number - self.brightness.minimum) / (self.brightness.maximum - self.brightness.minimum) * 100
        # requires that the channel is valid
        if self.channel not in ['PRO7', 'RTL', 'SAT1', 'ARD', 'KIKA']:
            self.channel = 'ZDF'







        print(f"record()")
        self.real_device.record()

# Turn on the tv.
smartTV = SmartTV()
smartTV.off = False
smartTV.action1()


simulator = smartTV.real_device
print(not simulator.off)