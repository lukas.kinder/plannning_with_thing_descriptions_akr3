class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True


class Oven:
    def __init__(self):
        self.real_device = OvenSimulator()
        self.on = False
        self.heating_method = "convection"

    def action1(self):
        # :param None: No parameter

        # does nothing

# SE:     def action2(self):
        # :param None: No parameter

        # Does something

# SE:     def action3(self):
        # :param None: No parameter

        # Does something

# SE:     def action4(self):
        # :param None: No parameter

        # Does something




# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 


        print(f"switch_on()")
        self.real_device.switch_on()

# Preheat the oven with convection mode.
oven = Oven()
oven.action1()
oven.action2()
oven.action3()
oven.action4()










simulator = oven.real_device
print(simulator.is_preheating and simulator.heating_method == 'convection')