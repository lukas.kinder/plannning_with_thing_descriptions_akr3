class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False # Whether the dehumidifier is on or off
        self.tank = "empty" # The current level of water in the tank
        self.humidity = 45 # The current humidity level

    def action1(self):
        # Description goes here

# SE:     def action2(self):
        # Description goes here

# SE:     def action3(self):
        # Description goes here

# SE:     def action4(self,humidity):
        # Sets the humidity level
        self.humidity = humidity # :param integer humidity: The desired humidity level






        print(f"toggle_status()")
        self.real_device.toggle_status()

# Turn the dehumidifier on.
dehumidifier = Dehumidifier()
dehumidifier.action1()


simulator = dehumidifier.real_device
print(simulator.status)