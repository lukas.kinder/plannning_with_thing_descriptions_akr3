class StoveSimulator:
    def __init__(self):
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def get_plate_1_power(self):
        return self.plate_1_power
    
    def get_plate_2_power(self):
        return self.plate_2_power
    
    def get_plate_1_level(self):
        return self.plate_1_level
    
    def get_plate_2_level(self):
        return self.plate_2_level

    def activate_plate_1(self):
        if not self.plate_1_power:
            self.plate_1_power = True

    def activate_plate_2(self):
        if not self.plate_2_power:
            self.plate_2_power = True

    def deactivate_plate_1(self):
        if self.plate_1_power:
            self.plate_1_power = False

    def deactivate_plate_2(self):
        if self.plate_2_power:
            self.plate_2_power = False

    def increase_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level < 9:
            self.plate_1_level += 1

    def increase_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level < 9:
            self.plate_2_level += 1

    def decrease_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level > 0:
            self.plate_1_level -= 1

    def decrease_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level > 0:
            self.plate_2_level -= 1


class Stove:
    def __init__(self):
        self.real_device = StoveSimulator()
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def action1(self):
        # Description of action 1
        pass

        print(f"activate_plate_1()")
        self.real_device.activate_plate_1()

    def action2(self):
        # Description of action 2
        pass

        print(f"activate_plate_2()")
        self.real_device.activate_plate_2()

    def action3(self):
        # Description of action 3
        pass

        print(f"deactivate_plate_1()")
        self.real_device.deactivate_plate_1()

    def action4(self):
        # Description of action 4
        pass

        print(f"deactivate_plate_2()")
        self.real_device.deactivate_plate_2()

    def action5(self):
        # Description of action 5
        pass

        print(f"increase_heat_plate_1()")
        self.real_device.increase_heat_plate_1()

    def action6(self):
        # Description of action 6
        pass

        print(f"increase_heat_plate_2()")
        self.real_device.increase_heat_plate_2()

    def action7(self):
        # Description of action 7
        pass

        print(f"decrease_heat_plate_1()")
        self.real_device.decrease_heat_plate_1()

    def action8(self):
        # Description of action 8
        pass


        print(f"decrease_heat_plate_2()")
        self.real_device.decrease_heat_plate_2()

# Set plate 1 to level 3 and plate 2 to level 5
stove = Stove()
stove.action1()
stove.action2()
stove.action3()
stove.action4()
stove.action5()
stove.action6()
stove.action7()
stove.action8()


simulator = stove.real_device
print(simulator.plate_1_level == 3 and simulator.plate_2_level == 5)