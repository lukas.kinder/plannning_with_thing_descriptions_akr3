class SmartLockSimulator:
    def __init__(self):
        self.locked = True

        self.statistic = {"counter_locked" : 0}

    def get_locked(self):
        return self.locked

    def lock(self):
        if not self.locked:
            self.locked = True
            self.statistic["counter_locked"] +=1

    def unlock(self):
        if self.locked:
            self.locked = False


class SmartLock:
    def __init__(self):
        self.real_device = SmartLockSimulator()
        self.locked = True

    def action1(self):
        # Action 1
        pass

        print(f"lock()")
        self.real_device.lock()

    def action2(self):
        # Action 2
        pass

        print(f"unlock()")
        self.real_device.unlock()

# Open the lock
smartLock = SmartLock()
smartLock.action1()







simulator = smartLock.real_device
print(not simulator.locked)