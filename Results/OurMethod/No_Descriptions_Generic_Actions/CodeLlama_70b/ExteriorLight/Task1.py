class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10


class ExteriorLight:
    def __init__(self):
        self.real_device = ExteriorLightSimulator()
        self.status = False
        self.brightness = 100

    def action1(self):
        # Description: Turn the light on

        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Description: Turn the light off

        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        # Description: Adjust brightness

        # requires that the device is on
        if self.status:
            self.brightness = max(self.brightness - 10, 0)

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        # Description: Adjust brightness

        # requires that the device is on
        if self.status:
            self.brightness = min(self.brightness + 10, 100)



        print(f"decrease()")
        self.real_device.decrease()

# Light!
exteriorLight = ExteriorLight()
exteriorLight.action1()


simulator = exteriorLight.real_device
print(simulator.status)