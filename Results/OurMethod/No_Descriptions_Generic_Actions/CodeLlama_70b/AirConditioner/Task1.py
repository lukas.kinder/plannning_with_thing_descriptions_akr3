class AirConditionerSimulator:
    def __init__(self):
        self.goal_Temperature = 22
        self.status = False

    def get_goal_Temperature(self):
        return self.goal_Temperature
    
    def get_status(self):
        return self.status
    
    def toggle(self):
        self.status = not self.status

    def temperature_Up(self):
        if self.status:
            self.goal_Temperature = min(35,self.goal_Temperature +1)

    def temperature_Down(self):
        if self.status:
            self.goal_Temperature = max(10,self.goal_Temperature -1)


class AirConditioner:
    def __init__(self):
        self.real_device = AirConditionerSimulator()
        self.goal_Temperature = 22
        self.status = False

    def action1(self):
        # Does something when goal temperature is reached
        pass

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # Does something when status is true
        pass

        print(f"temperature_Up()")
        self.real_device.temperature_Up()

    def action3(self):
        # Does something when status is false
        pass



        print(f"temperature_Down()")
        self.real_device.temperature_Down()

# Turn the Conditioner on.
airConditioner = AirConditioner()
airConditioner.action1()


simulator = airConditioner.real_device
print(simulator.status)