class SmartSpeakerSimulator:
    def __init__(self):
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

        self.current_service = "spotify"

    def get_connection(self):
        return self.connection
    
    def get_playing(self):
        return self.playing
    
    def get_status(self):
        return self.status
    
    def get_volume(self):
        return self.volume

    def reconnect(self):
        if self.status and not self.connection:
            self.connection = True

    def play(self):
        if self.status and self.connection and not self.playing:
            self.playing = True

    def pause(self):
        if self.status and self.connection and self.playing:
            self.playing = False

    def toggle(self):
        self.status = not self.status

    def standby(self):
        if self.status:
            self.playing = False
            self.connection = False

    def change_player(self):
        if self.status and self.connection:
           available_services = ["spotify", "youtube", "cloud", "radio", "new_channel"]
           idex = available_services.index(self.current_service)
           self.current_service = available_services[idex +1 % len(available_services)]

    def increase_volume(self):
        if self.status and self.connection and self.playing and self.volume < 120:
            self.volume += 5

    def decrease_volume(self):
        if self.status and self.connection and self.playing and self.volume > 10:
            self.volume -= 5


class SmartSpeaker:
    def __init__(self):
        self.real_device = SmartSpeakerSimulator()
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

    def action1(self):
        # Not implemented

# SE:     def action2(self):
        # Not implemented

# SE:     def action3(self):
        # Not implemented

# SE:     def action4(self):
        # Not implemented

# SE:     def action5(self):
        # Not implemented

# SE:     def action6(self):
        # Not implemented

# SE:     def action7(self):
        # Not implemented

# SE:     def action8(self):
        # Not implemented







# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 
# SE: 


        print(f"reconnect()")
        self.real_device.reconnect()

# Connect the Speaker to the internet.
smartSpeaker = SmartSpeaker()



























































































































































































































































































































































































































































































































































































































































MS






































































































































MS














MS










MS













MS






MS




MS


MS






MS

MS

MS
MS



MS


MS


MS
MS



MMS

MS
MS


MS
MS

MS

MS

MS

MS
MS
MS
MS

MS
MSMS
MSMS

MS
MSMS

# SE: MSMS MS

MSMS
MS

MS
MS
MS
M
MS
MSMS
MS
MS
MS

MS
MSMS
MS
MS
MS


simulator = smartSpeaker.real_device
print(simulator.status and simulator.connection)