class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration


class Alarm:
    def __init__(self):
        self.real_device = AlarmSimulator()
        self.set = False
        self.vibration = False

    def action1(self):
        # Description of action 1
        pass

        print(f"set_alarm()")
        self.real_device.set_alarm()

    def action2(self):
        # Description of action 2
        pass

        print(f"unset()")
        self.real_device.unset()

    def action3(self):
        # Description of action 3
        pass



        print(f"toggle_vibration_mode()")
        self.real_device.toggle_vibration_mode()

# Turn on the alarm. But not on vibration
alarm = Alarm()
alarm.action1()
alarm.action2()
alarm.action3()
alarm.set(True)



simulator = alarm.real_device
print(simulator.set and not simulator.vibration)