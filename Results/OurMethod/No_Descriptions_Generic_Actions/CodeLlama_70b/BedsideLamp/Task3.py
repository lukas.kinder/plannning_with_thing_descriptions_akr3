class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False
        self.brightness = 50 # The brightness of the lamp (minimum = 0, maximum = 100)

    def action1(self):
        # Description: Turns the lamp on

        self.on = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Description: Sets the brightness

        # requires that the device is on
        if not self.on:
            self.action1()
        # requires that the brightness is between 0 and 100
        if self.brightness < 0:
            self.brightness = 0
        elif self.brightness > 100:
            self.brightness = 100
        else:
            self.brightness = self.brightness - 1

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        # Description: Toggles the lamp on/off

        self.on = not self.on

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        # Description: Sets the brightness to a specific value

        # requires that the device is on
        if not self.on:
            self.action1()
        # requires that the brightness is between 0 and 100
        if self.brightness < 0:
            self.brightness = 0
        elif self.brightness > 100:
            self.brightness = 100
        else:
            self.brightness = int(self.brightness)



        print(f"decrease()")
        self.real_device.decrease()

# Make it is bright as possible.
bedsideLamp = BedsideLamp()
bedsideLamp.action3()
bedsideLamp.action2()
bedsideLamp.action1()


simulator = bedsideLamp.real_device
print(simulator.on and simulator.brightness == 100)