from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}

    def action1(self):
        # Does something
        pass

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Does something
        pass

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self,value):
        # Sets the brightness of the light
        self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def action4(self,value):
        # Sets the saturation of the light
        self.state["sat"] = value



        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# Brightness to 30.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.action3(30)



simulator = ambientLightstrip.real_device
print(simulator.state['bri'] == 30)