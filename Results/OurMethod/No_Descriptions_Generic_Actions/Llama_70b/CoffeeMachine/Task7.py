import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False



class CoffeeMachine:
    def __init__(self):
        self.real_device = CoffeeMachineSimulator()
        self.water = True
        self.storage = True
        self.on = False

    def action1(self):
        pass

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        pass

        print(f"refill()")
        self.real_device.refill()

    def action3(self):
        pass

        print(f"empty()")
        self.real_device.empty()

    def action4(self):
        pass


        print(f"make_coffee()")
        self.real_device.make_coffee()

# make 22 coffee.
coffeeMachine = CoffeeMachine()
for i in range(22):
    coffeeMachine.action1()
    coffeeMachine.action2()
    coffeeMachine.action3()
    coffeeMachine.action4()


simulator = coffeeMachine.real_device
print(simulator.statistic['coffee_made'] == 22)