import random

class DryerSimulator:
    def __init__(self):
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def get_running(self):
        return self.running
    
    def get_temperature(self):
        return self.temperature
    
    def get_distilled_storage(self):
        return self.distilled_storage

    def start(self):
        if not self.running and self.distilled_storage > 0:
            self.running = True
            self.distilled_storage -= random.randint(10,30)

    def clear(self):
        self.distilled_storage = 100

    def increase_temperature(self):
        if not self.running and self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        if not self.running and self.temperature > 40:
            self.temperature -= 10


class Dryer:
    def __init__(self):
        self.real_device = DryerSimulator()
        self.running = False # True if the device is currently drying laundry
        self.temperature = 50 # minimum = 40, maximum = 80  - Displays the current drying temperature, temperature can not be changed during drying process
        self.distilled_storage = 50 # minimum = 0, maximum = 100 - Displays the current remaining storage for the distilled water output in percentage

    def start(self):
        # Starts drying process
        if self.distilled_storage == 0:
            self.clear()

        self.running = True

        print(f"start()")
        self.real_device.start()

    def clear(self):
        # Clear distilled water storage
        self.distilled_storage = 100

        print(f"clear()")
        self.real_device.clear()

    def increase_temperature(self):
        # Increases drying temperature by 10
        if self.temperature < 80:
            self.temperature += 10

        print(f"increase_temperature()")
        self.real_device.increase_temperature()

    def decrease_temperature(self):
        # Decreases drying temperature by 10
        if self.temperature > 40:
            self.temperature -= 10
        print(f"decrease_temperature()")
        self.real_device.decrease_temperature()

# Dry the cloth. But not hotter than 40 degree celsius.
dryer = Dryer()
while dryer.temperature > 40:
    dryer.decrease_temperature()
dryer.start()

simulator = dryer.real_device
print(simulator.running and simulator.temperature == 40)