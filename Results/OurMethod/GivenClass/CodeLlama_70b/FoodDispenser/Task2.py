import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True # States if the device is set on automatic or manual food dispension
        self.stored = False # Indicates whether enough food is available for the next dispension process

    def dispense(self):
        # Manually dispenses food for the animal

        # requires that there is food stored
        if not self.stored:
            self.refill()

        # Code to dispense food here

        print(f"dispense()")
        self.real_device.dispense()

    def refill(self):
        # refills the stored food
        self.stored = True

        print(f"refill()")
        self.real_device.refill()

    def set_state(self,state):
        # Sets the feeding bowl to automatic or manual
        # :param string state: The state the dispenser should be in

        if state == 'manual':
            self.automatic = False
        elif state == 'automatic':
            self.automatic = True
        print(f"set_state({state})")
        self.real_device.set_state(state)

# Turn to automatic and make sure the storage is full.
foodDispenser = FoodDispenser()
foodDispenser.set_state("automatic")
foodDispenser.refill()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Robot:
    def __init__(self):
        self.arm = False # Whether the arm is extended or retracted
        self.legs = False # Whether the legs are extended or retracted
        self.sensors = False # Whether the sensors are active or not

    def extend_arm(self):
        # Extends the arm
        self.arm = True

    def extend_legs(self):
        # Extends the legs
        self.legs = True

    def activate_sensors(self):
        # Activates the sensors
        self.sensors = True

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Extend the arm, extend the legs, and activate the sensors.".






simulator = foodDispenser.real_device
print(simulator.food_storage == 150 and simulator.automatic)