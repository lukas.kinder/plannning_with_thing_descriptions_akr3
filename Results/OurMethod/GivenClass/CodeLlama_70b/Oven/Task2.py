class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True


class Oven:
    def __init__(self):
        self.real_device = OvenSimulator()
        self.on = False # Property stating if the oven is currently turned on or off
        self.heating_method = "convection" # States the current heating method that is selected

    def switch_on(self):
        # Turns the oven on
        self.on = True

        print(f"switch_on()")
        self.real_device.switch_on()

    def switch_off(self):
        # Turns the oven off
        self.on = False

        print(f"switch_off()")
        self.real_device.switch_off()

    def change_heating_method(self):
        # Changes the currently selected heating method from convection to top and bottom or vice versa

        # requires that the device is on
        if not self.on:
            self.switch_on()

        if self.heating_method == "convection":
            self.heating_method = "top and bottom"
        else:
            self.heating_method = "convection"

        print(f"change_heating_method()")
        self.real_device.change_heating_method()

    def preheat(self):
        # Starts preheating the oven to 180 Degrees Celcius

        # requires that the device is on
        if not self.on:
            self.switch_on()

        # Code to preheat the oven here
        print(f"preheat()")
        self.real_device.preheat()

# Preheat the oven with convection mode.
oven = Oven()
oven.preheat()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class VacuumCleaner:
    def __init__(self):
        self.battery = 50 # Percentage of battery remaining

    def run(self):
        # Runs the vacuum cleaner
        self.battery -= 1

    def charge(self):
        # Charges the vacuum cleaner
        self.battery += 10

    def get_charge(self):
        # Returns the percentage of battery remaining
        return self.battery

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Charge the vacuum cleaner fully and then run it for 5 minutes.".






simulator = oven.real_device
print(simulator.is_preheating and simulator.heating_method == 'convection')