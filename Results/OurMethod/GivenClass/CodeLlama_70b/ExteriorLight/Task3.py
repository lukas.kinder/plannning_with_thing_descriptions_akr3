class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10


class ExteriorLight:
    def __init__(self):
        self.real_device = ExteriorLightSimulator()
        self.status = False # True if Light is turned on
        self.brightness = 100 # minimum = 0, maximum = 100 - Brightness level of the light in percentage

    def turn_on(self):
        # Turns the light on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the light off
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase(self):
        # Increases brightness by 10%

        # requires that the light is on
        if not self.status:
            self.turn_on()

        # Caps brightness at 100
        if self.brightness < 100:
            self.brightness += 10

        print(f"increase()")
        self.real_device.increase()

    def decrease(self):
        # Decreases brightness by 10%

        # requires that the light is on
        if not self.status:
            self.turn_on()

        # Caps brightness at 0
        if self.brightness > 0:
            self.brightness -= 10
        print(f"decrease()")
        self.real_device.decrease()

# Reduce the brightness to 20.
exteriorLight = ExteriorLight()
exteriorLight.decrease()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left in the car
        self.speed = 0 # Speed of the car in km/h
        self.direction = "forward" # Direction the car is facing
        self.lights = ExteriorLight() # Lighting system of the car

    def drive(self, distance):
        # Drives the car for the given distance
        # requires that the car has fuel
        if not self.fuel:
            print("Not enough fuel")
            return

        # requires that the car is moving forward
        if not self.direction == "forward":
            print("Car must be facing forward")
            return

        self.fuel -= distance * 3.6 # Consumption of fuel per km


    def turn_left(self):
        # Turns the car left
        self.direction = "left"

    def turn_right(self):
        # Turns the car right
        self.direction = "right"


# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive for 20 kilometers with the lights on full blast, then turn left and drive another 10 kilometers.".





simulator = exteriorLight.real_device
print(simulator.brightness == 20)