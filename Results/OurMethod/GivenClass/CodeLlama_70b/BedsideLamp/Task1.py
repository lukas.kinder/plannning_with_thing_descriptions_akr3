class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False # True if Light is turned on
        self.brightness = 50 # Brightness level of the light in percentage

    def turn_on(self):
        # Turn the lamp on
        self.on = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turn the lamp off
        self.on = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase(self):
        # increase the brightness of the lamp

        # requires that the lamp is on
        if not self.on:
            self.turn_on()

        # Increase the brightness by 10 to a maximum of 100
        self.brightness = min(100,self.brightness +10)

        print(f"increase()")
        self.real_device.increase()

    def decrease(self):
        # decrease the brightness of the lamp

        # requires that the lamp is on
        if not self.on:
            self.turn_on()

        # Decrease the brightness by 10 to a minimum of 0
        if self.brightness > 0:
            self.brightness = max(0,self.brightness -10)
        print(f"decrease()")
        self.real_device.decrease()

# Turn it on.
bedsideLamp = BedsideLamp()
bedsideLamp.turn_on()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class VacuumCleaner:
    def __init__(self):

        self.power = False # Whether the vacuum cleaner is on
        self.bucket = 0 # Amount of dirt in the bucket (minimum = 0, maximum = 10)

    def turn_on(self):
        # Turn the vacuum cleaner on
        self.power = True

    def turn_off(self):
        # Turn the vacuum cleaner off
        self.power = False

    def empty_bucket(self):
        # Empty the bucket
        self.bucket = 0

    def fill_bucket(self,amount):
        # Adds a certain amount of dirt to the bucket
        # :param integer amount: The amount of dirt to be added

        # requires that the device is on
        if not self.power:
            self.turn_on()

        # requires that the device has enough space for the amount of dirt
        if self.bucket < amount:
            self.empty_bucket()

        self.bucket += amount

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Fill the bucket with 5 dirt.".



# Fill the bucket with 5 dirt.
VacuumCleaner = VacuumCleaner()
VacuumCleaner.fill_bucket(5)









simulator = bedsideLamp.real_device
print(simulator.on)