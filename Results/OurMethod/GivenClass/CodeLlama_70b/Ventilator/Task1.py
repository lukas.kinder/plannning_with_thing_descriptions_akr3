class VentilatorSimulator:
    def __init__(self):
        self.status = "off"
        self.level = 3

    def get_status(self):
        return self.status
    
    def get_level(self):
        return self.level

    def start(self):
        if self.status == "off":
            self.status = "on"

    def stop(self):
        if self.status == "on":
            self.status = "off"

    def speed_up(self):
        if self.status == "on" and self.level < 3:
            self.level += 1

    def speed_down(self):
        if self.status == "on" and self.level > 1:
            self.level -= 1


class Ventilator:
    def __init__(self):
        self.real_device = VentilatorSimulator()
        self.status = 'off' # Displays the current status of the device
        self.level = 3   # 1,2 or 3 - Displays the current fan speed level

    def start(self):
        # Starts the ventilator
        self.status = 'on'

        print(f"start()")
        self.real_device.start()

    def stop(self):
        # Stops the ventilator
        self.status = 'off'

        print(f"stop()")
        self.real_device.stop()

    def speed_up(self):
        # Increases fan level

        # requires that the device is on
        if self.status != 'on':
            self.start()

        if self.level < 3:  # Level can't be greater than 3
            self.level += 1

        print(f"speed_up()")
        self.real_device.speed_up()

    def speed_down(self):
        # Decreases fan level

        # requires that the device is on
        if self.status != 'on':
            self.start()

        if self.level > 1:  # Level can't be less than 1
            self.level -= 1
        print(f"speed_down()")
        self.real_device.speed_down()

# I need some ventilation.
ventilator = Ventilator()
ventilator.start()
ventilator.speed_up()
ventilator.speed_down()


simulator = ventilator.real_device
print(simulator.status == 'on')