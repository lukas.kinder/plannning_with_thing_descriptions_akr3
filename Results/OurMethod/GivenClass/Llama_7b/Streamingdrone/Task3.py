import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False # Indicates if the drone is currently recording or not
        self.streaming = False # Indicates whether the drone is currently live streaming
        self.battery = 50 # minimum = 0, maximum = 100 - Represents the current battery in percentage

    def stream(self):
        # Starts streaming

        # requires battery to have energy left
        if self.battery == 0:
            self.recharge()

        self.streaming = True

        print(f"stream()")
        self.real_device.stream()

    def stop_stream(self):
        # Stops streaming
        self.streaming = False

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def start_recording(self):
        # Starts the recording

        # requires battery to have energy left
        if self.battery == 0:
            self.recharge()

        self.recording = True

        print(f"start_recording()")
        self.real_device.start_recording()

    def end(self):
        # Ends recording process deleting not saved recording
        self.recording = False

        print(f"end()")
        self.real_device.end()

    def recharge(self):
        # Recharges the battery, can not be recharged during streaming or recording

        # requires that the device is not streaming
        if self.streaming:
            self.stop_stream()
        # requires that the device is not currently recording
        if self.recording:
            self.start_recording()

        self.battery = 100
        print(f"recharge()")
        self.real_device.recharge()

# Record a video. Recharge the drone after you are done.
streamingdrone = Streamingdrone()
streamingdrone.start_recording()
streamingdrone.end()
streamingdrone.recharge()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left in the tank
        self.speed = 0 # Current speed of the car
        self.acceleration = 0 # Acceleration of the car

    def accelerate(self):
        # Increases the acceleration of the car
        self.acceleration += 0.1

    def decelerate(self):
        # Decreases the acceleration of the car
        self.acceleration -= 0.1

    def turn(self, angle):
        # Turns the car

        # requires that the car has enough fuel
        if self.fuel < 0:
            self.decelerate()

        # requires that the car is moving
        if not self.speed:
            self.accelerate()

        # :param float angle: The angle to turn in radians

        self.angle = angle * math.pi / 180

    def drive(self, distance):
        # Drives the car for the specified distance

        # requires that the car has enough fuel
        if self.fuel < distance:
            self.decelerate()

        # requires that the car is moving
        if not self.speed:
            self.accelerate()

        self.fuel -= distance

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive from point A to point B. Drive 50 kilometers after reaching point B.".


# Drive from point A to point B. Drive 50 kilometers after reaching point B.
car = Car()
car.drive(50)









simulator = streamingdrone.real_device
print(simulator.battery == 100 and simulator.statistic['n_recordings'] == 1)