import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False # Indicates if the drone is currently recording or not
        self.streaming = False # Indicates whether the drone is currently live streaming
        self.battery = 50 # minimum = 0, maximum = 100 - Represents the current battery in percentage

    def stream(self):
        # Starts streaming

        # requires battery to have energy left
        if self.battery == 0:
            self.recharge()

        self.streaming = True

        print(f"stream()")
        self.real_device.stream()

    def stop_stream(self):
        # Stops streaming
        self.streaming = False

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def start_recording(self):
        # Starts the recording

        # requires battery to have energy left
        if self.battery == 0:
            self.recharge()

        self.recording = True

        print(f"start_recording()")
        self.real_device.start_recording()

    def end(self):
        # Ends recording process deleting not saved recording
        self.recording = False

        print(f"end()")
        self.real_device.end()

    def recharge(self):
        # Recharges the battery, can not be recharged during streaming or recording

        # requires that the device is not streaming
        if self.streaming:
            self.stop_stream()
        # requires that the device is not currently recording
        if self.recording:
            self.start_recording()

        self.battery = 100
        print(f"recharge()")
        self.real_device.recharge()

# Make 12 videos with the drone. Stream all of them.
streamingdrone = Streamingdrone()
for i in range(1,13):
    streamingdrone.start_recording()
    streamingdrone.stream()
    time.sleep(30) # Wait for 30 seconds before starting next recording



simulator = streamingdrone.real_device
print(simulator.statistic['n_recordings'] == 12 and simulator.statistic['n_streams'] == 12)