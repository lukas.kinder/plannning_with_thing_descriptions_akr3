class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False # Indicates if the kettle is currently turned on and ready for the heating process or in standby
        self.filled = False # Indicates wether the kettle is filled with water or not

    def switch_power(self):
        # Turns the kettle on or off depending on the current state
        self.power = not self.power

        print(f"switch_power()")
        self.real_device.switch_power()

    def heat(self):
        # Start the heating process of the kettle

        # requires that the device is on
        if not self.power:
            self.switch_power()

        # requires that the kettle is filled
        if not self.filled:
            self.fill()

        print(f"heat()")
        self.real_device.heat()

    def fill(self):
        # Fills the kettle with water
        self.filled = True

        print(f"fill()")
        self.real_device.fill()

    def empty(self):
        # Empties the kettle
        self.filled = False
        print(f"empty()")
        self.real_device.empty()

# Turn the kettle on.
kettle = Kettle()
kettle.switch_power()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Robot:
    def __init__(self):

        self.arm = False # Indicates if the arm is currently raised or lowered
        self.leg = False # Indicates if the leg is currently extended or retracted
        self.eye = False # Indicates if the eye is currently open or closed

    def raise_Arm(self):
        # Raises the arm
        self.arm = True

    def extend_Leg(self):
        # Extends the leg
        self.leg = True

    def open_Eye(self):
        # Opens the eye
        self.eye = True

    def close_Eye(self):
        # Closes the eye
        self.eye = False

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfillks the task: "Raise the arm, extend the leg, and open the eye.".






simulator = kettle.real_device
print(simulator.power)