import random

class DryerSimulator:
    def __init__(self):
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def get_running(self):
        return self.running
    
    def get_temperature(self):
        return self.temperature
    
    def get_distilled_storage(self):
        return self.distilled_storage

    def start(self):
        if not self.running and self.distilled_storage > 0:
            self.running = True
            self.distilled_storage -= random.randint(10,30)

    def clear(self):
        self.distilled_storage = 100

    def increase_temperature(self):
        if not self.running and self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        if not self.running and self.temperature > 40:
            self.temperature -= 10


class Dryer:
    def __init__(self):
        self.real_device = DryerSimulator()
        self.running = False # True if the device is currently drying laundry
        self.temperature = 50 # minimum = 40, maximum = 80  - Displays the current drying temperature, temperature can not be changed during drying process
        self.distilled_storage = 50 # minimum = 0, maximum = 100 - Displays the current remaining storage for the distilled water output in percentage

    def start(self):
        # Starts drying process
        if self.distilled_storage == 0:
            self.clear()

        self.running = True

        print(f"start()")
        self.real_device.start()

    def clear(self):
        # Clear distilled water storage
        self.distilled_storage = 100

        print(f"clear()")
        self.real_device.clear()

    def increase_temperature(self):
        # Increases drying temperature by 10
        if self.temperature < 80:
            self.temperature += 10

        print(f"increase_temperature()")
        self.real_device.increase_temperature()

    def decrease_temperature(self):
        # Decreases drying temperature by 10
        if self.temperature > 40:
            self.temperature -= 10
        print(f"decrease_temperature()")
        self.real_device.decrease_temperature()

# Start the dryer.
dryer = Dryer()
dryer.start()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left
        self.speed = 0 # Current speed of the car

    def drive(self,distance):
        # Drives the car for the specified distance
        # :param integer distance: The distance to be driven
        # requires that the car has fuel
        if self.fuel < distance:
            self.fuel += distance

        self.speed += distance / 100

    def fill_fuel(self,fuel):
        # Adds the specified amount of fuel to the car
        # :param integer fuel: The amount of fuel to be added
        # requires that the car has fuel
        self.fuel += fuel

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive the car 200 miles and fill it with 5 gallons of gas.".





simulator = dryer.real_device
print(simulator.running)