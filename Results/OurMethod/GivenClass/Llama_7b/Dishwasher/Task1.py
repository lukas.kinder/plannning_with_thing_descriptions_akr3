class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False # States whether the dishwasher is currently running and washing dishes or not
        self.door = "closed" # Represents if the dishwasher is currently closed or open
    
    def close(self):
        # Closes the door
        self.door = "closed"
    
        print(f"close()")
        self.real_device.close()

    def open(self):
        # Opens the door
        # Requires that the dishwasher is not running
        if self.running:
            self.stop()

        self.door = "open"

        print(f"open()")
        self.real_device.open()

    def stop(self):
        # Stops the washing process
        self.running = False

        print(f"stop()")
        self.real_device.stop()

    def start(self):
        # Starts the washing process
        # Requires that the door is closed
        if self.door == "open":
            self.close()

        self.running = True
        print(f"start()")
        self.real_device.start()

# Start the dishwasher
dishwasher = Dishwasher()
dishwasher.start()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class VacuumCleaner:
    def __init__(self):
        self.power = False # States whether the vacuum cleaner is currently turned on or off
        self.filter = None # Represents the current filter used in the vacuum cleaner
        self.bag = None # Represents the current bag used in the vacuum cleaner

    def turn_on(self):
        # Turns the vacuum cleaner on
        self.power = True

    def turn_off(self):
        # Turns the vacuum cleaner off
        self.power = False

    def change_filter(self, new_filter):
        # Changes the filter of the vacuum cleaner
        self.filter = new_filter

    def empty_bag(self):
        # Empties the bag of the vacuum cleaner
        self.bag = None

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Change the filter and empty the bag".






simulator = dishwasher.real_device
print(simulator.running)