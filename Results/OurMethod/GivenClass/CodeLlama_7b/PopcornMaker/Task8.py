class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0


class PopcornMaker:

    def __init__(self):
        self.real_device = PopcornMakerSimulator()
        self.power = False # True if machine is turned on and ready for use

    def toggle(self):
        # Turns the device on or off based on the current state
        self.power = not self.power

        print(f"toggle()")
        self.real_device.toggle()

    def add_corn(self):
        # Adds corn to the heating section of the maker

        # Code to add corn here
        pass

        print(f"add_corn()")
        self.real_device.add_corn()

    def add_sugar(self):
        # Adds sugar to the heating section making the popcorn sweet

        # Code to add sugar here
        pass

        print(f"add_sugar()")
        self.real_device.add_sugar()

    def add_salt(self):
        # Adds salt to the heating section making the popcorn salty

        # Code to add salt here
        pass

        print(f"add_salt()")
        self.real_device.add_salt()

    def heat(self):
        # Start heating up the heating section, turning added corn into popcorn
        # requires that the device is on
        if not self.power:
            self.toggle()

        # Code to heat popcorn maker here
        print(f"heat()")
        self.real_device.heat()

# Make some extra sweet popcorn. It should have twice as much sugar as normal.
popcornMaker = PopcornMaker()
popcornMaker.toggle()
popcornMaker.add_corn()
popcornMaker.add_sugar()
popcornMaker.add_salt()
popcornMaker.heat()
popcornMaker.toggle()


simulator = popcornMaker.real_device
print(simulator.statistic['made_extra_sweet_popcorn'] == 1)