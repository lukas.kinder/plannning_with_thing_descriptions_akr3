class SmartHeaterSimulator:
    def __init__(self):
        self.level = 4
        self.status = False

    def get_level(self):
        return self.level
    
    def get_status(self):
        return self.status

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase_level(self):
        if self.status and self.level < 6:
            self.level += 1

    def decrease_level(self):
        if self.status and self.level > 0:
            self.level -= 1


class SmartHeater:
    def __init__(self):
        self.real_device = SmartHeaterSimulator()
        self.level = 4 # from 0 (minimum) to 6 (maximum) - Integer Value indicating the current heating level of the device. Higher values represent higher temperatures.
        self.status = False # Boolean Value that describes if the device is on or off

    def turn_on(self):
        # Turns the Heating Device on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the Heating device off
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase_level(self):
        # "Increases the Heating level of the device.

        # requires that the device is on
        if not self.status:
            self.turn_on()
        # if the level is less than the max, increase it
        if self.level < 6:
            self.level += 1

        print(f"increase_level()")
        self.real_device.increase_level()

    def decrease_level(self):
        # Decreases the Heating level of the device.

        # requires that the device is on
        if not self.status:
            self.turn_on()
        # if the level is more than the min, decrease it
        if self.level > 0:
            self.level -= 1
        print(f"decrease_level()")
        self.real_device.decrease_level()

# Turn the heater on.
smartHeater = SmartHeater()
smartHeater.turn_on()


simulator = smartHeater.real_device
print(simulator.status)