from copy import deepcopy
class ObjectOverflow(Exception):
    pass

class AutomatedWarehouseSimulator:
    def __init__(self):
        self.currentProductPosition = {
            "row1col1" : "Wood",
            "row1col2" : "Pipe",
            "row2col2" : "PlasticContainer",
            "row3col1" : "WaterBottles",
            "row3col2" : "Trash",
            "row3col3" : "Cables"
        }

        self.object_inside = None  # wether there is an object inside on the conveyor belt
        self.object_in_arm = None # wether the robotic arm is holding something
        self.arm_is_home = True # if the arm is at the 'home position'
        self.object_waiting_to_get_inside = ["Glass", "FuelContainer"]
        self.objects_moved_out = []

    def get_currentProductPosition(self):
        return deepcopy( self.currentProductPosition )

    def goHome(self):
        self.arm_is_home  = True

    def pickObjectAtPosition(self, row, column):
        # Pick object at a specified warehouse position
        self.arm_is_home  = False

        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            if self.object_in_arm != None:
                raise ObjectOverflow
            self.object_in_arm = self.currentProductPosition[position]
            del self.currentProductPosition[position]

    def putObjectAtPosition(self, row, column):
        # Put object at a specified warehouse position
        self.arm_is_home  = False
        if self.object_in_arm == None:
            #nothing to put
            return
        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            raise ObjectOverflow
        self.currentProductPosition[position] = self.object_in_arm
        self.object_in_arm = None
        

    def pickObjectFromConveyor(self):
        self.arm_is_home  = False
        # Pick object from a conveyor belt

        if self.object_in_arm != None:
            raise ObjectOverflow

        if self.object_inside != None:
            self.object_in_arm = self.object_inside
            self.object_inside = None

    def putObjectOnConveyor(self):
        self.arm_is_home  = False
        if self.object_in_arm != None:
            # An object is put on the conveyor belt
            if self.object_inside:
                #  There is already an object inside. 
                raise ObjectOverflow
            self.object_inside = self.object_in_arm
            self.object_in_arm = None

    def rollObjectForward(self):
        # An object is moved inside using the conveyor belt.
        if self.object_inside:
            #  There is already an object inside. 
            raise ObjectOverflow
        
        if len(self.object_waiting_to_get_inside) != 0:
            self.object_inside = self.object_waiting_to_get_inside.pop()

    def rollObjectBackward(self):
        if self.object_inside:
            self.objects_moved_out.append(self.object_inside)
            self.object_inside = None


class AutomatedWarehouse:
    def __init__(self):
        self.real_device = AutomatedWarehouseSimulator()
        # Current position of product 
        self.currentProductPosition = {
            "row1col1" : "Wood",
            "row1col2" : "Pipe",
            "row2col2" : "PlasticContainer",
            "row3col1" : "WaterBottles",
            "row3col2" : "Trash",
            "row3col3" : "Cables"
        }

        self.holding_object = None

    def goHome(self):
        # Sends the arm home
        pass

        print(f"goHome()")
        self.real_device.goHome()

    def pickObjectAtPosition(self,row,column):
        # Picks an object at a given position
        # :param integer row: the row where the product is
        # :param integer column: the column where the product is
        position = f"row{row}col{column}"
        self.holding_object = self.currentProductPosition[position]
        del self.currentProductPosition[position]

        print(f"pickObjectAtPosition({row},{column})")
        self.real_device.pickObjectAtPosition(row, column)

    def putObjectAtPosition(self,row,column):
        # Put object at a specified warehouse position
        self.arm_is_home  = False
        position = f"row{row}col{column}"
        self.currentProductPosition[position] = self.holding_object
        self.holding_object = None

        print(f"putObjectAtPosition({row},{column})")
        self.real_device.putObjectAtPosition(row, column)

    def pickObjectFromConveyor(self):
        # Pick object from a conveyor belt

        self.arm_is_home  = False
        self.holding_object = "object"
        self.object_inside = None

        print(f"pickObjectFromConveyor()")
        self.real_device.pickObjectFromConveyor()

    def putObjectOnConveyor(self):
        self.arm_is_home  = False
        self.holding_object = None

        print(f"putObjectOnConveyor()")
        self.real_device.putObjectOnConveyor()

    def rollObjectForward(self):
        # Start the conveyor until object reaches inside sensors
        pass

        print(f"rollObjectForward()")
        self.real_device.rollObjectForward()

    def rollObjectBackward(self):
        # Start the conveyor until object reaches outside sensors
        pass
        print(f"rollObjectBackward()")
        self.real_device.rollObjectBackward()

# Roll the conveyor forward. This will cause a new object to be inside that can be picked up. Take the object and place it at an empty spot.
automatedWarehouse = AutomatedWarehouse()
automatedWarehouse.rollObjectForward()
automatedWarehouse.pickObjectFromConveyor()

# Identify the first empty spot to place the item.
for row in range(1, 4):
    for col in range(1, 4):
        position = f"row{row}col{col}"
        if position not in automatedWarehouse.currentProductPosition:
            automatedWarehouse.putObjectAtPosition(row, col)
            break
    else:
        continue
    break

simulator = automatedWarehouse.real_device
print('FuelContainer' in simulator.currentProductPosition.values())