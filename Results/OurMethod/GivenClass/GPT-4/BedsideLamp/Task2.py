class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)


class BedsideLamp:
    def __init__(self):
        self.real_device = BedsideLampSimulator()
        self.on = False # True if Light is turned on
        self.brightness = 50 # Brightness level of the light in percentage

    def turn_on(self):
        # Turn the lamp on
        self.on = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turn the lamp off
        self.on = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase(self):
        # increase the brightness of the lamp

        # requires that the lamp is on
        if not self.on:
            self.turn_on()

        # Increase the brightness by 10 to a maximum of 100
        self.brightness = min(100,self.brightness +10)

        print(f"increase()")
        self.real_device.increase()

    def decrease(self):
        # decrease the brightness of the lamp

        # requires that the lamp is on
        if not self.on:
            self.turn_on()

        # Decrease the brightness by 10 to a minimum of 0
        if self.brightness > 0:
            self.brightness = max(0,self.brightness -10)
        print(f"decrease()")
        self.real_device.decrease()

# Give me a brightness of 30
bedsideLamp = BedsideLamp()
while bedsideLamp.brightness > 30:
    bedsideLamp.decrease()
while bedsideLamp.brightness < 30:
    bedsideLamp.increase()

simulator = bedsideLamp.real_device
print(simulator.on and simulator.brightness == 30)