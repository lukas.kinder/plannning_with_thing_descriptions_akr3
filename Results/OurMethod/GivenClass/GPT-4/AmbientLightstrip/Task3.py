from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {
            'on': False,    # Boolean Value that describes if the device is on or off
            'bri': 254,     # Value between 0 and 254 indicating the Brightness of the Light
            'sat': 128      # Sets the intensity of the colours. Value between 0 and 254, with 254 indicating maximum saturation
        }

    def turn_on(self):
        # Turns on the device
        self.state['on'] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns off the device
        self.state['on'] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self, value):
        # Sets the brightness
        # :param integer value: The value to set the brightness to

        # Requires that the device is on
        if not self.state['on']:
            self.turn_on()
        if value < 255 and value >= 0:
            self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def set_saturation(self, value):
        # Sets the saturation
        # :param integer value: The value to set the saturation to

        # Requires that the device is on
        if not self.state['on']:
            self.turn_on()
        if value < 255 and value >= 0:
            self.state["sat"] = value
        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# Give me some light with maximum saturation.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.set_saturation(254)

simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['sat'] == 254)