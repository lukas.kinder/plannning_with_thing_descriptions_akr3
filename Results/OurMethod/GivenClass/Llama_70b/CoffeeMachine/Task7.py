import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False



class CoffeeMachine:
    def __init__(self):
        self.real_device = CoffeeMachineSimulator()
        self.water = True # Illustrates water level of the machine. If false the machine does not contain enough water to make a coffIee and needs to be refilled
        self.storage = True # Indicates the remaining coffee ground storage. If false no space is remaining and the container needs to be emptied.
        self.on = False # Is the machine on or off

    def toggle(self):
        # Toggles the power status of the coffee machine 
        self.on = not self.on

        print(f"toggle()")
        self.real_device.toggle()

    def refill(self):
        # Refills the water tank of the machine
        self.water = True

        print(f"refill()")
        self.real_device.refill()

    def empty(self):
        # Empties coffee ground container
        self.storage = True

        print(f"empty()")
        self.real_device.empty()

    def make_coffee(self):
        # Creates a medium cup of coffee

        if not self.on:
            self.toggle()
        # Refill if there's no water or coffee grains
        if not self.water:
            self.refill()
        if not self.storage:
            self.empty()
        print(f"make_coffee()")
        self.real_device.make_coffee()

# make 22 coffee.
coffeeMachine = CoffeeMachine()
coffeeMachine.make_coffee()
for i in range(22):
    coffeeMachine.make_coffee()


simulator = coffeeMachine.real_device
print(simulator.statistic['coffee_made'] == 22)