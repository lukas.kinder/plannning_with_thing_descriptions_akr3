from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {"status": False}  # power status of the Display
        self.content = 'Video'  # the current content showing on the Display

    def toggle(self):
        # Toggle power on/off of Display
        self.power["status"] = not self.power["status"]

        print(f"toggle()")
        self.real_device.toggle()

    def setVolume(self, value):
        # Set the volume of the Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to set volume would be here

        print(f"setVolume({value})")
        self.real_device.setVolume(value)

    def setBright(self,value):
        # Set the brightness of the Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to set brightness would be here

        print(f"setBright({value})")
        self.real_device.setBright(value)

    def showText(self,text):
        # Show text on Display
        # :param string text: The text that should be displayed

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to show text would be here
        
        print(f"showText({text})")
        self.real_device.showText(text)

    def playVideo(self, video_info):
        # Play video on Display
        # :param string identifier, name, description, url: Video details

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        video_identifier = video_info["identifier"]
        video_name = video_info["name"]
        video_description = video_info["description"]
        video_url = video_info["url"]

        # Code to play video would be here

        print(f"playVideo({video_info})")
        self.real_device.playVideo(video_info)

    def pauseVideo(self):
        # Pause video on Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to pause video would be here
        
        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def stopVideo(self):
        # Stop video on Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to stop video would be here
        
        print(f"stopVideo()")
        self.real_device.stopVideo()

    def presentationWebApp(self, app_info):
        # Presentation of web app on Display
        # :param string identifier, name, description, url: Web app details

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        app_identifier = app_info["identifier"]
        app_name = app_info["name"]
        app_description = app_info["description"]
        app_url = app_info["url"]

        # Code to present web app goes here

        print(f"presentationWebApp({app_info})")
        self.real_device.presentationWebApp(app_info)

    def launchNewsApp(self):
        # Launch news app on Display

        # requires that the device is on
        if not self.power["status"]:
            self.toggle()

        # Code to launch news app would be here
        print(f"launchNewsApp()")
        self.real_device.launchNewsApp()

# Show me a video with url 'https://www.youtube.com/watch?v=UIySlKLIVBo'. Use  name: 'chess', description: 'some game' and identifier: 'NA'.
display = Display()
display.playVideo({"identifier":"NA", "name":"chess", "description":"some game", "url":"https://www.youtube.com/watch?v=UIySlKLIVBo"})


simulator = display.real_device
print(simulator.content == 'Video' and simulator.video_details['identifier'] == 'NA' and simulator.video_details['name'] == 'chess' and simulator.video_details['description'] == 'some game'and simulator.video_details['url'] == 'https://www.youtube.com/watch?v=UIySlKLIVBo')