class VentilatorSimulator:
    def __init__(self):
        self.status = "off"
        self.level = 3

    def get_status(self):
        return self.status
    
    def get_level(self):
        return self.level

    def start(self):
        if self.status == "off":
            self.status = "on"

    def stop(self):
        if self.status == "on":
            self.status = "off"

    def speed_up(self):
        if self.status == "on" and self.level < 3:
            self.level += 1

    def speed_down(self):
        if self.status == "on" and self.level > 1:
            self.level -= 1


class Ventilator:
    def __init__(self):
        self.real_device = VentilatorSimulator()
        self.status = "off"
        self.level = 3

    def action1(self):
        # Starts the ventilator
        self.status = "on"

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Stops the ventilator
        self.status = "off"

        print(f"stop()")
        self.real_device.stop()

    def action3(self):
        # Increases fan level
        self.level += 1

        print(f"speed_up()")
        self.real_device.speed_up()

    def action4(self):
        # Decreases fan level
        self.level -= 1


        print(f"speed_down()")
        self.real_device.speed_down()

# Turn on the ventilator with level 1.
ventilator = Ventilator()
ventilator.action1()





simulator = ventilator.real_device
print(simulator.status == 'on' and simulator.level == 1)