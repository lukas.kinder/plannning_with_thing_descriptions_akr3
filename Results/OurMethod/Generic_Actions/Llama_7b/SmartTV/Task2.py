class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False # True if the TV is currently recording the selected channel
        self.off = True # Indicates if the TV is turned off(True) or turned on (False)
        self.brightness = 70 # Describes brightness of the TV screen in percent
        self.channel = "ZDF" # States the currently selected channel that will be displayed if the device is turned on

    def action1(self):
        # Starts recording the current channel
        self.recording = True

        print(f"record()")
        self.real_device.record()

    def action2(self):
        # Switches to a different channel
        pass

        print(f"switch()")
        self.real_device.switch()

    def action3(self):
        # Turns the TV on
        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def action4(self):
        # Turns the TV off
        self.off = True

        print(f"turn_off()")
        self.real_device.turn_off()

    def action5(self,value):
        # Sets the brightness of the tv.
        self.brightness = value



        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# Start recording whats currently on tv.
smartTV = SmartTV()
smartTV.action1()


simulator = smartTV.real_device
print(simulator.recording)