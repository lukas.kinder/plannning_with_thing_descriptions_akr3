class SmartRadioDeviceSimulator:
    def __init__(self):
        self.power = "on"
        self.channel = "Jamz"

    def get_power(self):
        return self.power
    
    def get_channel(self):
        return self.channel

    def power_on(self):
        if self.power == "off":
            self.power = "on"

    def switch_channel(self):
        if self.power == "on":
            channels = ["MTV", "Jamz", "The Beat", "The Mix"]
            current_index = channels.index(self.channel)
            next_index = (current_index + 1) % len(channels)
            self.channel = channels[next_index]

    def power_off(self):
        if self.power == "on":
            self.power = "off"


class SmartRadioDevice:
    def __init__(self):
        self.real_device = SmartRadioDeviceSimulator()
        self.power = "on"
        self.channel = "Jamz"

    def action1(self):
        # Turns the radio on

        self.power = "off"
        self.power = "on"

        print(f"power_on()")
        self.real_device.power_on()

    def action2(self):
        # Switches channel of the radio to the next available channel

        # requires that the device is on
        if not self.power:
            self.action1()

        channel_list = ["MTV", "Jamz", "The Beat", "The Mix"]
        current_channel = self.channel
        next_channel = (current_channel + 1) % len(channel_list)
        self.channel = channel_list[next_channel]


        print(f"switch_channel()")
        self.real_device.switch_channel()

# Play MTV on the radio.
smartRadioDevice = SmartRadioDevice()
smartRadioDevice.action2()


simulator = smartRadioDevice.real_device
print(simulator.power and simulator.channel == 'MTV')