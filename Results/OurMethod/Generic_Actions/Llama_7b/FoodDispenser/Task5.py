import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True
        self.stored = False

    def action1(self):
        # Manually dispenses food for the animal
        pass

        print(f"dispense()")
        self.real_device.dispense()

    def action2(self):
        # reloads the stored food
        pass

        print(f"refill()")
        self.real_device.refill()

    def action3(self, mode):
        # Sets the feeding bowl to automatic or manual
        if mode == "manual":
            self.automatic = False
        elif mode == "automatic":
            self.automatic = True





        print(f"set_state({mode})")
        self.real_device.set_state(mode)

# Give 22 portions of food.
foodDispenser = FoodDispenser()
foodDispenser.action1()
for i in range(22):
    foodDispenser.action2()



simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 10)