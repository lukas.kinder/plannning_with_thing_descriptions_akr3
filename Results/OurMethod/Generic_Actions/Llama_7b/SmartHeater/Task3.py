class SmartHeaterSimulator:
    def __init__(self):
        self.level = 4
        self.status = False

    def get_level(self):
        return self.level
    
    def get_status(self):
        return self.status

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase_level(self):
        if self.status and self.level < 6:
            self.level += 1

    def decrease_level(self):
        if self.status and self.level > 0:
            self.level -= 1


class SmartHeater:
    def __init__(self):
        self.real_device = SmartHeaterSimulator()
        self.level = 4 # Integer Value indicating the current heating level of the device. Higher values represent higher temperatures. Maximum value is 6, minimum value is 0
        self.status = False # Boolean Value that describes if the device is on or off

    def action1(self):
        # Turns the Heating Device on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Turns the Heating device off
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        # Increases the Heating level of the device.
        self.level += 1

        print(f"increase_level()")
        self.real_device.increase_level()

    def action4(self):
        # Decreases the Heating level of the device.
        self.level -= 1





        print(f"decrease_level()")
        self.real_device.decrease_level()

# Turn the heater on. But only level 1.
smartHeater = SmartHeater()
smartHeater.action1()


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left in the tank
        self.speed = 0 # Current speed of the vehicle
        self.direction = "forward" # Direction the vehicle is facing

    def moveForward(self):
        # Moves the vehicle forward
        self.direction = "forward"

    def turnRight(self):
        # Turns the vehicle right
        self.direction = "right"

    def turnLeft(self):
        # Turns the vehicle left
        self.direction = "left"

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive forward for 5 miles.".






simulator = smartHeater.real_device
print(simulator.status and simulator.level == 1)