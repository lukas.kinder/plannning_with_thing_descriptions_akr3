class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10


class ExteriorLight:
    def __init__(self):
        self.real_device = ExteriorLightSimulator()
        self.status = False # True if Light is turned on
        self.brightness = 100 # Brightness level of the light in percentage

    def action1(self):
        # Turns the light on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Turns the light off
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self):
        # Increases brightness by 10%
        self.brightness += 10

        print(f"increase()")
        self.real_device.increase()

    def action4(self):
        # Decreases brightness by 10%
        self.brightness -= 10


        print(f"decrease()")
        self.real_device.decrease()

# Light!
exteriorLight = ExteriorLight()
exteriorLight.action1()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class House:
    def __init__(self):
        self.exterior_light = ExteriorLight()
        self.interior_light = InteriorLight()

    def action1(self):
        # Turns the exterior light on
        self.exterior_light.action1()

    def action2(self):
        # Turns the interior light on
        self.interior_light.action1()

    def action3(self):
        # Dims the interior light
        self.interior_light.action3()



# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Dim the interior light for 1 hour.".






simulator = exteriorLight.real_device
print(simulator.status)