import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False



class CoffeeMachine:
    def __init__(self):
        self.real_device = CoffeeMachineSimulator()
        self.water = True # Illustrates water level of the machine. If False the machine does not contain enough water and needs to be refilled
        self.storage = True # Indicates the remaining coffee ground storage. If False no space is remaining and the container needs to be emptied
        self.on = False # Displays the current power state of the machine

    def action1(self):
        # Toggles the on property of the device
        self.on = not self.on

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # Refills the water tank of the machine
        self.water = True

        print(f"refill()")
        self.real_device.refill()

    def action3(self):
        # Empties coffee ground container
        self.storage = True

        print(f"empty()")
        self.real_device.empty()

    def action4(self):
        # Creates a medium cup of coffee

        # Requires that the machine is on
        if not self.on:
            self.action1()
        # Requires that the water is filled
        if not self.water:
            self.action2()
        # Requires that the storage is not full
        if not self.storage:
            self.action3()

        # Code to make the coffee here
        
        # After making coffee, assume water is used and storage is filled
        self.water = False
        self.storage = False

        print(f"make_coffee()")
        self.real_device.make_coffee()

# Make me 12 coffee
coffeeMachine = CoffeeMachine()
for i in range(12):
    coffeeMachine.action4()
    if i < 11: # No need to refill water or empty storage after the last coffee
        coffeeMachine.action2()
        coffeeMachine.action3()

simulator = coffeeMachine.real_device
print(simulator.statistic['coffee_made'] == 12)