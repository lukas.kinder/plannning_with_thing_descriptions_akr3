class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False # Indicates if the kettle is currently turned on and ready for the heating process or in standby
        self.filled = False # Indicates whether the kettle is filled with water or not

    def action1(self):
        # Turns the kettle on or off depending on the current state
        self.power = not self.power

        print(f"switch_power()")
        self.real_device.switch_power()

    def action2(self):
        # Start the heating process of the kettle

        # requires that the device is on and is filled with water
        if not self.power:
            self.action1()
        if not self.filled:
            self.action3()

        # Code to heat the water here

        print(f"heat()")
        self.real_device.heat()

    def action3(self):
        # Fills the kettle with water
        self.filled = True

        print(f"fill()")
        self.real_device.fill()

    def action4(self):
        # Empties the kettle with water
        self.filled = False

        print(f"empty()")
        self.real_device.empty()

# Fill the kettle.
kettle = Kettle()
kettle.action3()

simulator = kettle.real_device
print(simulator.filled)