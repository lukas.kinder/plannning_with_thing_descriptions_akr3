class AirConditionerSimulator:
    def __init__(self):
        self.goal_Temperature = 22
        self.status = False

    def get_goal_Temperature(self):
        return self.goal_Temperature
    
    def get_status(self):
        return self.status
    
    def toggle(self):
        self.status = not self.status

    def temperature_Up(self):
        if self.status:
            self.goal_Temperature = min(35,self.goal_Temperature +1)

    def temperature_Down(self):
        if self.status:
            self.goal_Temperature = max(10,self.goal_Temperature -1)


class AirConditioner:
    def __init__(self):
        self.real_device = AirConditionerSimulator()
        # Indicates temperature the air conditioner aims to reach
        self.goal_Temperature = 22
        # Status of the device indicating if the device is turned on(true) or off (false)
        self.status = False

    def action1(self):
        # Toggles status property, turning the device either off or on depending on the current status
        self.status = not self.status

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # Increases the goal temperature by 1 degree celsius.

        # requires that the device is on
        if not self.status:
            self.action1()

        if self.goal_Temperature < 35:
            self.goal_Temperature += 1

        print(f"temperature_Up()")
        self.real_device.temperature_Up()

    def action3(self):
        # Decreases the goal temperature by 1 degree celsius.

        # requires that the device is on
        if not self.status:
            self.action1()

        if self.goal_Temperature > 10:
            self.goal_Temperature -= 1

        print(f"temperature_Down()")
        self.real_device.temperature_Down()

# Turn the Conditioner on.
airConditioner = AirConditioner()
if not airConditioner.status:
    airConditioner.action1()

simulator = airConditioner.real_device
print(simulator.status)