class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False # Boolean value representing the status of the device. If true the device is reachable and able to execute actions
        self.tank = "empty" # Describes the current state of the waste water tank
        self.humidity = 45 # Current humidity level the device aims to reach (minimum = 0, maximum = 100)

    def action1(self):
        # Toggle status of the device turning it either on or off

        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def action2(self):
        # Starts the dehumidification process

        # requires that the device is on and the tank is not full
        if not self.status or self.tank == 'full':
            if not self.status:
                self.action1()
            if self.tank == 'full':
                self.action3()

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def action3(self):
        # Drains waste water tank
        self.tank = 'empty'

        print(f"drain()")
        self.real_device.drain()

    def action4(self,value):
        # Sets the goal humidity
        # :param integer value: the desired humidity level

        # requires that the device is on and the tank is not full
        if not self.status or self.tank == 'full':
            if not self.status:
                self.action1()
            if self.tank == 'full':
                self.action3()

        if value >= 0 and value <= 100:
            self.humidity = value

        print(f"set_humidity({value})")
        self.real_device.set_humidity(value)

# I want this room to have a humidity of 50
dehumidifier = Dehumidifier()
dehumidifier.action4(50)

simulator = dehumidifier.real_device
print(simulator.status and simulator.humidity == 50)