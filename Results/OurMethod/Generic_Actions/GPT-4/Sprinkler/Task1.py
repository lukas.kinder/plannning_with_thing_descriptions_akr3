class SprinklerSimulator:
    def __init__(self):
        self.water = True
        self.on = False

        self.stored_water = 4 # initial stored water. Can store up to 12

        self.statistic = {"n_sprinkle" : 0}

    def get_water(self):
        return self.water
    
    def get_on(self):
        return self.on

    def power_on(self):
        if not self.on:
            self.on = True

    def power_off(self):
        if self.on:
            self.on = False

    def sprinkle(self):
        if self.on and self.water:
            self.stored_water -=1

            if self.stored_water == 0:
                self.water = False

            self.statistic['n_sprinkle'] +=1
  
    def refill(self):
        self.water = True
        self.stored_water = 12


class Sprinkler:
    def __init__(self):
        self.real_device = SprinklerSimulator()
        self.water = True # States whether the water tank is full or empty
        self.on = False # Describes whether the device is turned on or off

    def action1(self):
        # Turns power of the watering system on
        self.on = True

        print(f"power_on()")
        self.real_device.power_on()

    def action2(self):
        # Turns power of the watering system off
        self.on = False

        print(f"power_off()")
        self.real_device.power_off()

    def action3(self):
        # Initiates watering process wetting all surrounding plants

        # requires that the device is on
        if not self.on:
            self.action1()
        
        # requires that water tank has water
        if not self.water:
            self.action4()
            
        # Code to water the plants here

        # after watering, the water tank needs refill
        self.water = False

        print(f"sprinkle()")
        self.real_device.sprinkle()

    def action4(self):
        # Refills water tank
        self.water = True

        print(f"refill()")
        self.real_device.refill()

# Turn the sprinkler on.
sprinkler = Sprinkler()
sprinkler.action1()

simulator = sprinkler.real_device
print(simulator.on)