class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False
        self.door = "closed"

    def action1(self):
        # Closes the door
        self.door = "closed"

        print(f"close()")
        self.real_device.close()

    def action2(self):
        # Opens the door
        self.door = "open"

        print(f"open()")
        self.real_device.open()

    def action3(self):
        # Stops the washing process
        self.running = False

        print(f"stop()")
        self.real_device.stop()

    def action4(self):
        # Starts the washing process
        self.running = True



        print(f"start()")
        self.real_device.start()

# Start the dishwasher
dishwasher = Dishwasher()
dishwasher.action4()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Light:
    def __init__(self):
        self.brightness = 50 # Brightness of the light (0-100)


    def increaseBrightness(self):
        # Increases the brightness of the light
        self.brightness += 10

    def decreaseBrightness(self):
        # Decreases the brightness of the light
        self.brightness -= 10

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Make the light brighter".





simulator = dishwasher.real_device
print(simulator.running)