from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}

    def action1(self):
        # Turns the Light on
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Turns the Light off
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self, value):
        # Sets the brightness
        self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def action4(self, value):
        # Sets the saturation
        self.state["sat"] = value



        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# Brightness to 30.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.action3(30)
ambientLightstrip.action4(30)


# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left in the tank
        self.speed = 0 # Current speed of the car
        self.accelerator = 0 # Accelerator pedal position
        self.brake = 0 # Brake pedal position

    def accelerate(self):
        # Accelerates the car
        self.accelerator += 1

    def brake(self):
        # Brakes the car
        self.brake += 1

    def increase_fuel(self):
        # Increases the fuel level
        self.fuel += 1

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Drive for 10 minutes at a speed of 30 km/h, then brake to a stop.".



# Drive for 10 minutes at a speed of 30 km/h, then brake to a stop.
car = Car()
for i in range(10):
    car.accelerate()
    time.sleep(1)

    car.brake()
    time.sleep(1)










simulator = ambientLightstrip.real_device
print(simulator.state['bri'] == 30)