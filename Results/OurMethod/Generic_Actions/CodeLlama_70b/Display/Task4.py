from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {"status": False}
        self.content = "Video"

    def action1(self):
        # Toggle Power Status
        self.power["status"] = not self.power["status"]

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # Set Volume
        pass

        print(f"setVolume()")
        self.real_device.setVolume()

    def action3(self):
        # Set Brightness
        pass

        print(f"setBright()")
        self.real_device.setBright()

    def action4(self):
        # Show Text
        pass

        print(f"showText()")
        self.real_device.showText()

    def action5(self):
        # Play Video
        pass

        print(f"playVideo()")
        self.real_device.playVideo()

    def action6(self):
        # Pause Video
        pass

        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def action7(self):
        # Stop Video
        pass

        print(f"stopVideo()")
        self.real_device.stopVideo()

    def action8(self):
        # Launch Web App
        pass

        print(f"presentationWebApp()")
        self.real_device.presentationWebApp()

    def action9(self):
        # Launch News App
        pass


        print(f"launchNewsApp()")
        self.real_device.launchNewsApp()

# Show me a video with url 'https://www.youtube.com/watch?v=UIySlKLIVBo'. Use  name: 'chess', description: 'some game' and identifier: 'NA'.
display = Display()
display.action1()
display.action2("chess")
display.action3()
display.action4()



simulator = display.real_device
print(simulator.content == 'Video' and simulator.video_details['identifier'] == 'NA' and simulator.video_details['name'] == 'chess' and simulator.video_details['description'] == 'some game'and simulator.video_details['url'] == 'https://www.youtube.com/watch?v=UIySlKLIVBo')