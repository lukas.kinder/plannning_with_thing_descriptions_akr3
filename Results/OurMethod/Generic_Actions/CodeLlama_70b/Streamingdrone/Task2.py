import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False # Indicates if the drone is currently recording or not
        self.streaming = False # Indicates whether the drone is currently live streaming
        self.battery = 50 # Represents the current battery in percentage

    def action1(self):
        # Starts streaming
        self.streaming = True

        print(f"stream()")
        self.real_device.stream()

    def action2(self):
        # Stops streaming
        self.streaming = False

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def action3(self):
        # Starts the recording
        self.recording = True

        print(f"start_recording()")
        self.real_device.start_recording()

    def action4(self):
        # Ends recording process deleting not saved recording
        if self.recording:
            self.recording = False
            del self.recording

        print(f"end()")
        self.real_device.end()

    def action5(self):
        # Recharges the battery,
        # can not be recharged during streaming or recording
        if self.streaming or self.recording:
            raise ValueError("Can not recharge the battery while streaming or recording")
        else:
            self.battery += 10


        print(f"recharge()")
        self.real_device.recharge()

# Start recording a video and also stream it.
streamingdrone = Streamingdrone()
streamingdrone.action1()
streamingdrone.action2()
streamingdrone.action3()


simulator = streamingdrone.real_device
print(simulator.recording and simulator.streaming)