# Was not able to entangle with real device! (Are still unconnected)
class ChillerSimulator:
    def __init__(self):
        self.type = "" #Type of the chiller
        self.firmware = "v1.0" #Firmware version
        self.model = "" #Chiller model
        self.location = "" #Location of the chiller
        self.latitude = 0 #Latitude of the chiller
        self.longitude = 0 #Longitude of the chiller
        self.location_tag = "" #Location tag
        self.floor_tag = "" #floor level
        self.campus_tag = "" #name of the campus
        self.online = False #online status
        self.temperature = 20 #temperature
        self.temperature_unit = "C" #C for Celsius, F for Fahrenheit
        self.humidity = 30 #humidity of the chiller
        self.humidity_unit = "percentage" #percentage or grams per m3
        self.pressure = 1.5 #pressure
        self.pressure_unit = "bar" #psi or pascal or bar
        self.ora_latitude = 0 #ora_latitude
        self.ora_longitude = 0 #ora_longitude
        self.ora_altitude = 0 #ora_altitude
        self.ora_uncertainty = 0 #ora_accuracy
        self.ora_zone = "" #ora_zone
        self.ora_txPower = 0 #ora_mssi
        self.ora_rssi = 0 #ora_rssi

        self.n_rebooted  = 0

    # Getter functions
    def get_type(self):
        return self.type

    def get_firmware(self):
        return self.firmware

    def get_model(self):
        return self.model

    def get_location(self):
        return self.location

    def get_latitude(self):
        return self.latitude

    def get_longitude(self):
        return self.longitude

    def get_location_tag(self):
        return self.location_tag

    def get_floor_tag(self):
        return self.floor_tag

    def get_campus_tag(self):
        return self.campus_tag

    def get_online(self):
        return self.online

    def get_temperature(self):
        return self.temperature

    def get_temperature_unit(self):
        return self.temperature_unit

    def get_humidity(self):
        return self.humidity

    def get_humidity_unit(self):
        return self.humidity_unit

    def get_pressure(self):
        return self.pressure

    def get_pressure_unit(self):
        return self.pressure_unit

    def get_ora_latitude(self):
        return self.ora_latitude

    def get_ora_longitude(self):
        return self.ora_longitude

    def get_ora_altitude(self):
        return self.ora_altitude

    def get_ora_uncertainty(self):
        return self.ora_uncertainty

    def get_ora_zone(self):
        return self.ora_zone

    def get_ora_txPower(self):
        return self.ora_txPower

    def get_ora_rssi(self):
        return self.ora_rssi
    
    #actions
    def reboot(self):
        self.n_rebooted +=1

    def firmwareUpdate(self, version):
        self.firmware = version

    def emergencyValveRelease(self):
        self.pressure = 1

    def increasePressure(self, value):
        if value >= 0 and value <=5:
            self.pressure += value
class Chiller:
    def __init__(self):
        self.type = '' # Type of the chiller
        self.firmware = 'v1.0' # Firmware version
        self.model = '' # Model of the chiller
        self.location = '' # Location of the chiller
        self.latitude = 0 # Latitude of the chiller
        self.longitude = 0 # Longitude of the chiller
        self.location_tag = '' # Location tag of the chiller
        self.floor_tag = '' # Floor tag of the chiller
        self.campus_tag = '' # Campus tag of the chiller
        self.online = False # Online status of the chiller
        self.temperature = 20 # Temperature of the chiller
        self.temperature_unit = 'C' # Temperature unit of the chiller
        self.humidity = 30 # Humidity of the chiller
        self.humidity_unit = 'percentage' # Humidity unit of the chiller
        self.pressure = 1.5 # Pressure of the chiller
        self.pressure_unit = 'bar' # Pressure unit of the chiller
        self.ora_latitude = 0 # Ora latitude of the chiller
        self.ora_longitude = 0 # Ora longitude of the chiller
        self.ora_altitude = 0 # Ora altitude of the chiller
        self.ora_uncertainty = 0 # Ora uncertainty of the chiller
        self.ora_zone = '' # Ora zone of the chiller
        self.ora_txPower = 0 # Ora txPower of the chiller
        self.ora_rssi = 0 # Ora rssi of the chiller

    def action1(self):
        # Reboot the chiller
        print("Rebooting the chiller...")

    def action2(self, firmware_version):
        # Update the firmware to the version specified in the argument
        print("Updating the firmware to {}...".format(firmware_version))

    def action3():
        # Open valve for emergency reasons
        print("Opening valve for emergency reasons...")

    def action4(self, pressure):
        # Increase the pressure with the value in the argument
        print("Increasing pressure to {}...".format(pressure))

    def get_temperature_range(self):
        # Returns the temperature range of the chiller
        pass

    def get_humidity_range(self):
        # Returns the humidity range of the chiller
        pass

    def get_pressure_range(self):
        # Returns the pressure range of the chiller
        pass

    def get_ora_latitude(self):
        # Returns the Ora latitude of the chiller
        pass

    def get_ora_longitude(self):
        # Returns the Ora longitude of the chiller
        pass

    def get_ora_altitude(self):
        # Returns the Ora altitude of the chiller
        pass

    def get_ora_uncertainty(self):
        # Returns the Ora uncertainty of the chiller
        pass

    def get_ora_zone(self):
        # Returns the Ora zone of the chiller
        pass

    def get_ora_txPower(self):
        # Returns the Ora txPower of the chiller
        pass

    def get_ora_rssi(self):
        # Returns the Ora rssi of the chiller
        pass



# Update the chiller to 'v2.3'.
chiller = Chiller()
chiller.action2('v2.3')

simulator = chiller.real_device
print(simulator.firmware == 'v2.3')