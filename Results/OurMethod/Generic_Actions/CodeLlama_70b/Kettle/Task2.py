class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False # Indicates if the kettle is currently turned on and ready for the heating process or in standby
        self.filled = False # Indicates whether the kettle is filled with water or not

    def action1(self):
        # Turns the kettle on or off depending on the current state
        self.power = not self.power

        print(f"switch_power()")
        self.real_device.switch_power()

    def action2(self):
        # Starts the heating process of the kettle

        # requires that the device is on
        if not self.power:
            self.action1()

        print(f"heat()")
        self.real_device.heat()

    def action3(self):
        # Fills the kettle with water

        # requires that the device is on
        if not self.power:
            self.action1()

        print(f"fill()")
        self.real_device.fill()

    def action4(self):
        # Empties the kettle with water

        # requires that the device is on
        if not self.power:
            self.action1()

        print(f"empty()")
        self.real_device.empty()

# Turn the kettle on.
kettle = Kettle()
kettle.action1()




# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Car:
    def __init__(self):
        self.fuel = 0 # Amount of fuel left in the car
        self.speed = 0 # Speed of the car
        self.direction = "forward" # Direction the car is facing

    def move(self, distance):
        # Moves the car forward
        # :param integer distance: The distance to move

        # requires that the car is facing the correct direction
        if self.direction != "backward":
            self.fuel -= distance * 0.01
            self.speed += distance * 0.01
            self.direction += distance * 0.01

    def stop(self):
        # Stops the car
        self.fuel = 0
        self.speed = 0

    def reverse(self, distance):
        # Reverses the car
        # :param integer distance: The distance to reverse

        # requires that the car is facing the correct direction
        if self.direction != "backward":
            self.fuel += distance * 0.01
            self.speed -= distance * 0.01
            self.direction -= distance * 0.01

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Travel 5 kilometers forward and then stop.".





simulator = kettle.real_device
print(simulator.power)