import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100


class VacuumRobot:
    def __init__(self):
        self.real_device = VacuumRobotSimulator()
        self.cleaning = False # States if the robot is currently cleaning or not
        self.storage_full = True # States whether the storage of the robot is full or not
        self.battery = 30 # Describes the current power charge in percent
        self.mapped = True # States whether a current mapping of the room is available, needed in order to start the vacuuming
        self.at_base = True # States whether the robot is currently docked at the charging base station

    def action1(self):
        # Start vacuuming the room, only valid if a mapping for the room and sufficient storage and battery are available

        # requires that the device is on
        if not self.at_base:
            self.action4()

        # requires that the device has a map of the room
        if not self.mapped:
            self.action2()

        # requires that the device has enough space in storage
        if self.storage_full:
            self.action3()

        # requires that the device has enough power
        if self.battery < 70:
            self.action5()

        # Code to vacuum the room here
        self.cleaning = True

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Scans room, creating a mapping

        # requires that the device is on
        if not self.at_base:
            self.action4()

        # requires that the device does not already have a map of the room
        if self.mapped:
            self.mapped = False

        # Code to scan room here
        self.mapped = True

        print(f"scan()")
        self.real_device.scan()

    def action3(self):
        # Empties storage

        # requires that the device is on
        if not self.at_base:
            self.action4()

        # requires that the device has something stored
        if self.storage_full:
            self.storage_full = False

        print(f"empty_storage()")
        self.real_device.empty_storage()

    def action4(self):
        # Returns to the base, enables further actions

        # requires that the device is not on
        if self.at_base:
            self.at_base = False

        # requires that the device is not cleaning
        if self.cleaning:
            self.cleaning = False

        print(f"return_to_base()")
        self.real_device.return_to_base()

    def action5(self):
        # Charges robot

        # requires that the device is not on
        if self.at_base:
            self.at_base = False

        # requires that the device has enough power
        if self.battery >= 100:
            return

        # Code to charge the robot here
        self.battery = 100


        print(f"charge()")
        self.real_device.charge()

# Clean the room. Afterwards, return to the base, empty the storage and recharge.
vacuumRobot = VacuumRobot()
vacuumRobot.action1()
vacuumRobot.action4()
vacuumRobot.action3()
vacuumRobot.action5()


simulator = vacuumRobot.real_device
print(simulator.statistic['n_cleaned'] == 1 and simulator.battery == 100 and not simulator.storage_full)