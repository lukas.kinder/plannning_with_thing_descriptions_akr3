from datetime import datetime

class CounterSimulator:
    def __init__(self):
        self.count = 0  # current counter value
        self.countAsImage = None  # current counter value as SVG image
        self.redDotImage = None  # Red dot image as PNG
        self.lastChange = ""  # last change of counter value

        self.max_value = 0
        self.min_value = 0

    def get_count(self):
        return self.count
    
    def get_countAsImage(self):
        return self.countAsImage
    
    def get_redDotImage(self):
        return self.redDotImage
    
    def get_lastChange(self):
        return self.lastChange

    def increment(self):
        # increment value
        self.lastChange = str(datetime.now())
        self.count += 1

        if self.count > self.max_value:
            self.max_value = self.count

    def decrement(self):
        # decrement value
        self.lastChange = str(datetime.now())
        self.count -= 1
        if self.min_value > self.count:
            self.min_value = self.count

    def reset(self):
        # Resetting counter value
        self.lastChange = str(datetime.now())
        self.count = 0


class Counter:
    def __init__(self):
        self.real_device = CounterSimulator()
        self.count = 0 # current counter value
        self.countAsImage = None # current counter value as SVG image
        self.redDotImage = None # Red dot image as PNG
        self.lastChange = "" # last change of counter value

    def action1(self):
        # increment value

        self.count += 1

        print(f"increment()")
        self.real_device.increment()

    def action2(self):
        # decrement value

        self.count -= 1

        print(f"decrement()")
        self.real_device.decrement()

    def action3(self):
        # Resetting counter value

        self.count = 0
        self.lastChange = ""


        print(f"reset()")
        self.real_device.reset()

# Increment by 30, then decrement 50 times.
counter = Counter()
for i in range(50):
    counter.action2()
counter.action1()


simulator = counter.real_device
print(simulator.max_value == 30 and simulator.count == -20)