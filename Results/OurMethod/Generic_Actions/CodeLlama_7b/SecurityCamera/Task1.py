import random

class SecurityCameraSimulator:
    def __init__(self):
        self.recording = False
        self.storage = 256

        self.statistic = {"recorded_videos" : 0}

    def get_recording(self):
        return self.recording
    
    def get_storage(self):
        return self.storage

    def record(self):
        if not self.recording and self.storage > 0:
            self.recording = True

    def stop(self):
        if self.recording:
            self.recording = False

            recording_size = random.randint(0,3)
            self.storage -= recording_size

            self.statistic['recorded_videos'] += 1

    def delete(self):
        if not self.recording:
            self.storage = 256


class SecurityCamera:
    def __init__(self):
        self.real_device = SecurityCameraSimulator()
        self.recording = False
        self.storage = 256

    def action1(self):
        # Starts the recording

        self.recording = True

        print(f"record()")
        self.real_device.record()

    def action2(self):
        # Stops the recording

        self.recording = False

        print(f"stop()")
        self.real_device.stop()

    def action3(self):
        # Deletes video files freeing storage

        # requires that there is storage left
        if self.storage == 0:
            return

        self.storage += 256



        print(f"delete()")
        self.real_device.delete()

# Start recording
securityCamera = SecurityCamera()
securityCamera.action1()


simulator = securityCamera.real_device
print(simulator.recording)