import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False



class CoffeeMachine:
    def __init__(self):
        self.real_device = CoffeeMachineSimulator()
        self.water = True # Illustrates water level of the machine. If false the machine does not contain enough water to make a coffIee and needs to be refilled
        self.storage = True # Indicates the remaining coffee ground storage. If false no space is remaining and the container needs to be emptied
        self.on = False # Displays the current power state of the machine

    def action1(self):
        # Toggles on property of the device
        self.on = not self.on

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # Refills the water tank of the machine

        # requires that the device is off
        if self.on:
            self.action1()

        self.water = True

        print(f"refill()")
        self.real_device.refill()

    def action3(self):
        # Empties coffee ground container

        # requires that the device is off
        if self.on:
            self.action1()

        self.storage = False

        print(f"empty()")
        self.real_device.empty()

    def action4(self):
        # Creates a medium cup of coffee

        # requires that the device is on
        if not self.on:
            self.action1()

        # requires that there is enough water
        if not self.water:
            self.action2()

        # requires that there is enough storage
        if not self.storage:
            self.action3()

        self.storage = False


        print(f"make_coffee()")
        self.real_device.make_coffee()

# Make me a coffee. Refill and empty the machine once you are done.
coffeeMachine = CoffeeMachine()
coffeeMachine.action4()
coffeeMachine.action2()
coffeeMachine.action3()


simulator = coffeeMachine.real_device
print(simulator.statistic['coffee_made'] == 1 and simulator.empty_storage == 20 and simulator.water_left == 10)