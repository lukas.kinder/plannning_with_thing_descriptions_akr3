import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True # States if the device is set on automatic or manual food dispension
        self.stored = False # Indicates whether enough food is available for the next dispension process

    def action1(self):
        # Manually dispenses food for the animal

        # requires that the device is on
        if not self.automatic:
            self.stored = True

        print(f"dispense()")
        self.real_device.dispense()

    def action2(self):
        # refills the stored food

        # requires that the device is on
        if not self.automatic:
            self.stored = True

        print(f"refill()")
        self.real_device.refill()

    def action3(self,option):
        # Sets the feeding bowl to automatic or manual
        # :param string option: Set state to either "manual" or "automatic"

        # requires that the device is on
        if not self.automatic:
            self.stored = True

        if option == "manual":
            self.automatic = False
        elif option == "automatic":
            self.automatic = True
        else:
            raise ValueError("Invalid input")


        print(f"set_state({option})")
        self.real_device.set_state(option)

# Refill the dispenser.
foodDispenser = FoodDispenser()
if not foodDispenser.automatic:
    foodDispenser.action2()


simulator = foodDispenser.real_device
print(simulator.stored)