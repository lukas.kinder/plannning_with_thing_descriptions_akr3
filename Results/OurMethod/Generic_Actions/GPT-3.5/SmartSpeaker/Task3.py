class SmartSpeakerSimulator:
    def __init__(self):
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

        self.current_service = "spotify"

    def get_connection(self):
        return self.connection
    
    def get_playing(self):
        return self.playing
    
    def get_status(self):
        return self.status
    
    def get_volume(self):
        return self.volume

    def reconnect(self):
        if self.status and not self.connection:
            self.connection = True

    def play(self):
        if self.status and self.connection and not self.playing:
            self.playing = True

    def pause(self):
        if self.status and self.connection and self.playing:
            self.playing = False

    def toggle(self):
        self.status = not self.status

    def standby(self):
        if self.status:
            self.playing = False
            self.connection = False

    def change_player(self):
        if self.status and self.connection:
           available_services = ["spotify", "youtube", "cloud", "radio", "new_channel"]
           idex = available_services.index(self.current_service)
           self.current_service = available_services[idex +1 % len(available_services)]

    def increase_volume(self):
        if self.status and self.connection and self.playing and self.volume < 120:
            self.volume += 5

    def decrease_volume(self):
        if self.status and self.connection and self.playing and self.volume > 10:
            self.volume -= 5


class SmartSpeaker:
    def __init__(self):
        self.real_device = SmartSpeakerSimulator()
        self.connection = False  # States whether the speaker is connected to the Internet or not
        self.playing = False  # States whether the speaker is currently playing music or not
        self.status = False  # Indicates if the speaker is currently switched on or off
        self.volume = 50  # The speaker volume in db (minimum = 10, maximum = 120)

    def action1(self):
        # Reconnects device with the internet, enabling music to be played if connection was lost
        self.connection = True

        print(f"reconnect()")
        self.real_device.reconnect()

    def action2(self):
        # Starts playing music from the selected player
        # requires that the device is on
        if not self.status:
            self.action4()
        self.playing = True

        print(f"play()")
        self.real_device.play()

    def action3(self):
        # Pauses music if currently playing
        self.playing = False

        print(f"pause()")
        self.real_device.pause()

    def action4(self):
        # turn the speaker on of off
        self.status = not self.status

        print(f"toggle()")
        self.real_device.toggle()

    def action5(self):
        # Puts the device into standby, making it unavailable
        self.status = False

        print(f"standby()")
        self.real_device.standby()

    def action6(self):
        # Changes music streaming service.
        pass  # Placeholder for changing music streaming service

        print(f"change_player()")
        self.real_device.change_player()

    def action7(self):
        # Increases the volume by 5 db
        if self.volume < 120:
            self.volume += 5

        print(f"increase_volume()")
        self.real_device.increase_volume()

    def action8(self):
        # Decreases volume by 5 db
        if self.volume > 10:
            self.volume -= 5

        print(f"decrease_volume()")
        self.real_device.decrease_volume()

# Play me some music.
smartSpeaker = SmartSpeaker()
smartSpeaker.action4()
smartSpeaker.action1()
smartSpeaker.action2()

simulator = smartSpeaker.real_device
print(simulator.playing)