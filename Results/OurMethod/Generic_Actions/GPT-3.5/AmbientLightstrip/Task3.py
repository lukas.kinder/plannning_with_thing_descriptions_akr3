from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {'on': False, 'bri': 254, 'sat': 128}

    def action1(self):
        # Turns the Light on
        self.state['on'] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def action2(self):
        # Turns the Light off
        self.state['on'] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def action3(self, brightness):
        # Sets the brightness
        # :param integer brightness: Value between 0 and 254 indicating the Brightness of the Light

        self.state['bri'] = brightness

        print(f"set_brightness({brightness})")
        self.real_device.set_brightness(brightness)

    def action4(self, saturation):
        # Sets the saturation
        # :param integer saturation: Value between 0 and 254 indicating the saturation of the colors

        self.state['sat'] = saturation

        print(f"set_saturation({saturation})")
        self.real_device.set_saturation(saturation)

# Give me some light with maximum saturation.
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.action1()
ambientLightstrip.action4(254)

simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['sat'] == 254)