import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True # States if the device is set on automatic or manual food dispension
        self.stored = False # Indicates whether enough food is available for the next dispension process

    def action1(self):
        # Manually dispenses food for the animal

        self.stored = False

        print(f"dispense()")
        self.real_device.dispense()

    def action2(self):
        # refills the stored food
        self.stored = True

        print(f"refill()")
        self.real_device.refill()

    def action3(self,state):
        # Sets the feeding bowl to automatic or manual
        # :param string state: "manual" or "automatic"
        
        if state == "manual" or state == "automatic":
            self.automatic = (state == "automatic")

        print(f"set_state({state})")
        self.real_device.set_state(state)

# Turn to automatic and make sure the storage is full.
foodDispenser = FoodDispenser()
foodDispenser.action2()
foodDispenser.action3("automatic")

simulator = foodDispenser.real_device
print(simulator.food_storage == 150 and simulator.automatic)