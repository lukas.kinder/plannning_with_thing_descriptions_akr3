import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100


class VacuumRobot:
    def __init__(self):
        self.real_device = VacuumRobotSimulator()
        self.cleaning = False # States if the robot is currently cleaning or not
        self.storage_full = True # States whether the storage of the robot is full or not
        self.battery = 30  # Describes the current power charge in percent
        self.mapped = True  # States whether a current mapping of the room is available, needed in order to start the vacuuming
        self.at_base = True  # States whether the robot is currently docked at the charging base station

    def action1(self):
        # Starts vacuuming the room, only valid if a mapping for the room and sufficient storage and battery are available

        # requires that the robot has a mapping for the room, sufficient storage, and enough battery
        if self.mapped and not self.storage_full and self.battery > 20:
            self.cleaning = True

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Scans room, creating a mapping

        self.mapped = True  # Assuming the scanning is successful and a mapping is created

        print(f"scan()")
        self.real_device.scan()

    def action3(self):
        # Empties storage
        self.storage_full = False

        print(f"empty_storage()")
        self.real_device.empty_storage()

    def action4(self):
        # Returns to the base, enables further actions

        self.cleaning = False
        self.at_base = True

        print(f"return_to_base()")
        self.real_device.return_to_base()

    def action5(self):
        # Charges robot
        self.battery = 100  # Assuming the robot gets fully charged after this action

        print(f"charge()")
        self.real_device.charge()

# Recharge the battery and empty the storage.
vacuumRobot = VacuumRobot()
vacuumRobot.action5()
vacuumRobot.action3()

simulator = vacuumRobot.real_device
print(simulator.battery == 100 and not simulator.storage_full)