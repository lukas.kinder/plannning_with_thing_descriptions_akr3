class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0


class PopcornMaker:
    def __init__(self):
        self.real_device = PopcornMakerSimulator()
        self.power = False  # True if machine is turned on and ready for use

    def action1(self):
        # Turns the device on or off based on the current state
        self.power = not self.power

        print(f"toggle()")
        self.real_device.toggle()

    def action2(self):
        # Adds corn to the heating section of the maker

        # requires that the device is on
        if not self.power:
            self.action1()

        print(f"add_corn()")
        self.real_device.add_corn()

    def action3(self):
        # Adds sugar to the heating section making the popcorn sweet

        # requires that the device is on
        if not self.power:
            self.action1()

        print(f"add_sugar()")
        self.real_device.add_sugar()

    def action4(self):
        # Adds salt to the heating section making the popcorn salty

        # requires that the device is on
        if not self.power:
            self.action1()

        print(f"add_salt()")
        self.real_device.add_salt()

    def action5(self):
        # Start heating up the heating section, turning added corn into popcorn

        # requires that the device is on
        if not self.power:
            self.action1()

        # Code to heat and pop the corn here

        print(f"heat()")
        self.real_device.heat()

# Make 5 portions of sweet popcorn
popcornMaker = PopcornMaker()
for _ in range(5):
    popcornMaker.action3()
    popcornMaker.action2()
    popcornMaker.action5()

# Make 3 portions of salty popcorn
for _ in range(3):
    popcornMaker.action4()
    popcornMaker.action2()
    popcornMaker.action5()

simulator = popcornMaker.real_device
print(simulator.statistic['made_sweet_popcorn'] == 5 and simulator.statistic['made_salty_popcorn'] == 3)