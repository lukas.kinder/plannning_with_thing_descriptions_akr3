import random

class DryerSimulator:
    def __init__(self):
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def get_running(self):
        return self.running
    
    def get_temperature(self):
        return self.temperature
    
    def get_distilled_storage(self):
        return self.distilled_storage

    def start(self):
        if not self.running and self.distilled_storage > 0:
            self.running = True
            self.distilled_storage -= random.randint(10,30)

    def clear(self):
        self.distilled_storage = 100

    def increase_temperature(self):
        if not self.running and self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        if not self.running and self.temperature > 40:
            self.temperature -= 10


class Dryer:
    def __init__(self):
        self.real_device = DryerSimulator()
        self.running = False # True if the device is currently drying laundry
        self.temperature = 50  # Displays the current drying temperature
        self.distilled_storage = 50  # Displays the current remaining storage for the distilled water output in percentage

    def action1(self):
        # Starts drying process
        self.running = True

        print(f"start()")
        self.real_device.start()

    def action2(self):
        # Clear distilled water storage
        self.distilled_storage = 0

        print(f"clear()")
        self.real_device.clear()

    def action3(self):
        # Increases drying temperature by 10

        # requires that the device is not running
        if not self.running:
            self.temperature += 10

        print(f"increase_temperature()")
        self.real_device.increase_temperature()

    def action4(self):
        # Decreases drying temperature by 10

        # requires that the device is not running
        if not self.running:
            self.temperature -= 10

        print(f"decrease_temperature()")
        self.real_device.decrease_temperature()

# Dry the cloth. But not hotter than 40 degree celsius.
dryer = Dryer()
while dryer.temperature > 40:
    dryer.action4()
dryer.action1()

simulator = dryer.real_device
print(simulator.running and simulator.temperature == 40)