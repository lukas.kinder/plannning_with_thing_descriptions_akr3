class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration


class Alarm:
    def __init__(self):
        self.real_device = AlarmSimulator()
        self.set = False # Indicates whether alarm is currently set or not
        self.vibration = False # States whether the alarm is set to vibration only mode

    def action1(self):
        # Sets alarm,
        # ringing or vibration at the set time

        # requires that the alarm is not set
        if self.set:
            self.action2()

        self.set = True

        print(f"set_alarm()")
        self.real_device.set_alarm()

    def action2(self):
        # Unsets alarm

        self.set = False

        print(f"unset()")
        self.real_device.unset()

    def action3(self):
        # Switches vibration only mode on or off depending on the current setting.

        # requires that the alarm is set
        if not self.set:
            self.action1()

        # requires that the alarm is set to vibration only mode
        if self.vibration:
            self.vibration = False
        else:
            self.vibration = True


        print(f"toggle_vibration_mode()")
        self.real_device.toggle_vibration_mode()

# I want you to set an alarm with vibration.
alarm = Alarm()
alarm.action1()
alarm.action3()


simulator = alarm.real_device
print(simulator.set and simulator.vibration)