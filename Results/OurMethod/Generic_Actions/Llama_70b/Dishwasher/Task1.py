class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1


class Dishwasher:
    def __init__(self):
        self.real_device = DishwasherSimulator()
        self.running = False # States whether the dishwasher is currently running and washing dishes or not
        self.door = "closed" # Represents if the dishwasher is currently closed or open

    def action1(self):
        # Closes the door

        self.door = "closed"

        print(f"close()")
        self.real_device.close()

    def action2(self):
        # Opens the door

        self.door = "open"

        print(f"open()")
        self.real_device.open()

    def action3(self):
        # Stops the washing process

        # requires that the dishwasher is running
        if not self.running:
            self.action4()

        self.running = False

        print(f"stop()")
        self.real_device.stop()

    def action4(self):
        # Starts the washing process

        self.running = True


        print(f"start()")
        self.real_device.start()

# Start the dishwasher
dishwasher = Dishwasher()
dishwasher.action3()
dishwasher.action4()


simulator = dishwasher.real_device
print(simulator.running)