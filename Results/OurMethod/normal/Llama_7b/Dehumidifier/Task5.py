class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    


class Dehumidifier:
    def __init__(self):
        self.real_device = DehumidifierSimulator()
        self.status = False # Boolean value representing the status of the device. If true the device is reachable and able to execute actions
        self.tank = "empty" # Describes the current state of the waste water tank
        self.humidity = 45 # Current humidity level the device aims to reach.

    def toggle_status(self):
        # Toggles the status of the device turning it either on or off
        self.status = not self.status

        print(f"toggle_status()")
        self.real_device.toggle_status()

    def dehumidify(self):
        # Starts the dehumidification process
        pass

        print(f"dehumidify()")
        self.real_device.dehumidify()

    def drain(self):
        # Drains waste water tank
        pass

        print(f"drain()")
        self.real_device.drain()

    def set_humidity(self,value):
        # Sets the goal humidity
        pass

        # requires that the device is on
        if not self.status:
            self.toggle_status()

        # requires that the device is in dehumidification mode
        if self.tank != "full":
            self.drain()

        # sets the humidity level according to the input
        if value > 0 and value <= 100:
            self.humidity = value

        else:
            self.humidity = 45

        print(f"set_humidity({value})")
        self.real_device.set_humidity(value)

# Make the air as dry as possible.
dehumidifier = Dehumidifier()
dehumidifier.toggle_status()
dehumidifier.dehumidify()
dehumidifier.set_humidity(40)


simulator = dehumidifier.real_device
print(simulator.status and simulator.humidity == 0)