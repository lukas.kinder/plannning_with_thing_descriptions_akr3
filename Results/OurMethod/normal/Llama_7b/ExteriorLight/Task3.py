class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10


class ExteriorLight:
    def __init__(self):

        self.real_device = ExteriorLightSimulator()
        self.status = False # True if Light is turned on
        self.brightness = 100 # Brightness level of the light in percentage

    def turn_on(self):
        # Turns the light on
        self.status = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the light off
        self.status = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def increase(self):
        # Increases brightness by 10%

        self.brightness += 10

        print(f"increase()")
        self.real_device.increase()

    def decrease(self):
        # Decreases brightness by 10%

        self.brightness -= 10

        print(f"decrease()")
        self.real_device.decrease()

# Reduce the brightness to 20.
exteriorLight = ExteriorLight()
exteriorLight.decrease()





# COMMENTED OUT - (SYNTAX ERROR): Please provide the code for each step of the given tasks.


simulator = exteriorLight.real_device
print(simulator.brightness == 20)