class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration


class Alarm:
    def __init__(self):
        self.real_device = AlarmSimulator()
        self.set = False
        self.vibration = False

    def set_alarm(self,time):
        # Sets alarm,
        # :param time: The time the alarm should go off at (A timestamp in ISO8601 format)

        # requires that the device is on
        if not self.set:
            self.set = True

        # requires that the device is in vibration only mode
        if self.vibration:
            self.set = False

        # sets the alarm time
        self.setTime(time)

        print(f"set_alarm({time})")
        self.real_device.set_alarm(time)

    def unset(self):
        # Unsets alarm
        self.set = False

        print(f"unset()")
        self.real_device.unset()

    def toggle_vibration_mode(self):
        # Switches vibration only mode on or off depending on the current setting.

        # requires that the device is on
        if not self.set:
            self.unset()

        # requires that the device is in vibration only mode
        if self.vibration:
            self.vibration = False

        else:
            self.vibration = True

        print(f"toggle_vibration_mode()")
        self.real_device.toggle_vibration_mode()

# I want you to set an alarm with vibration.
alarm = Alarm()
alarm.set_alarm("2022-07-13T09:00:00")
alarm.toggle_vibration_mode()


simulator = alarm.real_device
print(simulator.set and simulator.vibration)