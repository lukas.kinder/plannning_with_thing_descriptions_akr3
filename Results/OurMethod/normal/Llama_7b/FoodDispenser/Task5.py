import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True


class FoodDispenser:
    def __init__(self):
        self.real_device = FoodDispenserSimulator()
        self.automatic = True
        self.stored = False

    def dispense(self):
        # Manually dispenses food for the animal
        pass

# COMMENTED OUT - (SYNTAX ERROR):     def re fill(self):
        # re fills the stored food
        pass

        print(f"dispense()")
        self.real_device.dispense()

    def set_state(self,state):
        # Sets the feeding bowl to either manual or automatic
        if state == "manual":
            self.automatic = False
        elif state == "automatic":
            self.automatic = True
        else:
            raise ValueError("Invalid state")



        print(f"set_state({state})")
        self.real_device.set_state(state)

# Give 22 portions of food.
foodDispenser = FoodDispenser()
foodDispenser.dispense(22)

simulator = foodDispenser.real_device
print(simulator.statistics['n_dispensed'] == 10)