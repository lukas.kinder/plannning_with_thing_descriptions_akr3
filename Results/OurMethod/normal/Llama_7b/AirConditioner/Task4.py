class AirConditionerSimulator:
    def __init__(self):
        self.goal_Temperature = 22
        self.status = False

    def get_goal_Temperature(self):
        return self.goal_Temperature
    
    def get_status(self):
        return self.status
    
    def toggle(self):
        self.status = not self.status

    def temperature_Up(self):
        if self.status:
            self.goal_Temperature = min(35,self.goal_Temperature +1)

    def temperature_Down(self):
        if self.status:
            self.goal_Temperature = max(10,self.goal_Temperature -1)


class AirConditioner:
    def __init__(self):
        self.real_device = AirConditionerSimulator()
        self.goal_Temperature = 22 # Goal temperature
        self.status = False # Status of the device indicating if the device is turned on(true) or off (false)

    def toggle(self):
        # Toggles status property
        self.status = not self.status

        print(f"toggle()")
        self.real_device.toggle()

    def temperature_Up(self):
        # Increases the goal temperature by 1 degree celsius
        self.goal_Temperature += 1

        print(f"temperature_Up()")
        self.real_device.temperature_Up()

    def temperature_Down(self):
        # Decreases the goal temperature by 1 degree celsius
        self.goal_Temperature -= 1




        print(f"temperature_Down()")
        self.real_device.temperature_Down()

# Make it as cold as possible!
airConditioner = AirConditioner()
airConditioner.temperature_Up()
airConditioner.temperature_Up()
airConditioner.temperature_Up()
airConditioner.temperature_Up()
airConditioner.temperature_Up()
airConditioner.temperature_Up()
airConditioner.temperature_Up()


simulator = airConditioner.real_device
print(simulator.status and simulator.goal_Temperature == 10)