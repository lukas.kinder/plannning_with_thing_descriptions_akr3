class SmartSpeakerSimulator:
    def __init__(self):
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

        self.current_service = "spotify"

    def get_connection(self):
        return self.connection
    
    def get_playing(self):
        return self.playing
    
    def get_status(self):
        return self.status
    
    def get_volume(self):
        return self.volume

    def reconnect(self):
        if self.status and not self.connection:
            self.connection = True

    def play(self):
        if self.status and self.connection and not self.playing:
            self.playing = True

    def pause(self):
        if self.status and self.connection and self.playing:
            self.playing = False

    def toggle(self):
        self.status = not self.status

    def standby(self):
        if self.status:
            self.playing = False
            self.connection = False

    def change_player(self):
        if self.status and self.connection:
           available_services = ["spotify", "youtube", "cloud", "radio", "new_channel"]
           idex = available_services.index(self.current_service)
           self.current_service = available_services[idex +1 % len(available_services)]

    def increase_volume(self):
        if self.status and self.connection and self.playing and self.volume < 120:
            self.volume += 5

    def decrease_volume(self):
        if self.status and self.connection and self.playing and self.volume > 10:
            self.volume -= 5


class SmartSpeaker:
    def __init__(self):
        self.real_device = SmartSpeakerSimulator()
        self.connection = False # States whether the speaker is connected to the Internet or not. If false music can not be played and the speaker must first be reconnected to the network
        self.playing = False # States whether the speaker is currently playing music or not
        self.status = False # Indicates if the speaker is currently switched on or off
        self.volume = 50 # The speaker volume in db

    def reconnect(self):
        # Reconnects device with the internet,
        # enables music to be played if connection was lost
        pass
        print(f"reconnect()")
        self.real_device.reconnect()

    def play(self, player):
        # Starts playing music from the selected player
        pass
        print(f"play({player})")
        self.real_device.play(player)

    def pause(self):
        # Pauses music if currently playing
        pass
        print(f"pause()")
        self.real_device.pause()

    def toggle(self):
        # Turn the speaker on or off
        pass
        print(f"toggle()")
        self.real_device.toggle()

    def standby(self):
        # Puts the device into standby,
        # making it unavailable
        pass
        print(f"standby()")
        self.real_device.standby()

    def change_player(self, player):
        # Changes music streaming service.
        pass
        print(f"change_player({player})")
        self.real_device.change_player(player)

    def increase_volume(self):
        # Increases the volume by 5 db
        pass
        print(f"increase_volume()")
        self.real_device.increase_volume()

    def decrease_volume(self):
        # Decreases volume by 5 db
        pass




        print(f"decrease_volume()")
        self.real_device.decrease_volume()

# Connect the Speaker to the internet.
smartSpeaker = SmartSpeaker()
smartSpeaker.reconnect()










simulator = smartSpeaker.real_device
print(simulator.status and simulator.connection)