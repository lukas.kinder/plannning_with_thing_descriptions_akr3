class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False


class Kettle:
    def __init__(self):
        self.real_device = KettleSimulator()
        self.power = False # Indicates if the kettle is currently turned on and ready for the heating process or in standby
        self.filled = False # Indicates wether the kettle is filled with water or not

    def switch_power(self):
        # Turns the kettle on or off depending on the current state

        if self.power:
            self.power = False
        else:
            self.power = True

        print(f"switch_power()")
        self.real_device.switch_power()

    def heat(self):
        # Start the heating process of the kettle

        # requires that the device is on
        if not self.power:
            self.switch_power()

        # requires that the kettle is empty
        if self.filled:
            self.empty()

        print(f"heat()")
        self.real_device.heat()

    def fill(self):
        # Fills the kettle with water

        self.filled = True

        print(f"fill()")
        self.real_device.fill()

    def empty(self):
        # Empties the kettle with water

        self.filled = False


        print(f"empty()")
        self.real_device.empty()

# Start heating up some water.
kettle = Kettle()
kettle.heat()


simulator = kettle.real_device
print(simulator.contain_hot_water)