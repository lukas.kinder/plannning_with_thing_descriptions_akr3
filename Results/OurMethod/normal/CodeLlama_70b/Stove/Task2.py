class StoveSimulator:
    def __init__(self):
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def get_plate_1_power(self):
        return self.plate_1_power
    
    def get_plate_2_power(self):
        return self.plate_2_power
    
    def get_plate_1_level(self):
        return self.plate_1_level
    
    def get_plate_2_level(self):
        return self.plate_2_level

    def activate_plate_1(self):
        if not self.plate_1_power:
            self.plate_1_power = True

    def activate_plate_2(self):
        if not self.plate_2_power:
            self.plate_2_power = True

    def deactivate_plate_1(self):
        if self.plate_1_power:
            self.plate_1_power = False

    def deactivate_plate_2(self):
        if self.plate_2_power:
            self.plate_2_power = False

    def increase_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level < 9:
            self.plate_1_level += 1

    def increase_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level < 9:
            self.plate_2_level += 1

    def decrease_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level > 0:
            self.plate_1_level -= 1

    def decrease_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level > 0:
            self.plate_2_level -= 1


class Stove:
    def __init__(self):

        self.real_device = StoveSimulator()
        self.plate_1_power = False # Indicates whether plate 1 is currently turned on or off
        self.plate_2_power = False # Indicates whether plate 2 is currently turned on or off
        self.plate_1_level = 0 # Represents the current heating level of plate 1
        self.plate_2_level = 0 # Represents the current heating level of plate 2

    def activate_plate_1(self):
        # Activates plate 1 setting its power level to true
        self.plate_1_power = True

        print(f"activate_plate_1()")
        self.real_device.activate_plate_1()

    def activate_plate_2(self):
        # Activates plate 2 setting its power level to true
        self.plate_2_power = True

        print(f"activate_plate_2()")
        self.real_device.activate_plate_2()

    def deactivate_plate_1(self):
        # Deactivates plate 1 setting its power level to false
        self.plate_1_power = False

        print(f"deactivate_plate_1()")
        self.real_device.deactivate_plate_1()

    def deactivate_plate_2(self):
        # Deactivates plate 2 setting its power level to false
        self.plate_2_power = False

        print(f"deactivate_plate_2()")
        self.real_device.deactivate_plate_2()

    def increase_heat_plate_1(self):
        # Increases the heating level for plate 1 by 1
        self.plate_1_level += 1

        print(f"increase_heat_plate_1()")
        self.real_device.increase_heat_plate_1()

    def increase_heat_plate_2(self):
        # Increases the heating level for plate 2 by 1
        self.plate_2_level += 1

        print(f"increase_heat_plate_2()")
        self.real_device.increase_heat_plate_2()

    def decrease_heat_plate_1(self):
        # Decreases the heating level for plate 1 by 1
        self.plate_1_level -= 1

        print(f"decrease_heat_plate_1()")
        self.real_device.decrease_heat_plate_1()

    def decrease_heat_plate_2(self):
        # Decreases the heating level for plate 2 by 1
        self.plate_2_level -= 1





        print(f"decrease_heat_plate_2()")
        self.real_device.decrease_heat_plate_2()

# Set the plate 2 to level 2.
stove = Stove()
stove.deactivate_plate_1()
stove.activate_plate_2()
stove.increase_heat_plate_2()





















simulator = stove.real_device
print(simulator.plate_2_level == 2)