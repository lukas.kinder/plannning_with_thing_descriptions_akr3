class ChillerSimulator:
    def __init__(self):
        self.type = "" #Type of the chiller
        self.firmware = "v1.0" #Firmware version
        self.model = "" #Chiller model
        self.location = "" #Location of the chiller
        self.latitude = 0 #Latitude of the chiller
        self.longitude = 0 #Longitude of the chiller
        self.location_tag = "" #Location tag
        self.floor_tag = "" #floor level
        self.campus_tag = "" #name of the campus
        self.online = False #online status
        self.temperature = 20 #temperature
        self.temperature_unit = "C" #C for Celsius, F for Fahrenheit
        self.humidity = 30 #humidity of the chiller
        self.humidity_unit = "percentage" #percentage or grams per m3
        self.pressure = 1.5 #pressure
        self.pressure_unit = "bar" #psi or pascal or bar
        self.ora_latitude = 0 #ora_latitude
        self.ora_longitude = 0 #ora_longitude
        self.ora_altitude = 0 #ora_altitude
        self.ora_uncertainty = 0 #ora_accuracy
        self.ora_zone = "" #ora_zone
        self.ora_txPower = 0 #ora_mssi
        self.ora_rssi = 0 #ora_rssi

        self.n_rebooted  = 0

    # Getter functions
    def get_type(self):
        return self.type

    def get_firmware(self):
        return self.firmware

    def get_model(self):
        return self.model

    def get_location(self):
        return self.location

    def get_latitude(self):
        return self.latitude

    def get_longitude(self):
        return self.longitude

    def get_location_tag(self):
        return self.location_tag

    def get_floor_tag(self):
        return self.floor_tag

    def get_campus_tag(self):
        return self.campus_tag

    def get_online(self):
        return self.online

    def get_temperature(self):
        return self.temperature

    def get_temperature_unit(self):
        return self.temperature_unit

    def get_humidity(self):
        return self.humidity

    def get_humidity_unit(self):
        return self.humidity_unit

    def get_pressure(self):
        return self.pressure

    def get_pressure_unit(self):
        return self.pressure_unit

    def get_ora_latitude(self):
        return self.ora_latitude

    def get_ora_longitude(self):
        return self.ora_longitude

    def get_ora_altitude(self):
        return self.ora_altitude

    def get_ora_uncertainty(self):
        return self.ora_uncertainty

    def get_ora_zone(self):
        return self.ora_zone

    def get_ora_txPower(self):
        return self.ora_txPower

    def get_ora_rssi(self):
        return self.ora_rssi
    
    #actions
    def reboot(self):
        self.n_rebooted +=1

    def firmwareUpdate(self, version):
        self.firmware = version

    def emergencyValveRelease(self):
        self.pressure = 1

    def increasePressure(self, value):
        if value >= 0 and value <=5:
            self.pressure += value


class Chiller:
    def __init__(self):
        self.real_device = ChillerSimulator()
        self.type = '' # Type of the chiller
        self.firmware = 'v1.0' # Version of the firmware
        self.model = '' # Model of the chiller
        self.location = '' # Location of the chiller
        self.latitude = 0 # Latitude of the chiller
        self.longitude = 0 # Longitude of the chiller
        self.location_tag = '' # Tag of the location where the chiller is placed
        self.floor_tag = '' # Tag of the floor where the chiller is placed
        self.campus_tag = '' # Tag of the campus where the chiller is placed
        self.online = False # Online status of the chiller
        self.temperature = 20 # Temperature of the chiller
        self.temperature_unit = 'C' # Unit of temperature
        self.humidity = 30 # Humidity of the chiller
        self.humidity_unit = 'percentage' # Unit of humidity
        self.pressure = 1.5 # Pressure of the chiller
        self.pressure_unit = 'bar' # Unit of pressure
        self.ora_latitude = 0 # Latitude of the object relative to absolute coordinate
        self.ora_longitude = 0 # Longitude of the object relative to absolute coordinate
        self.ora_altitude = 0 # Altitude of the object relative to absolute coordinate
        self.ora_uncertainty = 0 # Uncertainty of the object relative to absolute coordinate
        self.ora_zone = '' # Zone of the object
        self.ora_txPower = 0 # Transmit power of the object
        self.ora_rssi = 0 # Receive power of the object

    def reboot(self):
        # Reboots the chiller
        pass

        print(f"reboot()")
        self.real_device.reboot()

    def firmwareUpdate(self, firmware_version):

        # Updates the firmware to the given version
        pass

        print(f"firmwareUpdate({firmware_version})")
        self.real_device.firmwareUpdate(firmware_version)

    def emergencyValveRelease(self):

        # Opens the emergency valve
        pass

        print(f"emergencyValveRelease()")
        self.real_device.emergencyValveRelease()

    def increasePressure(self, pressure):

        # Increases the pressure by the given value
        pass



        print(f"increasePressure({pressure})")
        self.real_device.increasePressure(pressure)

# Make sure the chiller has a pressure of 2.5 bar.
chiller = Chiller()
chiller.set_pressure(2.5)



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Robot:
    def __init__(self):
        self.arm = False # Whether the arm is extended or retracted
        self.legs = False # Whether the legs are extended or retracted
        self.head = False # Whether the head is turned on or off

    def extend_Arm(self):
        # Extends the arm
        self.arm = True

    def extend_Legs(self):
        # Extends the legs
        self.legs = True

    def turn_on_Head(self):
        # Turns on the head
        self.head = True

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Extend the arm and turn on the head.".



# Extend the arm and turn on the head.
robot = Robot()
robot.extend_Arm()
robot.turn_on_Head()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Door:
    def __init__(self):

        self.lock = False # Whether the door is locked or unlocked
        self.hinge = False # Whether the hinge is closed or open
        self.door = False # Whether the door is open or closed

    def lock_door(self):
        # Locks the door
        self.lock = True

    def unlock_door(self):
        # Unlocks the door
        self.lock = False

    def close_door(self):
        # Closes the door
        self.door = True

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Lock the door and close it.".



# Lock the door and close it.
door = Door()
door.lock_door()
door.close_door()




# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Lamp:
    def __init__(self):

        self.light = False # Whether the light is on or off

    def turn_light_on(self):
        # Turns the light on
        self.light = True

    def turn_light_off(self):
        # Turns the light off
        self.light = False

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Turn the lamp on.".



# Turn the lamp on.
lamp = Lamp()
lamp.turn_light_on()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Microwave:
    def __init__(self):

        self.power = False # Whether the microwave is on or off
        self.timer = False # Whether the timer is running or not

    def start_Timer(self, time):
        # Starts the timer
        self.timer = True

    def stop_Timer(self):
        # Stops the timer
        self.timer = False

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Start the timer for 3 minutes.".



# Start the timer for 3 minutes.
microwave = Microwave()
microwave.start_Timer(3)



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Oven:
    def __init__(self):


        self.heat = False # Whether the oven is heated or cooled
        self.door = False # Whether the oven door is open or closed

    def heat_Oven(self, temperature):
        # Heats the oven to the given temperature
        self.heat = True

    def cool_Oven(self):
        # Cools the oven
        self.heat = False

    def open_Door(self):
        # Opens the oven door
        self.door = True

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Heat the oven to 350 degrees Fahrenheit and open the door.".



# Heat the oven to 350 degrees Fahrenheit and open the door.
oven = Oven()
oven.heat_Oven(350)
oven.open_Door()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class Car:
    def __init__(self):

        self.engine = False # Whether the engine is on or off
        self.ac = False # Whether the air












simulator = chiller.real_device
print(round(simulator.pressure,3) == 2.5)