class SmartRadioDeviceSimulator:
    def __init__(self):
        self.power = "on"
        self.channel = "Jamz"

    def get_power(self):
        return self.power
    
    def get_channel(self):
        return self.channel

    def power_on(self):
        if self.power == "off":
            self.power = "on"

    def switch_channel(self):
        if self.power == "on":
            channels = ["MTV", "Jamz", "The Beat", "The Mix"]
            current_index = channels.index(self.channel)
            next_index = (current_index + 1) % len(channels)
            self.channel = channels[next_index]

    def power_off(self):
        if self.power == "on":
            self.power = "off"


class SmartRadioDevice:
    def __init__(self):
        self.real_device = SmartRadioDeviceSimulator()
        self.power = "on"
        self.channel = "Jamz"

    def power_on(self):
        # Turns the radio on
        self.power = "off"

        print(f"power_on()")
        self.real_device.power_on()

    def switch_channel(self):
        # Switches channel of the radio to the next available channel

        # requires that the device is on
        if self.power == "on":
            self.power_off()

        # requires that the current channel is not occupied by another station
        station = self.get_station()
        if station:
            self.power_on()
            self.channel = station
        else:
            self.power_off()
            self.channel = "Jamz"

        print(f"switch_channel()")
        self.real_device.switch_channel()

    def power_off(self):
        # Turns the radio off
        self.power = "on"



        print(f"power_off()")
        self.real_device.power_off()

# Play MTV on the radio.
smartRadioDevice = SmartRadioDevice()
smartRadioDevice.switch_channel()
smartRadioDevice.power_on()
smartRadioDevice.power_off()


simulator = smartRadioDevice.real_device
print(simulator.power and simulator.channel == 'MTV')