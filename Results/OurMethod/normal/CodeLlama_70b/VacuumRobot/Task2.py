import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100


class VacuumRobot:
    def __init__(self):
        self.real_device = VacuumRobotSimulator()
        self.cleaning = False # States if the robot is currently cleaning or not
        self.storage_full = True # States whether the storage of the robot is full or not
        self.battery = 30 # Describes the current power charge in percent
        self.mapped = True # States whether a current mapping of the room is available
        self.at_base = True # States whether the robot is currently docked at the charging base station

    def start(self):
        # Start vacuuming the room
        # requires that a mapping for the room is available
        # requires that there is sufficient storage and battery life
        if not self.mapped or not self.storage_full or self.battery < 10:
            return None

        self.cleaning = True

        print(f"start()")
        self.real_device.start()

    def scan(self):
        # Scans room, creating a mapping
        # requires that the device is on

        if not self.cleaning:
            self.start()

        print(f"scan()")
        self.real_device.scan()

    def empty_storage(self):
        # Empties storage
        # requires that the device is on

        if not self.cleaning:
            self.start()

        print(f"empty_storage()")
        self.real_device.empty_storage()

    def return_to_base(self):
        # Returns to the base, enabling further actions
        # requires that the device is on

        if not self.cleaning:
            self.start()

        print(f"return_to_base()")
        self.real_device.return_to_base()

    def charge(self):
        # Charges the robot
        # requires that the device is on

        if not self.cleaning:
            self.start()



        print(f"charge()")
        self.real_device.charge()

# Start cleaning.
vacuumRobot = VacuumRobot()
vacuumRobot.scan()


simulator = vacuumRobot.real_device
print(simulator.cleaning)