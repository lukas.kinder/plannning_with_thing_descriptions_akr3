from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    


class AmbientLightstrip:
    def __init__(self):
        self.real_device = AmbientLightstripSimulator()
        self.state = {"on": False, "bri": 254, "sat": 128}

    def turn_on(self):
        # Turns the light on
        self.state["on"] = True

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the light off
        self.state["on"] = False

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self,value):
        # Sets the brightness
        self.state["bri"] = value

        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

    def set_saturation(self,value):
        # Sets the saturation
        self.state["sat"] = value



        print(f"set_saturation({value})")
        self.real_device.set_saturation(value)

# Lights please!
ambientLightstrip = AmbientLightstrip()
ambientLightstrip.turn_on()
ambientLightstrip.set_brightness(75)
ambientLightstrip.set_saturation(100)



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:

class Robot:
    def __init__(self):
        self.arm = False # Whether the arm is extended
        self.legs = False # Whether the legs are extended
        self.head = False # Whether the head is turned

    def extend_arm(self):
        # Extends the arm
        self.arm = True

    def extend_legs(self):
        # Extends the legs
        self.legs = True

    def turn_head(self):
        # Turns the head
        self.head = True

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Bring me the remote control please!".


# Bring me the remote control please!
robot = Robot()
robot.extend_arm()
robot.extend_legs()
robot.turn_head()









simulator = ambientLightstrip.real_device
print(simulator.state['on'] and simulator.state['bri'] > 0)