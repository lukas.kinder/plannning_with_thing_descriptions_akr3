class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value


class SmartTV:
    def __init__(self):
        self.real_device = SmartTVSimulator()
        self.recording = False # True if the TV is currently recording the selected channel

        self.off = True # Indicates if the TV is turned off(True) or turned on (False)

        self.brightness = 70 # Describes brightness of the TV screen in percent

        self.channel = "ZDF" # States the currently selected channel that will be displayed if the device is turned on

    def record(self):
        # Starts recording the current channel
        self.recording = True

        print(f"record()")
        self.real_device.record()

    def switch(self):
        # Switches to a different channel
        pass

        print(f"switch()")
        self.real_device.switch()

    def turn_on(self):
        # Turns the TV on
        self.off = False

        print(f"turn_on()")
        self.real_device.turn_on()

    def turn_off(self):
        # Turns the TV off
        self.off = True

        print(f"turn_off()")
        self.real_device.turn_off()

    def set_brightness(self,value):
        # Sets the brightness of the tv.
        # :param integer value: the new brightness

        # requires that the device is on
        if self.off:
            raise Exception("Can not set brightness when TV is off")

        # requires that the brightness is within range
        if value < 0 or value > 100:
            raise Exception("Brightness must be between 0 and 100")

        self.brightness = value


        print(f"set_brightness({value})")
        self.real_device.set_brightness(value)

# Turn on the tv.
smartTV = SmartTV()
smartTV.turn_on()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class CoffeeMachine:
    def __init__(self):
        self.coffee = False # Indicates whether coffee has been brewed (true) or not (false)

        self.water = False # Indicates whether water is running (true) or not (false)

        self.on = False # Indicates whether the machine is on (true) or off (false)

    def brewCoffee(self):
        # Brews coffee
        self.coffee = True

    def turnOn(self):
        # Turns the machine on
        self.on = True

    def turnOff(self):
        # Turns the machine off
        self.on = False

    def fillWater(self):
        # Fills the machine's water tank
        pass

# COMMENTED OUT - (SYNTAX ERROR): Write code that fulfills the task: "Brew coffee and turn on the machine.".



# Brew coffee and turn on the machine.
CoffeeMachine = CoffeeMachine()
CoffeeMachine.brewCoffee()
CoffeeMachine.turnOn()



# COMMENTED OUT - (SYNTAX ERROR): Consider the following python class:


class VacuumCleaner:
    def __init__(self):
        self.power = False # Indicates whether the vacuum is on (true) or off (false)
        self.bucket = 0 # Indicates how much dirt the vacuum can hold (minimum = 0, maximum = 10)


    def suck(self):
        # Sucks dirt
        pass

    def emptyBucket(self):
        # Empties the bucket
        pass

    def turn_on(self):
        # Turns the vacuum on
        self.power = True


def main():
    # Create a vacuum cleaner object
    vacuum = VacuumCleaner()

    # Set the initial state of the vacuum
    vacuum.bucket = 5

    # Perform actions on the vacuum
    vacuum.suck()
    vacuum.emptyBucket()

if __name__ == "__main__":
    main()








































































































































































































































































































































































































































































































simulator = smartTV.real_device
print(not simulator.off)