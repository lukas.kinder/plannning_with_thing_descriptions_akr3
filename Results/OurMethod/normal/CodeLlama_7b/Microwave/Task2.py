class MicrowaveSimulator:
    def __init__(self):
        self.running = False

        self.statistic = {"n_runs" : 0}

    def get_running(self):
        return self.running

    def start(self):
        if not self.running:
            self.running = True

    def stop(self):
        if self.running:
            self.running = False
            self.statistic['n_runs'] +=1


class Microwave:
    def __init__(self):
        self.real_device = MicrowaveSimulator()
        self.running = False

    def start(self):
        # Starts the heating process

        self.running = True

        print(f"start()")
        self.real_device.start()

    def stop(self):
        # Stops the heating process

        self.running = False


        print(f"stop()")
        self.real_device.stop()

# Start the microwave and stop it afterwards.
microwave = Microwave()
microwave.start()
microwave.stop()


simulator = microwave.real_device
print(simulator.statistic['n_runs'] == 1)