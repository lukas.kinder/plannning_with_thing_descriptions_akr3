class SmartSpeakerSimulator:
    def __init__(self):
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

        self.current_service = "spotify"

    def get_connection(self):
        return self.connection
    
    def get_playing(self):
        return self.playing
    
    def get_status(self):
        return self.status
    
    def get_volume(self):
        return self.volume

    def reconnect(self):
        if self.status and not self.connection:
            self.connection = True

    def play(self):
        if self.status and self.connection and not self.playing:
            self.playing = True

    def pause(self):
        if self.status and self.connection and self.playing:
            self.playing = False

    def toggle(self):
        self.status = not self.status

    def standby(self):
        if self.status:
            self.playing = False
            self.connection = False

    def change_player(self):
        if self.status and self.connection:
           available_services = ["spotify", "youtube", "cloud", "radio", "new_channel"]
           idex = available_services.index(self.current_service)
           self.current_service = available_services[idex +1 % len(available_services)]

    def increase_volume(self):
        if self.status and self.connection and self.playing and self.volume < 120:
            self.volume += 5

    def decrease_volume(self):
        if self.status and self.connection and self.playing and self.volume > 10:
            self.volume -= 5


class SmartSpeaker:
    def __init__(self):
        self.real_device = SmartSpeakerSimulator()
        self.connection = False
        self.playing = False
        self.status = False
        self.volume = 50

    def reconnect(self):
        # Reconnects device with the internet,
        # enabling music to be played if connection was lost

        self.connection = True

        print(f"reconnect()")
        self.real_device.reconnect()

    def play(self):
        # Starts playing music from the selected player

        # requires that the device is on
        if not self.status:
            self.toggle()

        self.playing = True

        print(f"play()")
        self.real_device.play()

    def pause(self):
        # Pauses music if currently playing

        if not self.playing:
            return

        self.playing = False

        print(f"pause()")
        self.real_device.pause()

    def toggle(self):
        # Turn the speaker on of off

        self.status = not self.status

        print(f"toggle()")
        self.real_device.toggle()

    def standby(self):
        # Puts the device into standby,
        # making it unavailable

        self.status = False

        print(f"standby()")
        self.real_device.standby()

    def change_player(self):
        # Changes music streaming service.
        pass

        print(f"change_player()")
        self.real_device.change_player()

    def increase_volume(self):
        # Increases the volume by 5 db

        # requires that the device is on
        if not self.status:
            self.toggle()

        self.volume += 5

        print(f"increase_volume()")
        self.real_device.increase_volume()

    def decrease_volume(self):
        # Decreases volume by 5 db

        # requires that the device is on
        if not self.status:
            self.toggle()

        self.volume -= 5


        print(f"decrease_volume()")
        self.real_device.decrease_volume()

# Play something with a volume of 90!
smartSpeaker = SmartSpeaker()
smartSpeaker.change_player()
smartSpeaker.decrease_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.play()
smartSpeaker.pause()
smartSpeaker.standby()
smartSpeaker.reconnect()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
smartSpeaker.increase_volume()
# COMMENTED OUT - (SYNTAX ERROR): smartSpeaker.in


simulator = smartSpeaker.real_device
print(simulator.volume == 90 and simulator.playing)