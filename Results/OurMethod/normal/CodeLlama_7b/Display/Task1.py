# Was not able to entangle with real device! (Are still unconnected)
from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"
class Display:
    def __init__(self):
        self.power = {'status': False}
        self.content = "Video"

    def toggle(self):
        # Toggles power status

        self.power['status'] = not self.power['status']

    def set_Volume(self):
        # Sets Volume

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

    def set_Bright(self):
        # Sets Brightness

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

    def show_Text(self):
        # Shows Text

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

    def play_Video(self):
        # Plays Video

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

    def pause_Video(self):
        # Pauses Video

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

    def stop_Video(self):
        # Stops Video

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

    def presentation_Web_App(self):
        # Launches Presentation Web App

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

    def launch_News_App(self):
        # Launches News App

        # requires that the device is on
        if not self.power['status']:
            self.toggle()

# Turn the display on.
display = Display()
display.toggle()


simulator = display.real_device
print(simulator.power['status'])