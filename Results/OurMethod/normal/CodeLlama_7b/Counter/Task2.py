from datetime import datetime

class CounterSimulator:
    def __init__(self):
        self.count = 0  # current counter value
        self.countAsImage = None  # current counter value as SVG image
        self.redDotImage = None  # Red dot image as PNG
        self.lastChange = ""  # last change of counter value

        self.max_value = 0
        self.min_value = 0

    def get_count(self):
        return self.count
    
    def get_countAsImage(self):
        return self.countAsImage
    
    def get_redDotImage(self):
        return self.redDotImage
    
    def get_lastChange(self):
        return self.lastChange

    def increment(self):
        # increment value
        self.lastChange = str(datetime.now())
        self.count += 1

        if self.count > self.max_value:
            self.max_value = self.count

    def decrement(self):
        # decrement value
        self.lastChange = str(datetime.now())
        self.count -= 1
        if self.min_value > self.count:
            self.min_value = self.count

    def reset(self):
        # Resetting counter value
        self.lastChange = str(datetime.now())
        self.count = 0


class Counter:
    def __init__(self):
        self.real_device = CounterSimulator()
        self.count = 0 # current counter value
        self.countAsImage = None # current counter value as SVG image
        self.redDotImage = None # Red dot image as PNG
        self.lastChange = "" # last change of counter value

    def increment(self):
        # increment value

        self.count += 1
        self.lastChange = f"Incremented by one"

        print(f"increment()")
        self.real_device.increment()

    def decrement(self):
        # decrement value

        if self.count == 0:
            raise ValueError("Can't decrement below zero")

        self.count -= 1
        self.lastChange = f"Decremented by one"

        print(f"decrement()")
        self.real_device.decrement()

    def reset(self):
        # Resetting counter value

        self.count = 0
        self.lastChange = f"Reset to zero"


        print(f"reset()")
        self.real_device.reset()

# increment, increment, increment, decrement
counter = Counter()
for i in range(3):
    counter.increment()
counter.decrement()


simulator = counter.real_device
print(simulator.count == 2 and simulator.max_value == 3)