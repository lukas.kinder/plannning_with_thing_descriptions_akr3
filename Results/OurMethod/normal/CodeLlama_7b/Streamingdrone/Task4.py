import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100


class Streamingdrone:
    def __init__(self):
        self.real_device = StreamingdroneSimulator()
        self.recording = False
        self.streaming = False
        self.battery = 50

    def stream(self):
        # Starts streaming

        # requires that the device is not currently recording
        if self.recording:
            raise Exception("Can not start streaming when recording")

        # requires that the device is not currently streaming
        if self.streaming:
            raise Exception("Device already streaming")

        self.streaming = True

        print(f"stream()")
        self.real_device.stream()

    def stop_stream(self):
        # Stops streaming

        # requires that the device is currently streaming
        if not self.streaming:
            raise Exception("Device not streaming")

        self.streaming = False

        print(f"stop_stream()")
        self.real_device.stop_stream()

    def start_recording(self):
        # Starts the recording

        # requires that the device is not currently streaming
        if self.streaming:
            raise Exception("Can not record while streaming")

        # requires that the device is not currently recording
        if self.recording:
            raise Exception("Device already recording")

        self.recording = True

        print(f"start_recording()")
        self.real_device.start_recording()

    def end(self):
        # Ends recording process deleting not saved recording

        # requires that the device is currently recording
        if not self.recording:
            raise Exception("Device not recording")

        self.recording = False

        print(f"end()")
        self.real_device.end()

    def recharge(self):
        # Recharges the battery, can not be recharged during streaming or recording

        # requires that the device is not currently streaming
        if self.streaming:
            raise Exception("Can not recharge while streaming")

        # requires that the device is not currently recording
        if self.recording:
            raise Exception("Can not recharge while recording")

        self.battery = 100


        print(f"recharge()")
        self.real_device.recharge()

# Use the drone to record 5 videos.
streamingdrone = Streamingdrone()
for i in range(0,5):
    streamingdrone.start_recording()
    streamingdrone.stop_stream()
    streamingdrone.end()


simulator = streamingdrone.real_device
print(simulator.statistic['n_recordings'] == 5)