class SmartLockSimulator:
    def __init__(self):
        self.locked = True

        self.statistic = {"counter_locked" : 0}

    def get_locked(self):
        return self.locked

    def lock(self):
        if not self.locked:
            self.locked = True
            self.statistic["counter_locked"] +=1

    def unlock(self):
        if self.locked:
            self.locked = False


class SmartLock:
    def __init__(self):
        self.real_device = SmartLockSimulator()
        self.locked = True  # Property stating if the locked is currently locked(true) or unlocked(false)

    def lock(self):
        # Locks the lock securing connected objects
        self.locked = True

        print(f"lock()")
        self.real_device.lock()

    def unlock(self):
        # Unlocks the lock making connected objects accessible
        self.locked = False

        print(f"unlock()")
        self.real_device.unlock()

# Open the lock
smartLock = SmartLock()
smartLock.unlock()

simulator = smartLock.real_device
print(not simulator.locked)