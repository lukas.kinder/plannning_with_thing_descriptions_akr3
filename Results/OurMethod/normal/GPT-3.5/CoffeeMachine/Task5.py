import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False



class CoffeeMachine:
    def __init__(self):
        self.real_device = CoffeeMachineSimulator()
        self.water = True  # Illustrates water level of the machine. If false the machine does not contain enough water to make a coffee and needs to be refilled
        self.storage = True  # Indicates the remaining coffee ground storage. If false no space is remaining and the container needs to be emptied
        self.on = False  # Displays the current power state of the machine

    def toggle(self):
        # Toggles on property of the device
        self.on = not self.on

        print(f"toggle()")
        self.real_device.toggle()

    def refill(self):
        # Refills the water tank of the machine
        self.water = True

        print(f"refill()")
        self.real_device.refill()

    def empty(self):
        # Empties coffee ground container
        self.storage = True

        print(f"empty()")
        self.real_device.empty()

    def make_coffee(self):
        # Creates a medium cup of coffee
        # requires that the device is on
        if not self.on:
            self.toggle()
        
        # requires that the machine has enough water and space for the coffee grounds
        if not self.water:
            self.refill()

        if not self.storage:
            self.empty()

        # Code to make coffee here

        print(f"make_coffee()")
        self.real_device.make_coffee()

# I want you to make 5 cups of coffee. Remember to refill the machine afterwards.
coffeeMachine = CoffeeMachine()
for _ in range(5):
    coffeeMachine.make_coffee()
coffeeMachine.refill()

simulator = coffeeMachine.real_device
print(simulator.statistic['coffee_made'] == 5 and simulator.water_left == 10)