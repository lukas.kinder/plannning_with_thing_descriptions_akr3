from copy import deepcopy

class DisplaySimulator:
    def __init__(self):
        self.power = {"status": False} 
        self.content = "Video"

        self.volume = 20
        self.brightness = 50
        self.text_buffer = ""
        self.video_details = {
            "identifier" : "NA",
            "name" : "Can LLMs Really Reason & Plan? (Keynote at SCAI AI Day; Nov 17, 2023)",
            "description" : "Slides @ https://bit.ly/3G8yP0r",
            "url" : "https://www.youtube.com/watch?v=uTXXYi75QCU",
            "paused" : False
        }

        self.web_app_details = {
            "identifier" : "",
            "name" : "",
            "description" : "",
            "url" : ""
        }

    def get_power(self):
        return deepcopy( self.power )

    def get_content(self):
        return self.content

    def toggle(self):
        # toggles the power status
        self.power["status"] = not self.power["status"]

    def setVolume(self, volume):
        if self.power["status"]:
            self.volume = volume

    def setBright(self, brightness):
        if self.power["status"]:
            self.brightness = brightness

    def showText(self, text):
        if self.power["status"]:
            self.text_buffer = text
            self.content = "Text"

    def playVideo(self, video_info):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = video_info["identifier"]
            self.video_details["name"] = video_info["name"]
            self.video_details["description"] = video_info["description"]
            self.video_details["url"] = video_info["url"]

    def pauseVideo(self):
        if self.power["status"]:
            self.video_details["paused"] = True
        
    def stopVideo(self):
        if self.power["status"]:
            self.content = "Video"
            self.video_details["identifier"] = ""
            self.video_details["name"] = ""
            self.video_details["description"] = ""
            self.video_details["url"] = '"'

    def presentationWebApp(self, app_info):
        if self.power["status"]:
            self.content = "WebApp"
            self.web_app_details["identifier"] = app_info["identifier"]
            self.web_app_details["name"] = app_info["name"]
            self.web_app_details["description"] = app_info["description"]
            self.web_app_details["url"] = app_info["url"]

    def launchNewsApp(self):
        if self.power["status"]:
            self.content = "News"


class Display:
    def __init__(self):
        self.real_device = DisplaySimulator()
        self.power = {"status": False}
        self.content = "Video" # Available content

    def toggle(self):
        # togglePowerStatus
        self.power["status"] = not self.power["status"]

        print(f"toggle()")
        self.real_device.toggle()

    def setVolume(self,value):
        # setVolume
        # :param integer value: the value the volume should be
        pass

        print(f"setVolume({value})")
        self.real_device.setVolume(value)

    def setBright(self,value):
        # setBright
        # :param integer value: the value the brightness should be
        pass

        print(f"setBright({value})")
        self.real_device.setBright(value)

    def showText(self,text):
        # showText
        # :param string text: the text that should be displayed
        pass

        print(f"showText({text})")
        self.real_device.showText(text)

    def playVideo(self, identifier, name, description, url):
        # playVideo
        # :param string identifier: identifier of the video
        # :param string name: name of the video
        # :param string description: description of the video
        # :param string url: url of the video

        # needs the device to be on 
        if not self.power["status"]:
            self.toggle()
        pass

        print(f"playVideo({identifier},{name},{description},{url})")
        self.real_device.playVideo(identifier, name, description, url)

    def pauseVideo(self):
        # pauseVideo
        # needs a video to be playing 
        pass

        print(f"pauseVideo()")
        self.real_device.pauseVideo()

    def stopVideo(self):
        # stopVideo
        # needs a video to be playing 
        pass

        print(f"stopVideo()")
        self.real_device.stopVideo()

    def presentationWebApp(self, identifier, name, description, url):
        # presentationWebApp
        # :param string identifier: identifier of the presentation
        # :param string name: name of the presentation
        # :param string description: description of the presentation
        # :param string url: url of the presentation

        # needs the device to be on 
        if not self.power["status"]:
            self.toggle()
        pass

        print(f"presentationWebApp({identifier},{name},{description},{url})")
        self.real_device.presentationWebApp(identifier, name, description, url)

    def launchNewsApp(self):
        # launchNewsApp
        # needs the device to be on 
        if not self.power["status"]:
            self.toggle()
        pass

        print(f"launchNewsApp()")
        self.real_device.launchNewsApp()

# Turn the display on.
display = Display()
if not display.power["status"]:
    display.toggle()

simulator = display.real_device
print(simulator.power['status'])