import random

class SecurityCameraSimulator:
    def __init__(self):
        self.recording = False
        self.storage = 256

        self.statistic = {"recorded_videos" : 0}

    def get_recording(self):
        return self.recording
    
    def get_storage(self):
        return self.storage

    def record(self):
        if not self.recording and self.storage > 0:
            self.recording = True

    def stop(self):
        if self.recording:
            self.recording = False

            recording_size = random.randint(0,3)
            self.storage -= recording_size

            self.statistic['recorded_videos'] += 1

    def delete(self):
        if not self.recording:
            self.storage = 256


class SecurityCamera:
    def __init__(self):
        self.real_device = SecurityCameraSimulator()
        self.recording = False  # Boolean value indicating if the camera is currently recording
        self.storage = 256      # States the current remaining storage in gigabytes

    def record(self):
        # Starts the recording
        # After a recording is started, it's assumed that some storage is used. Hence, decrementing the storage by 1.
        # This is a simplification.
        
        # Requires that there is storage available
        if self.storage > 0:
            self.recording = True
            self.storage -= 1

        print(f"record()")
        self.real_device.record()

    def stop(self):
        # Stops the recording

        self.recording = False

        print(f"stop()")
        self.real_device.stop()

    def delete(self):
        # Deletes video files freeing storage
        # For simplicity, we'll assume that each delete action restores 1GB of storage,
        # up to a maximum of 256.

        if self.storage < 256:
            self.storage += 1
            # deleted files mean recording is not happening
            self.recording = False


        print(f"delete()")
        self.real_device.delete()

# Start recording
securityCamera = SecurityCamera()
securityCamera.record()

simulator = securityCamera.real_device
print(simulator.recording)