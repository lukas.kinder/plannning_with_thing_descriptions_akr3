class MicrowaveSimulator:
    def __init__(self):
        self.running = False

        self.statistic = {"n_runs" : 0}

    def get_running(self):
        return self.running

    def start(self):
        if not self.running:
            self.running = True

    def stop(self):
        if self.running:
            self.running = False
            self.statistic['n_runs'] +=1

#Start the microwave and stop it afterwards.
simulator = MicrowaveSimulator()


# Full Output:
# 1. start() 2. stop() 3. done.

simulator.start()
simulator.stop()
print(simulator.statistic['n_runs'] == 1)