class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)

#Make it is bright as possible.
simulator = BedsideLampSimulator()


# Full Output:
# 1. turn_on() 2. increase() 3. increase() 4. increase() 5. increase() 6. increase() 7. done.

simulator.turn_on()
simulator.increase()
simulator.increase()
simulator.increase()
simulator.increase()
simulator.increase()
print(simulator.on and simulator.brightness == 100)