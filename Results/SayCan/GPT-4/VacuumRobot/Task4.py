import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100

#Let the robot move out to clean and come back. Repeat this 7 times
simulator = VacuumRobotSimulator()


# Full Output:
# 1. empty_storage() 2. start() 3. return_to_base() 4. charge() 5. empty_storage() 6. start() 7. return_to_base() 8. charge() 9. empty_storage() 10. start() 11. return_to_base() 12. charge() 13. empty_storage() 14. start() 15. return_to_base() 16. charge() 17. empty_storage() 18. start() 19. return_to_base() 20. charge() 21. empty_storage() 22. start() 23. return_to_base() 24. charge() 25. empty_storage() 26. start() 27. return_to_base() 28. charge() 29. done.

simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
print(simulator.statistic['n_cleaned'] == 7)