class SmartLockSimulator:
    def __init__(self):
        self.locked = True

        self.statistic = {"counter_locked" : 0}

    def get_locked(self):
        return self.locked

    def lock(self):
        if not self.locked:
            self.locked = True
            self.statistic["counter_locked"] +=1

    def unlock(self):
        if self.locked:
            self.locked = False

#Open the lock and close afterwards.
simulator = SmartLockSimulator()


# Full Output:
# 1. unlock() 2. lock() 3. done.

simulator.unlock()
simulator.lock()
print(simulator.locked and simulator.statistic['counter_locked'] == 1)