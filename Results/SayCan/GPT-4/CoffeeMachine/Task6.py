import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False


#Make me 12 coffee.
simulator = CoffeeMachineSimulator()


# Full Output:
# 1. toggle() 2. make_coffee() 3. empty() 4. refill() 5. make_coffee() 6. empty() 7. refill() 8. make_coffee() 9. empty() 10. refill() 11. make_coffee() 12. empty() 13. refill() 14. make_coffee() 15. empty() 16. refill() 17. make_coffee() 18. empty() 19. refill() 20. make_coffee() 21. empty() 22. refill() 23. make_coffee() 24. empty() 25. refill() 26. make_coffee() 27. empty() 28. refill() 29. make_coffee() 30. empty() 31. refill() 32. make_coffee() 33. empty() 34. refill() 35. make_coffee() 36. empty() 37. refill() 38. make_coffee() 39. empty() 40. refill() 41. toggle() 42. done.

simulator.toggle()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.refill()
simulator.toggle()
print(simulator.statistic['coffee_made'] == 12)