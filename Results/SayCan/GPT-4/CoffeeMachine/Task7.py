import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False


#make 22 coffee.
simulator = CoffeeMachineSimulator()


# Full Output:
# 1. toggle(), 2. make_coffee(), 3. refill(), 4. make_coffee(), 5. refill(), 6. make_coffee(), 7. refill(), 8. make_coffee(), 9. refill(), 10. make_coffee(), 11. refill(), 12. make_coffee(), 13. refill(), 14. make_coffee(), 15. refill(), 16. make_coffee(), 17. refill(), 18. make_coffee(), 19. refill(), 20. make_coffee(), 21. refill(), 22. make_coffee(), 23. refill(), 24. make_coffee(), 25. refill(), 26. make_coffee(), 27. empty(), 28. make_coffee(), 29. refill(), 30. make_coffee(), 31. refill(), 32. make_coffee(), 33. empty(), 34. done.

simulator.toggle()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.empty()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.refill()
simulator.make_coffee()
simulator.empty()
print(simulator.statistic['coffee_made'] == 22)