class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False

#Heat up hot water and empty it. Do this 5 times.
simulator = KettleSimulator()


# Full Output:
# 1. fill() 
# 2. switch_power()
# 3. heat()
# 4. switch_power() 
# 5. empty()
# 
# 6. fill() 
# 7. switch_power()
# 8. heat()
# 9. switch_power()
# 10. empty()
# 
# 11. fill() 
# 12. switch_power()
# 13. heat()
# 14. switch_power()
# 15. empty()
# 
# 16. fill() 
# 17. switch_power()
# 18. heat()
# 19. switch_power()
# 20. empty()
# 
# 21. fill() 
# 22. switch_power()
# 23. heat()
# 24. switch_power()
# 25. empty()
# 26. done.

simulator.fill()
simulator.switch_power()
simulator.heat()
simulator.switch_power()
simulator.empty()
simulator.fill()
simulator.switch_power()
simulator.heat()
simulator.switch_power()
simulator.empty()
simulator.fill()
simulator.switch_power()
simulator.heat()
simulator.switch_power()
simulator.empty()
simulator.fill()
simulator.switch_power()
simulator.heat()
simulator.switch_power()
simulator.empty()
simulator.fill()
simulator.switch_power()
simulator.heat()
simulator.switch_power()
simulator.empty()
print(simulator.statistic['got_hot_water'] ==5)