class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration

#Turn on the alarm. But not on vibration
simulator = AlarmSimulator()


# Full Output:
# 1. set_alarm() 2. done.

simulator.set_alarm()
print(simulator.set and not simulator.vibration)