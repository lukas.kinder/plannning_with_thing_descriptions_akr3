import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True

#Dispense 10 potions of food times.
simulator = FoodDispenserSimulator()


# Full Output:
# 1. set_state('manual') 2. refill() 3. dispense() 4. refill() 5. dispense() 6. refill() 7. dispense() 8. refill() 9. dispense() 10. refill() 11. dispense() 12. refill() 13. dispense() 14. refill() 15. dispense() 16. refill() 17. dispense() 18. refill() 19. dispense() 20. refill() 21. dispense() 22. done.

simulator.set_state('manual')
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
simulator.refill()
simulator.dispense()
print(simulator.statistics['n_dispensed'] == 10)