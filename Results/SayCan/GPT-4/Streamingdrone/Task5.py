import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100

#Make 12 videos with the drone. Stream all of them.
simulator = StreamingdroneSimulator()


# Full Output:
# 1. start_recording() 2. end() 3. stream() 4. stop_stream() 5. start_recording() 6. end() 7. stream() 8. stop_stream() 9. start_recording() 10. end() 11. stream() 12. stop_stream() 13. start_recording() 14. end() 15. stream() 16. stop_stream() 17. start_recording() 18. end() 19. stream() 20. stop_stream() 21. start_recording() 22. end() 23. stream() 24. stop_stream() 25. start_recording() 26. end() 27. stream() 28. stop_stream() 29. start_recording() 30. end() 31. stream() 32. stop_stream() 33. start_recording() 34. end() 35. stream() 36. stop_stream() 37. start_recording() 38. end() 39. stream() 40. stop_stream() 41. start_recording() 42. end() 43. stream() 44. stop_stream() 45. start_recording() 46. end() 47. stream() 48. stop_stream() 49. done.

simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
simulator.start_recording()
simulator.end()
simulator.stream()
simulator.stop_stream()
print(simulator.statistic['n_recordings'] == 12 and simulator.statistic['n_streams'] == 12)