class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0

#Make salty popcorn 15 times. Turn the machine off afterwards.
simulator = PopcornMakerSimulator()


# Full Output:
# 1. toggle() 
# 2. add_corn() 
# 3. add_salt() 
# 4. heat() 
# 5. add_corn() 
# 6. add_salt() 
# 7. heat() 
# 8. add_corn() 
# 9. add_salt() 
# 10. heat() 
# 11. add_corn() 
# 12. add_salt() 
# 13. heat() 
# 14. add_corn() 
# 15. add_salt() 
# 16. heat() 
# 17. add_corn() 
# 18. add_salt() 
# 19. heat() 
# 20. add_corn() 
# 21. add_salt() 
# 22. heat() 
# 23. add_corn() 
# 24. add_salt() 
# 25. heat() 
# 26. add_corn() 
# 27. add_salt() 
# 28. heat() 
# 29. add_corn() 
# 30. add_salt() 
# 31. heat() 
# 32. add_corn() 
# 33. add_salt() 
# 34. heat() 
# 35. add_corn() 
# 36. add_salt() 
# 37. heat() 
# 38. add_corn() 
# 39. add_salt() 
# 40. heat() 
# 41. add_corn() 
# 42. add_salt() 
# 43. heat() 
# 44. toggle() 
# 45. done.

simulator.toggle()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.toggle()
print(simulator.statistic['made_salty_popcorn'] == 15 and not simulator.power)