class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0

#Please make us 3 times sweet popcorn, 2 salty popcorn, 1 extra salty popcorn and 3 extra sweet popcorn. For the extras just add the sugar/salt twice.
simulator = PopcornMakerSimulator()


# Full Output:
# 1. toggle(), 2. add_corn(), 3. add_sugar(), 4. heat(), 5. add_corn(), 6. add_sugar(), 7. heat(), 8. add_corn(), 9. add_sugar(), 10. heat(), 11. add_corn(), 12. add_salt(), 13. heat(), 14. add_corn(), 15. add_salt(), 16. heat(), 17. add_corn(), 18. add_salt(), 19. add_salt(), 20. heat(), 21. add_corn(), 22. add_sugar(), 23. add_sugar(), 24. heat(), 25. add_corn(), 26. add_sugar(), 27. add_sugar(), 28. heat(), 29. add_corn(), 30. add_sugar(), 31. add_sugar(), 32. heat(), 33. toggle(), 34. done.

simulator.toggle()
simulator.add_corn()
simulator.add_sugar()
simulator.heat()
simulator.add_corn()
simulator.add_sugar()
simulator.heat()
simulator.add_corn()
simulator.add_sugar()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.add_salt()
simulator.heat()
simulator.add_corn()
simulator.add_sugar()
simulator.add_sugar()
simulator.heat()
simulator.add_corn()
simulator.add_sugar()
simulator.add_sugar()
simulator.heat()
simulator.add_corn()
simulator.add_sugar()
simulator.add_sugar()
simulator.heat()
simulator.toggle()
print(simulator.statistic['made_sweet_popcorn'] == 3 and simulator.statistic['made_salty_popcorn'] == 2 and simulator.statistic['made_extra_salty_popcorn'] == 1 and simulator.statistic['made_extra_sweet_popcorn'] == 3)