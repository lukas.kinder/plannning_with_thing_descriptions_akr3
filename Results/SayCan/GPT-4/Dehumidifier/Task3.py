class DehumidifierSimulator:
    def __init__(self):
        self.status = False
        self.tank = "empty"
        self.humidity = 45

        self.actual_humidity = 60

    def get_status(self):
        return self.status
    
    def get_tank(self):
        return self.tank
    
    def get_humidity(self):
        return self.humidity

    def toggle_status(self):
        self.status = not self.status

    def dehumidify(self):
        if self.status and self.tank == "empty":
            if self.actual_humidity > self.humidity:
                self.tank = "full"

    def drain(self):
        self.tank = "empty"
    
    def set_humidity(self,target):
        if self.status:
            if target <= 100 and target >=0:
                self.humidity = target

    

#I want this room to have a humidity of 50
simulator = DehumidifierSimulator()


# Full Output:
# 1. toggle_status() 2. set_humidity(50) 3. dehumidify() 4. done.

simulator.toggle_status()
simulator.set_humidity(50)
simulator.dehumidify()
print(simulator.status and simulator.humidity == 50)