from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    

#I want some light, but not bright.
simulator = AmbientLightstripSimulator()


# Full Output:
# 1. turn_on() 2. set_brightness(100) 3. done.

simulator.turn_on()
simulator.set_brightness(100)
print(simulator.state['on'] and simulator.state['bri'] <= 128)