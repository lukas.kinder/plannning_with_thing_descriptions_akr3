class AirConditionerSimulator:
    def __init__(self):
        self.goal_Temperature = 22
        self.status = False

    def get_goal_Temperature(self):
        return self.goal_Temperature
    
    def get_status(self):
        return self.status
    
    def toggle(self):
        self.status = not self.status

    def temperature_Up(self):
        if self.status:
            self.goal_Temperature = min(35,self.goal_Temperature +1)

    def temperature_Down(self):
        if self.status:
            self.goal_Temperature = max(10,self.goal_Temperature -1)

#Cool down the room to 15 degree.
simulator = AirConditionerSimulator()


# Full Output:
# 1. toggle() 2. temperature_Down() 3. temperature_Down() 4. temperature_Down() 5. done.

simulator.toggle()
simulator.temperature_Down()
simulator.temperature_Down()
simulator.temperature_Down()
print(simulator.status and simulator.goal_Temperature == 15)