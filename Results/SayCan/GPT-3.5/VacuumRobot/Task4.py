import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100

#Let the robot move out to clean and come back. Repeat this 7 times
simulator = VacuumRobotSimulator()


# Full Output:
# 1. empty_storage() 2. start() 3. wait for the robot to finish 4. return_to_base() 5. charge() 6. empty_storage() 7. start() 8. wait for the robot to finish 9. return_to_base() 10. charge() 11. empty_storage() 12. start() 13. wait for the robot to finish 14. return_to_base() 15. charge() 16. empty_storage() 17. start() 18. wait for the robot to finish 19. return_to_base() 20. charge() 21. empty_storage() 22. start() 23. wait for the robot to finish 24. return_to_base() 25. charge() 26. empty_storage() 27. start() 28. wait for the robot to finish 29. return_to_base() 30. charge() 31. empty_storage() 32. start() 33. wait for the robot to finish 34. done.

simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
simulator.return_to_base()
simulator.charge()
simulator.empty_storage()
simulator.start()
print(simulator.statistic['n_cleaned'] == 7)