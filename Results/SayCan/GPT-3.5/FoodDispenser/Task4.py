import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True

#Dispense 10 potions of food times.
simulator = FoodDispenserSimulator()


# Full Output:
# 1. refill() 2. dispense() 3. dispense() 4. dispense() 5. dispense() 6. dispense() 7. dispense() 8. dispense() 9. dispense() 10. dispense() 11. dispense() 12. done.

simulator.refill()
simulator.dispense()
simulator.dispense()
simulator.dispense()
simulator.dispense()
simulator.dispense()
simulator.dispense()
simulator.dispense()
simulator.dispense()
simulator.dispense()
simulator.dispense()
print(simulator.statistics['n_dispensed'] == 10)