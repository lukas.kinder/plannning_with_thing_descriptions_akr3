import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True

#Let the device output food 130 times. Then refill and set to automatic.
simulator = FoodDispenserSimulator()


# Full Output:
# 1. set_state('manual') 2. dispense() (repeated 130 times) 3. refill() 4. set_state('automatic') 5. done.

simulator.set_state('manual')
simulator.dispense() (repeated 130 times)
simulator.refill()
simulator.set_state('automatic')
print(simulator.statistics['n_dispensed'] == 130 and simulator.food_storage == 150 and simulator.automatic)