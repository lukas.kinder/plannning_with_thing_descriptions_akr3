class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0

#15 people are coming. Six of them want sweet popcorn and the rest want salty. Use the machine to make popcorn for all of them.
simulator = PopcornMakerSimulator()


# Full Output:
# 1. toggle() 2. add_corn() 3. heat() 4. add_sugar() 5. heat() 6. heat() 7. heat() 8. add_corn() 9. heat() 10. add_salt() 11. heat() 12. heat() 13. heat() 14. add_corn() 15. heat() 16. add_salt() 17. heat() 18. heat() 19. heat() 20. done.

simulator.toggle()
simulator.add_corn()
simulator.heat()
simulator.add_sugar()
simulator.heat()
simulator.heat()
simulator.heat()
simulator.add_corn()
simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.heat()
simulator.heat()
simulator.add_corn()
simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.heat()
simulator.heat()
print(simulator.statistic['made_sweet_popcorn'] == 6 and simulator.statistic['made_salty_popcorn'] == 9)