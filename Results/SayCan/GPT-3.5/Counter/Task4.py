from datetime import datetime

class CounterSimulator:
    def __init__(self):
        self.count = 0  # current counter value
        self.countAsImage = None  # current counter value as SVG image
        self.redDotImage = None  # Red dot image as PNG
        self.lastChange = ""  # last change of counter value

        self.max_value = 0
        self.min_value = 0

    def get_count(self):
        return self.count
    
    def get_countAsImage(self):
        return self.countAsImage
    
    def get_redDotImage(self):
        return self.redDotImage
    
    def get_lastChange(self):
        return self.lastChange

    def increment(self):
        # increment value
        self.lastChange = str(datetime.now())
        self.count += 1

        if self.count > self.max_value:
            self.max_value = self.count

    def decrement(self):
        # decrement value
        self.lastChange = str(datetime.now())
        self.count -= 1
        if self.min_value > self.count:
            self.min_value = self.count

    def reset(self):
        # Resetting counter value
        self.lastChange = str(datetime.now())
        self.count = 0

#Increment by 30, then decrement 50 times.
simulator = CounterSimulator()


# Full Output:
# 1. increment() 2. increment() 3. increment() 4. increment() 5. increment() 6. increment() 7. increment() 8. increment() 9. increment() 10. increment() 11. increment() 12. increment() 13. increment() 14. increment() 15. increment() 16. increment() 17. increment() 18. increment() 19. increment() 20. increment() 21. increment() 22. increment() 23. increment() 24. increment() 25. increment() 26. increment() 27. increment() 28. increment() 29. increment() 30. decrement() 31. decrement() 32. decrement() 33. decrement() 34. decrement() 35. decrement() 36. decrement() 37. decrement() 38. decrement() 39. decrement() 40. decrement() 41. decrement() 42. decrement() 43. decrement() 44. decrement() 45. decrement() 46. decrement() 47. decrement() 48. decrement() 49. decrement() 50. Done.

simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.increment()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
simulator.decrement()
print(simulator.max_value == 30 and simulator.count == -20)