import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False


#Make me a coffee. Refill and empty the machine once you are done.
simulator = CoffeeMachineSimulator()


# Full Output:
# 1. make_coffee() 2. refill() 3. empty() 4. done.

simulator.make_coffee()
simulator.refill()
simulator.empty()
print(simulator.statistic['coffee_made'] == 1 and simulator.empty_storage == 20 and simulator.water_left == 10)