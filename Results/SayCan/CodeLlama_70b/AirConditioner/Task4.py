class AirConditionerSimulator:
    def __init__(self):
        self.goal_Temperature = 22
        self.status = False

    def get_goal_Temperature(self):
        return self.goal_Temperature
    
    def get_status(self):
        return self.status
    
    def toggle(self):
        self.status = not self.status

    def temperature_Up(self):
        if self.status:
            self.goal_Temperature = min(35,self.goal_Temperature +1)

    def temperature_Down(self):
        if self.status:
            self.goal_Temperature = max(10,self.goal_Temperature -1)

#Make it as cold as possible!
simulator = AirConditionerSimulator()


# Full Output:
# 1. toggle() 2. temperature_Up() 3. temperature_Up() 4. temperature_Up() 5. temperature_Down() 6. done.
# 

simulator.toggle()
simulator.temperature_Up()
simulator.temperature_Up()
simulator.temperature_Up()
simulator.temperature_Down()
print(simulator.status and simulator.goal_Temperature == 10)