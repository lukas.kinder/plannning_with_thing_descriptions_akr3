import random

class FoodDispenserSimulator:
    def __init__(self):
        self.automatic = True
        self.stored = False

        self.food_storage = 0
        self.statistics = {"n_dispensed" : 0}

    def get_automatic(self):
        return self.automatic
    
    def get_stored(self):
        return self.stored

    def dispense(self):
        if self.stored:
            self.statistics['n_dispensed'] +=1
            self.food_storage -= 10 + random.randint(0,1)
            if self.food_storage < 15:
                self.stored = False

    def refill(self):
        self.food_storage = 150
        self.stored = True

    def set_state(self,state):
        if state == "manual":
            self.automatic = False
        if state == "automatic":
            self.automatic = True

#Dispense 10 potions of food times.
simulator = FoodDispenserSimulator()


# Full Output:
# 1. dispense(10) 2. refill() 3. set_state("manual") 4. dispense(10) 5. done.
# 

simulator.dispense(10)
simulator.refill()
simulator.set_state("manual")
simulator.dispense(10)
print(simulator.statistics['n_dispensed'] == 10)