class KettleSimulator:
    def __init__(self, power=False, filled=False):
        self.power = False
        self.filled = False

        self.contain_hot_water = False
        self.statistic = {"got_hot_water" : 0}

    def get_power(self):
        return self.power
    
    def get_filled(self):
        return self.filled

    def switch_power(self):
        self.power = not self.power

    def heat(self):
        if self.power and self.filled:
            self.contain_hot_water = True

    def fill(self):
        if not self.filled:
            self.filled = True

    def empty(self):
        if self.filled:
            self.filled = False

            if self.contain_hot_water:
                self.statistic['got_hot_water'] +=1
                self.contain_hot_water = False

#Heat up hot water and empty it. Do this 5 times.
simulator = KettleSimulator()


# Full Output:
# 1. heating() 2. fill() 3. empty() 4. heating() 5. done.
# 

simulator.heating()
simulator.fill()
simulator.empty()
simulator.heating()
print(simulator.statistic['got_hot_water'] ==5)