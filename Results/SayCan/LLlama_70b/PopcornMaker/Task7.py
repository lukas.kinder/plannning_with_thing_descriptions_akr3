class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0

#15 people are coming. Six of them want sweet popcorn and the rest want salty. Use the machine to make popcorn for all of them.
simulator = PopcornMakerSimulator()


# Full Output:
# 1. 1. toggle(), 2. heat(), 3. add_corn(), 4. add_salt(), 5. add_salt(), 6. add_salt(), 7. add_salt(), 8. add_salt(), 9. add_salt(), 10. add_salt(), 11. add_salt(), 12. add_salt(), 13. add_salt(), 14. add_salt(), 15. add_salt(), 16. add_salt(), 17. add_salt(), 18. add_salt(), 19. add_salt(), 20. add_salt(), 21. add_salt(), 22. add_salt(), 23. add_salt(), 24. add_salt(), 25. add_salt(), 26. add_salt(), 27. add_salt(), 28. add_salt(), 29. add_salt(), 30. add_salt(), 31. add_salt(), 32. add_salt(), 33. add_salt(), 34. add_salt(), 35. add_salt(), 36. add_salt(), 37. add_salt(), 38. add_salt(), 39. add_salt(), 40. add_salt(), 41. add_salt(), 42. add_salt(), 43. add_salt(), 44. add_salt(), 45. add_salt(), 46. add_salt(), 47. add_salt(), 48. add_salt(), 49. add_salt(), 50. add_salt(), 51. add_salt(), 52. add_salt(), 53. add_salt(), 54. add_salt(), 55. add_salt(), 56. add_salt(), 57. add_salt(), 58. add_salt(), 59. add_salt(), 60. add_salt(), 61. add_salt(), 62. add_salt(), 63. add_salt(), 64. add_salt(), 65. add_salt(), 66. add_salt(), 67. add_salt(), 68. add_salt(), 69. add_salt(), 70. add_salt(), 71. add_salt(), 72. add_salt(), 73. add_salt(), 74. add_salt(), 75. add_salt(), 76. add_salt(), 77. add_salt(), 78. add_salt(), 79. add_salt(), 80. add_salt(), 81. add_salt(), 82. add_salt(), 83. add_salt(), 84. add_salt(), 85. add_salt(), 86. add_salt(), 87. add_salt(), 88. add_salt(), 89. add_salt(), 90. add_salt(), 91. add_salt(), 92. add_salt(), 93. add_salt(), 94. add_salt(), 95. add_salt(), 96. add_salt(), 97. add_salt(), 98. add_salt(), 99. add_salt(), 100. add_salt(), 101. add_salt(), 102. add_salt(), 103. add_salt(), 104. add_salt(), 105. add_salt(), 106. add_salt(), 107. add_salt(), 108. add_salt(), 109. add_salt(), 110. add_salt(), 111. add_salt(), 112. add_salt(), 113. add_salt(), 114. add_salt

simulator.1. toggle()
simulator.heat()
simulator.add_corn()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
simulator.. add_salt()
print(simulator.statistic['made_sweet_popcorn'] == 6 and simulator.statistic['made_salty_popcorn'] == 9)