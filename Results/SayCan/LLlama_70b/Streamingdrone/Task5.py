import random

class StreamingdroneSimulator:
    def __init__(self):
        self.recording = False
        self.streaming = False
        self.battery = 50

        self.statistic = {"n_recordings" : 0, "n_streams" : 0}

    def get_recording(self):
        return self.recording
    
    def get_streaming(self):
        return self.streaming
    
    def get_battery(self):
        return self.battery

    def stream(self):
        if self.battery > 0 and not self.streaming:
            self.streaming = True
            self.battery -= random.randint(1,4)

    def stop_stream(self):
        if self.streaming:
            self.streaming = False
            self.statistic['n_streams'] +=1

    def start_recording(self):
        if self.battery > 0 and not self.recording:
            self.recording = True

            self.battery -= random.randint(5,10)

    def end(self):
        if self.recording:
            self.recording = False
            self.statistic['n_recordings'] +=1

    def recharge(self):
        if self.battery < 100 and not self.recording and not self.streaming:
            self.battery = 100

#Make 12 videos with the drone. Stream all of them.
simulator = StreamingdroneSimulator()


# Full Output:
# 1. 12x start_recording() 2. 12x stream() 3. 12x stop_stream() 4. done.
# 

simulator.12x start_recording()
simulator.12x stream()
simulator.12x stop_stream()
print(simulator.statistic['n_recordings'] == 12 and simulator.statistic['n_streams'] == 12)