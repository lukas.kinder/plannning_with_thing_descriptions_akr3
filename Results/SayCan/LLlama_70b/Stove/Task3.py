class StoveSimulator:
    def __init__(self):
        self.plate_1_power = False
        self.plate_2_power = False
        self.plate_1_level = 0
        self.plate_2_level = 0

    def get_plate_1_power(self):
        return self.plate_1_power
    
    def get_plate_2_power(self):
        return self.plate_2_power
    
    def get_plate_1_level(self):
        return self.plate_1_level
    
    def get_plate_2_level(self):
        return self.plate_2_level

    def activate_plate_1(self):
        if not self.plate_1_power:
            self.plate_1_power = True

    def activate_plate_2(self):
        if not self.plate_2_power:
            self.plate_2_power = True

    def deactivate_plate_1(self):
        if self.plate_1_power:
            self.plate_1_power = False

    def deactivate_plate_2(self):
        if self.plate_2_power:
            self.plate_2_power = False

    def increase_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level < 9:
            self.plate_1_level += 1

    def increase_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level < 9:
            self.plate_2_level += 1

    def decrease_heat_plate_1(self):
        if self.plate_1_power and self.plate_1_level > 0:
            self.plate_1_level -= 1

    def decrease_heat_plate_2(self):
        if self.plate_2_power and self.plate_2_level > 0:
            self.plate_2_level -= 1

#There is a pot of water on plate 1. Boil it as quick as possible.
simulator = StoveSimulator()


# Full Output:
# 1. activate_plate_1() 2. increase_heat_plate_1() 3. increase_heat_plate_1() 4. increase_heat_plate_1() 5. increase_heat_plate_1() 6. increase_heat_plate_1() 7. increase_heat_plate_1() 8. increase_heat_plate_1() 9. increase_heat_plate_1() 10. increase_heat_plate_1() 11. decrease_heat_plate_1() 12. deactivate_plate_1() 13. done.
# 

simulator.activate_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.increase_heat_plate_1()
simulator.decrease_heat_plate_1()
simulator.deactivate_plate_1()
print(simulator.plate_1_power and simulator.plate_1_level == 9)