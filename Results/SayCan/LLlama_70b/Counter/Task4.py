from datetime import datetime

class CounterSimulator:
    def __init__(self):
        self.count = 0  # current counter value
        self.countAsImage = None  # current counter value as SVG image
        self.redDotImage = None  # Red dot image as PNG
        self.lastChange = ""  # last change of counter value

        self.max_value = 0
        self.min_value = 0

    def get_count(self):
        return self.count
    
    def get_countAsImage(self):
        return self.countAsImage
    
    def get_redDotImage(self):
        return self.redDotImage
    
    def get_lastChange(self):
        return self.lastChange

    def increment(self):
        # increment value
        self.lastChange = str(datetime.now())
        self.count += 1

        if self.count > self.max_value:
            self.max_value = self.count

    def decrement(self):
        # decrement value
        self.lastChange = str(datetime.now())
        self.count -= 1
        if self.min_value > self.count:
            self.min_value = self.count

    def reset(self):
        # Resetting counter value
        self.lastChange = str(datetime.now())
        self.count = 0

#Increment by 30, then decrement 50 times.
simulator = CounterSimulator()


# Full Output:
# 1. 30, increment() 2. 30, increment() 3. 30, increment() 4. 30, increment() 5. 30, increment() 6. 30, increment() 7. 30, increment() 8. 30, increment() 9. 30, increment() 10. 30, increment() 11. 30, increment() 12. 30, increment() 13. 30, increment() 14. 30, increment() 15. 30, increment() 16. 30, increment() 17. 30, increment() 18. 30, increment() 19. 30, increment() 20. 30, increment() 21. 30, increment() 22. 30, increment() 23. 30, increment() 24. 30, increment() 25. 30, increment() 26. 30, increment() 27. 30, increment() 28. 30, increment() 29. 30, increment() 30. 30, increment() 31. 30, increment() 32. 30, increment() 33. 30, increment() 34. 30, increment() 35. 30, increment() 36. 30, increment() 37. 30, increment() 38. 30, increment() 39. 30, increment() 40. 30, increment() 41. 30, increment() 42. 30, increment() 43. 30, increment() 44. 30, increment() 45. 30, increment() 46. 30, increment() 47. 30, increment() 48. 30, increment() 49. 30, increment() 50. 30, increment() 51. 30, increment() 52. 30, increment() 53. 30, increment() 54. 30, increment() 55. 30, increment() 56. 30, increment() 57. 30, increment() 58. 30, increment() 59. 30, increment() 60. 30, increment() 61. 30, increment() 62. 30, increment() 63. 30, increment() 64. 30, increment() 65. 30, increment() 66. 30, increment() 67. 30, increment() 68. 30, increment() 69. 30, increment() 70. 30, increment() 71. 30, increment() 72. 30, increment() 73. 30, increment() 74. 30, increment() 75. 30, increment() 76. 30, increment() 77. 30, increment() 78. 30, increment() 79. 30, increment() 80. 30, increment() 81. 30, increment() 82. 30, increment() 83. 30, increment() 84. 30, increment() 85. 30, increment() 86. 30, increment() 87. 30, increment() 88. 30, increment() 89. 30, increment() 90. 30, increment() 91. 30, increment() 92. 30, increment() 93. 30, increment() 94. 30, increment() 95. 30, increment() 96. 30, increment() 97. 30, increment() 98. 30, increment() 99. 30, increment() 100. 30, increment() 101. 30, increment() 102. 30, increment() 103. 30, increment() 10

simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.30, increment()
simulator.. 30, increment()
simulator.. 30, increment()
simulator.. 30, increment()
print(simulator.max_value == 30 and simulator.count == -20)