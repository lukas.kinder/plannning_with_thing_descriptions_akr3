import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False


#there is a wedding with 150 people. make coffee for all of them.
simulator = CoffeeMachineSimulator()


# Full Output:
# 1. make_coffee() 2.  make_coffee() 3.  make_coffee() 4.  make_coffee() 5.  make_coffee() 6.  make_coffee() 7.  make_coffee() 8.  make_coffee() 9.  make_coffee() 10.  make_coffee() 11.  make_coffee() 12.  make_coffee() 13.  make_coffee() 14.  make_coffee() 15.  make_coffee() 16.  make_coffee() 17.  make_coffee() 18.  make_coffee() 19.  make_coffee() 20.  make_coffee() 21.  make_coffee() 22.  make_coffee() 23.  make_coffee() 24.  make_coffee() 25.  make_coffee() 26.  make_coffee() 27.  make_coffee() 28.  make_coffee() 29.  make_coffee() 30.  make_coffee() 31.  make_coffee() 32.  make_coffee() 33.  make_coffee() 34.  make_coffee() 35.  make_coffee() 36.  make_coffee() 37.  make_coffee() 38.  make_coffee() 39.  make_coffee() 40.  make_coffee() 41.  make_coffee() 42.  make_coffee() 43.  make_coffee() 44.  make_coffee() 45.  make_coffee() 46.  make_coffee() 47.  make_coffee() 48.  make_coffee() 49.  make_coffee() 50.  make_coffee() 51.  make_coffee() 52.  make_coffee() 53.  make_coffee() 54.  make_coffee() 55.  make_coffee() 56.  make_coffee() 57.  make_coffee() 58.  make_coffee() 59.  make_coffee() 60.  make_coffee() 61.  make_coffee() 62.  make_coffee() 63.  make_coffee() 64.  make_coffee() 65.  make_coffee() 66.  make_coffee() 67.  make_coffee() 68.  make_coffee() 69.  make_coffee() 70.  make_coffee() 71.  make_coffee() 72.  make_coffee() 73.  make_coffee() 74.  make_coffee() 75.  make_coffee() 76.  make_coffee() 77.  make_coffee() 78.  make_coffee() 79.  make_coffee() 80.  make_coffee() 81.  make_coffee() 82.  make_coffee() 83.  make_coffee() 84.  make_coffee() 85.  make_coffee() 86.  make_coffee() 87.  make_coffee() 88.  make_coffee() 89.  make_coffee() 90.  make_coffee() 91.  make_coffee() 92.  make_coffee() 93.  make_coffee() 94.  make_coffee() 95

simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
print(simulator.statistic['coffee_made'] == 150)