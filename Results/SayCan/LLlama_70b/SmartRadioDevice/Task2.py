class SmartRadioDeviceSimulator:
    def __init__(self):
        self.power = "on"
        self.channel = "Jamz"

    def get_power(self):
        return self.power
    
    def get_channel(self):
        return self.channel

    def power_on(self):
        if self.power == "off":
            self.power = "on"

    def switch_channel(self):
        if self.power == "on":
            channels = ["MTV", "Jamz", "The Beat", "The Mix"]
            current_index = channels.index(self.channel)
            next_index = (current_index + 1) % len(channels)
            self.channel = channels[next_index]

    def power_off(self):
        if self.power == "on":
            self.power = "off"

#I want to listen to 'The Beat'.
simulator = SmartRadioDeviceSimulator()


# Full Output:
# 1. 2. power_off() 3. switch_channel('The Beat') 4. power_on() 5. done.
# 

simulator.power_off()
simulator.switch_channel('The Beat')
simulator.power_on()
print(simulator.power and simulator.channel == 'The Beat')