class SmartRadioDeviceSimulator:
    def __init__(self):
        self.power = "on"
        self.channel = "Jamz"

    def get_power(self):
        return self.power
    
    def get_channel(self):
        return self.channel

    def power_on(self):
        if self.power == "off":
            self.power = "on"

    def switch_channel(self):
        if self.power == "on":
            channels = ["MTV", "Jamz", "The Beat", "The Mix"]
            current_index = channels.index(self.channel)
            next_index = (current_index + 1) % len(channels)
            self.channel = channels[next_index]

    def power_off(self):
        if self.power == "on":
            self.power = "off"

#Play MTV on the radio.
simulator = SmartRadioDeviceSimulator()


# Full Output:
# 1. 2. power_off() 3. power_on() 4. switch_channel("MTV") 5. done.
# 

simulator.power_off()
simulator.power_on()
simulator.switch_channel("MTV")
print(simulator.power and simulator.channel == 'MTV')