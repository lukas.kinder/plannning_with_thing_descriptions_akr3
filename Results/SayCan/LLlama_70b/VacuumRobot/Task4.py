import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100

#Let the robot move out to clean and come back. Repeat this 7 times
simulator = VacuumRobotSimulator()


# Full Output:
# 1. 2. return_to_base() 3. scan() 4. return_to_base() 5. return_to_base() 6. return_to_base() 7. return_to_base() 8. return_to_base() 9. return_to_base() 10. return_to_base() 11. return_to_base() 12. return_to_base() 13. return_to_base() 14. return_to_base() 15. return_to_base() 16. return_to_base() 17. return_to_base() 18. return_to_base() 19. return_to_base() 20. return_to_base() 21. return_to_base() 22. return_to_base() 23. return_to_base() 24. return_to_base() 25. return_to_base() 26. return_to_base() 27. return_to_base() 28. return_to_base() 29. return_to_base() 30. return_to_base() 31. return_to_base() 32. return_to_base() 33. return_to_base() 34. return_to_base() 35. return_to_base() 36. return_to_base() 37. return_to_base() 38. return_to_base() 39. return_to_base() 40. return_to_base() 41. return_to_base() 42. return_to_base() 43. return_to_base() 44. return_to_base() 45. return_to_base() 46. return_to_base() 47. return_to_base() 48. return_to_base() 49. return_to_base() 50. return_to_base() 51. return_to_base() 52. return_to_base() 53. return_to_base() 54. return_to_base() 55. return_to_base() 56. return_to_base() 57. return_to_base() 58. return_to_base() 59. return_to_base() 60. return_to_base() 61. return_to_base() 62. return_to_base() 63. return_to_base() 64. return_to_base() 65. return_to_base() 66. return_to_base() 67. return_to_base() 68. return_to_base() 69. return_to_base() 70. return_to_base() 71. return_to_base() 72. return_to_base() 73. return_to_base() 74. return_to_base() 75. return_to_base() 76. return_to_base() 77. return_to_base() 78. return_to_base() 79. return_to_base() 80. return_to_base() 81. return_to_base() 82. return_to_base() 83. return_to_base() 84. return_to_base() 85. return_to_base() 86. return_to_base() 87. return_to_base() 88. return_to_base() 89. return_to_base() 90. return_to_base() 91. return_to_base() 92. return_to_base() 93. return_to_base() 94. return_to_base() 95. return_to_base() 96. return_to_base() 97. return_to_base() 98. return_to_base() 99. return_to_base() 100. return_to_base() 101. return_to_base() 102. return_to_base() 103. return_to_base() 104. return_to_base() 1

simulator.return_to_base()
simulator.scan()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.return_to_base()
simulator.. return_to_base()
simulator.. return_to_base()
simulator.. return_to_base()
simulator.. return_to_base()
print(simulator.statistic['n_cleaned'] == 7)