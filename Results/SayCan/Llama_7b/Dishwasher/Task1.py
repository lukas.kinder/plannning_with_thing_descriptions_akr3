class DishwasherSimulator:
    def __init__(self):
        self.running = False
        self.door = "closed"

        self.statistics = {"used" : 0}

    def get_running(self):
        return self.running
    
    def get_door(self):
        return self.door

    def close(self):
        if self.door == "open":
            self.door = "closed"

    def open(self):
        if self.door == "closed" and not self.running:
            self.door = "open"

    def stop(self):
        if self.running:
            self.running = False

    def start(self):
        if not self.running and self.door == "closed":
            self.running = True

            self.statistics["used"] +=1

#Start the dishwasher
simulator = DishwasherSimulator()


# Full Output:
# 1. start() 2. done.
# 

simulator.start()
print(simulator.running)