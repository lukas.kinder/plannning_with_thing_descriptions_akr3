import random

class CoffeeMachineSimulator:
    def __init__(self):
        self.water = True
        self.storage = True
        self.on = False

        self.empty_storage = 20
        self.water_left = 10
        self.statistic = {"coffee_made" : 0}

    def get_water(self):
        return self.water
    
    def get_storage(self):
        return self.storage
    
    def get_on(self):
        return self.on

    def toggle(self):
        self.on = not self.on

    def refill(self):
        self.water = True
        self.water_left = 10

    def empty(self):
        self.storage = True
        self.empty_storage = 20

    def make_coffee(self):
        if self.on and self.water and self.storage:

            self.statistic["coffee_made"] +=1

            self.empty_storage -= random.randint(1,3)
            if self.empty_storage < 3:
                self.storage = False
            self.water_left-=1
            if self.water_left == 0:
                self.water = False


#Make me 12 coffee.
simulator = CoffeeMachineSimulator()


# Full Output:
# 1. toggle() 2. refill() 3. make_coffee() 4. make_coffee() 5. make_coffee() 6. make_coffee() 7. make_coffee() 8. make_coffee() 9. make_coffee() 10. make_coffee() 11. make_coffee() 12. done.
# 

simulator.toggle()
simulator.refill()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
simulator.make_coffee()
print(simulator.statistic['coffee_made'] == 12)