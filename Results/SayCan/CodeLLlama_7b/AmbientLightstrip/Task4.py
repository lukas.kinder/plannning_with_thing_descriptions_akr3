from copy import deepcopy

class AmbientLightstripSimulator:
    def __init__(self):
        self.state = {"on": False, "bri": 254, "sat":128}

    def get_state(self):
        return deepcopy(self.state)

    def turn_on(self):
        self.state['on'] = True

    def turn_off(self):
        self.state['on'] = False

    def set_brightness(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["bri"] = value

    def set_saturation(self,value):
        if self.state["on"] and value < 255 and value >= 0:
            self.state["sat"] = value

    

#Turn the lights on with 200 brightness and a saturation of 50.
simulator = AmbientLightstripSimulator()


# Full Output:
# 1. turn_on() 2.  set_brightness(200), 3. set_saturation(50) 4. done.
# 

simulator.turn_on()
simulator.set_brightness(200)
simulator.set_saturation(50)
print(simulator.state['on'] and simulator.state['bri'] == 200 and simulator.state['sat'] == 50)