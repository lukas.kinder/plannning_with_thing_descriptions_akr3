class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10

#Turn on the light, but please decrease the brightness.
simulator = ExteriorLightSimulator()


# Full Output:
# 1. turn_on(), 2. decrease(), 3. done.
# 

simulator.turn_on()
simulator.decrease()
print(simulator.status and simulator.brightness < 100)