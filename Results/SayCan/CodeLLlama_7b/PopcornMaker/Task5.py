class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0

#Make salty popcorn 15 times. Turn the machine off afterwards.
simulator = PopcornMakerSimulator()


# Full Output:
# 1. heat() 2. add_salt() 3. heat() 4. add_salt() 5. heat() 6. add_salt() 7. heat() 8. add_salt() 9. heat() 10. add_salt() 11. heat() 12. add_salt() 13. heat() 14. add_salt() 15. heat() 16. done.
# 

simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.add_salt()
simulator.heat()
simulator.add_salt()
simulator.heat()
print(simulator.statistic['made_salty_popcorn'] == 15 and not simulator.power)