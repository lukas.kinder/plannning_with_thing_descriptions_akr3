class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0

#Please make us 3 times sweet popcorn, 2 salty popcorn, 1 extra salty popcorn and 3 extra sweet popcorn. For the extras just add the sugar/salt twice.
simulator = PopcornMakerSimulator()


# Full Output:
# 1. 3 times sweet popcorn, 2 salty popcorn, 1 extra salty popcorn and 3 extra sweet popcorn. For the extras just add the sugar/salt twice.
# 2. add_corn() 3. add_sugar() 4. add_salt() 5. heat() 6. heat() 7. heat() 8. add_sugar() 9. add_salt() 10. heat() 11. heat() 12. heat() 13. done.
# 

simulator.add_corn()
simulator.add_sugar()
simulator.add_salt()
simulator.heat()
simulator.heat()
simulator.heat()
simulator.add_sugar()
simulator.add_salt()
simulator.heat()
simulator.heat()
simulator.heat()
print(simulator.statistic['made_sweet_popcorn'] == 3 and simulator.statistic['made_salty_popcorn'] == 2 and simulator.statistic['made_extra_salty_popcorn'] == 1 and simulator.statistic['made_extra_sweet_popcorn'] == 3)