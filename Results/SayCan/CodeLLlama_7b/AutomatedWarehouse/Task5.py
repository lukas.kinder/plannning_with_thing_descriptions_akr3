from copy import deepcopy
class ObjectOverflow(Exception):
    pass

class AutomatedWarehouseSimulator:
    def __init__(self):
        self.currentProductPosition = {
            "row1col1" : "Wood",
            "row1col2" : "Pipe",
            "row2col2" : "PlasticContainer",
            "row3col1" : "WaterBottles",
            "row3col2" : "Trash",
            "row3col3" : "Cables"
        }

        self.object_inside = None  # wether there is an object inside on the conveyor belt
        self.object_in_arm = None # wether the robotic arm is holding something
        self.arm_is_home = True # if the arm is at the 'home position'
        self.object_waiting_to_get_inside = ["Glass", "FuelContainer"]
        self.objects_moved_out = []

    def get_currentProductPosition(self):
        return deepcopy( self.currentProductPosition )

    def goHome(self):
        self.arm_is_home  = True

    def pickObjectAtPosition(self, row, column):
        # Pick object at a specified warehouse position
        self.arm_is_home  = False

        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            if self.object_in_arm != None:
                raise ObjectOverflow
            self.object_in_arm = self.currentProductPosition[position]
            del self.currentProductPosition[position]

    def putObjectAtPosition(self, row, column):
        # Put object at a specified warehouse position
        self.arm_is_home  = False
        if self.object_in_arm == None:
            #nothing to put
            return
        position = f"row{row}col{column}"
        if position in self.currentProductPosition.keys():
            raise ObjectOverflow
        self.currentProductPosition[position] = self.object_in_arm
        self.object_in_arm = None
        

    def pickObjectFromConveyor(self):
        self.arm_is_home  = False
        # Pick object from a conveyor belt

        if self.object_in_arm != None:
            raise ObjectOverflow

        if self.object_inside != None:
            self.object_in_arm = self.object_inside
            self.object_inside = None

    def putObjectOnConveyor(self):
        self.arm_is_home  = False
        if self.object_in_arm != None:
            # An object is put on the conveyor belt
            if self.object_inside:
                #  There is already an object inside. 
                raise ObjectOverflow
            self.object_inside = self.object_in_arm
            self.object_in_arm = None

    def rollObjectForward(self):
        # An object is moved inside using the conveyor belt.
        if self.object_inside:
            #  There is already an object inside. 
            raise ObjectOverflow
        
        if len(self.object_waiting_to_get_inside) != 0:
            self.object_inside = self.object_waiting_to_get_inside.pop()

    def rollObjectBackward(self):
        if self.object_inside:
            self.objects_moved_out.append(self.object_inside)
            self.object_inside = None

#Take all objects in row 3 and take them out using the conveyor.
simulator = AutomatedWarehouseSimulator()


# Full Output:
# 1. 2. 3. rollObjectBackward() 4. rollObjectForward() 5. pickObjectAtPosition(3, 1) 6. pickObjectAtPosition(3, 2) 7. pickObjectAtPosition(3, 3) 8. done.
# 

simulator.rollObjectBackward()
simulator.rollObjectForward()
simulator.pickObjectAtPosition(3, 1)
simulator.pickObjectAtPosition(3, 2)
simulator.pickObjectAtPosition(3, 3)
print('Trash' in simulator.objects_moved_out and 'Cables' in simulator.objects_moved_out)