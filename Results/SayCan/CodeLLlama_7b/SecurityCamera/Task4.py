import random

class SecurityCameraSimulator:
    def __init__(self):
        self.recording = False
        self.storage = 256

        self.statistic = {"recorded_videos" : 0}

    def get_recording(self):
        return self.recording
    
    def get_storage(self):
        return self.storage

    def record(self):
        if not self.recording and self.storage > 0:
            self.recording = True

    def stop(self):
        if self.recording:
            self.recording = False

            recording_size = random.randint(0,3)
            self.storage -= recording_size

            self.statistic['recorded_videos'] += 1

    def delete(self):
        if not self.recording:
            self.storage = 256

#Use the camera to practice how to make videos. But delete everything once you are done
simulator = SecurityCameraSimulator()


# Full Output:
# 1. 2. 3. delete() 4. done.
# 

simulator.delete()
print(simulator.statistic['recorded_videos'] > 0 and simulator.storage == 256)