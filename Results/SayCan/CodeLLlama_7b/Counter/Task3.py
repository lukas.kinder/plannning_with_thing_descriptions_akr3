from datetime import datetime

class CounterSimulator:
    def __init__(self):
        self.count = 0  # current counter value
        self.countAsImage = None  # current counter value as SVG image
        self.redDotImage = None  # Red dot image as PNG
        self.lastChange = ""  # last change of counter value

        self.max_value = 0
        self.min_value = 0

    def get_count(self):
        return self.count
    
    def get_countAsImage(self):
        return self.countAsImage
    
    def get_redDotImage(self):
        return self.redDotImage
    
    def get_lastChange(self):
        return self.lastChange

    def increment(self):
        # increment value
        self.lastChange = str(datetime.now())
        self.count += 1

        if self.count > self.max_value:
            self.max_value = self.count

    def decrement(self):
        # decrement value
        self.lastChange = str(datetime.now())
        self.count -= 1
        if self.min_value > self.count:
            self.min_value = self.count

    def reset(self):
        # Resetting counter value
        self.lastChange = str(datetime.now())
        self.count = 0

#Count to 100. then reset.
simulator = CounterSimulator()


# Full Output:
# 1. 11... increment() 2. 21... increment() 3. 31... increment() 4. 41... increment() 5. 51... increment() 6. 61... increment() 7. 71... increment() 8. 81... increment() 9. 91... increment() 10. 101... increment() 11. 111... increment() 12. 121... increment() 13. 131... increment() 14. 141... increment() 15. 151... increment() 16. 161... increment() 17. 171... increment() 18. 181... increment() 19. 191... increment() 20. 201... increment() 21. 211... increment() 22. 221... increment() 23. 231... increment() 24. 241... increment() 25. 251... increment() 26. 261... increment() 27. 271... increment() 28. 281... increment() 29. 291... increment() 30. 301... increment() 31. 311... increment() 32. 321... increment() 33. 331... increment() 34. 341... increment() 35. 351... increment() 36. 361... increment() 37. 371... increment() 38. 381... increment() 39. 391... increment() 40. 401... increment() 41. 411... increment() 42. 421... increment() 43. 431... increment() 44. 441... increment() 45. 451... increment() 46. 461... increment() 47. 471... increment() 48. 481... increment() 49. 491... increment() 50. 501... increment() 51. 511... increment() 52. 521... increment() 53. 531... increment() 54. 541... increment() 55. 551... increment() 56. 561... increment() 57. 571... increment() 58. 581... increment() 59. 591... increment() 60. 601... increment() 61. 611... increment() 62. 621... increment() 63. 631... increment() 64. 641... increment() 65. 651... increment() 66. 661... increment() 67. 671... increment() 68. 681... increment() 69. 691... increment() 70. 701... increment() 71. 711... increment() 72. 721... increment() 73. 731... increment() 74. 741... increment() 75. 751... increment() 76. 761... increment() 77. 771... increment() 78. 781... increment() 79. 791... increment() 80. 801... increment() 81. 811... increment() 82. 821... increment() 83. 831... increment() 84. 841... increment() 85. 851... increment() 86. 861... increment() 87. 871... increment() 88. 881... increment() 89. 891... increment() 90. 901... increment() 91. 911... increment() 92. 921... increment() 93. 931... increment() 94. 941... increment() 95. 951... increment() 

simulator.11... increment()
simulator.21... increment()
simulator.31... increment()
simulator.41... increment()
simulator.51... increment()
simulator.61... increment()
simulator.71... increment()
simulator.81... increment()
simulator.91... increment()
simulator.101... increment()
simulator.111... increment()
simulator.121... increment()
simulator.131... increment()
simulator.141... increment()
simulator.151... increment()
simulator.161... increment()
simulator.171... increment()
simulator.181... increment()
simulator.191... increment()
simulator.201... increment()
simulator.211... increment()
simulator.221... increment()
simulator.231... increment()
simulator.241... increment()
simulator.251... increment()
simulator.261... increment()
simulator.271... increment()
simulator.281... increment()
simulator.291... increment()
simulator.301... increment()
simulator.311... increment()
simulator.321... increment()
simulator.331... increment()
simulator.341... increment()
simulator.351... increment()
simulator.361... increment()
simulator.371... increment()
simulator.381... increment()
simulator.391... increment()
simulator.401... increment()
simulator.411... increment()
simulator.421... increment()
simulator.431... increment()
simulator.441... increment()
simulator.451... increment()
simulator.461... increment()
simulator.471... increment()
simulator.481... increment()
simulator.491... increment()
simulator.501... increment()
simulator.511... increment()
simulator.521... increment()
simulator.531... increment()
simulator.541... increment()
simulator.551... increment()
simulator.561... increment()
simulator.571... increment()
simulator.581... increment()
simulator.591... increment()
simulator.601... increment()
simulator.611... increment()
simulator.621... increment()
simulator.631... increment()
simulator.641... increment()
simulator.651... increment()
simulator.661... increment()
simulator.671... increment()
simulator.681... increment()
simulator.691... increment()
simulator.701... increment()
simulator.711... increment()
simulator.721... increment()
simulator.731... increment()
simulator.741... increment()
simulator.751... increment()
simulator.761... increment()
simulator.771... increment()
simulator.781... increment()
simulator.791... increment()
simulator.801... increment()
simulator.811... increment()
simulator.821... increment()
simulator.831... increment()
simulator.841... increment()
simulator.851... increment()
simulator.861... increment()
simulator.871... increment()
simulator.881... increment()
simulator.891... increment()
simulator.901... increment()
simulator.911... increment()
simulator.921... increment()
simulator.931... increment()
simulator.941... increment()
print(simulator.max_value == 100 and simulator.count == 0)