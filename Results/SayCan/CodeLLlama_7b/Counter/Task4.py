from datetime import datetime

class CounterSimulator:
    def __init__(self):
        self.count = 0  # current counter value
        self.countAsImage = None  # current counter value as SVG image
        self.redDotImage = None  # Red dot image as PNG
        self.lastChange = ""  # last change of counter value

        self.max_value = 0
        self.min_value = 0

    def get_count(self):
        return self.count
    
    def get_countAsImage(self):
        return self.countAsImage
    
    def get_redDotImage(self):
        return self.redDotImage
    
    def get_lastChange(self):
        return self.lastChange

    def increment(self):
        # increment value
        self.lastChange = str(datetime.now())
        self.count += 1

        if self.count > self.max_value:
            self.max_value = self.count

    def decrement(self):
        # decrement value
        self.lastChange = str(datetime.now())
        self.count -= 1
        if self.min_value > self.count:
            self.min_value = self.count

    def reset(self):
        # Resetting counter value
        self.lastChange = str(datetime.now())
        self.count = 0

#Increment by 30, then decrement 50 times.
simulator = CounterSimulator()


# Full Output:
# 1. 30, increment() 2. 49, decrement() 3. 48, decrement() 4. 47, decrement() 5. 46, decrement() 6. 45, decrement() 7. 44, decrement() 8. 43, decrement() 9. 42, decrement() 10. 41, decrement() 11. 40, decrement() 12. 39, decrement() 13. 38, decrement() 14. 37, decrement() 15. 36, decrement() 16. 35, decrement() 17. 34, decrement() 18. 33, decrement() 19. 32, decrement() 20. 31, decrement() 21. 30, decrement() 22. 29, decrement() 23. 28, decrement() 24. 27, decrement() 25. 26, decrement() 27. 25, decrement() 28. 24, decrement() 29. 23, decrement() 30. 22, decrement() 31. 21, decrement() 32. 20, decrement() 33. 19, decrement() 34. 18, decrement() 35. 17, decrement() 36. 16, decrement() 37. 15, decrement() 38. 14, decrement() 39. 13, decrement() 40. 12, decrement() 41. 11, decrement() 42. 10, decrement() 43. 9, decrement() 44. 8, decrement() 45. 7, decrement() 46. 6, decrement() 47. 5, decrement() 48. 4, decrement() 49. 3. 30, decrement() 4. 30, decrement() 3. 30, decrement() 2. 30, decrement() 1. 30, decrement() 0. done.
# 

simulator.30, increment()
simulator.49, decrement()
simulator.48, decrement()
simulator.47, decrement()
simulator.46, decrement()
simulator.45, decrement()
simulator.44, decrement()
simulator.43, decrement()
simulator.42, decrement()
simulator.41, decrement()
simulator.40, decrement()
simulator.39, decrement()
simulator.38, decrement()
simulator.37, decrement()
simulator.36, decrement()
simulator.35, decrement()
simulator.34, decrement()
simulator.33, decrement()
simulator.32, decrement()
simulator.31, decrement()
simulator.30, decrement()
simulator.29, decrement()
simulator.28, decrement()
simulator.27, decrement()
print(simulator.max_value == 30 and simulator.count == -20)