class SmartHeaterSimulator:
    def __init__(self):
        self.level = 4
        self.status = False

    def get_level(self):
        return self.level
    
    def get_status(self):
        return self.status

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase_level(self):
        if self.status and self.level < 6:
            self.level += 1

    def decrease_level(self):
        if self.status and self.level > 0:
            self.level -= 1

# from Devices import SmartHeater # class comes with functions: turn_on(), turn_off(), increase_level(), decrease_level(), 

# I am so cold! Make it as warm as possible.
simulator = SmartHeaterSimulator()
simulator.turn_on()
simulator.increase_level()
simulator.increase_level()
simulator.increase_level()
simulator.increase_level()
simulator.increase_level()
simulator.increase_level()
print(simulator.status and simulator.level == 6)