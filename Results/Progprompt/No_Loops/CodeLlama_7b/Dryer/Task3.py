import random

class DryerSimulator:
    def __init__(self):
        self.running = False
        self.temperature = 50
        self.distilled_storage = 50

    def get_running(self):
        return self.running
    
    def get_temperature(self):
        return self.temperature
    
    def get_distilled_storage(self):
        return self.distilled_storage

    def start(self):
        if not self.running and self.distilled_storage > 0:
            self.running = True
            self.distilled_storage -= random.randint(10,30)

    def clear(self):
        self.distilled_storage = 100

    def increase_temperature(self):
        if not self.running and self.temperature < 80:
            self.temperature += 10

    def decrease_temperature(self):
        if not self.running and self.temperature > 40:
            self.temperature -= 10

# from Devices import Dryer # class comes with functions: start(), clear(), increase_temperature(), decrease_temperature(), 

# Run the dryer with a temperature of 60.
simulator = DryerSimulator()
simulator.start()
simulator.increase_temperature()
simulator.increase_temperature()
simulator.increase_temperature()
simulator.increase_temperature()
simulator.increase_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
simulator.decrease_temperature()
d
print(simulator.running and simulator.temperature == 60)