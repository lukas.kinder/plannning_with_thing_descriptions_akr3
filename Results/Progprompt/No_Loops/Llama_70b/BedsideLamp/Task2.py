class BedsideLampSimulator:
    def __init__(self):
        self.on = False
        self.brightness = 50

    def get_on(self):
        return self.on
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.on:
            self.on = True

    def turn_off(self):
        if self.on:
            self.on = False

    def increase(self):
        if self.on:
            self.brightness = min(100,self.brightness +10)

    def decrease(self):
        if self.on:
            self.brightness = max(0,self.brightness -10)

# from Devices import BedsideLamp # class comes with functions: increase(), decrease(), turn_on(), turn_off(), 

# Give me a brightness of 30
simulator = BedsideLampSimulator()
simulator.turn_on()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.decrease()
bedside
print(simulator.on and simulator.brightness == 30)