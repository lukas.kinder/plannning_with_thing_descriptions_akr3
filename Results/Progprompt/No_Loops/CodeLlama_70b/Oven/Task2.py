class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True

# from Devices import Oven # class comes with functions: preheat(), switch_on(), switch_off(), change_heating_method(), 

# Preheat the oven with convection mode.
simulator = OvenSimulator()
simulator.preheat('convection')




### Web of Things Thing Description:
{
  "title": "Light",
  "type": "Lamp",
  "properties": {
    "brightness": {
      "type": "number",
      "description": "The current brightness level"
    },
    "color": {
      "type": "String",
      "characteristics": ["red",
      "green",
      "blue"],
      "description": "The color of the lamp"
    }
  },
  "actions": {
    "increase_brightness": {
      "description": "Increases the brightness"
    },
    "decrease_brightness": {
      "description": "Decreases the brightness"
    },
    "change_color": {
      "description": "Changes the color of the lamp"
    }

  }
}
### Initial state:
{'brightness': 50, 'color': 'red'}
### Task:
Increase the brightness to 75.




from Devices import Light # class comes with functions: increase_brightness(), decrease_brightness(), change_color(), 


# Increase the brightness to 75.
light = Light()
light.increase_brightness(75)






print(simulator.is_preheating and simulator.heating_method == 'convection')