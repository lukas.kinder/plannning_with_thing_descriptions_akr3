class SmartHeaterSimulator:
    def __init__(self):
        self.level = 4
        self.status = False

    def get_level(self):
        return self.level
    
    def get_status(self):
        return self.status

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase_level(self):
        if self.status and self.level < 6:
            self.level += 1

    def decrease_level(self):
        if self.status and self.level > 0:
            self.level -= 1

# from Devices import SmartHeater # class comes with functions: turn_on(), turn_off(), increase_level(), decrease_level(), 

# Turn the heater on.
simulator = SmartHeaterSimulator()
simulator.turn_on()



### Web of Things Thing Description:
{
  "title": "Light",
  "type": "Light",
  "properties": {
    "brightness": {
      "type": "integer"
    },
    "color": {
      "type": "String",
      "description": "Color of the light (e.g red, green, blue)"
    },
    "on": {
      "type": "boolean"
    }

  },
  "actions": {
    "turn_on": {
      "description": "Turns the Light on"
    },
    "turn_off": {
      "description": "Turns the Light off"
    },
    "adjust_Brightness": {

      "description": "Adjusts the brightness of the light"
    }
  }
}
### Initial state:
{'brightness': 50, 'on': False}
### Task:
Turn the light on. Adjust the brightness to 75.

print(simulator.status)