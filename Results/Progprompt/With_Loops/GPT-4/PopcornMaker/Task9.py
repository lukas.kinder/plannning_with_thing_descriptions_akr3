class MachineFull(Exception):
    pass

class PopcornMakerSimulator:
    def __init__(self):
        self.power = False

        self.contains_corn = False
        #number of times ingredients are added
        self.contains_sugar = 0
        self.contains_salt = 0

        self.statistic = {"made_salty_popcorn" : 0, "made_sweet_popcorn" : 0,
                          "made_extra_salty_popcorn" : 0, "made_extra_sweet_popcorn" : 0}

    def get_power(self):
        return self.power

    def toggle(self):
        self.power = not self.power

    def add_corn(self):
        if self.contains_corn:
            raise MachineFull
        self.contains_corn = True
        
    def add_sugar(self):
        self.contains_sugar +=1

    def add_salt(self):
        self.contains_salt +=1

    def heat(self):
        if self.power:

            if self.contains_corn and self.contains_salt ==1:
                self.statistic['made_salty_popcorn'] +=1

            if self.contains_corn and self.contains_salt ==2:
                self.statistic['made_extra_salty_popcorn'] +=1
            
            if self.contains_corn and self.contains_sugar ==1:
                self.statistic['made_sweet_popcorn'] +=1

            if self.contains_corn and self.contains_sugar ==2:
                self.statistic['made_extra_sweet_popcorn'] +=1

            # Machine gets emptied automatically after heating
            self.contains_corn = 0
            self.contains_sugar = 0
            self.contains_salt = 0

# from Devices import PopcornMaker # class comes with functions: toggle(), heat(), add_corn(), add_sugar(), add_salt(), 

# Please make us 3 times sweet popcorn, 2 salty popcorn, 1 extra salty popcorn and 3 extra sweet popcorn. For the extras just add the sugar/salt twice.
simulator = PopcornMakerSimulator()
simulator.toggle()

def make_popcorn(sweet=False, extra=False):
    simulator.add_corn()
    if sweet:
        simulator.add_sugar()
        if extra:
            simulator.add_sugar()
    else:
        simulator.add_salt()
        if extra:
            simulator.add_salt()
    simulator.heat()

for _ in range(3):
    make_popcorn(sweet=True)
for _ in range(2):
    make_popcorn(sweet=False)
make_popcorn(sweet=False, extra=True)
for _ in range(3):
    make_popcorn(sweet=True, extra=True)

simulator.toggle()
print(simulator.statistic['made_sweet_popcorn'] == 3 and simulator.statistic['made_salty_popcorn'] == 2 and simulator.statistic['made_extra_salty_popcorn'] == 1 and simulator.statistic['made_extra_sweet_popcorn'] == 3)