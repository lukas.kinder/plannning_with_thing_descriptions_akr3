class ExteriorLightSimulator:
    def __init__(self):
        self.status = False
        self.brightness = 100

    def get_status(self):
        return self.status
    
    def get_brightness(self):
        return self.brightness

    def turn_on(self):
        if not self.status:
            self.status = True

    def turn_off(self):
        if self.status:
            self.status = False

    def increase(self):
        if self.status and self.brightness < 100:
            self.brightness += 10

    def decrease(self):
        if self.status and self.brightness > 0:
            self.brightness -= 10

# from Devices import ExteriorLight # class comes with functions: increase(), decrease(), turn_on(), turn_off(), 

# Light!
simulator = ExteriorLightSimulator()
simulator.turn_on()
simulator.increase()
simulator.increase()
simulator.decrease()
simulator.decrease()
simulator.decrease()
simulator.turn_off()

print(simulator.status)