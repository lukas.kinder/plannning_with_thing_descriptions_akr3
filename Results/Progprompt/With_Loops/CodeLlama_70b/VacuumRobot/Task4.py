import random

class VacuumRobotSimulator:
    def __init__(self):
        self.cleaning = False
        self.storage_full = True # is currently full
        self.battery = 30
        self.mapped = True
        self.at_base = True

        self.statistic = {"n_cleaned" : 0}

    def get_cleaning(self):
        return self.cleaning
    
    def get_storage_full(self):
        return self.storage_full
    
    def get_battery(self):
        return self.battery
    
    def get_mapped(self):
        return self.mapped
    
    def get_at_base(self):
        return self.at_base
    
    def start(self):
        if self.battery > 0 and not self.storage_full and self.mapped and not self.cleaning:
            self.cleaning = True
            self.storage_full = True
            self.statistic['n_cleaned'] +=1

    def scan(self):
        if self.battery > 0:
            self.mapped = True
            self.battery -= 10

    def empty_storage(self):
        if self.at_base:
            self.storage_full = False

    def return_to_base(self):
        if not self.at_base:
            self.at_base = True
            self.cleaning = False

            self.battery -= min(self.battery, random.randint(5,20)) #consumes some battery

    def charge(self):
        if self.at_base:
            self.battery = 100

# from Devices import VacuumRobot # class comes with functions: start(), scan(), empty_storage(), return_to_base(), charge(), 

# Let the robot move out to clean and come back. Repeat this 7 times
simulator = VacuumRobotSimulator()
for i in range(7):
    simulator.start()
    simulator.scan()
    simulator.empty_storage()
    simulator.return_to_base()
    simulator.charge()

print(simulator.statistic['n_cleaned'] == 7)