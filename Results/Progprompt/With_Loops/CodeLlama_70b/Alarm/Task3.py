class AlarmSimulator:
    def __init__(self):
        self.set = False
        self.vibration = False

    def get_set(self):
        return self.set
    
    def get_vibration(self):
        return self.vibration

    def set_alarm(self):
        if not self.set:
            self.set = True

    def unset(self):
        if self.set:
            self.set = False

    def toggle_vibration_mode(self):
        self.vibration = not self.vibration

# from Devices import Alarm # class comes with functions: unset(), set_alarm(), toggle_vibration_mode(), 

# I want you to set an alarm with vibration.
simulator = AlarmSimulator()
simulator.set(True)
simulator.toggle_vibration_mode()

print(simulator.set and simulator.vibration)