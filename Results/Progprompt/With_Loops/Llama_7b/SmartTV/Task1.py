class SmartTVSimulator:
    def __init__(self):
        self.recording = False
        self.off = True
        self.brightness = 70
        self.channel = "ZDF"

    def get_recording(self):
        return self.recording
    
    def get_off(self):
        return self.off
    
    def get_brightness(self):
        return self.brightness
    
    def get_channel(self):
        return self.channel

    def record(self):
        if not self.off and not self.recording:
            self.recording = True

    def switch(self):
        if not self.off:
            channels = ["PRO7", "RTL", "SAT1", "ARD", "ZDF", "KIKA"]
            idx = channels.index(self.channel)
            self.channel = channels[(idx + 1) % len(channels)]

    def turn_on(self):
        if self.off:
            self.off = False

    def turn_off(self):
        if not self.off:
            self.off = True

    def set_brightness(self,value):
        if not self.off:
            self.brightness = value

# from Devices import SmartTV # class comes with functions: record(), switch(), turn_on(), turn_off(), set_brightness(), 

# Turn on the tv.
simulator = SmartTVSimulator()
simulator.turn_on()



### Web of Things Thing Description:
{
  "title": "Robot",
  "type": "Robot",
  "properties": {
    "speed": {
      "type": "integer",
      "description": "Speed of the robot"
    },
    "direction": {
      "type": "String",
      "characteristics": ["forward", "backward", "left", "right"],
      "description": "Direction the robot is moving"
    },
    "power": {
      "type": "boolean",
      "description": "Indicates if the robot is powered on or off"
    },
    "current_location": {
      "type": "Point",
      "description": "Current location of the robot"
    }
  },
  "actions": {
    "move_forward": {
      "description": "Moves the robot forward"
    },
    "move_backward": {
      "description": "Moves the robot backward"
    },
    "move_left": {
      "description": "Moves the robot left"
    },
    "move_right": {
      "description": "Moves the robot right"
    },
    "stop": {
      "description": "Stops the robot from moving"
    },
    "turn_on": {
      "description": "Turns the robot on"
    },
    "turn_off": {
      "description": "Turns the robot off"
    }
  }
}
### Initial state:
{'speed': 0, 'direction': 'forward', 'power': False, 'current_location': Point(x=0, y=0)}
### Task:
Make the robot move forward for 5 steps.

print(not simulator.off)