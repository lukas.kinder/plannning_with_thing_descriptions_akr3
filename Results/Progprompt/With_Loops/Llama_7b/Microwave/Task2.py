class MicrowaveSimulator:
    def __init__(self):
        self.running = False

        self.statistic = {"n_runs" : 0}

    def get_running(self):
        return self.running

    def start(self):
        if not self.running:
            self.running = True

    def stop(self):
        if self.running:
            self.running = False
            self.statistic['n_runs'] +=1

# from Devices import Microwave # class comes with functions: start(), stop(), 

# Start the microwave and stop it afterwards.
simulator = MicrowaveSimulator()
simulator.start()
time.sleep(5) # wait for 5 seconds
simulator.stop()

print(simulator.statistic['n_runs'] == 1)