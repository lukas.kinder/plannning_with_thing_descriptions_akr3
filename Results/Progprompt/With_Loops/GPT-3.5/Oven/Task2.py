class OvenSimulator:
    def __init__(self):
        self.on = False
        self.heating_method = "convection"

        self.is_preheating = False

    def get_on(self):
        return self.on
    
    def get_heating_method(self):
        return self.heating_method
    
    def switch_on(self):
        if not self.on:
            self.on = True

    def switch_off(self):
        if self.on:
            self.on = False
            self.is_preheating = False

    def change_heating_method(self):
        if self.on: 
            if self.heating_method == "convection":
                self.heating_method = "top and bottom"
            else:
                self.heating_method = "convection"

    def preheat(self):
        if self.on:
            self.is_preheating = True

# from Devices import Oven # class comes with functions: preheat(), switch_on(), switch_off(), change_heating_method(), 

# Preheat the oven with convection mode.
simulator = OvenSimulator()
simulator.switch_on()
simulator.change_heating_method()
simulator.preheat()
print(simulator.is_preheating and simulator.heating_method == 'convection')