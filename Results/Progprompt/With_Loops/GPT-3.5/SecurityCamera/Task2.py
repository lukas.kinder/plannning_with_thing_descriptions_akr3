import random

class SecurityCameraSimulator:
    def __init__(self):
        self.recording = False
        self.storage = 256

        self.statistic = {"recorded_videos" : 0}

    def get_recording(self):
        return self.recording
    
    def get_storage(self):
        return self.storage

    def record(self):
        if not self.recording and self.storage > 0:
            self.recording = True

    def stop(self):
        if self.recording:
            self.recording = False

            recording_size = random.randint(0,3)
            self.storage -= recording_size

            self.statistic['recorded_videos'] += 1

    def delete(self):
        if not self.recording:
            self.storage = 256

# from Devices import SecurityCamera # class comes with functions: record(), stop(), delete(), 

# Make a recording and stop once done.
simulator = SecurityCameraSimulator()
simulator.record()
# Recording process (e.g., detecting motion, saving file to storage, etc.)
simulator.stop()
print(simulator.statistic['recorded_videos'] == 1)