from WoT_Thing_Description import *
from generate_prompt import find_initial_state
from llm import *
import subprocess

#81 LLama big
#82 = small Codellama

LLM = "Llama" # one of: "Llama" ,"gpt-4" , "gpt-3.5"
ENDPOINT = "home_endpoint_81.txt"
RESULT_FOLDER = "Results/Progprompt/No_Loops/Llama_70b/"
USE_LOOPS = False

WOT_TD_FOLDER =  "WoT-TD/"
TASKS_FOLDER =  "Tasks/"
SIMULATORS_FOLDER =  "Simulators/"

STATISTIC = {
    "number_of_successes" : 0,
    "number_of_failures" : 0,
    "failures_list" : [],
    "number_of_errors" : 0,
    "errors_list" : [],
    "number_of_timeouts" : 0,
    "timeouts_list" : []
}

def getDeviceNames():
    names = []
    for _ , _ , files in os.walk(TASKS_FOLDER):
        for file in files:
            names.append(file[:-5])
    return names

def load_task_file(device):
    with open(TASKS_FOLDER + device + ".json", "r") as f:
        file = json.load(f)
    return file

def load_real_simulator(device):
    with open(SIMULATORS_FOLDER + device + "Simulator.py", "r") as f:
        file = f.read()
    return file

class ProgpromptPromptGenerator:

    def __init__(self,llm,use_loops):
        self.model = llm
        self.use_loops = use_loops
        
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt = []
        else:
            self.prompt = ""

        if use_loops:
            self.system_prompt = """You are an expert programmer that writes simple and concise code.
You are provided with a Web-ofThings Thing Description that describes the workings of a device. This device
has already been implemented as a python class. Your task is to call functions of this class such that a command
given in natural language is fulfilled.

Rules:
    - Avoid code duplication and use loops or create auxiliary functions if necessary.
    - You only reply by writing the code, no explanations in natural language.
    - Be carful, as some actions have requirements. For example, interacting with a device often requires that it is turned on initially.
    - You only manipulate the device using the functions, not by directly changing variables of the class.
"""  
        else:
            self.system_prompt = """You are an expert programmer that writes simple and concise code.
You are provided with a Web-ofThings Thing Description that describes the workings of a device. This device
has already been implemented as a python class. Your task is to call functions of this class such that a command
given in natural language is fulfilled.

Rules:
    - There should be one line for every time a function is called. Do not use loops.
    - You only reply by writing the code, no explanations in natural language.
    - Be carful, as some actions have requirements. For example, interacting with a device often requires that it is turned on initially.
    - You only manipulate the device using the functions, not by directly changing variables of the class.
"""  

        self.examples = ["prompt_examples/GarageDoor","prompt_examples/MP3Player","prompt_examples/Printer"]

    def task_description(self,wot_td,task,initial_state):
        description =  f"""
### Web of Things Thing Description:
{wot_td.as_string()}
### Initial state:
{str(initial_state)}
### Task:
{task}"""

        if self.model == "gpt-3.5" or self.model == "gpt-4":
            
            description += f"\n\nStart your reply with:\n{self.initial_Lines(wot_td,full=True,nl_task=task)}"
        
        return  description
    
    def reset_prompt(self):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt = []
        else:
            self.prompt = ""

    def initial_Lines(self,wot_td: WoT_Thing_Description, full = False,nl_task = None):
        lines = f"from Devices import {wot_td.class_name()} # class comes with functions: "
        functions = wot_td.action_list()
        for function in functions:
            lines += function + "(), "
        lines +="\n\n"

        if full:
            lines += f"# {nl_task}\n"
            lines += f"{rewrite_string_to_variable(wot_td.class_name())} = {wot_td.class_name()}()\n"
        return lines


    def generate_prompt(self,wot_td :WoT_Thing_Description,task):
        self.reset_prompt()
        self.add_to_prompt(self.system_prompt,"system")

        for example in self.examples:
            example_wot_td = WoT_Thing_Description(example + ".json")
            example_wot_td.simplify_wotd()
            example_wot_td.standardize_action_names()
            example_wot_td.standardize_property_names()

            with open(example + "Task.txt", "r") as f:
                example_task = f.read()
            example_initial_state = find_initial_state(example_wot_td)

            example_task_prompt = self.task_description(example_wot_td,example_task,example_initial_state)

            self.add_to_prompt(example_task_prompt,"user")

            if self.use_loops:
                with open(example + "TaskWithLoops.py","r") as f:
                    plan = f.read()
            else:
                with open(example + "TaskNoLoops.py","r") as f:
                    plan = f.read()

            self.add_to_prompt(self.initial_Lines(example_wot_td) + plan,"assistant")

        wot_td.simplify_wotd()
        wot_td.standardize_action_names()
        wot_td.standardize_property_names()

        initial_state = find_initial_state(wot_td)
        task_prompt = self.task_description(wot_td,task,initial_state)

        self.add_to_prompt(task_prompt,"user")

        if self.model == "Llama":
            self.prompt += "\n<step> Source: assistant\n\n\n" + self.initial_Lines(wot_td,full = True,nl_task= task)

        return self.prompt
    
    def add_to_prompt(self, text, origin = "user"):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt.append({"role": origin, "content": text})
        else:
            if not self.prompt == "":
                self.prompt += "\n<step> "
            self.prompt += f"Source: {origin}\n\n\n{text}"


    def print_prompt_in_nice_way(self):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            for message in self.prompt:
                print(message["role"].upper() + ":")
                print(message["content"])
        else:
            print(self.prompt)

def verify_task(path):
    try:
        res = str(subprocess.check_output(["python",path],timeout= 1))[2:].split("\\r\\n")[:-1]

        if "True" in str(res[-1]):
            print("SUCCESS")
            STATISTIC["number_of_successes"] +=1
        else:
            print("PLAN FAILED")
            STATISTIC["failures_list"].append(path)
            STATISTIC["number_of_failures"] +=1
    except subprocess.TimeoutExpired:
        print("TIMEOUT")
        # generated code did not run error-free
        STATISTIC["number_of_timeouts"] +=1
        STATISTIC["timeouts_list"].append(path)
    except:
        print("ERROR")
        # generated code did not run error-free
        STATISTIC["errors_list"].append(path)
        STATISTIC["number_of_errors"] +=1

def post_process_plan(plan,wot_td: WoT_Thing_Description):
    first_linebreak = plan.index("\n")
    plan = "# " + plan[:first_linebreak] + plan[first_linebreak:]
    class_name = wot_td.class_name()
    instance_name = rewrite_string_to_variable(class_name)

    plan = plan.replace(instance_name + ".","simulator.")
    plan = plan.replace(f"{instance_name} = {class_name}()",f"simulator = {class_name}Simulator()")
    return plan



def runProgprompt(device):
    wot_td = WoT_Thing_Description(WOT_TD_FOLDER  + device + ".json")

    tasks = load_task_file(device)
    task_numbers = tasks.keys()

    for task_number in task_numbers:

        nl_task  = tasks[task_number]['nl']
        print("TASK: " + nl_task)
        pg = ProgpromptPromptGenerator(LLM,use_loops=USE_LOOPS)
        prompt = pg.generate_prompt(wot_td,nl_task)

        llm_wrapper = LLM_Wrapper(LLM,ENDPOINT)
        plan = llm_wrapper.runLLM(prompt)
        print("PLAN: " + plan)
        
        plan = post_process_plan(plan,wot_td)

        verification = f"\nprint({tasks[task_number]["verification"]})"

        simulator = load_real_simulator(wot_td.class_name())

        with open(RESULT_FOLDER + device + "/"+ task_number +".py", "w", encoding='utf-8') as f:
            f.write(simulator + "\n\n" + plan + verification)

        verify_task(RESULT_FOLDER + device + "/"+ task_number +".py")

    

def main():

    devices = getDeviceNames()
    for device in devices:
        print(device.upper())
        try:
            os.makedirs(RESULT_FOLDER + device)
        except FileExistsError:
            pass
        runProgprompt(device)     

    print(f"""SUCCESSES: {STATISTIC["number_of_successes"]}
FAILURES: {STATISTIC["number_of_failures"]} - {STATISTIC["failures_list"]}
ERRORS: {STATISTIC["number_of_errors"]} - {STATISTIC["errors_list"]}
TIMEOUTS: {STATISTIC["number_of_timeouts"]} - {STATISTIC["timeouts_list"]}""")   

if __name__ == "__main__":
    main()