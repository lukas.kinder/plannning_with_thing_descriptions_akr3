
def get_syntax_error_line(code):
    try:
        exec(code)
    except SyntaxError as e:
        return True, e.lineno
    except:
        # All other errors are ignored
        pass
    return False, -1

def comment_out_line(lineno,code):
    code = code.split("\n")
    code[lineno -1] = "pass # Syntax Error: " +  code[lineno -1]

    new_code = ""
    for line in code:
        new_code += line + "\n"
    
    return new_code

def remove_lines_causing_syntax_errors(code):
    # Runs the code and chacks if there is a syntax error.
    # While there is a syntax error the line causing it is commented out.
    # A reason to do this is for example to remoce natural language the LLM may have written.
    bool_error, line = get_syntax_error_line(code)

    n_lines = len(code.split("\n"))

    i = 0
    while bool_error:
        i+=1
        if i == 100:
            return code
        print(f"syntax error in line : {line}")
        code = comment_out_line(line,code)
        if line == n_lines -1:
            break
        bool_error, line = get_syntax_error_line(code)
    return code

def remove_after_no_leading_spaces(code):
    #after the first line, it will cut of once there is a line without a leading space of tab

    code_lines = code.split("\n")
    new_code = code_lines[0] + "\n"
    for line in code_lines[1:]:
        if len(line) == 0 or line[0] == " " or line[0] == "\t" or line[0] == "\n":
            new_code += line + "\n"
        else:
            break
    return new_code


def main():

    path = "Results/Llama_7b_No_Descriptions/Display/Task3.py"

    with open(path, "r") as f:
        code = f.read()
    
    code = remove_lines_causing_syntax_errors(code)

    with open("new.py", "w") as f:
        f.write(code)

    some_string = """class MyClass:

    def __init__()
        pass:
    
    def spomeFunction():
        pass

outside_class = 1
    bla
"""
    print(remove_after_no_leading_spaces(some_string))

if __name__ == "__main__":
    main()