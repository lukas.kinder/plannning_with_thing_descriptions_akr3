import subprocess
import os

from generate_prompt import *
from llm import *
from code_entangler import *

#81 Big LLama
#82 = small codeLlama

LLM = "gpt-3.5"
ENDPOINT = "openai_api_key.txt"
INCLUDE_DESCRIPTIONS = True
GENERIC_ACTION_NAMES = False

SIMULATORS_FOLDER =  "Simulators/"

def load_real_simulator(device):
    with open(SIMULATORS_FOLDER + device + "Simulator.py", "r") as f:
        file = f.read()
    return file


# 1. Input: WoT description + Task
wot_description_file = sys.argv[1]
nl_task = sys.argv[2]

# 1. Simplify WoT description.
wot_td = WoT_Thing_Description(wot_description_file)
wot_td.simplify_wotd(INCLUDE_DESCRIPTIONS)
if GENERIC_ACTION_NAMES:
    wot_td.replace_action_names_generic()


initial_state = find_initial_state(wot_td,return_none_on_error=False)
print(f"INITIAL STATE IS: {initial_state}")

pg = promptGenerator(model=LLM)
class_creation_prompt = pg.generate_class_creation_prompt(wot_td,initial_state)

llm_wrapper = LLM_Wrapper(model = LLM,endpoint_key=ENDPOINT)

simulator = llm_wrapper.runLLM(class_creation_prompt,stop_after_no_leading_space=True,comment_out_syntax_errors=True)

print(simulator)

task_creation_prompt = pg.generate_task_prompt(nl_task,simulator,wot_td)

task_code = llm_wrapper.runLLM(task_creation_prompt,stop_after_no_leading_space=False,comment_out_syntax_errors=True)

real_simulator = load_real_simulator(wot_td.class_name())
weaver = Code_Entangler(wot_td,simulator,real_simulator)
simulators_entangled = weaver.get_modified_code()

with open( f"{wot_td.class_name()}_{nl_task}.py", "w", encoding='utf-8') as f:
    f.write(simulators_entangled + task_code)

res = str(subprocess.check_output(["python",f"{wot_td.class_name()}_{nl_task}.py"],timeout= 1))[2:].split("\\r\\n")[:-1]

print("Generated Plan: ",end  ="")
for line in res:
    print(line, end = ", ")
print("")