from WoT_Thing_Description import *
from generate_prompt import find_initial_state,rewrite_string_to_variable
from llm import *
import subprocess

LLM = "Llama" # one of: "Llama" ,"gpt-4" , "gpt-3.5"
ENDPOINT = "home_endpoint_82.txt" 
RESULT_FOLDER = "Results/SayCan/Llama_7b/"

WOT_TD_FOLDER =  "WoT-TD/"
TASKS_FOLDER =  "Tasks/"
SIMULATORS_FOLDER =  "Simulators/"

def getDeviceNames():
    names = []
    for _ , _ , files in os.walk(TASKS_FOLDER):
        for file in files:
            names.append(file[:-5])
    return names

def load_task_file(device):
    with open(TASKS_FOLDER + device + ".json", "r") as f:
        file = json.load(f)
    return file

def load_real_simulator(device):
    with open(SIMULATORS_FOLDER + device + "Simulator.py", "r") as f:
        file = f.read()
    return file

class SayCanPromptGenerator:

    def __init__(self,llm):
        self.model = llm
        
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt = []
        else:
            self.prompt = ""

        self.system_prompt = """You are a expert planning system that is provided with a Web-of-things Thing Description and a command in natural language.
Your task is to generate a sequence of actions that can be performed with the device such that the command is fulfilled. Be carful, as some actions
have requirements. For example, interacting with a device often requires that it is turned on initially. 
You only answer with the list of actions and do not give explanations in natural language."""

        self.examples = ["prompt_examples/GarageDoor","prompt_examples/MP3Player","prompt_examples/Printer"]

    

    def task_description(self,wot_td,task,initial_state):
        return f"""
### Web of Things Thing Description:
{wot_td}
### Initial state:
{str(initial_state)}
### Task:
{task}"""
    
    def reset_prompt(self):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt = []
        else:
            self.prompt = ""

    def generate_prompt(self,wot_td :WoT_Thing_Description,task):
        self.reset_prompt()
        self.add_to_prompt(self.system_prompt,"system")

        for example in self.examples:
            example_wot_td = WoT_Thing_Description(example + ".json")
            example_wot_td.simplify_wotd()
            example_wot_td.standardize_action_names()
            example_wot_td.standardize_property_names()
            example_wot_td_string = example_wot_td.as_string()

            with open(example + "Task.txt", "r") as f:
                example_task = f.read()
            example_initial_state = find_initial_state(example_wot_td)

            example_task_prompt = self.task_description(example_wot_td_string,example_task,example_initial_state)

            self.add_to_prompt(example_task_prompt,"user")

            with open(example + "Plan.txt","r") as f:
                plan = f.read()

            self.add_to_prompt(plan,"assistant")

        wot_td.simplify_wotd()
        wot_td.standardize_action_names()
        wot_td.standardize_property_names()
        wot_td_string = wot_td.as_string()

        initial_state = find_initial_state(wot_td)
        task_prompt = self.task_description(wot_td_string,task,initial_state)

        self.add_to_prompt(task_prompt,"user")

        if self.model == "Llama":
            self.prompt += "\n<step> Source: assistant\n\n\n1. "

        return self.prompt
    
    def add_to_prompt(self, text, origin = "user"):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt.append({"role": origin, "content": text})
        else:
            if not self.prompt == "":
                self.prompt += "\n<step> "
            self.prompt += f"Source: {origin}\n\n\n{text}"


    def print_prompt_in_nice_way(self):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            for message in self.prompt:
                print(message["role"].upper() + ":")
                print(message["content"])
        else:
            print(self.prompt)


#81 Big LLama
#82 = small codeLlama

STATISTIC = {
    "number_of_successes" : 0,
    "number_of_failures" : 0,
    "failures_list" : [],
    "number_of_errors" : 0,
    "errors_list" : [],
    "number_of_timeouts" : 0,
    "timeouts_list" : []
}

def plan_to_commands(plan: str):
    
    commands  = ""

    commands += "\n\n# Full Output:"
    for line in plan.split("\n"):
        commands += "\n# " + line
    commands += "\n"

    for i in range(1,1000):

        if plan[:3] == f"{i}. " or plan[:4] == f"{i}. " or plan[:5] == f"{i}. ":
            try:
                next = plan.index(f"{i+1}. ")
            except ValueError:
                break
            next_command = plan[3:next]
            while len(next_command) > 0 and next_command[-1] != ")":
                next_command = next_command[:-1]
            while len(next_command) > 0 and next_command[0] == " ":
                next_command = next_command[1:]
            
            if "(" in  next_command and ")" in next_command:
                commands += "\nsimulator." + next_command
            plan = plan[next:]
        else:
            break

    return commands

def verify_task(path):
    try:
        res = str(subprocess.check_output(["python",path],timeout= 1))[2:].split("\\r\\n")[:-1]

        if "True" in str(res[-1]):
            print("SUCCESS")
            STATISTIC["number_of_successes"] +=1
        else:
            print("PLAN FAILED")
            STATISTIC["failures_list"].append(path)
            STATISTIC["number_of_failures"] +=1
    except subprocess.TimeoutExpired:
        print("TIMEOUT")
        # generated code did not run error-free
        STATISTIC["number_of_timeouts"] +=1
        STATISTIC["timeouts_list"].append(path)
    except:
        print("ERROR")
        # generated code did not run error-free
        STATISTIC["errors_list"].append(path)
        STATISTIC["number_of_errors"] +=1
    

def runSayCan(device,result_folder):

    wot_td = WoT_Thing_Description(WOT_TD_FOLDER  + device + ".json")

    tasks = load_task_file(device)
    task_numbers = tasks.keys()

    for task_number in task_numbers:

        nl_task  = tasks[task_number]['nl']
        print("TASK: " + nl_task)
        pg = SayCanPromptGenerator(LLM)
        prompt = pg.generate_prompt(wot_td,nl_task)

        llm_wrapper = LLM_Wrapper(LLM,ENDPOINT)
        plan = llm_wrapper.runLLM(prompt)
        print("PLAN: " + plan)
        plan_commands = plan_to_commands(plan)


        initialization = f"\n\n#{nl_task}\nsimulator = {device}Simulator()\n"
        verification = f"\nprint({tasks[task_number]["verification"]})"

        simulator = load_real_simulator(wot_td.class_name())

        with open(result_folder + device + "/"+ task_number +".py", "w", encoding='utf-8') as f:
            f.write(simulator  + initialization + plan_commands + verification)

        verify_task(result_folder + device + "/"+ task_number +".py")

def main():



    devices = getDeviceNames()
    for device in devices:
        print(device.upper())
        try:
            os.makedirs(RESULT_FOLDER + device)
        except FileExistsError:
            pass
        runSayCan(device,RESULT_FOLDER)     

    print(f"""SUCCESSES: {STATISTIC["number_of_successes"]}
FAILURES: {STATISTIC["number_of_failures"]} - {STATISTIC["failures_list"]}
ERRORS: {STATISTIC["number_of_errors"]} - {STATISTIC["errors_list"]}
TIMEOUTS: {STATISTIC["number_of_timeouts"]} - {STATISTIC["timeouts_list"]}""")   

if __name__ == "__main__":
    main()