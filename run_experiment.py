import subprocess
import os
import sys

from WoT_Thing_Description import *
from generate_prompt import *
from llm import *
from code_entangler import *


LLM = "gpt-3.5"                                             # one of: "Llama" ,"gpt-4" , "gpt-3.5"

ENDPOINT = "openai_api_key.txt"                             
RESULT_FOLDER = "Results/OurMethod/normal/GPT-3.5/"        

GENERATE_CLASS = True                                       # If false, a correct class implementation will be provided and only the task needs to be written
TD_AS_RDF = False                                           # only if GENERATE_CLASS = True
USE_DESCRIPTIONS = True                                     # only if GENERATE_CLASS = True and TD_AS_RDF = False
GENERIC_ACTION_NAMES  = False                               # only if GENERATE_CLASS = True and TD_AS_RDF = False

print(f"""Run with:
LLM = {LLM}
ENDPOINT = {ENDPOINT}
RESULT_FOLDER = {RESULT_FOLDER}
GENERATE_CLASS = {GENERATE_CLASS}
TD_AS_RDF = {TD_AS_RDF}
USE_DESCRIPTIONS = {USE_DESCRIPTIONS}
GENERIC_ACTION_NAMES = {GENERIC_ACTION_NAMES}
""")


WOT_TD_FOLDER =  "WoT-TD/"
TASKS_FOLDER =  "Tasks/"
SIMULATORS_FOLDER =  "Simulators/"
CORRECTLY_GENERATED_CLASSES_FOLDER =  "CorrectClasses/"

STATISTIC = {
    "number_of_successes" : 0,
    "number_of_failures" : 0,
    "failures_list" : [],
    "number_of_errors" : 0,
    "errors_list" : [],
    "number_of_timeouts" : 0,
    "timeouts_list" : [],
    "not_able_to_create_class" : 0,
    "not_able_to_create_class_list" : [],
    "token_limit_error" : 0,
    "token_limit_error_list" : []
}

def getDeviceNames():
    names = []
    for _ , _ , files in os.walk(TASKS_FOLDER):
        for file in files:
            names.append(file[:-5])
    return names

def load_task_file(device):
    with open(TASKS_FOLDER + device + ".json", "r") as f:
        file = json.load(f)
    return file

def load_real_simulator(device):
    with open(SIMULATORS_FOLDER + device + "Simulator.py", "r") as f:
        file = f.read()
    return file

def load_correct_class(device):
    with open(CORRECTLY_GENERATED_CLASSES_FOLDER + device + ".py", "r") as f:
        file = f.read()
    return file

def write_task_as_function(func_name,task_code,nl_task, verification, instance_name):
    task_lines = task_code.split("\n")
    func = "\ndef {0}():\n".format(func_name)
    func += "    # " + nl_task + "\n\n"
    for line in task_lines:
        func += "    " + line + "\n\n"

    func += "    simulator = {0}.real_device\n".format(instance_name)
    func += "    return " + verification 
    return func

def verify_task(path):
    try:
        res = str(subprocess.check_output(["python",path],timeout= 1))[2:].split("\\r\\n")[:-1]
        print("Generated Plan: ",end  ="")
        for line in res[:-1]:
            print(line, end = ", ")
        print("")

        if "True" in str(res[-1]):
            print("SUCCESS")
            STATISTIC["number_of_successes"] +=1
        else:
            print("PLAN FAILED")
            STATISTIC["failures_list"].append(path)
            STATISTIC["number_of_failures"] +=1
    except subprocess.TimeoutExpired:
        print("TIMEOUT")
        # generated code did not run error-free
        STATISTIC["number_of_timeouts"] +=1
        STATISTIC["timeouts_list"].append(path)
    except:
        print("ERROR")
        # generated code did not run error-free
        STATISTIC["errors_list"].append(path)
        STATISTIC["number_of_errors"] +=1


def results_task(task_number,tasks,wot_td: WoT_Thing_Description,llm_wrapper, generated_simulator,generated_simulators_entangled,pg):
    nl_task  = tasks[task_number]['nl']

    device = wot_td.class_name()
    instance_name = rewrite_string_to_variable(device)

    task_creation_prompt = pg.generate_task_prompt(nl_task,generated_simulator,wot_td)

    task_code = llm_wrapper.runLLM(task_creation_prompt,stop_after_no_leading_space=False,comment_out_syntax_errors=True)

    print(task_code)
    verification = f"\n\nsimulator = {instance_name}.real_device\n"
    verification += f"print({tasks[task_number]["verification"]})"

    with open(RESULT_FOLDER + device + "/"+ task_number +".py", "w", encoding='utf-8') as f:
        f.write(generated_simulators_entangled  + task_code + verification)

    verify_task(RESULT_FOLDER + device + "/"+ task_number +".py")

def results_device(device):
    print(device.upper() + ":")

     # 0. Load tasks
    tasks = load_task_file(device)
    task_numbers = tasks.keys()

    # 1. Simplify WoT description.
    wot_td = WoT_Thing_Description(WOT_TD_FOLDER  + device + ".json")
    
    wot_td.simplify_wotd(USE_DESCRIPTIONS)
    if GENERIC_ACTION_NAMES:
        wot_td.replace_action_names_generic()

    # 2. Task: Write and save the simulator created by the llm
    initial_state = find_initial_state(wot_td,return_none_on_error=False)

    llm_wrapper = LLM_Wrapper(model = LLM,endpoint_key=ENDPOINT)

    pg = promptGenerator(model=LLM)
    simulator = "# Unable to create class (Maybe token limit error or LLM does not work)"
    if GENERATE_CLASS:
        for i in [3,2,1,0]:
            class_creation_prompt = pg.generate_class_creation_prompt(wot_td,asRDF=TD_AS_RDF,n_examples=i)
            try:
                simulator = llm_wrapper.runLLM(class_creation_prompt,stop_after_no_leading_space=True,comment_out_syntax_errors=True)
            except TokenLimitError:
                print(f"Try with {i-1} examples")
                continue
            break
    else:
        simulator = load_correct_class(device)
    print(simulator)

    try:
        real_simulator = load_real_simulator(wot_td.class_name())
        weaver = Code_Entangler(wot_td,simulator,real_simulator)
        simulators_entangled = weaver.get_modified_code()
    except:
        print("Generated python class could not be connected with simulator -> All tasks will be unsuccessful")
        simulators_entangled = "# Was not able to entangle with real device! (Are still unconnected)\n" +  real_simulator + "\n" + simulator


    for task_number in task_numbers:
        # generate task code using task 
        results_task(task_number,tasks,wot_td,llm_wrapper,simulator,simulators_entangled,pg)

def main():
    try:
        os.makedirs(RESULT_FOLDER)
    except FileExistsError:
        pass


    devices = getDeviceNames()
    for device in devices:
        try:
            os.mkdir(RESULT_FOLDER + device)
        except FileExistsError:
            pass
        results_device(device)     

    print(f"""SUCCESSES: {STATISTIC["number_of_successes"]}
FAILURES: {STATISTIC["number_of_failures"]} - {STATISTIC["failures_list"]}
ERRORS: {STATISTIC["number_of_errors"]} - {STATISTIC["errors_list"]}
FAILURE TO CREATE CLASS: {STATISTIC['not_able_to_create_class']} - {STATISTIC["not_able_to_create_class_list"]}
TOKEN LIMIT ERROR: {STATISTIC["token_limit_error"]} - {STATISTIC["token_limit_error_list"]}
TIMEOUTS: {STATISTIC["number_of_timeouts"]} - {STATISTIC["timeouts_list"]}""")   

if __name__ == "__main__":
    main()
