import sys
import json
import os

def rewrite_string_to_variable(prop):
    prop = prop.replace(" ", "_")
    prop = prop[0].lower() + prop[1:]
    return prop

# A class that takes a raw web-of-things description (wotd) in json and rewrites it..

class WoT_Thing_Description:

    def __init__(self, wot_td_path,rdfPath = None):

        self.path = wot_td_path

        if rdfPath ==None:
            self.rdf_path = "WoT-TD/AsRDF/"
        else:
            self.rdf_path = rdfPath
        
        try:
            with open(wot_td_path, "r") as f:
                self.wot_td_json = json.load(f)

        except FileNotFoundError:
            wot_td_path = wot_td_path.replace(".json",".jsonld")
            with open(wot_td_path, "r") as f:
                self.wot_td_json = json.load(f)

        self.action_name_mapping = dict()
        for action in self.action_list():
            self.action_name_mapping[action] = action

    def simplify_rec(self,simplified,original,key ):
        if len(key) == 1:
            if key[0] == ":":
                for option in original:
                    simplified[option] = original[option]
            else:
                try:
                    simplified[key[0]] = original[key[0]]
                except (KeyError,TypeError):
                    #information does not exist
                    pass
        else:
            if key[0] == ":":
                for option in original:
                    if option not in simplified:
                        simplified[option] = dict()
                    self.simplify_rec(simplified[option], original[option],key[1:])
            else:
                if key[0] not in simplified:
                    simplified[key[0]] = dict()
                try:
                    self.simplify_rec(simplified[key[0]], original[key[0]],key[1:])
                except (KeyError,TypeError):
                    #information does not exist
                    pass

    def simplify_wotd(self, include_descriptions = True):

        #specify which information to grab, use ':' for all branches
        if include_descriptions:
            whitelist_keys = [
                "name",
                "title",
                "type",
                "description",
                "properties->:->title",
                "properties->:->name",
                "properties->:->label",
                "properties->:->minimum",
                "properties->:->maximum",
                "properties->:->characteristics",
                "properties->:->properties",
                "properties->:->type",
                "properties->:->unit",
                "properties->:->additionalProperties",
                "properties->:->enum",
                "properties->:->maxItems",
                "properties->:->items",
                "properties->:->description",
                "properties->:->descriptions",
                "actions->:->label",
                "actions->:->name",
                "actions->:->title",
                "actions->:->input",
                "actions->:->requirement",
                "actions->:->requirements",
                "actions->:->idempotent",
                "actions->:->required",
                "actions->:->description",
                "actions->:->descriptions",
            ]
        else:
            whitelist_keys = [
                "properties->:->minimum",
                "properties->:->maximum",
                "properties->:->characteristics",
                "properties->:->properties",
                "properties->:->type",
                "properties->:->unit",
                "properties->:->additionalProperties",
                "properties->:->enum",
                "properties->:->maxItems",
                "properties->:->items",
                "actions->:->input",
                "actions->:->requirement",
                "actions->:->requirements",
                "actions->:->idempotent",
                "actions->:->required"
            ]

        wot_td_simplified = dict()
        for key in whitelist_keys:
            key = key.split("->")
            self.simplify_rec(wot_td_simplified, self.wot_td_json,key)
        self.wot_td_json = wot_td_simplified

    def asRDF(self):
        with open(self.rdf_path + self.class_name() + ".ttl") as f:
            rdf_data = f.read()
        return rdf_data

    def replace_action_names_generic(self):
        # replaces action names as generic 'action1', action2' etc. 
        # returns mapping to originals

        new_mapping = dict()

        actions = list(self.wot_td_json["actions"].keys())
        for counter, action in enumerate(actions):
            self.wot_td_json["actions"][f"action{counter+1}"] = self.wot_td_json["actions"][action]
            del self.wot_td_json["actions"][action]
            new_mapping[f"action{counter+1}"] = action
        self.action_name_mapping =  new_mapping
    
    def action_list(self):
        return list(self.wot_td_json ["actions"].keys())

    def properties_list(self):
        return list(self.wot_td_json ["properties"].keys())

    def class_name(self):
        return self.path.replace("\\","/").split("/")[-1].split(".")[0]
    
    def standardize_action_names(self):
        for action in self.action_list():
            new_name = rewrite_string_to_variable(action)
            if new_name == action:
                continue
            self.wot_td_json["actions"][new_name] = self.wot_td_json["actions"][action]
            del self.wot_td_json["actions"][action]

    def standardize_property_names(self):
        for property in self.properties_list():
            new_name = rewrite_string_to_variable(property)
            if new_name == property:
                continue
            self.wot_td_json["properties"][new_name] = self.wot_td_json["properties"][property]
            del self.wot_td_json["properties"][property]


    def as_string(self):
        s = str(self.wot_td_json)
        s = s.replace("\'","\"")
        s = s.replace("{","{\n")
        s = s.replace("}","\n}")
        s = s.replace(", ",",\n")
        s = s.replace("\n\n","\n")

        leading_spaces = 0
        res = ""
        for line in s.splitlines():
            if "}" in line:
                leading_spaces -=2

            for i in range(leading_spaces):
                res += " "

            if "{" in line:
                leading_spaces +=2

            res += line + f"\n"

        return res[:-1] # without last linebreak


def main():

    folder = "WoT-TD/"

    for subdir, dirs, files in os.walk(folder):
        for file in files:
            wot_td = WoT_Thing_Description(folder + file)
            wot_td.simplify_wotd(include_descriptions=True)
            wot_td.replace_action_names_generic()
            print(wot_td.as_string())
            print(wot_td.properties_list())
            print(wot_td.action_list())
            print(wot_td.class_name())
            input(wot_td.action_name_mapping)

if __name__ == "__main__":
    main()