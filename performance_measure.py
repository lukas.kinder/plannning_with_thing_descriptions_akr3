import subprocess
import os
import json
import sys

from code_postprocessing import remove_lines_causing_syntax_errors

REMOVE_SYNTAX_ERRORS = False

RESULT_FOLDER = sys.argv[1]
TASKS_FOLDER =  "Tasks/"

if not os.path.exists(RESULT_FOLDER):
    exit(f"Folder: {RESULT_FOLDER} does not exist!")

def getDeviceNames():
    names = []
    for _ , _ , files in os.walk(TASKS_FOLDER):
        for file in files:
            names.append(file[:-5])
    return names

def load_task_file(device):
    with open(TASKS_FOLDER + device + ".json", "r") as f:
        file = json.load(f)
    return file

STATISTIC = {
    "number_of_successes" : 0,
    "number_of_failures" : 0,
    "failures_list" : [],
    "number_of_errors" : 0,
    "errors_list" : [],
    "number_of_timeouts" : 0,
    "timeouts_list" : [],
    "not_able_to_create_class" : 0,
    "not_able_to_create_class_list" : []
}


device_names = getDeviceNames()

for device in device_names:

    tasks = load_task_file(device)
    task_numbers = tasks.keys()

    for task_number in task_numbers:
        file = RESULT_FOLDER + device + "/"+ task_number +".py"

        with open(file,"r", encoding="utf8") as f:
            code = f.read()
        if REMOVE_SYNTAX_ERRORS:
            code = remove_lines_causing_syntax_errors(code)
        with open("temp.py","w", encoding="utf8") as f:
            f.write(code)

        if not os.path.exists(file):
            STATISTIC["not_able_to_create_class"] +=1
            STATISTIC["not_able_to_create_class_list"].append(file)
            continue

        try:
            res = str(subprocess.check_output(["python","temp.py"],timeout= 1))[2:].split("\\r\\n")[:-1]

            if "True" in str(res[-1]):
                STATISTIC["number_of_successes"] +=1
            else:
                STATISTIC["failures_list"].append(RESULT_FOLDER + device + "/"+ task_number +".py")
                STATISTIC["number_of_failures"] +=1
        except subprocess.TimeoutExpired:
            # generated code did not run error-free
            STATISTIC["number_of_timeouts"] +=1
            STATISTIC["timeouts_list"].append(RESULT_FOLDER + device + "/"+ task_number +".py")
        except:
            # generated code did not run error-free
            STATISTIC["errors_list"].append(RESULT_FOLDER + device + "/"+ task_number +".py")
            STATISTIC["number_of_errors"] +=1

print(f"""SUCCESSES: {STATISTIC["number_of_successes"]}
FAILURES: {STATISTIC["number_of_failures"]} - {STATISTIC["failures_list"]}
ERRORS: {STATISTIC["number_of_errors"]} - {STATISTIC["errors_list"]}
TIMEOUTS: {STATISTIC["number_of_timeouts"]} - {STATISTIC["timeouts_list"]}""")   

print(f"Sum = {STATISTIC["number_of_successes"] + STATISTIC["number_of_failures"] + STATISTIC["number_of_errors"] + STATISTIC["number_of_timeouts"]}")
os.remove("temp.py")