import sys
import json
import tiktoken
import importlib

from WoT_Thing_Description import *


def find_initial_state(wot_td: WoT_Thing_Description, return_none_on_error = False):
    initial_state = dict()
    
    try:
        simulator_module = importlib.import_module("Simulators." + wot_td.class_name() + "Simulator")
    except ImportError:
        if return_none_on_error:
            return None
        raise ImportError
    
    class_ = getattr(simulator_module, wot_td.class_name() + "Simulator")
    instance = class_()

    for property in wot_td.properties_list():
        var_name = rewrite_string_to_variable(property)
        function_name = "get_" + var_name
        initial_state[var_name] = getattr(instance,function_name)()

    return initial_state

class promptGenerator:

    def __init__(self, model = 'gpt-3.5'):
        self.model = model

        if model == 'gpt-3.5' or model == 'gpt-4':
            self.prompt = []
        else:
            self.prompt = ""

        self.class_creation_examples = {
            "prompt_examples/Printer.json" : "prompt_examples/Printer.py",
            "prompt_examples/MP3Player.json" : "prompt_examples/MP3Player.py",
            "prompt_examples/GarageDoor.json" : "prompt_examples/GarageDoor.py"
        }

        self.task_examples = {
            "prompt_examples/Printer.py" : ("prompt_examples/PrinterTask.py","prompt_examples/PrinterTask.txt"),
            "prompt_examples/GarageDoor.py" : ("prompt_examples/GarageDoorTask.py","prompt_examples/GarageDoorTask.txt"),
            "prompt_examples/MP3Player.py" : ("prompt_examples/MP3PlayerTask.py","prompt_examples/MP3PlayerTask.txt")
        }

        self.class_generator_system  = """You are an expert programmer that writes simple and concise code. Your task is it to
translate web of things thing descriptions into python classes. You use the properties of devices as variables and
the actions of devices as functions. You make sure that all background information provided in the device description are 
preserved in the python class as comments.
If you believe that in order to execute an action/function requirements need to be fulfilled you: 
    1) First, check if the requirements are fulfilled. (For example, interacting with a device often requires that it is on.) 
    2) If not fulfilled: Make function calls that cause the requirements to be fulfilled. (For example, first call the function to turn the device on.)"""
        
        self.task_generator_system = """You are an expert programmer that writes simple and concise code.
You avoid code duplication and use loops or create auxiliary functions if necessary.
You are provided with a python class that abstracts a device and a task in natural language. You
write code that interacts with the python class such that the task is fulfilled. However, you only 
manipulate the device using the functions, not by directly changing variables of the class.
You only reply by writing the code, no explanations in natural language.
"""

    #transforms a list of strings into a string in natural language
    def list_to_natural_language(self,l):
        if len(l) == 0:
            return ""
        if len(l) ==1:
            return l[0]
        
        nat = ""
        for i in l[0:-2]:
            nat += i + ", "
        nat += l[-2] + " and " + l[-1]
        return nat

    #Write a prompt that includes the json file and the instruction to write a class for this, 
    # such that properties are variables and actions are classes.
    def prompt_for_one_device(self,wot_td: WoT_Thing_Description,asRDF):

        if asRDF:
            prompt = "Consider the following device description in RDF format:\n"
            prompt += wot_td.asRDF()
        else:
            prompt = "Consider the following device description:\n"
            prompt += wot_td.as_string()

        variables = ["\"" + rewrite_string_to_variable(var) + "\"" for var in wot_td.properties_list()]
        functions = ["\"" + rewrite_string_to_variable(func) + "()\"" for func in wot_td.action_list()]

        prompt += f"""\nWrite a python class called '{wot_td.class_name()}' about the device. It should have the variables {self.list_to_natural_language(variables)}. 
It should have exactly {len(functions)} functions: {self.list_to_natural_language(functions)} All functions are void functions (They do not return anything). 
""" 
        initial_state = find_initial_state(wot_td)
        if initial_state != None:
            prompt += f"Assume the initial state is {initial_state}\n"
        
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            prompt += f"Start your response with 'class {wot_td.class_name()}:'\n"
        
        return prompt
    
    def reset_prompt(self):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt = []
        else:
            self.prompt = ""

    # Generates a prompt including examples
    def generate_class_creation_prompt(self,wot_td: WoT_Thing_Description,asRDF = False, n_examples = 3):
        self.reset_prompt()

        self.add_to_prompt(self.class_generator_system,"system")

        for i,example_wot_td_path in enumerate(self.class_creation_examples):
            if i == n_examples:
                break

            example_wot_td = WoT_Thing_Description(example_wot_td_path,"prompt_examples/")
            example_task_prompt = self.prompt_for_one_device(example_wot_td,asRDF)
            self.add_to_prompt(example_task_prompt,"user")

            with open(self.class_creation_examples[example_wot_td_path], "r") as f:
                python_class_description = f.read()

            self.add_to_prompt(python_class_description,"assistant")

        current_task_prompt = self.prompt_for_one_device(wot_td,asRDF)
        self.add_to_prompt(current_task_prompt,"user")

        if self.model == "Llama":
            self.prompt += "\n<step> Source: assistant\n\n\n"
            self.prompt += f"class {wot_td.class_name()}:"

        return self.prompt
    
    def add_to_prompt(self, text, origin = "user"):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            self.prompt.append({"role": origin, "content": text})
        else:
            if not self.prompt == "":
                self.prompt += "\n<step> "
            self.prompt += f"Source: {origin}\n\n\n{text}"

    def task_prompt_one_device(self,class_code,task,instance_name,device):

        task_prompt = f"""Consider the following python class:

{class_code}

Write code that fulfills the task: "{task}".
"""   
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            task_prompt += f"Your response should start with '{instance_name} = {device}()'\n"

        return task_prompt

    # Generates a prompt including examples (chatgpt message dialogue)
    def generate_task_prompt(self,task,python_code,wot_td: WoT_Thing_Description,n_examples = 3):

        device = wot_td.class_name()
        instance_name = rewrite_string_to_variable(device)

        self.reset_prompt()

        self.add_to_prompt(self.task_generator_system ,"system")

        for i,example_class_path in enumerate(self.task_examples):
            if i == n_examples:
                break 
            
            with open(example_class_path,"r") as f:
                example_class = f.read()
            with open(self.task_examples[example_class_path][0],"r") as f:
                example_task_solution = f.read()
            with open(self.task_examples[example_class_path][1],"r") as f:
                example_task = f.read()
            
            device_name = example_class_path.split("/")[-1].split(".")[0]

            example_task_prompt = self.task_prompt_one_device(example_class,example_task,rewrite_string_to_variable(device_name),device_name)
            self.add_to_prompt(example_task_prompt,"user")
            self.add_to_prompt(example_task_solution,"assistant")

        
        task_prompt = self.task_prompt_one_device(python_code,task,instance_name,device)
        self.add_to_prompt(task_prompt ,"user")

        if self.model == "Llama":
            self.add_to_prompt(f"# {task}\n{instance_name} = {device}()",origin="assistant")

        return self.prompt

    # prints a chatgpt message dialog in a nice way
    def print_prompt_in_nice_way(self):
        if self.model == 'gpt-3.5' or self.model == 'gpt-4':
            for message in self.prompt:
                print(message["role"].upper() + ":")
                print(message["content"])
        else:
            print(self.prompt)

    # count the tokens of a prompt
    def count_tokens_of_prompt(self,model = "gpt-3.5-turbo"):
        encoding = tiktoken.encoding_for_model(model)

        if type(self.prompt) == str:
            all_content = self.prompt
        else:
            all_content = ""
            for message in self.prompt:
                all_content += message["content"]

        return len(encoding.encode(all_content))

def main():

    INCLUDE_DESCRIPTIONS = True
    GENERIC_ACTION_NAMES = False
    AS_RDF = False

    wot_description_file = "WoT-TD/Display.json"
    task  = "Print 4 pages of 'copyVersion2'. Turn the Printer off at the end."


    wot_td = WoT_Thing_Description(wot_description_file)
    python_class = f"class {wot_td.class_name}():\n    pass\n"
    wot_td.simplify_wotd(INCLUDE_DESCRIPTIONS)
    if GENERIC_ACTION_NAMES:
        wot_td.replace_action_names_generic()

    for m in ['gpt-3.5','Llama']:
        input("ClASS CREATION " + m + "(Any Input to continue)")
        pg  = promptGenerator(model = m)
        prompt = pg.generate_class_creation_prompt(wot_td,AS_RDF,n_examples=3)
    
        pg.print_prompt_in_nice_way()

        print("\nNumber of tokens in prompt is: {0}".format(pg.count_tokens_of_prompt()))

        input("TASK CREATION " + m + "(Any Input to continue)")

        prompt = pg.generate_task_prompt(task,python_class,wot_td)
        pg.print_prompt_in_nice_way()

        print("\nNumber of tokens in prompt is: {0}".format(pg.count_tokens_of_prompt()))

if __name__ == "__main__":
    main()

