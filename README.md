

Example prompts for our method can be viewed by running <python generate_promp.py>

The results and some analysis of an experiment can be performed with the file "performance_measure.py" e.g. <python .\performance_measure.py .\Results\OurMethod\normal\GPT-3.5\> 


In order to run the models, an openai api-key or endpoint must be provided in a .txt file.

The "main.py" file can be used to run a experiment with one WoT-TD and one task in natural language. e.g. <python .\main.py .\WoT-TD\CoffeeMachine.json "Make 3 cups of coffee.">

The file "run_experiment.py", "SayCan.py" and "Progprompt.py" can be used to run the different models on all 123 tasks. Conditions must be specified via the global variables at the beginning of the files. 

