class Printer:
    def __init__(self):
        self.out_of_ink = False # Flags if the printer has no ink
        self.has_paper = 0 # Indicates the amount of paper available (minimum = 0, maximum = 60)
        self.on = False

    def turn_on(self):
        # Turns the device on
        self.on = True

    def turn_off(self):
        # Turns the device off
        self.on = False

    def refill_ink(self):
        # Refills the ink
        self.out_of_ink = False

    def add_Paper(self):
        # Fills up the printer with paper
        self.has_paper = 60

    def printPage(self,document,number_pages):
        # Prints a page. And uses up a piece of paper
        # :param pdf document: The document to be printed
        # :param integer number_pages: The number of pages to be printed
        
        # requires that the device is on
        if not self.on:
            self.turn_on()
        # requires that the device has ink
        if self.out_of_ink:
            self.refill_ink()
        # requires that the device has enough paper
        if self.has_paper < number_pages:
            self.add_Paper()

        # Code to print the document here
            
        self.has_paper -= number_pages