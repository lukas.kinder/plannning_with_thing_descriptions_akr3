class MP3Player:
    def __init__(self):
        self.on = False
        self.song = "Hey Ya!" # the current song playing
        self.mode = "MP3" # Mode of player
        self.volume = 15 # The volume

    def toggle(self):
        # toggle the on switch
        self.on = not self.on

    def toggle_mode(self):
        # toggle the mode

        # requires that the device is on
        if not self.on:
            self.toggle()

        if self.mode == "MP3":
            self.mode = "Radio"
        else:
            self.mode = "MP3"

    def set_volume(self,value):
        # sets the volume
        # :param integer value: the value the volume should be

        # requires that the device is on
        if not self.on:
            self.toggle()

        if value > 0 and value <= 30:
            self.volume = value

    def next_Song(self):
        # Switch to the next song

        # requires that the device is on
        if not self.on:
            self.toggle()
        # requires that the device is in MP3 mode
        while self.mode != "MP3":
            self.toggle_mode()

        songs = ["Toxic","Hey Ya!","Take On Me","Thriller"]
        current_index = songs.index(self.song)
        next_index = (current_index + 1) % len(songs)
        self.song = songs[next_index]