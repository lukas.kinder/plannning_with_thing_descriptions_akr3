class GarageDoor:
    def __init__(self):
        self.state = {"power": False, "door": "closed"}

    def toggle(self):
        # Turns power off/on

        self.state["power"] = not self.state["power"]

    def close(self):
        # closes garage

        # requires that the device is on
        if not self.state["power"]:
            self.toggle()

        self.state["door"] = "closed"

    def open_Door(self):
        # opens the door

        # requires that the device is on
        if not self.state["power"]:
            self.toggle()

        self.state["door"] = "open"